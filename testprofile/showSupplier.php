<?php
    include_once 'functions.php';

    if($_POST) { 
      $geocodeData = getGeocodeData($_POST['searchAddress']); 
      if($geocodeData) {         
         $latitude = $geocodeData[0];
         $longitude = $geocodeData[1];
         $address = $geocodeData[2];                     
?> 
<div id="gmap">Cargando mapa...</div>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=API-KEY"></script> 
<script type="text/javascript">
   function init_map() {
      var options = {
         zoom: 14,
         center: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>),
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map($("#gmap")[0], options);
      marker = new google.maps.Marker({
         map: map,
         position: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>)
      });
      infowindow = new google.maps.InfoWindow({
          content: "<?php echo $address; ?>"
      });
      google.maps.event.addListener(marker, "click", function () {
           infowindow.open(map, marker);
      });
   }
   google.maps.event.addDomListener(window, 'load', init_map);
</script> 
<?php 
     } else {
        echo "Detalles incorrectos!";
     }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Geolocalizar proveedor</title>
</head>
<body>
    <form action="" method="post">
        <div class="row">       
            <div class="col-sm-4">  
                <div class="form-group">
                    <input type='text' name='searchAddress' class="form-control" placeholder='Pon la dirección aquí' />
                </div>
            </div>
            <div class="form-group">
                <input type='submit' value='Localizar' class="btn btn-success" />
            </div>
        </div>
    </form> 
</body>
</html>