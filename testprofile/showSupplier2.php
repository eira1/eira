<?php
    include_once '../models/model_cities.php';
    if(isset($_GET["addressStreet"]) AND isset($_GET["addressNumber"])){
        $addressStreet=$_GET["addressStreet"];
        $addressNumber=$_GET["addressNumber"];
        $address=$addressStreet." ".$addressNumber;
        $msg='<iframe src="https://maps.google.com/maps?q='.urlencode($address).'&output=embed" width="80%" height="500"></iframe>';
        if(isset($_GET["supplierName"])){
            $msg.='<br><br><u>Proveedor:</u> '.$_GET["supplierName"];
        }
        if(isset($_GET["contactName"])){
            $msg.='<br><br><u>Nombre del contacto:</u> '.$_GET["contactName"];
        }
        $msg.='<br><br><u>Dirección:</u> '.$_GET["addressStreet"].' '.$_GET["addressNumber"];
        if(isset($_GET["cityID"])){
            $msg.='<br><br><u>Ciudad:</u> '.getCityByID($_GET["cityID"])["name"];
        }
        if(isset($_GET["postalCode"])){
            $msg.='<br><br><u>Código postal:</u> '.$_GET["postalCode"];
        }
        if(isset($_GET["phoneNumber"])){
            $msg.='<br><br><u>Número de teléfono:</u> '.$_GET["phoneNumber"];
        }
    }else{
        $msg='Debe ingresar una dirección de proveedor para ver en el mapa esa dirección';
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Encontrar proveedor</title>
</head>
<body>
    <center><h1>Localización de proveedor</h1>
    <?php echo $msg;?>
    </center>
</body>
</html>