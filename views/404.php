<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error</title>
    <link rel="stylesheet" href="/eira/css/404.css">
    <link rel="icon" href="img/system/icon_coin.svg">
    <!-- <script src="js/404.js"></script> -->
</head>
<body>
    <div class="error">
        <h1>Error 404</h1>
        <h2>El recurso solicitado no existe.</h2>
    </div>

    <a href="/eira" class="button">Ir al inicio</div>
</body>
</html>