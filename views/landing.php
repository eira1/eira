<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="Eira | El sistema de control de stock para farmacias" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://escuelarobertoarlt.com.ar/eira" />
    <meta property="og:image" content="https://escuelarobertoarlt.com.ar/eira/img/system/icon_alter.png" />
    <meta property="og:description" content="Realice de manera rápida y simple el inventario de la farmacia. Registre los ingresos y egresos de los artículos con unos pocos clics. Opere en la nube, con actualización autómatica del stock en tiempo real.">
    <title>Eira | El sistema de control de stock para farmacias</title>
    <script src="js/global.js"></script>
    <script src="js/landing.js"></script>
    <script src="https://kit.fontawesome.com/f5394feeef.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="/eira/css/landing.css">
    <link rel="icon" href="img/system/icon_coin.svg">
</head>
<body>
    <!-- <div id="splash"></div> -->
    <!-- <header>
        <h1>Bienvenido a Eira</h1>
        <h2>El sistema de control de stock para farmacias</h2>
    </header> -->
    {{NAV}}
    <main>
        <h1>Bienvenido a Eira</h1>
        
        <section id="card-container">
            <div class="card info">
                <div class="img" style="background-image: url(/eira/img/system/img1.png);"></div>
                <div class="info-container">
                    <h2>
                        Administre todos sus productos
                        desde un solo lugar
                    </h2>
                    <!-- <h2>
                        Desde un solo lugar
                    </h2> -->
                    <!-- Realice de manera rápida y ágil el inventario de la farmacia. Regularización de stock online
                        directamente en el artículo. Consulta de cualquier producto y modificación de dato. Órdenes de
                        traspaso y traspasos de artículos entre sectores del hospital ajustando el stock de cada uno de ellos. 
                        Actualización de stocks mediante inventario. Recepción de Pedidos de Proveedor optimizando al máximo el tiempo. -->
                    <p>
                        Realice de manera rápida y simple el inventario de la farmacia.
                        Registre los ingresos y egresos de los artículos con unos pocos clics.
                        Opere en la nube, con actualización autómatica del stock en tiempo real.
                    </p>
                    <a href="/eira/login" class="button">Comenzar ahora</a>
                </div>
            </div>
            <!-- <div class="card">
                <div class="img"></div>
                <div class="info-container">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae quod dolores eum laborum suscipit illum molestias voluptas, ratione animi cupiditate amet commodi itaque aliquam sapiente consequuntur minus eaque incidunt non!
                </div>
            </div> -->
        </section>
        <section id="about_us">
            <h2>Quiénes somos</h2>
            <div class="container">
                <div class="card person">
                    <div class="img" style="background-image: url(/eira/img/system/group/fausto.jpeg);"></div>
                    <div class="name">Fausto Martínez</div>
                </div>
                <div class="card person">
                    <div class="img" style="background-image: url(/eira/img/system/group/choque.jpeg);"></div>
                    <div class="name">Gabriel Choque</div>
                </div>
                <div class="card person">
                    <div class="img" style="background-image: url(/eira/img/system/group/manu.jpeg);"></div>
                    <div class="name">Manuel Rafael</div>
                </div>
                <div class="card person">
                    <div class="img" style="background-image: url(/eira/img/system/group/mauri.jpeg);"></div>
                    <div class="name">Mauricio Carbon</div>
                </div>
            </div>
        </section>

    </main>
    {{FOOTER}}
</body>
</html>