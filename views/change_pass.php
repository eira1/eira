<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cambio de contraseña</title>
    <script src="https://kit.fontawesome.com/f5394feeef.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/eira/css/login_register.css">
    <link rel="icon" href="img/system/icon_coin.svg">
    <style>
        body {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        h1 {
            margin-top: 200px;
            margin-bottom: 75px;
            text-align: center;
        }
    </style>
    <script src="/eira/js/global.js"></script>
    <script src="/eira/js/change_pass.js"></script>
</head>
<body>
    <div id="loading_screen" class="loading_screen"></div>
    <div id="dark_background" class="dark_background"></div>
    <h1>Cambiar contraseña de su cuenta de Eira</h1>
    <form>
        <div class="input-info">
            <label for="input_pass">Nueva contraseña</label>
            <input type="password" id="input_pass" maxlength="32">
            <i id="btn-show-pass" class="text-button fas fa-eye" onclick="toggleViewPass('pass')"></i>
        </div>
        <h5 id="pass_length_warning" class="hidden input-error">La contraseña debe contener al menos 8 caracteres.</h5>
        <h5 id="empty_pass_warning" class="hidden input-error">Complete este campo.</h5>
        <div class="input-info">
            <label for="input_repass">Repetir contraseña</label>
            <input type="password" id="input_repass" maxlength="32">
            <i id="btn-show-repass" class="text-button fas fa-eye" onclick="toggleViewPass('repass')"></i>
        </div>
        <h5 id="compare_pass_warning" class="hidden input-error">Las contraseñas no coinciden.</h5>
        <h5 id="empty_repass_warning" class="hidden input-error">Complete este campo.</h5>
        <input type="submit" value="Cambiar contraseña" id="btn_change_pass" class="button">
    </form>
</body>
</html>