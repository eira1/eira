<nav>
    <a id="nav-title" href="/eira">
        <div id="nav-icon"></div>
    </a>
    <div id="nav-options">
        <a href="/eira/login" id="nav-link-login" class="nav-button alter">Iniciar Sesión</a>
        <a href="/eira/register" id="nav-link-register" class="nav-button alter">Registrarse</a>
    </div>
    <div id="nav-icons">
        <label for="btn_nav_menu">
            <i class="text-button fas fa-bars"></i>
        </label>
    </div>
    <input type="checkbox" id="btn_nav_menu">
    <!-- <div id="nav_menu">
        <a href="/eira/login" id="nav-menu-link-login" class="text-button">Iniciar Sesión</a>
        <a href="/eira/register" id="nav-menu-link-register" class="text-button">Registrarse</a>
    </div> -->
</nav>