<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cambio de contraseña</title>
    <style> 
        body {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        h1 {
            margin-top: 200px;
            margin-bottom: 75px;
            text-align: center;
        }
    </style>
</head>
<body>
    <h1>Cambiar contraseña de su cuenta de Eira</h1>

    <p>
        Para cambiar su contraseña, haga clic <a href="https://escuelarobertoarlt.com.ar/eira/change_pass/{{EMAIL}}/{{CODE}}">en este enlace</a>.
    </p>
</body>
</html>