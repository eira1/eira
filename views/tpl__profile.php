<div id="profile" class="popup">
    <h1>Mi perfil</h1>

        <div class="inputs">
            <div class="input-info">
                <label for="input_firstName">Nombre</label>
                <input type="text" id="input_firstName" maxlength="70">
            </div>
            <div class="input-info">
                <label for="input_lastName">Apellido</label>
                <input type="text" id="input_lastName" maxlength="70">
            </div>
            <div class="input-info">
                <label for="input_dni">DNI</label>
                <input type="text" id="input_dni" maxlength="70" inputmode="numeric" pattern="[0-9]+">
            </div>
            <div class="input-info">
                <label for="input_birthDate">Fecha de nacimiento</label>
                <input type="date" id="input_birthDate">
            </div>
            <div class="input-info">
                <label for="input_medicalLicense">Matrícula médica</label>
                <input type="text" id="input_medicalLicense" maxlength="30" inputmode="numeric" pattern="[0-9]+">
            </div>
            <div class="input-info">
                <label for="input_phoneNumber">Número de teléfono</label>
                <input type="tel" id="input_phoneNumber" maxlength="25" inputmode="numeric" pattern="[0-9]+">
            </div>
        </div>
        <input type="submit" value="Modificar datos" id="btn_update_profile" class="button">

    <input type="checkbox" id="btn_dark_mode">
    <label for="btn_dark_mode" id="label_dark_mode">
        <div>
            <i class="fa-solid fa-sun"></i>
            <i class="fa-solid fa-moon"></i>
        </div>
    </label>
</div>