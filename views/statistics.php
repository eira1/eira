<body>
<style>
    body{
    background-color: #ededed;
  }
  .board{
      margin: auto;
      width: 70%;
      height: 450px;
      padding: 10px;
      box-sizing: border-box;
      overflow: hidden;
  }
  .titulo_grafica{
      width: 100%;
      height: 10%;
  }
  .titulo_grafica>h1{
      padding: 0;
      margin: 0px;
      text-align: center;
      color: #005248ff;
  }
  .sub_board{
      width: 100%;
      height: 90%;
      padding: 10px;
      margin-top: 0px;
      overflow: hidden;
      box-sizing: border-box;
  }
  .sep_board{
      width: 100%;
      height: 10%;
  }
  .cont_board{
      width: 100%;
      height: 80%;
  }
  .graf_board{
      width: 100%;
      height: 100%;
      float: right;
      margin-top: 0px;
      box-sizing: border-box;
      display: flex;
  }
  .barra{
      width:100px;
      /*min-width: 10%;*/
      height: 100%;
      margin-right: 15px;
      margin-left: 15px;
      background-color: none;
      display: flex;
      flex-wrap: wrap;
      align-items: flex-end;
  }
  .sub_barra{
      width: 100%;
      height: 80%;
      background: #00c8afff;      
      -webkit-border-radius: 3px 3px 0 0;
      border-radius: 3px 3px 0 0;
  }
  .tag_g{
      position: relative;
      color: #005248ff;
      width: 100%;
      height: 100%;
      margin-bottom: 30px;
      text-align: center;
      margin-top: -20px;
      z-index: 2;
  }
  .tag_leyenda{
      width: 100%;
      color: #005248ff;
      text-align: center;
      margin-top: 5px;
  }
  .tag_board{
      height: 100%;
      width: 15%;
      border-bottom: 2px solid rgba(0,0,0,0);
      box-sizing: border-box;
  }
  .sub_tag_board{
      height: 100%;
      width: 100%;
      display: flex;
      align-items: flex-end;
      flex-wrap: wrap;
  }
  .sub_tag_board>div{
      width: 100%;
      height: 10%;
      text-align: right;
      padding-right: 10px;
      box-sizing: border-box;
  }
  /*------------------ Altura de las barras --------------------*/
  .b1{ height: 35%}
  .b2{ height: 45%}
  .b3{ height: 55%}
  .b4{ height: 75%}
  .b5{ height: 85%}
  footer{
      position: absolute;
      bottom: 0px;
      width: 100%;
      text-align: center;
      font-size: 12px;
      font-family: sans-serif;
  }
</style>
    <div class="board">
        <div class="titulo_grafica">
            <h1 class="t_grafica">PRODUCTOS MAS GASTADOS</h1>
        </div>
        <div class="sub_board">
            <div class="sep_board"></div>
            <div class="cont_board">
                <div class="graf_board">
                    <div class="barra">
                        <div class="sub_barra b1">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b2">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b3">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b4">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b5">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b5">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b2">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b3">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b4">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                    <div class="barra">
                        <div class="sub_barra b1">
                            <div class="tag_g">{{CANTIDAD}}</div>
                            <div class="tag_leyenda">{{NAME}}</div>
                        </div>
                    </div>
                </div>
           </div> 
            <div class="sep_board"></div>
       </div>    
    </div>
</body>