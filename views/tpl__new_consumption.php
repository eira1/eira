<div id="new_consumption_container" class="new_container popup">
    <div class="btn_quit"></div>
    <div id="new_consumption_details" class="details">
        <h3>Nuevo registro consumo</h3>
        <!-- <div class="input-info">
            <label for="new_consumption_sector">Sector</label>
            <select id="new_consumption_sector">
                <option value="placeholder">Cargando...</option>
            </select>
        </div> -->
        <div class="input-info new_add_product_card">
            <label for="new_consumption_details_add_product_card">Agregar Producto al registro</label>
            <input type="text" id="new_consumption_details_add_product_card" placeholder="Producto" list="product_datalist">
        </div>
        <div id="new_consumption_details_product_list" class="product_list discrete_scrollbar">
            <p class="details_msg on">Arrastre o toque un producto de la lista de la derecha para agregarlo al registro del consumo.</p>
        </div>
        <div class="btn_clean_list button btn_no" id="btn_clean_consumption_details_list">
            Vaciar lista
            <i class="fa-solid fa-trash"></i>
        </div>
        <input type="submit" id="btn_new_consumption" class="button" value="Crear registro">
    </div>
    <div id="new_consumption_products" class="new_products">
        <input type="search" id="new_consumption_search_products" class="search" placeholder="Escriba el nombre de algún producto" autocomplete="off">
        <div id="new_consumption_product_list" class="product_list discrete_scrollbar">
            
        </div>
    </div>
</div>