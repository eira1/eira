<section id="work_zone">
    <section id="product_list" class="work_zone_list product_list discrete_scrollbar hidden">
        <input type="checkbox" id="hide_side_product_list_filters" class="btn_show_hide vertical">
        <div class="filters">
            <input type="search" class="search" placeholder="Escriba el nombre de algún producto" autocomplete="off">
            <label for="hide_side_product_list_filters" id="btn_hide_side_product_list_filters" title="Mostrar/Ocultar filtros" class="label_btn_show_hide">
                <i class="fa-solid fa-caret-up"></i>
            </label>
        </div>
        <div class="button_plus hidden" id="btn_show_new_product" title="Nuevo producto">
            <i class="fa-solid fa-plus"></i>
        </div>
    </section>
    <section id="notification_list" class="work_zone_list row_list"></section>
    <section id="transfer_list" class="work_zone_list row_list spaced">
        <div class="button_plus hidden" id="btn_show_new_transfer" title="Nuevo egreso de productos">
            <i class="fa-solid fa-plus"></i>
        </div>
    </section>
    <section id="order_list" class="work_zone_list row_list spaced">
        <div class="button_plus" id="btn_show_new_order" title="Nuevo ingreso de productos">
            <i class="fa-solid fa-plus"></i>
        </div>
    </section>
    <section id="request_list" class="work_zone_list row_list spaced">
    <div class="button_plus hidden" id="btn_show_new_request" title="Nueva petición de productos">
            <i class="fa-solid fa-plus"></i>
        </div>
    </section>
    <section id="expiredLote_list" class="work_zone_list row_list"></section>
    <section id="stat_list" class="work_zone_list"></section>
    <section id="unverifiedUser_list" class="work_zone_list row_list">
        <p class="on">No hay ningún usuario por verificar.</p>
    </section>
    <section id="supplier_list" class="work_zone_list row_list"></section>
    <section id="consumption_list" class="work_zone_list row_list spaced">
        <div class="button_plus" id="btn_show_new_consumption" title="Nuevo registro de consumo">
            <i class="fa-solid fa-plus"></i>
        </div>
    </section>
</section>