<?php

 /**
  * ! NO HACER UN CONTROLER DE ESTA PÁGINA
  *
  * Solo la voy a usar como plantilla para los estilos
  * -Manu
  */

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hoja de estilos</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/forms.css">
    <link rel="icon" href="../img/system/icon_coin.svg">
    <style>
        main {
            display: flex;
            flex-direction: column;
        }
        #normal-area {
            flex: 1;
            background-color: var(--background);
            padding: 20px;
            bottom: 0;
        }
        #alter-area {
            flex: 1;
            background-color: var(--main);
            padding: 20px;
            bottom: 0;
        }
    </style>
</head>
<body>
    <?php include 'nav_bar.php'; ?>
    <main>
        <section id="normal-area">
            <a class="button">Botón</a>
            <a class="button2">Botón 2</a>
            <a class="text-button">Link</a>
            <a class="text-button underline">Link subrayado</a>
            <form action="">
                <input type="text" placeholder="Input">
                <input type="text" class="error" placeholder="Input error">
                <h5 class="input-error">Error de input</h5>
            </form>
            <h5 class="msg error">Mensaje de error</h5>
            <h5 class="msg success">Mensaje exitoso</h5>
        </section>
        <section id="alter-area">
            <a class="button alter">Botón</a>
            <a class="button2 alter">Botón 2</a>
            <a class="text-button alter">Link</a>
            <a class="text-button underline alter">Link subrayado</a>
        </section>
    </main>
</body>
</html>