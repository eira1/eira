<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verificación de correo para Eira</title>
    <link rel="stylesheet" href="https://escuelarobertoarlt.com.ar/eira/css/style.css">
    <style>
        body {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        h1 {
            margin-top: 200px;
            margin-bottom: 75px;
            text-align: center;
        }
    </style>
</head>
<body>
    <h1>Verificación de correo para Eira</h1>
    <a class="button" href="https://escuelarobertoarlt.com.ar/eira/email_verify/{{EMAIL}}/{{ACTIVATION_CODE}}">Verificar</a>
</body>
</html>