<div class="card" title="Ver producto">
    <div class="img" style="background-image: url({{PRODUCT_IMG_URL}});"></div>
    <div class="product-name">{{PRODUCT_NAME}}</div>
    <div class="product-amount">Unidades disponibles: {{PRODUCT_AMOUNT}}</div>
    <div class="product-options">
        <div class="option edit" title="Modificar"><i class="fas fa-edit"></i></div>
        <div class="option delete" title="Eliminar"><i class="fas fa-trash"></i></div>
    </div>
</div>