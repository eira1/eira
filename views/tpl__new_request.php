<div id="new_request_container" class="new_container popup">
    <div class="btn_quit"></div>
    <div id="new_request_details" class="details">
        <h3>Nuevo petición de productos</h3>
        <h5>Datos del paciente</h5>
        <div class="patient_data">
            <div class="input-info">
                <label for="new_request_patientName">Nombre</label>
                <input type="text" id="new_request_patientName" placeholder="(Opcional)">
            </div>
            <div class="input-info">
                <label for="new_request_patientDNI">DNI</label>
                <input type="text" id="new_request_patientDNI" placeholder="(Opcional)">
            </div>
        </div>
        <div class="input-info">
            <label for="new_request_detailsText">Observaciones</label>
            <textarea id="new_request_detailsText" cols="30" rows="2" placeholder="(Opcional)"></textarea>
        </div>
        <div class="input-info new_add_product_card">
            <label for="new_request_details_add_product_card">Agregar Producto a la petición</label>
            <input type="text" id="new_request_details_add_product_card" placeholder="Producto" list="product_datalist">
        </div>
        <div id="new_request_details_product_list" class="product_list discrete_scrollbar">
            <p class="details_msg on">Arrastre o toque un producto de la lista de la derecha para agregarlo al registro de la petición.</p>
        </div>
        <div class="btn_clean_list button btn_no" id="btn_clean_request_details_list">
            Vaciar lista
            <i class="fa-solid fa-trash"></i>
        </div>
        <input type="submit" id="btn_new_request" class="button" value="Crear petición">
    </div>
    <div id="new_request_products" class="new_products">
        <input type="search" id="new_request_search_products" class="search" placeholder="Escriba el nombre de algún producto" autocomplete="off">
        <div id="new_request_product_list" class="product_list discrete_scrollbar">
            
        </div>
    </div>
</div>