<div id="product_details" class="popup">
    <div class="info">
        <div class="img"></div>
        <div class="text_info">
            <div class="input-info">
                <label for="product_details__name">Nombre</label>
                <input type="text" id="product_details__name">
            </div>
            <div class="input-info">
                <label for="product_details__code">Código</label>
                <input type="text" id="product_details__code">
            </div>
            <div class="input-info">
                <label for="product_details__form">Forma</label>
                <input type="text" id="product_details__form">
            </div>
            <div class="input-info">
                <label for="product_details__amount">Cantidad</label>
                <input type="number" id="product_details__amount">
            </div>
            <div class="input-info" title="Si un producto tiene menos unidades que las indicadas en este campo, mostrará una alerta.">
                <label for="product_details__criticalStock">Cantidad de alerta</label>
                <input type="number" id="product_details__criticalStock">
            </div>
            <div class="input-info">
                <label for="product_details__expiredAmount">Vencidos</label>
                <input type="number" id="product_details__expiredAmount">
            </div>
            <div class="input-info"></div>
        </div>
    </div>
    <div class="desc">
        <div class="input-info">
            <label for="product_details__desc">Descripción</label>
            <textarea id="product_details__desc" cols="30" rows="5" class="discrete_scrollbar"></textarea>
        </div>
        <div id="product_details__lotes_container" class="discrete_scrollbar">
            <div class="cell title bold">Lote</div>
            <div class="cell title bold">Cant.</div>
            <div class="cell title bold">Vto.</div>
            <div class="cell lote_loteCode"></div>
            <div class="cell lote_realAmount"></div>
            <div class="cell lote_expirationDate"></div>
        </div>
    </div>
    <div class="buttons hidden">
        <input type="button" class="button" value="Guardar cambios">
        <button class="button">Ver estadísticas</button>
    </div>
    <div class="btn_quit"></div>
</div>