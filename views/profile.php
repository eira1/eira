<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi perfil | Eira</title>
    <script src="https://kit.fontawesome.com/f5394feeef.js" crossorigin="anonymous"></script>
    <script src="js/global.js"></script>
    <script src="js/profile.js"></script>

    <link rel="stylesheet" href="css/profile.css">
    <link rel="icon" href="img/system/icon_coin.svg">
</head>
<body>
    {{NAV}}
    <div id="loading_screen" class="loading_screen"></div>
    <h5 id="msg_success" class="msg success"></h5>
    <h5 id="msg_error" class="msg error"></h5>
    <!-- <main> -->
        <h1>Mi perfil</h1>
        <!-- <section id="content"> -->

            <form action="" method="post" id="profile-form">
                <div class="input-info">
                    <label for="input_firstName">Nombre</label>
                    <input type="text" name="input-firstName" id="input_firstName" maxlength="70" autofocus>
                </div>
                <div class="input-info">
                    <label for="input_lastName">Apellido</label>
                    <input type="text" name="input-lastName" id="input_lastName" maxlength="70">
                </div>
                <div class="input-info">
                    <label for="input_dni">DNI</label>
                    <input type="text" name="input-dni" id="input_dni" maxlength="70" inputmode="numeric" pattern="[0-9]+">
                </div>
                <div class="input-info">
                    <label for="input_birthDate">Fecha de nacimiento</label>
                    <input type="date" name="input-birthDate" id="input_birthDate">
                </div>
                <div class="input-info">
                    <label for="input_medicalLicense">Matrícula médica</label>
                    <input type="text" name="input-medicalLicense" id="input_medicalLicense" maxlength="30" inputmode="numeric" pattern="[0-9]+">
                </div>
                <div class="input-info">
                    <label for="input_phoneNumber">Número de teléfono</label>
                    <input type="tel" name="input-phoneNumber" id="input_phoneNumber" maxlength="25" inputmode="numeric" pattern="[0-9]+">
                </div>
                <input type="submit" value="Modificar datos" name="btn-update" id="btn_update" class="button">
            </form>
        <!-- </section> -->
   <!--  </main> -->
    
    <input type="checkbox" id="btn_dark_mode">
    <label for="btn_dark_mode" id="label_dark_mode">
        <div>
            <i class="fa-solid fa-sun"></i>
            <i class="fa-solid fa-moon"></i>
        </div>
    </label>

    {{FOOTER}}
</body>
</html>