<!-- TEMPLATES -->
<template id="tpl__transfer_card">
    <div class="card transfer">
        <h3 class="sector"></h3>
        <p class="date"></p>
        <div class="transfer_details"></div>
        <h5 class="username"></h5>
        <div class="create_pdf hidden" title="Generar PDF">
            <i class="fa-solid fa-file-pdf"></i>
        </div>
    </div>
</template>

<template id="tpl__transfer_detail">
    <div class="transfer_detail">
        <span class="transfer_detail_product"></span>
        <span class="transfer_detail_amount bold"></span>
    </div>
</template>

<template id="tpl__lote_card">
    <div class="card lote">
        <h3 class="loteCode">Lote: 3829a2</h3>
        <p class="date error">Vencimiento: 03/06/2020</p>
        <p class="product">
            <span class="name">Quack Grass</span>
            <span class="amount bold">x150</span>
        </p>

        <div class="btn_delete" title="Descartar lote">
            <i class="fa-solid fa-trash"></i>
        </div>
    </div>
</template>

<template id="tpl__product_card">
    <div class="card product" title="Ver producto">
        <img src="" alt="" onerror="this.onerror = null; this.src = 'img/productphotos/default.png';" loading="lazy">
        <div class="name">Pastilla Genérica</div>
        <div class="amount_values">
            <span class="amount"><i class="fa-solid fa-boxes-stacked"></i> 10</span>
            <span class="expired_amount error"><i class="fa-solid fa-boxes-stacked"></i> 10</span>
        </div>
        
    </div>
</template>

<template id="tpl__new_list_product">
    <div class="card new_list__product">
        <p class="product">Producto</p>
        <input type="text" placeholder="Producto" class="product" list="product_datalist">
        <input type="number" min="1" max="99999" placeholder="Cant." class="amount">
        <div class="btn_delete" title="Quitar producto de la lista">
            <i class="fa-solid fa-xmark"></i>
        </div>
    </div>
</template>

<template id="tpl__new_list_lote">
    <div class="card new_list__lote">
        <input type="text" class="product" placeholder="Producto" title="Producto" list="product_datalist">
        <input type="text" class="code" placeholder="Cód. Lote" title="Código de lote" disabled>
        <input type="number" class="amount" placeholder="Cantidad" title="Cantidad" disabled>
        <input type="text" class="expirationDate" placeholder="Vencimiento" title="Fecha de vencimiento" disabled>
        <i class="fa-solid fa-xmark btn_delete disabled"></i>
    </div>
</template>

<template id="tpl__notification">
    <div class="card notification">
        <h3>Título</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, eveniet tempora facilis magni ratione rerum, cumque iure <a href="">nam neque assumenda optio</a>.</p>
    </div>
</template>

<template id="tpl__side_panel_option">
    <div class="option" title="Texto">
        <i class="fa-solid"></i>
        <p>
            Texto
        </p>
    </div>
</template>

<template id="tpl__request_card">
    <div class="card request">
        <h3 class="sector"></h3>
        <p class="date"></p>
        <div class="transfer_details"></div>
        <hr>
        <p class="request_state" title="Estado">
            Estado: <span><span class="name">Pendiente</span> <i class="fa-solid"></i></span>
            <span class="set_given text-button underline_thin" title="Marcar como completo">Marcar como completa</span>
            <span class="create_transfer text-button underline_thin" title="Crear egreso para esta petición">Crear egreso</span>
            
        </p>
        <div class="patient_data" title="Paciente">
            Paciente: <span class="name"></span> DNI: <span class="dni"></span>
        </div>
        <div class="details" title="Observaciones"></div>
        <div class="btn_delete" title="Rechazar petición">
            <i class="fa-solid fa-ban"></i>
        </div>
        <h5 class="username"></h5>
    </div>
</template>

<template id="tpl__unverifiedUser_card">
        <div class="card unverifiedUser">
            <h3 class="username"></h3>
            <p class="email"></p>
            <br>
            <p class="date"></p>
            <div class="options">
                <div class="button btn_yes" title="Aceptar usuario">
                    <p>Aceptar</p>
                    <i class="fa-solid fa-check"></i>
                </div>
                <div class="button btn_no" title="Rechazar usuario">
                    <p>Rechazar</p>
                    <i class="fa-solid fa-xmark"></i>
                </div>
            </div>
        </div>
</template>