<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión | Eira</title>
    <script src="https://kit.fontawesome.com/f5394feeef.js" crossorigin="anonymous"></script>
	<script src="js/global.js"></script>
	<script src="js/login_register.js"></script>
    <link rel="stylesheet" href="css/login_register.css">
    <link rel="icon" href="img/system/icon_coin.svg">
</head>
<body>
    {{NAV}}
    <div id="loading_screen" class="loading_screen"></div>
    <h1>Iniciar sesión</h1>
    <h5 id="login_msg_success" class="msg success">{{LOGIN_MESSAGE_SUCCESS}}</h5>
    <h5 id="login_msg_error" class="msg error">{{LOGIN_MESSAGE_ERROR}}</h5>
    <form action="" method="post" id="login-form">
        <div class="input-info">
            <label for="input_email">E-mail</label>
            <input type="email" name="input-email" id="input_email" maxlength="60" autofocus inputmode="email">
        </div>
        <h5 id="valid_email_warning" class="hidden input-error">Ingrese un e-mail válido.</h5>
        <h5 id="empty_email_warning" class="hidden input-error">Complete este campo.</h5>
        <div class="input-info">
            <label for="input_pass">Contraseña</label>
            <input type="password" name="input-pass" id="input_pass" maxlength="32">
            <i id="btn-show-pass" class="text-button fas fa-eye" onclick="toggleViewPass('pass')"></i>
        </div>
        <h5 id="pass_length_warning" class="hidden input-error">La contraseña debe contener al menos 8 caracteres.</h5>
        <h5 id="empty_pass_warning" class="hidden input-error">Complete este campo.</h5>
        
        <input type="submit" value="Iniciar sesión" name="btn-login" id="btn_login" class="button">

        <p id="btn_change_pass" class="text-button underline">¿Olvidó su contraseña?</p>
    </form>
    ¿Aún no tiene una cuenta?
    <a href="/eira/register" class="text-button underline">Registrarse</a>
    {{FOOTER}}
</body>
</html>