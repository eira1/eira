<div id="new_request_container" class="new_container popup">
    <div class="btn_quit"></div>
    <div id="new_request_details" class="details">
        <h3>Nuevo petición de productos</h3>
        <div id="new_request_details_product_list" class="product_list discrete_scrollbar">
            <p class="on">Arrastre un producto de la lista de la derecha para agregarlo al registro de la petición.</p>
        </div>
        <input type="submit" id="btn_new_request" class="button" value="Crear petición">
    </div>
    <div id="new_request_products" class="new_products">
        <input type="search" id="new_request_search_products" class="search" placeholder="Escriba el nombre de algún producto" autocomplete="off">
        <div id="new_request_product_list" class="product_list discrete_scrollbar">
            
        </div>
    </div>
</div>