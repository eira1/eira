<div id="new_order_container" class="new_container popup">
    <div class="btn_quit"></div>
    <div id="new_order_details" class="details">
        <h3>Nuevo ingreso de productos</h3>
        <div class="input-info">
            <!-- <label for="new_order_supplier">Proveedor</label>
            <select id="new_order_supplier">
                <option value="placeholder">Cargando...</option>
            </select> -->
        </div>
        <div id="new_order_details_product_list" class="product_list discrete_scrollbar">
            <p class="on">Arrastre o toque un producto de la lista de la derecha para agregarlo al registro del ingreso.</p>
        </div>
        <input type="submit" id="btn_new_order" class="button" value="Crear ingreso">
    </div>
    <div id="new_order_products" class="new_products">
        <input type="search" id="new_order_search_products" class="search" placeholder="Escriba el nombre de algún producto" autocomplete="off">
        <div id="new_order_product_list" class="product_list discrete_scrollbar">
            
        </div>
    </div>
</div>