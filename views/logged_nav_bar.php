<nav>
    <a id="nav-title" href="/eira">
        <div id="nav-icon"></div>
    </a>
    <div id="nav-options">
        <!-- <a href="/eira/panel" id="nav-link-panel" class="nav-button alter">Ir al panel</a>
        <a href="/eira/logout" id="nav-link-logout" class="nav-button alter">Cerrar sesión</a> -->
        <label id="nav_username" for="btn_profile_menu"></label>
        <label id="label-profile" for="btn_profile_menu"><div class="img"></div></label>
    </div>

    <div id="nav-icons">
        <label for="btn_nav_menu">
            <i class="text-button fas fa-bars"></i>
        </label>
    </div>

    <input type="checkbox" id="btn_profile_menu">
    <!-- <div id="profile-menu">
        <a href="/eira/profile" id="nav-link-profile" class="text-button">Mi perfil</a>
        <div class="hline alter"></div>
        <a href="/eira/logout" id="nav-link-logout" class="text-button">Cerrar Sesión</a>
    </div> -->


    <input type="checkbox" id="btn_nav_menu">
    <div id="nav_menu">
        <a href="/eira/profile" id="nav-menu-link-profile" class="text-button">Mi perfil</a>
        <a href="/eira/logout" id="nav-menu-link-logout" class="text-button">Cerrar Sesión</a>
    </div>


    <div id="menu">
        <label id="menu_back" for="btn_profile_menu"><i class="fa-solid fa-xmark"></i></label>
        <h1>Mi perfil</h1>
        <div id="menu_img">
            <img src="" alt="" onerror="this.onerror = null; this.src = 'img/userphotos/default.png';">
            <div id="menu_img_hover">Cambiar imagen</div>
        </div>
        <!-- <hr>
        <a href="/eira/panel" class="option">
            <i class="fa-solid fa-table-list"></i>
            <p>
                Ir al panel
            </p>
        </a> -->
        <hr>
        <div class="option" id="btn_show_profile">
            <i class="fa-solid fa-pen-to-square"></i>
            <p>
                Modificar perfil
            </p>
        </div>
        <!-- <a href="/eira/profile" class="option">
            <i class="fa-solid fa-pen-to-square"></i>
            <p>
                Modificar perfil
            </p>
        </a> -->
        <hr>
        <a href="/eira/logout" class="option">
            <i class="fa-solid fa-arrow-right-from-bracket"></i>
            <p>
                Cerrar sesión
            </p>
        </a>
        <hr>

        <div id="photo_drop_container" class="popup">
            <div class="btn_quit"></div>
            <div id="photo_drop">
                <i class="fa-solid fa-image"></i>
                <p>
                    Arrastre una imagen aquí, o
                </p>
                <label for="input_photo" class="button" accept=".png, .jpg, .jpeg, .gif">Seleccione un archivo</label>
                <input type="file" id="input_photo">
            </div>
        </div>
        <div id="dark_nav_background"></div>

    </div>
</nav>