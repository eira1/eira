<div id="new_transfer_container" class="new_container popup">
    <div class="btn_quit"></div>
    <div id="new_transfer_details" class="details">
        <h3>Nuevo egreso de productos</h3>
        <div class="input-info">
            <label for="new_transfer_sector">Sector</label>
            <select id="new_transfer_sector">
                <option value="placeholder">Cargando...</option>
            </select>
        </div>
        <!-- <div class="input-info">
            <label for="new_transfer_person">Persona</label>
            <input type="text" id="new_transfer_person" placeholder="Persona que se llevó los productos">
        </div> -->
        <div class="input-info new_add_product_card">
            <label for="new_transfer_details_add_product_card">Agregar Producto al egreso</label>
            <input type="text" id="new_transfer_details_add_product_card" placeholder="Producto" list="product_datalist">
        </div>
        <div id="new_transfer_details_product_list" class="product_list discrete_scrollbar">
            
            <p class="details_msg on">Arrastre o toque un producto de la lista de la derecha para agregarlo al registro del egreso.</p>
            
        </div>
        <div class="btn_clean_list button btn_no" id="btn_clean_transfer_details_list">
            Vaciar lista
            <i class="fa-solid fa-trash"></i>
        </div>
        <input type="submit" id="btn_new_transfer" class="button" value="Crear egreso">
    </div>
    <div id="new_transfer_products" class="new_products">
        <input type="search" id="new_transfer_search_products" class="search" placeholder="Escriba el nombre de algún producto" autocomplete="off">
        <div id="new_transfer_product_list" class="product_list discrete_scrollbar">
            
        </div>
    </div>
</div>