<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eira | El sistema de control de stock para farmacias</title>
    <script src="https://kit.fontawesome.com/f5394feeef.js" crossorigin="anonymous"></script>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="js/global.js"></script>
    <script src="js/panel.js"></script>

    <link rel="stylesheet" href="/eira/css/panel.css">
    <link rel="icon" href="img/system/icon_coin.svg">
    
</head>
<body>
    {{NAV}}
    <div id="loading_screen" class="loading_screen" ></div>
    <main>
        <input type="checkbox" id="hide_side_panel" class="btn_show_hide">
        <section id="side_panel">
            <label for="hide_side_panel" id="btn_hide_side_panel" title="Mostrar/Ocultar panel lateral" class="label_btn_show_hide">
                <i class="fa-solid fa-caret-left"></i>
            </label>
        </section>
        <div id="dark_background" class="dark_background"></div>
    </main>

    <datalist id="product_datalist">
        <option value="M12300">Test Product</option>
    </datalist>
    
</body>
</html>