<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse | Eira</title>
    <script src="https://kit.fontawesome.com/f5394feeef.js" crossorigin="anonymous"></script>
	<script src="js/global.js"></script>
	<script src="js/login_register.js"></script>

    <link rel="stylesheet" href="css/login_register.css">
    <link rel="icon" href="img/system/icon_coin.svg">
</head>
<body>
    {{NAV}}
    <div id="loading_screen" class="loading_screen"></div>
    <h1>Registrarse</h1>
    <h5 id="login_msg_success" class="msg success">{{LOGIN_MESSAGE_SUCCESS}}</h5>
    <h5 id="login_msg_error" class="msg error">{{LOGIN_MESSAGE_ERROR}}</h5>
    <form action="" method="post" id="login-form">
        <div class="input-info">
            <label for="input_firstName">Nombre</label>
            <input type="email" name="input-firstName" id="input_firstName" maxlength="70" autofocus>
        </div>
        <h5 id="empty_firstName_warning" class="hidden input-error">Complete este campo.</h5>
        <div class="input-info">
            <label for="input_lastName">Apellido</label>
            <input type="email" name="input-lastName" id="input_lastName" maxlength="70">
        </div>
        <h5 id="empty_lastName_warning" class="hidden input-error">Complete este campo.</h5>
        <div class="input-info">
            <label for="input_email">E-mail</label>
            <input type="email" name="input-email" id="input_email" maxlength="60" inputmode="email">
        </div>
        <h5 id="valid_email_warning" class="hidden input-error">Ingrese un e-mail válido.</h5>
        <h5 id="empty_email_warning" class="hidden input-error">Complete este campo.</h5>
        <div class="input-info">
            <label for="input_pass">Contraseña</label>
            <input type="password" name="input-pass" id="input_pass" maxlength="32" title="La contraseña debe tener al menos 8 caracteres">
            <i id="btn-show-pass" class="text-button fas fa-eye" onclick="toggleViewPass('pass')"></i>
        </div>
        <h5 id="pass_length_warning" class="hidden input-error">La contraseña debe contener al menos 8 caracteres.</h5>
        <h5 id="empty_pass_warning" class="hidden input-error">Complete este campo.</h5>
        <div class="input-info">
            <label for="input_repass">Repetir contraseña</label>
            <input type="password" name="input-repass" id="input_repass" maxlength="32">
            <i id="btn-show-repass" class="text-button fas fa-eye" onclick="toggleViewPass('repass')"></i>
        </div>
        <h5 id="compare_pass_warning" class="hidden input-error">Las contraseñas no coinciden.</h5>
        <h5 id="empty_repass_warning" class="hidden input-error">Complete este campo.</h5>

        <select id="input_permission">
            <optgroup label="Farmacia">
                <option value="1">Farmacéutico</option>
            </optgroup>
            <optgroup label="Sectores del hospital">
                
            </optgroup>
        </select>

        <input type="submit" value="Registrarse" name="btn-login" id="btn_login" class="button">
        
    </form>
    ¿Ya tiene una cuenta?
    <a href="/eira/login" class="text-button underline">Iniciar sesión</a>
    {{FOOTER}}
</body>
</html>