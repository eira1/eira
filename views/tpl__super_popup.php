<div id="super_popup_dark_background" class="dark_background"></div>
<div id="super_popup" class="has_options">
    <div class="btn_quit" onclick="hideSuperPopup();"></div>
    <h3>¡Súper Popup!</h3>
    <p>Contenido.</p>
    <div class="options">
        <div class="button btn_yes">Yes</div>
        <div class="button btn_no">No</div>
    </div>
</div>