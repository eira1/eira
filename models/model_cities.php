<?php
    include_once 'connections.php';
    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    // 1- Retorna la ciudad que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getCityByID($cityID){
        return getSomethingByParameter("cities","cityID",$cityID)[0];
    }
    
    function getAllCities(){
        return getSomething("cities");
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si la ciudad con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function cityIDExists($cityID){
        return exists("cities","cityID",$cityID);
    }

    function cityNameExists($name){
        return exists("cities","name",$name);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteCityByID($cityID){
        return deleteSomethingByParameter("cities","cityID",$cityID);
    }

    function deleteCityByName($name){
        return deleteSomethingByParameter("cities","cityID",$name);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	//Actualiza los campos que le pasamos por un array en $data, de la ciudad que tiene el $cityID
	// TODO Ejemplo updateCityByID(1,array('name'=> 'Tortuguitas')
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateCityByID($cityID,$data){
		return updateSomethingByID("cities","cityID",$cityID,$data);
	}

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertCity (array('name'=>'Tortuguitas')
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertCity($array){
        return insertSomething ("cities",$array);
    }

?>