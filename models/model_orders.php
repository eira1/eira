<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    function getAllOrders(){
        $query = "SELECT orders.orderID,orders.orderDate,suppliers.supplierName AS supplier,orders.userID FROM orders INNER JOIN suppliers ON orders.supplierID = suppliers.supplierID ORDER BY orders.orderDate DESC LIMIT 10";
        $response = selectQuery($query);
        return array_reverse($response);
    }

    function getOrderDetails($orderID){
        $query = "SELECT orderdetails.amount,products.name FROM orderdetails INNER JOIN products ON orderdetails.productID=products.productID WHERE orderdetails.orderID =".$orderID;
        //$query = "SELECT orderdetails.amount, lotes.loteCode,lotes.expirationDate,orderdetails.amount,orderdetails.supposedAmount,products.name FROM orderdetails LEFT JOIN lotes ON orderdetails.loteID = lotes.loteID INNER JOIN products ON lotes.productID=products.productID WHERE orderdetails.orderID =".$orderID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }
    //var_dump(getOrderDetails(56));

    function getOrderLotes($orderID){
        $query = "SELECT lotes.loteCode,lotes.expirationDate,lotes.totalAmount,lotes.realAmount,products.name,forms.name AS form,products.concentration,products.code FROM orderlotes INNER JOIN lotes ON orderlotes.loteID=lotes.loteID INNER JOIN products ON lotes.productID=products.productID INNER JOIN forms ON products.formID=forms.formID WHERE orderlotes.orderID=".$orderID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }

    // 1- Retorna la orden que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getOrderByID($orderID){
        return getSomethingByParameter("orders","orderID",$orderID)[0];
    }

    // 1- Retorna la/s orden/es que efectuó el usuario pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getOrdersByUser($userID){
        return getSomethingByParameter("orders","userID",$userID);
    }

    // 1- Retorna la/s orden/es que efectuó el supplier pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getOrdersBySupplier($supplierID){
        return getSomethingByParameter("orders","supplierID",$supplierID);
    }

    // 1- Retorna la/s orden/es que fueron efectuadas el dia de la fecha pasada por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getOrdersByDate($orderDate){
        return getSomethingByParameter("orders","orderDate",$orderDate);
    }

    function getSupplierByOrderID($orderID){
        $query = "SELECT suppliers.supplierName as supplier
        FROM orders
        INNER JOIN suppliers
        ON orders.supplierID = suppliers.supplierID
        WHERE orders.orderID =".$orderID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }


    // 1- Retorna el/los detalles de la orden que tiene el ID pasado como parámetro si existe/n
    // 2- Retorna NULL si no existe
    function getOrderDetailsByID($orderID){
        return getSomethingByParameter("orderdetails","orderID",$orderID)[0];
    }

    function getOrderMaxID(){
		$query = "SELECT MAX(orderID) FROM orders;";
		$result = selectQuery($query);
		return $result[0]["MAX(orderID)"];
	}

    function getUserOrderName($orderID){
        $query = "SELECT users.firstName,users.lastName FROM `orders` INNER JOIN users ON orders.userID=users.userID WHERE orderID=".$orderID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }else{
            return $response[0];
        }
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el usuario con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function orderIDExists($orderID){
        return exists("orders","orderID",$orderID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function orderUserExists($userID){
        return exists("orders","userID",$userID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function orderSupplierExists($supplierID){
        return exists("orders","supplierID",$supplierID);
    }

    // Devuelve true si al menos una orden fue efectuada el dia que pasamos como parámetro,
    // caso contrario devuelve false
    function orderDateExists($orderDate){
        return exists("orders","orderDate",$orderDate);
    }

    function orderDetailsExist($orderID){
        return exists("orderdetails","orderID",$orderID);
    }

    function orderLotesExist($orderID){
        return exists("orderlotes","orderID",$orderID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteOrderByID($orderID){
        return deleteSomethingByParameter("orders","orderID",$orderID);
    }

    function deleteOrdersByUser($userID){
        return deleteSomethingByParameter("orders","userID",$userID);
    }

    function deleteOrdersBySupplier($supplierID){
        return deleteSomethingByParameter("orders","supplierID",$supplierID);
    }

    function deleteOrdersByDate($orderDate){
        return deleteSomethingByParameter("orders","orderDate",$orderDate);
    }

    function deleteOrderDetailsByOrderID($orderID){
        return deleteSomethingByParameter("orderdetails","orderID",$orderID);
    }

    function deleteOrderDetailByID($orderDetailID){
        return deleteSomethingByParameter("orderdetails","orderDetailID",$orderDetailID);
    }

    function deleteOrderLotesByOrderID($orderID){
        return deleteSomethingByParameter("orderlotes","orderID",$orderID);
    }

    function deleteOrderLotesByID($orderLoteID){
        return deleteSomethingByParameter("orderlotes","orderLoteID",$orderLoteID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la orden que tiene el $orderID
	// TODO Ejemplo updateOrderByID(1,array('userID'=> '16','supplierID'=>1))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateOrderByID($orderID,$data){
		return updateSomethingByID("orders","orderID",$orderID,$data);
	}

    function updateOrderDetailByID($orderDetailID,$data){
        return updateSomethingByID("orderdetails","orderDetailID",$orderDetailID,$data);
    }

    function updateOrderLoteByID($orderLoteID,$data){
        return updateSomethingByID("orderlotes","orderLoteID",$orderLoteID,$data);
    }

    // TODO Pendiente: function updateOrderDetailsByOrderID()

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertOrder(array('userID'=>16,'supplierID'=>1))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertOrder($array){
        return insertSomething ("orders",$array);
    }

    function insertOrderDetail($array){
        return insertSomething("orderdetails",$array);
    }

    function insertOrderLote($array){
        return insertSomething("orderlotes",$array);
    }

    function turnOrderLotesOnOrderDetails($array){
        $cant = count($array);
        for($x=0; $x<$cant ; $x++) {
            for ($i=0; $i < $cant-1; $i++) { 
                if( ($x!=$i) && ($array[$x]["productID"] == $array[$i]["productID"]) ){
                    $array[$x]["realAmount"] = strval($array[$x]["realAmount"] + $array[$i]["realAmount"]);
                    $array[$x]["totalAmount"] = strval($array[$x]["totalAmount"] + $array[$i]["totalAmount"]);
                    unset($array[$i]);
                    $array = array_values($array);
                    $x = 0;
                    $i = 0;
                }else{
                    $array[$x]["amount"]=$array[$x]["realAmount"];
                }
                $cant = count($array);
            }
        }
        for($f=0;$f<count($array);$f++){
            $response[$f]["productID"]=$array[$f]["productID"];
            $response[$f]["amount"]=$array[$f]["totalAmount"];
        }
        return $response;
    }

    // ! no se si entrega el supplier
    function getOrderPDF($orderID){
        require_once '../class/dom.php';
        $data_order = getOrderByID($orderID);
        //var_dump($data_order);
        $data_orderdetails = getOrderDetails($orderID);
        //var_dump($data_orderdetails);
        $data_orderlote = getOrderLotes($orderID);
        //var_dump($data_orderlote);
        $data_orderperson = getUserOrderName($orderID);
        //var_dump($data_orderperson);
        $data_ordersupplier = getSupplierByOrderID($orderID)[0];
        //var_dump($data_ordersupplier);
        createOrderDOMPDF($data_order,$data_orderdetails,$data_orderlote,$data_orderperson,$data_ordersupplier);
    }
    //getOrderPDF(91);
?>