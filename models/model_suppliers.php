<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS
    // ? No se si hacer getSupplierByContactName y getSupplierBySupplierName
    // ? por si hay algun buscador con eso, pero veremos en el futuro

    // 1- Retorna el proveedor que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getSupplierByID($supplierID){
        return getSomethingByParameter("suppliers","supplierID",$supplierID)[0];
    }

    // 1- Retorna el/los proveedor/es que posean de dirección la calle pasada como parámetro si existe/n
	// 2- Retorna NULL si no existe/n
    function getSuppliersByAddressStreet($addressStreet){
        return getSomethingByParameter("suppliers","addressStreet",$addressStreet);
    }

    // 1- Retorna el/los proveedor/es que sean de la ciudad pasada como parámetro si existe/n
	// 2- Retorna NULL si no existe/n
    function getSuppliersByCity($cityID){
        return getSomethingByParameter("suppliers","cityID",$cityID);
    }

    // 1- Retorna el/los proveedor/es que posean el código postal pasado como parámetro si existe/n
	// 2- Retorna NULL si no existe/n
    function getSuppliersByPostalCode($postalCode){
        return getSomethingByParameter("suppliers","postalCode",$postalCode);
    }

    // 1- Retorna el proveedor que posee el teléfono pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getSupplierByPhoneNumber($phoneNumber){
        return getSomethingByParameter("suppliers","phoneNumber",$phoneNumber)[0];
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el proveedor con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function supplierIDExists($supplierID){
        return exists("suppliers","supplierID",$supplierID);
    }

    function supplierAddressStreetExists($addressStreet){
        return exists("suppliers","addressStreet",$addressStreet);
    }

    // Aclaración obvia: devuelve si la ciudad pasada por parámetro está en algun registro en suppliers
    // NO si está en la tabla cities, es decir, si algún proveedor está en esa ciudad o no
    function supplierCityExists($cityID){
        return exists("suppliers","cityID",$cityID);
    }

    function supplierPostalCodeExists($postalCode){
        return exists("suppliers","postalCode",$postalCode);
    }

    function supplierPhoneNumberExists($phoneNumber){
        return exists("suppliers","phoneNumber",$phoneNumber);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteSupplierByID($supplierID){
        return deleteSomethingByParameter("suppliers","supplierID",$supplierID);
    }

    function deleteSuppliersByAddressStreet($addressStreet){
        return deleteSomethingByParameter("suppliers","addressStreet",$addressStreet);
    }

    function deleteSuppliersByCity($cityID){
        return deleteSomethingByParameter("suppliers","cityID",$cityID);
    }

    function deleteSuppliersByPostalCode($postalCode){
        return deleteSomethingByParameter("suppliers","postalCode",$postalCode);
    }

    function deleteSupplierByPhoneNumber($phoneNumber){
        return deleteSomethingByParameter("suppliers","phoneNumber",$phoneNumber);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

    //Actualiza los campos que le pasamos por un array en $data, del proveedor que tiene el $supplierID
	// TODO Ejemplo updateSupplierByID(1,array('supplierName'=> 'Las bolas de Pepito','contactName'=> 'Pepe Sanchez','addressStreet'=> 'Las Heras','addressNumber'=>325))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
    function updateSupplierByID($supplierID,$data){
        return updateSomethingByID("suppliers","supplierID",$supplierID,$data);
    }

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertSupplier (array('supplierName'=>'Las bolas de Pepito','contactName'=>'Pepe Sánchez','addressStreet'=>'Las Heras','addressNumber'=>'325','cityID'=>4,'postalCode'=>'1667','phoneNumber'=>'1176216532'))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertSupplier($array){
        return insertSomething ("suppliers",$array);
    }

    //funcion agregada por chak

    function getAllSuppliers(){
        return getSomething("suppliers");
    }
?>