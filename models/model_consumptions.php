<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    function getAllConsumptions(){
        $query = "SELECT consumptions.consumptionID,consumptions.consumptionDate,sectors.name AS sector,consumptions.userID FROM consumptions INNER JOIN sectors ON consumptions.sectorID = sectors.sectorID ORDER BY consumptions.consumptionDate ASC LIMIT 10";
        $response = selectQuery($query);
        return $response;
    }

    function getConsumptionsBySector($sectorID){
        $query = "SELECT consumptions.consumptionID,consumptions.consumptionDate,sectors.name AS sector,consumptions.userID FROM consumptions INNER JOIN sectors ON consumptions.sectorID = sectors.sectorID WHERE consumptions.sectorID=".$sectorID." ORDER BY consumptions.consumptionDate DESC LIMIT 10";
        $response = selectQuery($query);
        return $response;
    }

    function getConsumptionDetails($consumptionID){
        $query = "SELECT consumptiondetails.amount, products.name FROM consumptiondetails INNER JOIN products ON consumptiondetails.productID = products.productID INNER JOIN consumptions ON consumptiondetails.consumptionID=consumptions.consumptionID WHERE consumptiondetails.consumptionID =".$consumptionID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }

    function getUserConsumptionName($consumptionID){
        $query = "SELECT users.firstName,users.lastName FROM `consumptions` INNER JOIN users ON consumptions.userID=users.userID WHERE consumptionID=".$consumptionID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }else{
            return $response[0];
        }
    }

    // 1- Retorna la orden que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getConsumptionByID($consumptionID){
        return getSomethingByParameter("consumptions","consumptionID",$consumptionID)[0];
    }

    // 1- Retorna la/s orden/es que efectuó el usuario pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getConsumptionsByUser($userID){
        return getSomethingByParameter("consumptions","userID",$userID);
    }

    // 1- Retorna la/s orden/es que fueron efectuadas el dia de la fecha pasada por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getConsumptionsByDate($consumptionDate){
        return getSomethingByParameter("consumptions","consumptionDate",$consumptionDate);
    }

    // 1- Retorna el/los detalles de la orden que tiene el ID pasado como parámetro si existe/n
    // 2- Retorna NULL si no existe
    function getConsumptionDetailsByID($consumptionID){
        return getSomethingByParameter("consumptiondetails","consumptionID",$consumptionID)[0];
    }

    function getConsumptionMaxID(){
		$query = "SELECT MAX(consumptionID) FROM consumptions;";
		$result = selectQuery($query);
		return $result[0]["MAX(consumptionID)"];
	}

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el usuario con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function consumptionIDExists($consumptionID){
        return exists("consumptions","consumptionID",$consumptionID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function consumptionUserExists($userID){
        return exists("consumptions","userID",$userID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function consumptionSectorExists($sectorID){
        return exists("consumptions","sectorID",$sectorID);
    }

    // Devuelve true si al menos una orden fue efectuada el dia que pasamos como parámetro,
    // caso contrario devuelve false
    function consumptionDateExists($consumptionDate){
        return exists("consumptions","consumptionDate",$consumptionDate);
    }

    function consumptionDetailsExist($consumptionID){
        return exists("consumptiondetails","consumptionID",$consumptionID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteConsumptionByID($consumptionID){
        return deleteSomethingByParameter("consumptions","consumptionID",$consumptionID);
    }

    function deleteConsumptionsByUser($userID){
        return deleteSomethingByParameter("consumptions","userID",$userID);
    }

    function deleteConsumptionsBySector($sectorID){
        return deleteSomethingByParameter("consumptions","sectorID",$sectorID);
    }

    function deleteConsumptionsByDate($consumptionDate){
        return deleteSomethingByParameter("consumptions","consumptionDate",$consumptionDate);
    }

    function deleteConsumptionDetailsByconsumptionID($consumptionID){
        return deleteSomethingByParameter("consumptiondetails","consumptionID",$consumptionID);
    }

    function deleteConsumptionDetailByID($consumptionDetailID){
        return deleteSomethingByParameter("consumptiondetails","consumptionDetailID",$consumptionDetailID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la orden que tiene el $transferID
	// TODO Ejemplo updateTransferByID(1,array('userID'=> '16','sectorID'=>1))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateConsumptionByID($consumptionID,$data){
		return updateSomethingByID("consumptions","consumptionID",$consumptionID,$data);
	}

    function updateConsumptionDetailByID($consumptionDetailID,$data){
        return updateSomethingByID("consumptiondetails","consumptionDetailID",$consumptionDetailID,$data);
    }

    // TODO Pendiente: function updateTransferDetailsByTransferID()

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertTransfer(array('userID'=>16,'sectorID'=>1))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertConsumption($array){
        return insertSomething ("consumptions",$array);
    }

    function insertConsumptionDetail($array){
        return insertSomething("consumptiondetails",$array);
    }

    /*function duplicateProducts($array){
        $cant = count($array);
        for($x=0; $x<$cant ; $x++){
            for ($i=0; $i < $cant-1; $i++) { 
                if( ($x!=$i) && ($array[$x]->productID == $array[$i]->productID) ){
                    $array[$x]->amount = strval($array[$x]->amount + $array[$i]->amount);
                    unset($array[$i]);
                    $array = array_values($array);
                    $x = 0;
                    $i = 0;
                }
                $cant = count($array);
            }
        }
        return $array;
    }*/
?>