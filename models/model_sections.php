<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE DATOS A TRAVÉS DE PARÁMETROS

    function getSectionByID($sectionID){
        return getSomethingByParameter("sections","sectionID",$sectionID)[0];
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si la sección con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function sectionIDExists($sectionID){
        return exists("sections","sectionID",$sectionID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteSectionByID($sectionID){
        return deleteSomethingByParameter("sections","sectionID",$sectionID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la sección que tiene el $sectionID
    // TODO Ejemplo updateSectionByID(1,array('subcategoryID'=>2,'name'=> 'que se yo','description'=> 'bla bla'))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateSectionByID($sectionID,$data){
		return updateSomethingByID("sections","sectionID",$sectionID,$data);
	}
    
    //*FUNCIONES INSERT - INSERCIÓN DE PRODUCTOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertSection (array('subcategoryID'=>2,'name'=>'que se yo','description'=>'bla bla'))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertSection($array){
        return insertSomething ("sections",$array);
    }

?>