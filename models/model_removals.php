<?php
    include_once 'connections.php';

    include_once 'connections.php';
    include_once 'model_lotes.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    function getAllRemovals(){
        $query = "SELECT removals.removalID,removals.removalDate,removals.userID FROM removals ORDER BY removals.removalDate DESC LIMIT 10";
        $response = selectQuery($query);
        return array_reverse($response);
    }

    function getRemovalDetails($removalID){
        $query = "SELECT lotes.loteCode FROM removaldetails INNER JOIN lotes ON removaldetails.loteID=lotes.loteID WHERE removaldetails.removalID =".$removalID;
        //$query = "SELECT removaldetails.amount, lotes.loteCode,lotes.expirationDate,removaldetails.amount,removaldetails.supposedAmount,products.name FROM removaldetails LEFT JOIN lotes ON removaldetails.loteID = lotes.loteID INNER JOIN products ON lotes.productID=products.productID WHERE removaldetails.removalID =".$removalID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }

    function getUserRemovalName($removalID){
        $query = "SELECT users.firstName,users.lastName FROM `removals` INNER JOIN users ON removals.userID=users.userID WHERE removalID=".$removalID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }else{
            return $response[0];
        }
    }

    // 1- Retorna la orden que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getRemovalByID($removalID){
        return getSomethingByParameter("removals","removalID",$removalID)[0];
    }

    // 1- Retorna la/s orden/es que efectuó el usuario pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getRemovalsByUser($userID){
        return getSomethingByParameter("removals","userID",$userID);
    }

    // 1- Retorna la/s orden/es que fueron efectuadas el dia de la fecha pasada por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getRemovalsByDate($removalDate){
        return getSomethingByParameter("removals","removalDate",$removalDate);
    }

    // 1- Retorna el/los detalles de la orden que tiene el ID pasado como parámetro si existe/n
    // 2- Retorna NULL si no existe
    function getRemovalDetailsByID($removalID){
        return getSomethingByParameter("removaldetails","removalID",$removalID)[0];
    }

    function getRemovalMaxID(){
		$query = "SELECT MAX(removalID) FROM removals;";
		$result = selectQuery($query);
		return $result[0]["MAX(removalID)"];
	}

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el usuario con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function removalIDExists($removalID){
        return exists("removals","removalID",$removalID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function removalUserExists($userID){
        return exists("removals","userID",$userID);
    }

    // Devuelve true si al menos una orden fue efectuada el dia que pasamos como parámetro,
    // caso contrario devuelve false
    function removalDateExists($removalDate){
        return exists("removals","removalDate",$removalDate);
    }

    function removalDetailsExist($removalID){
        return exists("removaldetails","removalID",$removalID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteRemovalByID($removalID){
        return deleteSomethingByParameter("removals","removalID",$removalID);
    }

    function deleteRemovalsByUser($userID){
        return deleteSomethingByParameter("removals","userID",$userID);
    }

    function deleteRemovalsByDate($removalDate){
        return deleteSomethingByParameter("removals","removalDate",$removalDate);
    }

    function deleteRemovalDetailsByRemovalID($removalID){
        return deleteSomethingByParameter("removaldetails","removalID",$removalID);
    }

    function deleteRemovalDetailByID($removalDetailID){
        return deleteSomethingByParameter("removaldetails","removalDetailID",$removalDetailID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la orden que tiene el $removalID
	// TODO Ejemplo updateRemovalByID(1,array('userID'=> '16','sectorID'=>1))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateRemovalByID($removalID,$data){
		return updateSomethingByID("removals","removalID",$removalID,$data);
	}

    function updateRemovalDetailByID($removalDetailID,$data){
        return updateSomethingByID("removaldetails","removalDetailID",$removalDetailID,$data);
    }

    // TODO Pendiente: function updateRemovalDetailsByRemovalID()

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertRemoval(array('userID'=>16,'sectorID'=>1))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertRemoval($array){
        return insertSomething ("removals",$array);
    }

    function insertRemovalDetail($array){
        return insertSomething("removaldetails",$array);
    }
?>