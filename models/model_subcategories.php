<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE DATOS A TRAVÉS DE PARÁMETROS

    function getSubcategoryByID($subcategoryID){
        return getSomethingByParameter("subcategories","subcategoryID",$subcategoryID)[0];
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si la subcategoría con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function subcategoryIDExists($subcategoryID){
        return exists("subcategories","subcategoryID",$subcategoryID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteSubcategoryByID($subcategoryID){
        return deleteSomethingByParameter("subcategories","subcategoryID",$subcategoryID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la subcategoría que tiene el $subcategoryID
    // TODO Ejemplo updateSubcategoryByID(1,array('categoryID'=>2,'name'=> 'que se yo','description'=> 'bla bla'))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateSubcategoryByID($subcategoryID,$data){
		return updateSomethingByID("subcategories","subcategoryID",$subcategoryID,$data);
	}
    
    //*FUNCIONES INSERT - INSERCIÓN DE PRODUCTOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertSubcategory (array('categoryID'=>2,'name'=>'que se yo','description'=>'bla bla'))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertSubcategory($array){
        return insertSomething ("subcategories",$array);
    }

?>