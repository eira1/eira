<?php
    include_once 'connections.php';

    //* FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A TRAVÉS DE PARÁMETROS
    // 1- Retorna el usuario que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no hay Producto con ese ID
	function getProductByID($productID){
		return getSomethingByParameter("products","productID",$productID)[0];
	}

    function getProductStockByID($productID){
        return getProductByID($productID)["currentAmount"];
    }

    function getProductNameByID($productID){
        return getProductByID($productID)["name"];
    }

    function getProductFormByID($productID){
        $query = 'SELECT forms.name AS form 
        FROM products 
        INNER JOIN forms ON products.formID=forms.formID 
        WHERE productID='.$productID;
        $response = selectQuery($query);
        return $response[0]["form"];
    }

    function getAllProducts(){
        $query = 'SELECT `productID`, products.`name`, products.`description`, `price`, `photo`, `currentAmount`,expiredAmount,criticalStock, concentration,code, forms.name AS form 
        FROM `products` 
        INNER JOIN forms on products.formID=forms.formID;';
        
        $response = selectQuery($query);
        return $response;
        //return getSomething("products");
    }
    //var_dump(getAllProducts());
	
	// 1- Retorna los productos que vienen de determinado proveedor, si existe
	// 2- Retorna NULL si no hay producto que venga de ese proveedor
	function getProductsBySupplierID($supplierID){
		return getSomethingByParameter("products","supplierID",$supplierID);
	}

    function getProductsByText(string $text){
        return getSomethingByText("products","name",$text);
    }

    function countProductsByText(string $text){
        return countSomethingByText("products","name",$text);
    }

    function getProductsByTextLimit(string $text,int $limit){
        return getSomethingByTextLimit("products","name",$text,$limit);
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el producto con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function productIDExists($productID){
        return exists("products","productID",$productID);
    }

    function productSupplierExists($supplierID){
        return exists("products","supplierID",$supplierID);
    }

    function productPhotoExists($productPhoto){
        return exists("products","photo",$productPhoto);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteProductByID($productID){
        return deleteSomethingByParameter("products","productID",$productID);
    }

    function deleteProductsBySupplier($supplierID){
        return deleteSomethingByParameter("products","supplierID",$supplierID);
    }
    
    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	//Actualiza los campos que le pasamos por un array en $data, del producto que tiene el $productID
    // TODO Ejemplo updateProductByID(3,array('name'=>'Guantes de látex'))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateProductByID($productID,$data){
		return updateSomethingByID("products","productID",$productID,$data);
	}

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertProduct (array('name'=>'Guantes de Latex','description'=>'Peligrosos','price'=>10,'photo'=>'No hay, me dan miedo'))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertProduct($array){
        return insertSomething ("products",$array);
    }


    // * FUNCION PROMEDIO DE GASTO DIARIO DE PRODUCTOS

    // ! IMPORTANTE, DEBERIA MODIFICARSE DE TAL FORMA QUE RECIBA EL RANGO DE FECHAS

    // ! RETORNA PROMEDIO DIARIO, EL MAXIMO JUNTO A LA FECHA, EL MINIMO JUNTO A LA FECHA, Y EL TOTAL GASTADO

    function getStatisticsByProductID($productID,$startDate,$finishDate){
        /* $finishDate = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime($finishDate. ' - '.$months.' months'));
         */

        // * Query que devuelve el promedio diario gastado de un producto
        $statistics["avg"] = selectQuery('SELECT ROUND(AVG(transferdetails.amount),0) as "PROM_AMOUNT"
        FROM transferdetails 
        INNER JOIN transfers
        ON transferdetails.transferID = transfers.transferID
        WHERE transferdetails.productID ='.$productID.' 
        AND transfers.transferDate BETWEEN "'.$startDate.'" AND "'.$finishDate.'"')[0]['PROM_AMOUNT'];

        // * Query que devuelve el gasto total en x tiempo

        $statistics["spentAmount"] = selectQuery('SELECT SUM(transferdetails.amount) as SPENT_AMOUNT
        FROM transferdetails
        INNER JOIN transfers
        ON transferdetails.transferID = transfers.transferID
        WHERE transferdetails.productID ='.$productID.' AND transfers.transferDate BETWEEN "'.$startDate.'" AND "'.$finishDate.'";')[0]["SPENT_AMOUNT"];
        
        // * Query que devuelve el maximo gastado junto a la fecha.

        $response = selectQuery('SELECT transfers.transferDate as MAX_DATE, transferdetails.amount as MAX_AMOUNT
        FROM transfers
        INNER JOIN transferdetails
        ON transfers.transferID = transferdetails.transferID
        WHERE transferdetails.productID ='.$productID.' AND transfers.transferDate BETWEEN "'.$startDate.'" AND "'.$finishDate.'"
        ORDER BY transferdetails.amount DESC
        LIMIT 1')[0];
        $statistics["max"] = $response["MAX_AMOUNT"];
        $statistics["maxDate"] = $response["MAX_DATE"];

        // * Query que devuelve el minimo gastado junto a la fecha.

        $response = selectQuery('SELECT transfers.transferDate as MIN_DATE, transferdetails.amount as MIN_AMOUNT
        FROM transfers
        INNER JOIN transferdetails
        ON transfers.transferID = transferdetails.transferID
        WHERE transferdetails.productID ='.$productID.' AND transfers.transferDate BETWEEN "'.$startDate.'" AND "'.$finishDate.'"
        ORDER BY transferdetails.amount ASC
        LIMIT 1')[0];
        $statistics["min"] = $response["MIN_AMOUNT"];
        $statistics["minDate"] = $response["MIN_DATE"];

        return $statistics;
    }

    function getStatisticsMostSpentProducts(){
        $response = selectQuery("SELECT transferdetails.productID, SUM(transferdetails.amount) as cantidadTransferida,products.name as productName
        FROM transferdetails
        INNER JOIN products ON transferdetails.productID=products.productID
        GROUP BY transferdetails.productID 
        ORDER BY cantidadTransferida DESC LIMIT 10;");

        return $response;
    }

    function getStatisticsY(){
        $max = selectQuery("SELECT transferdetails.productID, SUM(transferdetails.amount) as cantidadTransferida,products.name as productName FROM transferdetails INNER JOIN products ON transferdetails.productID=products.productID GROUP BY transferdetails.productID ORDER BY cantidadTransferida DESC LIMIT 1;")[0]["cantidadTransferida"];
        $response["y1"]=round($max*(1/10));
        $response["y2"]=round($max*(2/10));
        $response["y3"]=round($max*(3/10));
        $response["y4"]=round($max*(4/10));
        $response["y5"]=round($max*(5/10));
        $response["y6"]=round($max*(6/10));
        $response["y7"]=round($max*(7/10));
        $response["y8"]=round($max*(8/10));
        $response["y9"]=round($max*(9/10));
        $response["y10"]=round($max);
        return $response;
    }

?>