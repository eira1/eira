<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE DATOS A TRAVÉS DE PARÁMETROS

    function getCategoryByID($categoryID){
        return getSomethingByParameter("categories","categoryID",$categoryID)[0];
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si la categoría con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function categoryIDExists($categoryID){
        return exists("categories","categoryID",$categoryID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteCategoryByID($categoryID){
        return deleteSomethingByParameter("categories","categoryID",$categoryID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la categoría que tiene el $categoryID
    // TODO Ejemplo updateCategoryByID(1,array('name'=> 'que se yo','description'=> 'bla bla'))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateCategoryByID($categoryID,$data){
		return updateSomethingByID("categories","categoryID",$categoryID,$data);
	}

    // lo agrego chak para insertar

    function insertCategory($array){
        return insertSomething ("categories",$array);
    }

?>