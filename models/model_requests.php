<?php
    include_once 'connections.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    function getAllRequests(){
        $query = "SELECT requests.requestID,requests.requestDate,
         requests.sectorID, sectors.name AS sector,requests.userID,
         requests.requestPerson,requests.requestPersonDNI,requests.details,
         requests.given,requests.accepted,requests.rejected 
        FROM requests 
        INNER JOIN sectors ON requests.sectorID = sectors.sectorID 
        ORDER BY requests.requestDate ASC,requests.given ASC 
        LIMIT 10";
        $response = selectQuery($query);
        return $response;
    }

    function getAllPendingRequests(){
        $query = "SELECT requests.requestID,requests.requestDate, requests.sectorID, sectors.name AS sector,
        requests.userID,requests.requestPerson,requests.requestPersonDNI,requests.details,requests.given,requests.accepted,requests.rejected
        FROM requests 
        INNER JOIN sectors ON requests.sectorID = sectors.sectorID
        WHERE accepted=0 AND rejected=0 AND given=0
        ORDER BY requests.requestDate ASC,requests.given ASC 
        LIMIT 10";
        $response = selectQuery($query);
        return $response;
    }

    function getAllNonPendingRequests(){
        $query = "SELECT requests.requestID,requests.requestDate, requests.sectorID, sectors.name AS sector,requests.userID,requests.requestPerson,requests.requestPersonDNI,requests.details,requests.given,requests.accepted,requests.rejected 
        FROM requests 
        INNER JOIN sectors ON requests.sectorID = sectors.sectorID
        WHERE accepted=1 OR rejected=1
        ORDER BY requests.requestDate ASC,requests.given ASC 
        LIMIT 10";
        $response = selectQuery($query);
        return $response;
    }
    // ? var_dump(getAllPendingRequests());

    function getRequestDetails($requestID){
        $query = "SELECT requestdetails.amount, products.name,
         requestdetails.productID, products.concentration 
         FROM requestdetails 
         INNER JOIN products ON requestdetails.productID = products.productID 
         WHERE requestdetails.requestID =".$requestID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }

    function getUserRequestName($requestID){
        $query = "SELECT users.firstName,users.lastName FROM `requests` INNER JOIN users ON requests.userID=users.userID WHERE requestID=".$requestID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }else{
            return $response[0];
        }
    }

    function getAllSectorRequests($sectorID){
        $query = "SELECT requests.requestID,requests.requestDate,sectors.name AS sector,requests.userID,
        requests.requestPerson,requests.requestPersonDNI,requests.details,requests.given,requests.accepted,requests.rejected
        FROM requests 
        INNER JOIN sectors ON requests.sectorID = sectors.sectorID 
        WHERE requests.sectorID=".$sectorID." ORDER BY requests.requestDate DESC LIMIT 10";
        $response = selectQuery($query);
        return $response;
    }

    // 1- Retorna la orden que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getRequestByID($requestID){
        return getSomethingByParameter("requests","requestID",$requestID)[0];
    }

    // 1- Retorna la/s orden/es que efectuó el usuario pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getRequestsByUser($userID){
        return getSomethingByParameter("requests","userID",$userID);
    }

    // 1- Retorna la/s orden/es que efectuó el sector pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getRequestsBySector($sectorID){
        return getSomethingByParameter("requests","sectorID",$sectorID);
    }

    // 1- Retorna la/s orden/es que fueron efectuadas el dia de la fecha pasada por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getRequestsByDate($requestDate){
        return getSomethingByParameter("requests","requestDate",$requestDate);
    }

    // 1- Retorna el/los detalles de la orden que tiene el ID pasado como parámetro si existe/n
    // 2- Retorna NULL si no existe
    function getRequestDetailsByID($requestID){
        return getSomethingByParameter("requestdetails","requestID",$requestID)[0];
    }

    function getRequestMaxID(){
		$query = "SELECT MAX(requestID) FROM requests;";
		$result = selectQuery($query);
		return $result[0]["MAX(requestID)"];
	}

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el usuario con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function requestIDExists($requestID){
        return exists("requests","requestID",$requestID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function requestUserExists($userID){
        return exists("requests","userID",$userID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function requestSectorExists($sectorID){
        return exists("requests","sectorID",$sectorID);
    }

    // Devuelve true si al menos una orden fue efectuada el dia que pasamos como parámetro,
    // caso contrario devuelve false
    function requestDateExists($requestDate){
        return exists("requests","requestDate",$requestDate);
    }

    function requestDetailsExist($requestID){
        return exists("requestdetails","requestID",$requestID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteRequestByID($requestID){
        return deleteSomethingByParameter("requests","requestID",$requestID);
    }

    function deleteRequestsByUser($userID){
        return deleteSomethingByParameter("requests","userID",$userID);
    }

    function deleteRequestsBySector($sectorID){
        return deleteSomethingByParameter("requests","sectorID",$sectorID);
    }

    function deleteRequestsByDate($requestDate){
        return deleteSomethingByParameter("requests","requestDate",$requestDate);
    }

    function deleteRequestDetailsByRequestID($requestID){
        return deleteSomethingByParameter("requestdetails","requestID",$requestID);
    }

    function deleteRequestDetailByID($requestDetailID){
        return deleteSomethingByParameter("requestdetails","requestDetailID",$requestDetailID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la orden que tiene el $transferID
	// TODO Ejemplo updateTransferByID(1,array('userID'=> '16','sectorID'=>1))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateRequestByID($requestID,$data){
		return updateSomethingByID("requests","requestID",$requestID,$data);
	}

    function updateRequestDetailByID($requestDetailID,$data){
        return updateSomethingByID("requestdetails","requestDetailID",$requestDetailID,$data);
    }

    function requestGiven($requestID){
		return updateSomethingByID("requests","requestID",$requestID,array("given"=>1));
	}
    function requestNotGiven($requestID){
		return updateSomethingByID("requests","requestID",$requestID,array("given"=>0));
	}

    function acceptRequest($requestID){
        return updateSomethingByID("requests","requestID",$requestID,array("accepted"=>1));
    }

    function rejectRequest($requestID){
        return updateSomethingByID("requests","requestID",$requestID,array("rejected"=>1));
    }

    // TODO Pendiente: function updateTransferDetailsByTransferID()

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertTransfer(array('userID'=>16,'sectorID'=>1))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertRequest($array){
        return insertSomething ("requests",$array);
    }

    function insertRequestDetail($array){
        return insertSomething("requestdetails",$array);
    }

    function getUserSector($token){
        $query = "SELECT sectorpermissions.sectorID FROM users INNER JOIN sectorpermissions ON users.permissionID = sectorpermissions.permissionID WHERE users.token = '".$token."'";
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response[0]["sectorID"];
    }

    /*function duplicateProducts($array){
        $cant = count($array);
        for($x=0; $x<$cant ; $x++){
            for ($i=0; $i < $cant-1; $i++) { 
                if( ($x!=$i) && ($array[$x]->productID == $array[$i]->productID) ){
                    $array[$x]->amount = strval($array[$x]->amount + $array[$i]->amount);
                    unset($array[$i]);
                    $array = array_values($array);
                    $x = 0;
                    $i = 0;
                }
                $cant = count($array);
            }
        }
        return $array;
    }*/
?>