<?php
    include_once 'connections.php';

    function getFormByID($formID){
        return getSomethingByParameter("forms","formID",$formID)[0];
    }

    function formIDExists($formID){
        return exists("forms","formID",$formID);
    }

    function deleteFormByID($formID){
        return deleteSomethingByParameter("forms","formID",$formID);
    }

    function updateFormByID($formID,$data){
		return updateSomethingByID("forms","formID",$formID,$data);
	}

    function insertForm($array){
        return insertSomething ("forms",$array);
    }
?>