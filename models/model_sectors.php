<?php
    include_once 'connections.php';
    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    // 1- Retorna el sector que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getSectorByID($sectorID){
        return getSomethingByParameter("sectors","sectorID",$sectorID)[0];
    }

    function getAllSectors(){
        return getSomething("sectors");
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el sector con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function sectorIDExists($sectorID){
        return exists("sectors","sectorID",$sectorID);
    }

    function sectorNameExists($name){
        return exists("sectors","name",$name);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteSectorByID($sectorID){
        return deleteSomethingByParameter("sectors","sectorID",$sectorID);
    }

    function deleteSectorByName($name){
        return deleteSomethingByParameter("sectors","sectorID",$name);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de el sector que tiene el $sectorID
	// TODO Ejemplo updateSectorByID(1,array('name'=> 'Oncología')
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateSectorByID($sectorID,$data){
		return updateSomethingByID("sectors","sectorID",$sectorID,$data);
	}

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertSector (array('name'=>'Oncología')
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertSector($array){
        return insertSomething ("sectors",$array);
    }

?>