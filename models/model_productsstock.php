<?php
    include_once 'connections.php';

    function getAllProductsIDs(){
        $query = 'SELECT `productID` FROM `products`';
        $response = selectQuery($query);
        return $response;
    }

    function getAllSectorsIDs(){
        $query = 'SELECT `sectorID` FROM `sectors`';
        $response = selectQuery($query);
        return $response;
    }

    function getProductStockByProductStockID($productStockID){
        return getSomethingByParameter("productsstock","productStockID",$productStockID);
    }

    function getProductsStockBySector($sectorID){
        $query="SELECT productsstock.productStockID,productsstock.productID,productsstock.currentAmount, products.name,products.description,products.photo,products.concentration,products.code,forms.name AS form FROM `productsstock` INNER JOIN products ON productsstock.productID=products.productID INNER JOIN forms ON products.formID=forms.formID WHERE productsstock.sectorID=".$sectorID;
        $response = selectQuery($query);
        return $response;
    }

    function getProductsStockByProduct($productID){
        return getSomethingByParameter("productsstock","productID",$productID);
    }

    function getProductStockAmountBySectorAndProduct($sectorID,$productID){
        $query = "SELECT currentAmount FROM productsstock WHERE sectorID=".$sectorID." AND productID=".$productID.";";
		$result = selectQuery($query);
		return $result[0]["currentAmount"];
    }
    //var_dump(getProductStockAmountBySectorAndProduct(1,1));

    function getProductsMaxID(){
		$query = "SELECT MAX(productID) FROM products;";
		$result = selectQuery($query);
		return $result[0]["MAX(orderID)"];
	}

    function getSectorsMaxID(){
		$query = "SELECT MAX(sectorID) FROM sectors;";
		$result = selectQuery($query);
		return $result[0]["MAX(sectorID)"];
	}

    function insertProductsStockRowBySectorID($sectorID){
        $productsIDs=getAllProductsIDs();
        $ver = true;
        //var_dump($productsIDs);
        // todo INSERT INTO `productsstock`(`sectorID`, `productID`, `currentAmount`) VALUES ('[value-2]','[value-3]','[value-4]')
        $cantProducts= count($productsIDs);
        for($i=0;$i<$cantProducts;$i++){
            $productID=$productsIDs[$i]["productID"];
            $query = 'INSERT INTO `productsstock`(`sectorID`, `productID`, `currentAmount`) VALUES ('.$sectorID.','.$productID.',0)';
            $result = insertQuery($query);
            if($result===TRUE){}else{
                $ver = $result;
                break;
            }
        }
        return $ver;
    }
    

    function insertProductsStockRowByProductID($productID){
        $sectorsIDs=getAllSectorsIDs();
        $ver = true;
        //var_dump($productsIDs);
        // todo INSERT INTO `productsstock`(`sectorID`, `productID`, `currentAmount`) VALUES ('[value-2]','[value-3]','[value-4]')
        $cantSectors= count($sectorsIDs);
        for($i=0;$i<$cantSectors;$i++){
            $sectorID=$sectorsIDs[$i]["sectorID"];
            $query = 'INSERT INTO `productsstock`(`sectorID`, `productID`, `currentAmount`) VALUES ('.$sectorID.','.$productID.',0)';
            $result = insertQuery($query);
            if($result===TRUE){}else{
                $ver = $result;
                break;
            }
        }
        return $ver;
    }
?>