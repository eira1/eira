<?php
    include_once 'connections.php';

    //* FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A TRAVÉS DE PARÁMETROS
    // 1- Retorna el usuario que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no hay loteo con ese ID
	function getLoteByID($loteID){
		return getSomethingByParameter("lotes","loteID",$loteID)[0];
	}

    function getLoteStockByID($loteID){
        return getLoteByID($loteID)["amount"];
    }

    function getLoteCodeByID($loteID){
        return getLoteByID($loteID)["loteCode"];
    }

    function getAllLotes(){
        $query = 'SELECT * FROM `lotes`';
        $response = selectQuery($query);
        return $response;
        //return getSomething("lotes");
    }
    //var_dump(getAlllotes());	
	
    function getLotesByCode(string $code){
        return getSomethingByText("lotes","loteCode",$code);
    }

    function getLotesMaxID(){
		$query = "SELECT MAX(loteID) FROM lotes;";
		$result = selectQuery($query);
		return $result[0]["MAX(loteID)"];
	}

    function countLotesByCode(string $code){
        return countSomethingByText("lotes","loteCode",$code);
    }

    function getLotesByCodeLimit(string $code,int $limit){
        return getSomethingByTextLimit("lotes","loteCode",$code,$limit);
    }

    function getUnexpiredLotes(){
        $query = 'SELECT * FROM `lotes` 
        WHERE expirationDate>"'.date("Y-m-d H:i:s",time()).'" 
        AND hide=0';
        $response = selectQuery($query);
        return $response;
    }

    function getExpiredLotes(){
        $query = 'SELECT * FROM `lotes` 
        INNER JOIN products ON lotes.productID=products.productID
        WHERE expirationDate<"'.date("Y-m-d H:i:s",time()).'" AND hide=0
        ORDER BY expirationDate DESC';
        $response = selectQuery($query);
        return $response;
    }

    function getExpiredProductLotes(){
        $query = 'SELECT lotes.loteID,lotes.productID,lotes.loteCode,lotes.expirationDate,lotes.totalAmount,lotes.realAmount,lotes.hide,
        products.productID,products.name,products.description,products.price,products.photo,products.currentAmount,products.expiredAmount,products.criticalStock,
        products.concentration,forms.name AS form,products.code,COUNT(loteID) as lotesAmount
        FROM `lotes` 
        INNER JOIN products ON lotes.productID=products.productID
        INNER JOIN forms ON products.formID=forms.formID
        WHERE expirationDate<"'.date("Y-m-d H:i:s",time()).'" AND hide=0
        GROUP BY products.productID 
        ORDER BY expirationDate ASC;';
        $response = selectQuery($query);
        return $response;
    }

    function getAboutToExpireProductLotes(){
        $query = 'SELECT lotes.loteID,lotes.productID,lotes.loteCode,lotes.expirationDate,lotes.totalAmount,lotes.realAmount,lotes.hide,
        products.productID,products.name,products.description,products.price,products.photo,products.currentAmount,products.expiredAmount,products.criticalStock,
        products.concentration,forms.name AS form,products.code,COUNT(loteID) as lotesAmount
        FROM `lotes` 
        INNER JOIN products ON lotes.productID=products.productID
        INNER JOIN forms ON products.formID=forms.formID
        WHERE (expirationDate BETWEEN "'.date("Y-m-d H:i:s",time()).'" AND date_add("'.date("Y-m-d H:i:s",time()).'",INTERVAL 7 DAY)) 
        AND hide=0
        GROUP BY products.productID 
        ORDER BY expirationDate ASC;';
        $response = selectQuery($query);
        return $response;
    }

    function getEmptyLotes(){
        $query = 'SELECT * FROM `lotes` WHERE realAmount=0 AND hide=0';
        $response = selectQuery($query);
        return $response;
    }

    function getNonEmptyLotes(){
        $query = 'SELECT * FROM `lotes` 
        WHERE realAmount>0 AND hide=0';
        $response = selectQuery($query);
        return $response;
    }

    function getNonEmptyLotesByProductID($productID){
        $query = "SELECT * FROM lotes 
        WHERE productID=".$productID." AND realAmount>0 AND hide=0 
        ORDER BY expirationDate ASC";
        $response = selectQuery($query);
        for($i=0;$i<count($response);$i++){
            if($response[$i]["expirationDate"]<date("Y-m-d H:i:s",time())){
                $response[$i]["expired"]=1;
            }else{
                $response[$i]["expired"]=0;
            }
            $response[$i]["percentage"]=round(($response[$i]["realAmount"]*100)/$response[$i]["totalAmount"],2);
        }
        return $response;
    }

    function getEmptyOrExpiredLotes(){
        $query = 'SELECT * FROM `lotes` WHERE expirationDate<"'.date("Y-m-d H:i:s",time()).'" OR realAmount=0 AND hide=0';
        $response = selectQuery($query);
        return $response;
    }

    function getLotesByProductID($productID){
        $query = "SELECT * FROM lotes WHERE productID=".$productID." AND hide=0 ORDER BY expirationDate ASC";
        $response = selectQuery($query);
        for($i=0;$i<count($response);$i++){
            if($response[$i]["expirationDate"]<date("Y-m-d H:i:s",time())){
                $response[$i]["expired"]=1;
            }else{
                $response[$i]["expired"]=0;
            }
            $response[$i]["percentage"]=round(($response[$i]["realAmount"]*100)/$response[$i]["totalAmount"],2);
        }
        return $response;
    }
    //var_dump(getLotesByProductID(1));

    

    function getLotesByProductIDOrderByExpirationDate($productID){
        $query = "SELECT * FROM lotes WHERE productID=".$productID." AND hide=0 ORDER BY expirationDate ASC";
        $response = selectQuery($query);
        return $response;
    }

    function getNonExpiredLotesByProductIDOrderByExpirationDate($productID){
        $query = "SELECT * 
        FROM lotes 
        WHERE productID=".$productID." AND hide=0 AND expirationDate>'".date("Y-m-d H:i:s",time())."' AND realAmount>0 
        ORDER BY expirationDate ASC";
        $response = selectQuery($query);
        return $response;
    }

    function getLotesByProductIDOrderByRealAmount($productID){
        $query = "SELECT * FROM lotes WHERE productID=".$productID." AND hide=0 ORDER BY realAmount ASC";
        $response = selectQuery($query);
        return $response;
    }

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el loteo con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function loteIDExists($loteID){
        return exists("lotes","loteID",$loteID);
    }

    function loteCodeExists($loteCode){
        return exists("lotes","loteCode",$loteCode);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteLoteByID($loteID){
        return deleteSomethingByParameter("lotes","loteID",$loteID);
    }

    function deleteLoteByCode($loteCode){
        return deleteSomethingByParameter("lotes","loteCode",$loteCode);
    }
    
    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	//Actualiza los campos que le pasamos por un array en $data, del loteo que tiene el $loteID
    // TODO Ejemplo updateLoteByID(3,array('sectionID'=> '1','supplierID'=> '1','name'=>'Guantes de látex'))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateLoteByID($loteID,$data){
		return updateSomethingByID("lotes","loteID",$loteID,$data);
	}

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertLote (array('sectionID'=>1,'supplierID'=>1,'name'=>'Guantes de Latex','description'=>'Peligrosos','price'=>10,'photo'=>'No hay, me dan miedo'))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertLote($array){
        return insertSomething ("lotes",$array);
    }
?>