<?php
    include_once 'credentials.php';
    // *Conexiones básicas a la base de datos

    // *Constantes de la funcion checkInput

    /* enum Check {
        case LETTER;
        case NUMBER;
        case ALPHANUMERIC;
        case EMAIL;
        case PASS;
    } */
    define('CHECK__LETTERS', 0);
    define('CHECK__NUMBERS', 1);
    define('CHECK__ALPHANUMERIC', 2);
    define('CHECK__EMAIL', 3);
    define('CHECK__PASS', 4);
    define('CHECK__DATE', 5);
    define('CHECK__SQL', 6);
    define('CHECK__PHONE__NUMBER', 7);
    define('CHECK__LOTE__CODE', 8);
    define('CHECK__PRODUCT__CODE', 9);
    define('CHECK__PASS__LOGIN', 10);
    define('CHECK__DATE__BIRTH', 11);
    define('CHECK__EXPIRATION__DATE', 12);
    
    // Conexión a la base de datos con las credenciales previamente declaradas, devuelve el objeto mysqli de la base de datos
    function connectDB() {
        $db = new mysqli(HOST, NAME, PASS, DB);
        if ($db->connect_errno != 0) {
            echo $db->connect_error;
            return null;
        }
        return $db;
    }
    // Recibe una consulta selectQuery y la ejecuta en la base de datos
    // 1- Si la consulta devuelve al menos una fila, devuelve todas las filas, con array asociativo
    // 2- Si la consulta no devuelve ninguna fila, devuelve null
    function selectQuery($selectQuery) {
        $db = connectDB();
        $result = $db->query($selectQuery);
        if(!isset($result -> num_rows)){
            return false;
        }else{
            if($result -> num_rows>0){
                while ($buffer = $result->fetch_array(MYSQLI_ASSOC)) {
                    $resultArray[] = $buffer;
                }
                return $resultArray;
            }else{
                return null;
            }
        }
        
    }

    // Recibe una consulta insertQuery y la ejecuta en la base de datos
    // 1- Si la consulta inserta correctamente, devuelve true
    // 2- Si la consulta no inserta correctamente, devuelve el error por un string
    function insertQuery($insertQuery){
        $db=connectDB();
        $result = $db -> query($insertQuery);
        if(!$result){
            return "Error: ".$db -> error;
        }else{
            return true;
        }
    }

    // Recibe una consulta deleteQuery y la ejecuta en la base de datos
    // 1- Si la consulta borra correctamente, devuelve true
    // 2- Si la consulta no borra nada, devuelve false
    // 3- Si la consulta tiene errores, devuelve los errores en un string
    function deleteQuery($deleteQuery){
        $db=connectDB();
        $result = $db -> query($deleteQuery);
        if(!$result){
            return "Error: ".$db -> error;
        }else{
            if($db -> affected_rows==0){
                return false;
            }else{
                return true;
            }
        }
    }


    // Recibe una consulta updateQuery y la ejecuta en la base de datos
    // 1- Si la consulta actualiza algún campo, devuelve true
    // 2- Si la consulta no actualiza ningún campo, devuelve false
    // 3- Si la consulta tiene errores, devuelve los errores en un string
    function updateQuery($updateQuery){
        $db = connectDB();
        $result = $db -> query($updateQuery);
        //var_dump($updateQuery);
        if(!$result){
            return "Error: ".$db -> error;
        }else{
            if($db -> affected_rows==0){
                return false;
            }else{
                return true;
            }
        }
    }

    // Recibe el nombre de una tabla, junto con el nombre de una columna, y luego el valor que deberá buscar en esa columna.
    // 1- Devuelve el array asociativo de los registros que encuentre
    // 2- Devuelve NULL si existe la tabla y la columna pero no el valor pedido
    // 3- Devuelve false si no existe la tabla y/o la columna
    function getSomethingByParameter(string $tableName, string $paramName, $param) {
        $query = 'SELECT * FROM '.$tableName.' WHERE '.$paramName.' = "'.$param.'"';
        $response = selectQuery($query);
        return $response;
    }

    function getSomething(string $tableName){
        $query = 'SELECT * FROM '.$tableName;
        $response = selectQuery($query);
        return $response;
    }

    function getSomethingByText(string $tableName, string $paramName,string $text){
        $text=str_replace(" ","%",$text);
        $text=str_replace("-","%",$text);
        $text=str_replace("_","%",$text);
        $text=str_replace(".","%",$text);
        $text=str_replace(",","%",$text);
        $text=str_replace("/","%",$text);
        $text=str_replace('"',"%",$text);
        $query = 'SELECT * FROM '.$tableName.' WHERE '.$paramName.' LIKE "%'.$text.'%"';
        $response = selectQuery($query);
        return $response;
    }

    function countSomethingByText(string $tableName, string $paramName,string $text){
        $text=str_replace(" ","%",$text);
        $text=str_replace("-","%",$text);
        $text=str_replace("_","%",$text);
        $text=str_replace(".","%",$text);
        $text=str_replace(",","%",$text);
        $text=str_replace("/","%",$text);
        $text=str_replace('"',"%",$text);
        $query = 'SELECT COUNT(*) FROM '.$tableName.' WHERE '.$paramName.' LIKE "%'.$text.'%"';
        $response = selectQuery($query);
        return $response[0]["COUNT(*)"];
    }

    function getSomethingByTextLimit(string $tableName, string $paramName,string $text,int $limit){
        $text=str_replace(" ","%",$text);
        $text=str_replace("-","%",$text);
        $text=str_replace("_","%",$text);
        $text=str_replace(".","%",$text);
        $text=str_replace(",","%",$text);
        $text=str_replace("/","%",$text);
        $text=str_replace('"',"%",$text);
        $query = 'SELECT * FROM '.$tableName.' WHERE '.$paramName.' LIKE "%'.$text.'%" LIMIT'.$limit;
        $response = selectQuery($query);
        return $response;
    }

    // Recibe el nombre de una tabla, junto con el nombre de una columna, y luego el valor que deberá buscar en esa columna.
    // 1- Devuelve true si existe la tabla, la columna y el valor pedido.
    // 2- Devuelve false si no existe la tabla y/o la columna, o no existe el valor pedido
    function exists(string $tableName, string $paramName, $param){
        $query = 'SELECT * FROM '.$tableName.' WHERE '.$paramName.' = "'.$param.'"';
        $response = selectQuery($query);
        if ($response == NULL || $response == false){
            return false;
        }else{
            return true;
        }
    }

    // Recibe el nombre de una tabla, junto con el nombre de una columna y el valor que deberá buscar en esa columna.
    // 1- Devuelve true si el/los registros se borran correctamente
    // 2- Devuelve false si la consulta no borra nada
    // 3- Devuelve un string si explotó y la consulta está mal hecha (EL STRING QUE DEVUELVE ES EL ERROR EN LA CONSULTA)
    function deleteSomethingByParameter(string $tableName, string $paramName,$param){
        $query = "DELETE FROM ".$tableName." WHERE ".$paramName." = '".$param."'";
        $response = deleteQuery($query);
        return $response;
    }

    
    // Recibe el nombre de la tabla, el nombre de la columna ID de la tabla (tiene que ser esa no jodan), el id que tiene que coincidir
    // y un array con la data que vamos a cambiar 
    // TODO Ejemplo updateSomethingByID('users','userID',16,array('firstName'=> 'Pepito','lastName'=> 'Sanchez','phoneNumber'=>1165123843))
    // 1- Devuelve true si la consulta modificó algún registro
    // 2- Devuelve false si no modificó ningún registro
    // 3- Devuelve un string si explotó y la consulta está mal hecha (EL STRING QUE DEVUELVE ES EL ERROR EN LA CONSULTA) 
    function updateSomethingByID(string $table, string $idName, $id, $data) {
        $dataKeys = array_keys($data);
        $updateQuery="UPDATE ".$table." SET ";
        for ($i=0; $i < sizeof($data); $i++) {
            if (is_numeric($data[$dataKeys[$i]]) or is_null($data[$dataKeys[$i]])) {
                $updateQuery.=$dataKeys[$i]." = ".$data[$dataKeys[$i]].",";
            }else {
               $updateQuery.=$dataKeys[$i]." = '".$data[$dataKeys[$i]]."',";
            }
        }
        $updateQuery=trim($updateQuery,',');
        if (is_numeric($id)){
            $updateQuery.=" WHERE ".$idName." = ".$id;
        }else{
            $updateQuery.=" WHERE ".$idName." = '".$id."'";
        }
        $response = updateQuery($updateQuery);
        return $response;
    }

    // Recibe el nombre de la tabla y un array con los datos que vamos a insertar
    // TODO Ejemplo insertSomething("productos",array('sectionID'=>1,'supplierID'=>1,'name'=>'Guantes de Latex','description'=>'Peligrosos','price'=>10,'photo'=>'No hay, me dan miedo'));
    // 1- Devuelve true si la consulta ingresa correctamente
    // 2- Si la consulta no ingresa correctamente, devuelve el error en un string
    function insertSomething(string $table, $data){
        $dataKeys = array_keys($data);
        $insertQuery = "INSERT INTO ".$table."(";
        for($i=0; $i < sizeof($data); $i++){
            $insertQuery.=$dataKeys[$i].",";
        }
        $insertQuery=trim($insertQuery,',');
        $insertQuery.=") VALUES (";
        for($i=0; $i < sizeof($data); $i++){
            $insertQuery.="'".$data[$dataKeys[$i]]."',";
        }
        $insertQuery=trim($insertQuery,',');
        $insertQuery.=")";
        //!---- quizas necesite descomentarlo var_dump($insertQuery);
        $response = insertQuery($insertQuery);
        return $response;
    }

    // Recibe un string y un caso 
    // ? htmlspecialchars
    // ? addslashes
    // ? caso = CHECK__LETTERS Revisa que un string solo posea letras
    // ? caso = CHECK__NUMBERS Revisa que un string solo posea numeros
    // ? caso = CHECK__ALPHANUMERIC Revisa que un string solo solo sea alfanumérico
    // ? caso = CHECK__EMAIL Revisa que un string sea un email
    // ? caso = CHECK__PASS Revisa que un string sea una password
    // Devuelve true si el string es válido
    function checkInput(string $string, int $case) {
        switch($case){
            case CHECK__LETTERS:
                $response = preg_match("/[^a-zA-ZáéíóúüñÁÉÍÓÚÜÑ\s]/", $string);
                return $response != 1;
            break;
            case CHECK__NUMBERS:
                $response = preg_match("/[^0-9]/", $string);
                return $response != 1;
            break;
            case CHECK__ALPHANUMERIC:
                $response = preg_match("/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ\s]/", $string);
                return $response != 1;
            break;
            case CHECK__EMAIL:
                $response = preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,10})$/', strtolower($string));
                return $response == 1;
            break;
            case CHECK__PASS:
                $response = preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/', $string);
                return $response == 1;
            break;
            case CHECK__DATE:
                $response = preg_match('/(19[5-9][0-9]|20[0-9][0-9])[\/-](0[1-9]|1[0-2])[\/-](0[1-9]|1[0-9]|2[0-9]|3[01])/', $string);
                return $response == 1;
            break;
            case CHECK__SQL:

            break;
            case CHECK__PHONE__NUMBER:
                $response = preg_match("/[^0-9\s-]/", $string);
                return $response != 1;
            break;
            case CHECK__LOTE__CODE:
                $response = preg_match("/([L]|[l])[0-9][0-9][0-9][0-9][0-9][0-9]/", $string);
                return $response == 1;
            break;
            case CHECK__PRODUCT__CODE:
                $response = preg_match("/[A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/", $string);
                return $response == 1;
            break;
            case CHECK__PASS__LOGIN:
                if(!preg_match("/[']/", $string)){
                    $response = true;
                }else{
                    $response = false;
                }
                if(!$response){
                    return $response;
                }
                if(!preg_match('/["]/', $string)){
                    $response = true;
                }else{
                    $response = false;
                } 
                return $response;
            break;
            case CHECK__DATE__BIRTH:
                if(!checkInput($string,CHECK__DATE)){
                    return false;
                }
                $date = new DateTime($string);
                //$date = explode(" ", $string);
                $currentDate = new DateTime(date("Y-m-d"));
                $age = $currentDate->diff($date);
                $age->format("%y");
                return $age -> y >= 18;
            break;
            case CHECK__EXPIRATION__DATE:
                if(!checkInput($string,CHECK__DATE)){
                    return false;
                }
                $date = new DateTime($string);
                //$date = explode(" ", $string);
                $currentDate = new DateTime(date("Y-m-d"));
                $age = $currentDate->diff($date);
                $age->format("%y");
                var_dump($age);
                return $age -> d >= 1;
            break;
            default:
                // ! Deberia ir algun tipo de error indicando que la opcion no es valida
                return false;
            break;
        }
    }

    //var_dump(checkInput("2022-12-12",CHECK__EXPIRATION__DATE));

    // todo Manu me troleo
    /*
    function getDirectoryContent($ruta){
        // Se comprueba que realmente sea la ruta de un directorio
        if (is_dir($ruta)){
            // Abre un gestor de directorios para la ruta indicada
            $gestor = opendir($ruta);
            echo "<ul>";
    
            // Recorre todos los elementos del directorio
            while (($archivo = readdir($gestor)) !== false)  {
                    
                $ruta_completa = $ruta . "/" . $archivo;
    
                // Se muestran todos los archivos y carpetas excepto "." y ".."
                if ($archivo != "." && $archivo != "..") {
                    // Si es un directorio se recorre recursivamente
                    if (is_dir($ruta_completa)) {
                        echo "<li>" . $archivo . "</li>";
                        getDirectoryContent($ruta_completa);
                    } else {
                        echo "<li>" . $archivo . "</li>";
                    }
                }
            }
            
            // Cierra el gestor de directorios
            closedir($gestor);
            echo "</ul>";
        } else {
            echo "No es una ruta de directorio valida<br/>";
        }
    }

    getDirectoryContent("../img");*/

    function fileExists($url){
        $dir="/home/u812890733/domains/escuelarobertoarlt.com.ar/public_html/eira/";
        //var_dump($dir.$url);
        return file_exists($dir.$url);
    }

    //var_dump(fileExists("img/userphotos/73.jpeg"));
    //echo __DIR__;

    // todo date_default_timezone_set('America/Argentina/Buenos_Aires');
    // todo echo date("Y-m-d H:i:s",time());
?> 