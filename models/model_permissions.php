<?php
    include_once 'connections.php';

    function getPharmacistPermissionFields() {
	    return array(
	        'orders' => 1,
	        'transfers' => 2,
	        'statistics' => 4,
	        'products' => 8
	    );
	}

    //* FUNCIONES SELECT - DEVOLUCIÓN DE DATOS A TRAVÉS DE PARÁMETROS

    // 1- Retorna el permiso que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no hay permiso con ese ID
    function getPermissionByID($permissionID){
        return getSomethingByParameter("permissions","permissionID",$permissionID)[0];
    }

    function getEncargadoPermissions(){
        $query = "SELECT * FROM permissions WHERE permissionID>3";
        $response = selectQuery($query);
        return $response;
    }

    function getSectorByPermissionID($permissionID){
        return getSomethingByParameter("sectorpermissions","permissionID",$permissionID)[0]["sectorID"];
    }
    
    //* FUNCION PHARMACIST PERMISSION - DEVUELVE LOS PERMISOS DEL FARMACEUTICO
    
    // 1- Retorna false si el usuario no existe o no tiene los permisos de farmaceutico.
    // 2- Retorna un array con los sub-permisos del usuario.
    function getPharmacistPermissionByID($userID, $permission){
        $permissionArray = array();
        $fields = getPharmacistPermissionFields();

        // Retorna false si el usuario no existe o no tiene los permisos de farmaceutico.
        if(userIDExists($userID)){
            if(getUserByID($userID)["permissionID"]!=1){
                return false;
            }
        } else{
            return false;
        }

        // Descarta la posibilidad de que tenga un solo permiso
        foreach($fields as $permissionName => $value){
            if($permission == $value){
                return array($permissionName);
            }
        }

        // ajá hace cositas.
        $permissionBackup = $permission;
        while($permission>0){
            $permission = $permissionBackup;
            for($x = 0; $x < count($fields); $x++){
                foreach($fields as $permissionName => $value){
                    $permission = $permission - $value;
                    $permissionArray[$permissionName];
                    if($permission < 0){
                        $permission = $permissionBackup;
                        unset($permissionArray);
                    } else if($permission == 0){
                        return $permissionArray;
                    }
                }
            }
        }
    }
?>