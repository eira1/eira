<?php

	// ! Fausto, hay dos funciones nuevas al final con lindos comentarios :).
	// ? Mi alias de mercado pago es 'lordpantuflas' *guiño *guiño.

	include_once 'connections.php';
	// * Funciones que retornan arrays con campos 
	// Devuelve todos los campos necesarios para que un usuario se registre
	function getUserRequiredFields() {
	    return array(
	        'firstName',
	        'lastName',
	        'email',
	        'pass',
	        'repass',
			'permissionID'
	    );
	}

	// Devuelve todos los campos que el usuario puede modificar
	function getUserEditableFields() {
	    return array(
	        'dni',
	        'firstName',
	        'lastName',
	        'medicalLicense',
	        'phoneNumber',
	        'birthDate',
	        /* 'pass', */
	        'photo'
	    );
	}

	// Devuelve todos los campos de la tabla usuarios
	function getUserFields() {
	    return array(
	        'userID',
	        'dni',
	        'firstName',
	        'lastName',
	        'medicalLicense',
	        'phoneNumber',
	        'email',
	       	'birthDate',
	        'permissionID',
	        'pass',
	        'active',
	        'activationCode',
	        'activationExpiry',
	        'activationTime',
	        'creationDate',
	        'photo'
	    );
	}

	function getUserFieldsTranslations() {
	    return array(
	        'userID' => "ID",
	        'dni'=> "DNI",
	        'firstName'=> "nombre",
	        'lastName' => "apellido",
	        'medicalLicense' => "matrícula médica",
	        'phoneNumber' => "número de télefono",
	        'email' => "email",
	       	'birthDate' => "fecha de nacimiento",
	        'permissionID' => "ID de permiso",
	        'pass' => "contraseña",
	        'active' => "activo",
	        'activationCode' => "código de activación",
	        'activationExpiry' => "expiración de activación",
	        'activationTime' => "tiempo de activación",
	        'creationDate' => "fecha de creación",
	        'photo' => "foto de perfil"
	    );
	}

	// * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

	// 1- Retorna el usuario que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
	function getUserByID($userID){
		return getSomethingByParameter("users","userID",$userID)[0];
	}

	// 1- Retorna el usuario que posee el token pasado como parámetro si existe
	// 2- Retorna NULL si no existe
	function getUserByToken($token){
		return getSomethingByParameter("users","token",$token)[0];
	}
	function getUserIDByToken($token){
		return getSomethingByParameter("users","token",$token)[0]["userID"];
	}

	// 1- Retorna el usuario que posee el DNI pasado como parámetro si existe
	// 2- Retorna NULL si no existe
	function getUserByDNI($dni){
		return getSomethingByParameter("users","dni",$dni)[0];
	}
	
	// 1- Retorna el usuario que posee la matricula pasada como parámetro si existe
	// 2- Retorna NULL si no existe
	function getUserByMedicalLicense($medicalLicense){
		return getSomethingByParameter("users","medicalLicense",$medicalLicense)[0];
	}

	// 1- Retorna el usuario que posee el número de teléfono pasado como parámetro si existe
	// 2- Retorna NULL si no existe
	function getUserByPhoneNumber($phoneNumber){
		return getSomethingByParameter("users","phoneNumber",$phoneNumber)[0];
	}

	// 1- Retorna el usuario que posee el email pasado como parámetro si existe
	// 2- Retorna NULL si no existe
	function getUserByEmail($email){
		return getSomethingByParameter("users","email",$email)[0];
	}

	// 1- Retorna los datos que el usuario puede modificar en la pantalla PROFILE
	// 2- Retorno NULL si no existe
	function getProfileByEmail($email){
		$userData = getUserByEmail($email);
		$editableFields = getUserEditableFields();
		foreach($editableFields as $key => $value){
			$response[$key] = $userData[$value]; 
		}
		return $response;
	}

	// 1- Devuelve 1 si el usuario es un usuario común sin privilegios
	// 2- Devuelve 2 si el usuario es un administrador
	// 3- Devuelve 3 si el usuario es un superadministrador
	// 4- Devuelve 0 si el usuario no tiene definidos los permisos, lo cual no debería ocurrir
	function getUserPermissionsByID($userID){
		return getSomethingByParameter("users","userID",$userID)[0]["permissionID"];
	}

	function getAllUsers(){
        $query = 'SELECT * FROM `users`';
        $response = selectQuery($query);
        return $response;
    }

	function getVerifiedUsers(){
		$query = 'SELECT * FROM `users` WHERE verified=1';
        $response = selectQuery($query);
        return $response;
	}
	function getUnverifiedUsers(){
		$query = 'SELECT `userID`, `dni`, `firstName`, `lastName`, `medicalLicense`, `phoneNumber`, `email`, `birthDate`, users.`permissionID`, `active`, `verified`, `activationCode`, `activationExpiry`, `activationTime`, `creationDate`, `photo`, `token`,permissions.name AS permissionName 
		FROM `users` 
		INNER JOIN permissions ON users.permissionID=permissions.permissionID 
		WHERE verified=0
		ORDER BY creationDate DESC';
        $response = selectQuery($query);
        return $response;
	}

	function getUserModificationCodeByEmail($email){
		$query='SELECT users.modificationCode FROM `users` WHERE email="'.$email.'";';
		$response = selectQuery($query);
		return $response[0]["modificationCode"];
	}
	//var_dump(getUserModificationCodeByEmail("manu44rafa44@gmail.com"));

	//* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el usuario con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

	function userIDExists($userID) {
		return exists("users","userID",$userID);
	}

	function userDNIexists($dni){
		return exists("users","dni",$dni);
	}

	function userMedicalLicenseExists($medicalLicense){
		return exists("users","medicalLicense",$medicalLicense);
	}

	function userPhoneNumberExists($phoneNumber){
		return exists("users","phoneNumber",$phoneNumber);
	}
	
	function userEmailExists($email){
		return exists("users","email",$email);
	}

	function userPhotoExists($photo){
		return exists("users","photo",$photo);
	}

	function userTokenExists($token){
		return exists("users","token",$token);
	}

	function userEmailExistsByToken($token){
		$query = "SELECT email FROM users WHERE token=".$token;
		$array = selectQuery($query);
		if($array == NULL || $array=false){
			return false;
		}else{
			return true;
		}
	}

	//* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

	function deleteUserByID($userID){
		return deleteSomethingByParameter("users","userID",$userID);
	}
	
	function deleteUserByDNI($dni){
		return deleteSomethingByParameter("users","dni",$dni);
	}

	function deleteUserByMedicalLicense($medicalLicense){
		return deleteSomethingByParameter("users","medicalLicense",$medicalLicense);
	}

	function deleteUserByPhoneNumber($phoneNumber){
		return deleteSomethingByParameter("users","phoneNumber",$phoneNumber);
	}

	function deleteUserByEmail($email){
		return deleteSomethingByParameter("users","email",$email);
	}

	function deleteUserByToken($token){
		return deleteSomethingByParameter("users","token",$token);
	}

	//*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	//Actualiza los campos que le pasamos por un array en $data, del usuario que tiene el $userID
	// TODO Ejemplo updateUserByID(16,array('firstName'=> 'Pepito','lastName'=> 'Sanchez','phoneNumber'=>1165123843))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR) 
	function updateUserByID($userID,$data){
		return updateSomethingByID("users","userID",$userID,$data);
	}

	function updateUserByToken($token,$data){
		return updateSomethingByID("users","token",$token,$data);
	}

	function updateUserByEmail($email,$data){
		return updateSomethingByID("users","email",$email,$data);
	}

	function updatePassByUserID($userID,string $pass){
		return updateSomethingByID("users","userID",$userID,array("pass"=>password_hash($pass,PASSWORD_DEFAULT)));
	}
	
	
	function updatePassByToken(string $token,string $pass){
		return updateSomethingByID("users","token",$token,array("pass"=>password_hash($pass,PASSWORD_DEFAULT)));
	}

	function updatePassByEmail(string $email,string $pass){
		return updateSomethingByID("users","email",$email,array("pass"=>password_hash($pass,PASSWORD_DEFAULT)));
	}

	//Cambia el campo 'active' del usuario a 1
	function activateUserByID($userID) {
		return updateSomethingByID('users', 'userID', $userID, array('active' => 1));
	}
	
	//Cambia el campo 'verified' del usuario a 1
	function verifyUserByID($userID){
		return updateSomethingByID('users', 'userID', $userID, array('verified' => 1));
	}
	//Cambia el campo 'verified' del usuario a 0
	function unverifyUserByID($userID){
		return updateSomethingByID('users', 'userID', $userID, array('verified' => 0));
	}
	
	//* FUNCIONES INSERT - REGISTRO DE USUARIOS

	//=== Registro ===============================================================================================================
	// ! --------------------------------------------
	// ! A PARTIR DE ACÁ TODO COPIADO DE MANU
	// ! --------------------------------------------
	// Genera un codigo de activación random
	function generateActivationCode() {
		return bin2hex(random_bytes(16));
	}

	// Envía un email para validar la cuenta de un usuario recién registrado.
	// Redirige a 'email_verify.php', y le manda el email y código por GET.
	//  TODO: Aplicarle estilos y cambiar un poco el texto porque está refeo.
	// ! Mega copiado de lo que hizo ManuRafaGod, todavía no cambiado
	function sendActivationEmail(string $email, string $code) {
		
		$msg = file_get_contents('../views/validation_email.php');//!CHEQUEAR DESPUES DE QUE MANU TERMINE LA PARTE ESA(JS)
		$msg = str_replace("{{EMAIL}}",$email,$msg);
		$msg = str_replace("{{ACTIVATION_CODE}}",$code,$msg);
		$msg = wordwrap($msg, 70, "\n\r",true);
		// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Cabeceras adicionales
		$headers .= 'From: Eira <eira@escuelarobertoarlt.com.ar>' . "\r\n";

		return true;
		return mail($email, "Verificación de cuenta", $msg, $headers);
	}

	/* sendActivationEmail("sexoentreperuanos@gmail.com",generateActivationCode()); */

	// Registra al usuario.
	// Si no encuentra alguno de los campos requeridos, cancela el registro y devuelve false.
	// Cuando termina, envía un mail de verificación.
	// ! Falta testear
	// ? Sería buena idea separar esta función en varias más cortitas? + edit: Será analizado
	// ! También mega copiado de Manu Rafa, lo chequeo la próxima
	// * Faus: Devuelve 0 si faltan campos, 1 si se registra bien y 2 si ya existe el mail

	function createUser($data) {
		if(strlen($data['pass'])<8){
			return 0;
		}
		if($data['pass']!=$data['repass']){
			return 0;
		}
		$data['pass'] = password_hash($data['pass'], PASSWORD_DEFAULT);
		$reqData = getUserRequiredFields();
		for ($i=0; $i < sizeof($reqData); $i++) {
			if (! key_exists($reqData[$i], $data) and $reqData[$i] != 'repass') {
				return 0;
			}
		}
		if ($data['email']==""){
			return 0;
		}else if($data['pass']==""){
			return 0;
		}else if($data['repass']==""){
			return 0;
		}else if(userEmailExists($data['email'])){
			return 2;
		}else{
			$query = "INSERT INTO users (firstName, lastName, email, pass,permissionID, activationCode, activationExpiry, active) VALUES (";
			for ($i=0; $i < sizeof($reqData); $i++) {
				if ($reqData[$i] != 'repass') {
					if (is_numeric($data[$reqData[$i]])) {
						$query .= $data[$reqData[$i]] . ", ";
					}else {
						$query .= "'" . $data[$reqData[$i]] . "', ";
					}
				}
			}
			$activationCode = generateActivationCode();
			$hashCode = password_hash($activationCode, PASSWORD_DEFAULT);
			$query .= "'" . $hashCode . "', ";
			$activationExpiry = selectQuery("SELECT GET_ACTIVATION_EXPIRY()")[0]['GET_ACTIVATION_EXPIRY()'];
			$query .= "'" . $activationExpiry . "', ";
			$query .= "1)";
			if (sendActivationEmail($data['email'], $activationCode)){
				insertQuery($query);
				return 1;
			}else{
				return 3;
			}
			$editableFields = getUserEditableFields();
			/* for ($i=0; $i < sizeof($editableFields); $i++) { 
				if ( ! in_array($editableFields[$i], $reqData)) {
					$additionalData[$editableFields[$i]] = $data[$editableFields[$i]];
				}
			} */
			if (isset($additionalData)) {
				updateQuery('users', 'userID', $data['userID'], $additionalData);
			}
			
		}
	}

	function setCreationDate(string $email,$creationDate){
		$userID = getUserByEmail($email)["userID"];
		updateUserByID($userID,array("creationDate"=>$creationDate));
	}

	/**
	 ** Trae el ID y código de activación de un usuario.
	* 
	* Si ya expiró su verificación, lo elimina de la base y devuelve null.
	* Retorna null si el código que recibe por parámetro no coincide con el del usuario.
	*/
	function findUnverifiedUser(string $email, string $code) {
		$userData = selectQuery("SELECT userID, activationCode, activationExpiry < NOW() AS expired FROM users WHERE active = 0 AND email = '" . $email . "'")[0];

		if ($userData) {
			if ((int) $userData['expired'] === 1) {
				deleteUserById($userData['userID']);
				return null;
			}            
			if (password_verify($code, $userData['activationCode'])) {
				return $userData;
			}
			return null;
		}
	}

	// * FUNCIONES PARA EL LOGIN

	// TODO Que retorne valores numéricos o algo,
	// TODO y que la API se encarge de los JSON
	// 1- Retorna true si el login es válido
	// 2- Retorna false si el login es inválido
	// ? Probablemente podría ampliarse para tirar alertas personalizadas
	function loginUser($email, $pass){
		if(!userEmailExists($email)){
			return 4;
		}else{
			$user = getUserByEmail($email);
			if($user["active"]==0){
				return 3;
			}else if($user["verified"]==0){
				return 2;
			}else{
				if(!password_verify($pass,$user['pass'])){
					return 1;
				}else{
					return 0;
				}
			}
		}
	}




	// ! Función hecha por los homies esperando a que Fausto haga una
	// ! de verdad XD.
	// ! Después que Fausto la haga un poquito más segura.

	function generateToken(string $userEmail, string $userID) {
		$rawToken = $userEmail . date('Ymdhis');
		return md5($rawToken) . md5($userID);
	}


	// ! Esta también, creo que faltan verificaciones y cosas así,
	// ! pero yo soy de frontend, así que no tengo idea de qué
	// ! estoy haciendo XD.

	function updateTokenByEmail(string $userEmail) {
		$userID = getUserByEmail($userEmail)['userID'];
		$newToken = generateToken($userEmail, $userID);

		$data = array('token' => $newToken);

		updateUserByID($userID, $data);
		return $newToken;
	}



	function getTokenByEmail(string $userEmail) {
		return getUserByEmail($userEmail)['token'];
	}

	function sendModificationMail(string $userEmail){
		$mail=file_get_contents("../views/change_pass_email.php");
		$mail=str_replace("{{EMAIL}}",$userEmail,$mail);
		$code = generateActivationCode();
		updateUserByEmail($userEmail,array("modificationCode"=>$code));
		$mail=str_replace("{{CODE}}",$code,$mail);
		return sendMail($userEmail,
		"Cambio de contraseña - Eira",
		$mail
	);
	}

	function sendMail($destinatario,$motivo,$contenido){
		include_once '../mp-mailer/sendmailNOTAPI.php';
		return $return;
	}

	// ? var_dump(mail("fausxdthree@gmail.com","Eira Test","Esto es un mensaje de prueba",'MIME-Version: 1.0 \r\n From: Eira <eira-no-reply@escuelarobertoarlt.com.ar> \r\n Content-type: text/html; charset=iso-8859-1'));
	//echo phpinfo();
?>