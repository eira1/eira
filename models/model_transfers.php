<?php
    include_once 'connections.php';
    include_once 'model_lotes.php';

    // * FUNCIONES GET - DEVOLUCIÓN DE REGISTROS A PARTIR DE PARAMETROS

    function getAllTransfers(){
        $query = "SELECT transfers.transferID,transfers.transferDate,sectors.name AS sector,transfers.userID 
        FROM transfers 
        INNER JOIN sectors ON transfers.sectorID = sectors.sectorID 
        ORDER BY transfers.transferDate DESC 
        LIMIT 10";
        $response = selectQuery($query);
        return array_reverse($response);
    }

    function getAllTransfersBySector($sectorID){
        $query = 'SELECT transfers.transferID,transfers.transferDate,sectors.name AS sector,transfers.userID 
        FROM transfers 
        INNER JOIN sectors ON transfers.sectorID = sectors.sectorID
        WHERE transfers.sectorID='.$sectorID.' 
        ORDER BY transfers.transferDate DESC 
        LIMIT 10';
        $response = selectQuery($query);
        return array_reverse($response);
    }

    function getTransferDetails($transferID){
        $query = "SELECT transferdetails.amount,products.name FROM transferdetails INNER JOIN products ON transferdetails.productID=products.productID WHERE transferdetails.transferID =".$transferID;
        //$query = "SELECT transferdetails.amount, lotes.loteCode,lotes.expirationDate,transferdetails.amount,transferdetails.supposedAmount,products.name FROM transferdetails LEFT JOIN lotes ON transferdetails.loteID = lotes.loteID INNER JOIN products ON lotes.productID=products.productID WHERE transferdetails.transferID =".$transferID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }

    function getTransferLotes($transferID){
        $query = "SELECT lotes.loteCode,lotes.expirationDate,lotes.totalAmount,transferlotes.amount,lotes.realAmount,products.name,forms.name AS form,products.concentration,products.code FROM transferlotes INNER JOIN lotes ON transferlotes.loteID=lotes.loteID INNER JOIN products ON lotes.productID=products.productID INNER JOIN forms ON products.formID=forms.formID WHERE transferlotes.transferID=".$transferID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }
        return $response;
    }


    function getUserTransferName($transferID){
        $query = "SELECT users.firstName,users.lastName FROM `transfers` INNER JOIN users ON transfers.userID=users.userID WHERE transferID=".$transferID;
        $response = selectQuery($query);
        if(is_null($response)){
            return array();
        }else{
            return $response[0];
        }
    }

    // 1- Retorna la orden que posee el ID pasado como parámetro si existe
	// 2- Retorna NULL si no existe
    function getTransferByID($transferID){
        return getSomethingByParameter("transfers","transferID",$transferID)[0];
    }

    function getTransferSectorByID($transferID){
        $query = 'SELECT * 
        FROM transfers
        INNER JOIN sectors ON transfers.sectorID=sectors.sectorID
        WHERE transferID='.$transferID;
        $response = selectQuery($query);
        return $response;
    }


    // 1- Retorna la/s orden/es que efectuó el usuario pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getTransfersByUser($userID){
        return getSomethingByParameter("transfers","userID",$userID);
    }

    // 1- Retorna la/s orden/es que efectuó el sector pasado por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getTransfersBySector($sectorID){
        return getSomethingByParameter("transfers","sectorID",$sectorID);
    }

    // 1- Retorna la/s orden/es que fueron efectuadas el dia de la fecha pasada por paramétro si existe/n
	// 2- Retorna NULL si no existe
    function getTransfersByDate($transferDate){
        return getSomethingByParameter("transfers","transferDate",$transferDate);
    }

    // 1- Retorna el/los detalles de la orden que tiene el ID pasado como parámetro si existe/n
    // 2- Retorna NULL si no existe
    function getTransferDetailsByID($transferID){
        return getSomethingByParameter("transferdetails","transferID",$transferID)[0];
    }

    function getTransferMaxID(){
		$query = "SELECT MAX(transferID) FROM transfers;";
		$result = selectQuery($query);
		return $result[0]["MAX(transferID)"];
	}

    //* FUNCIONES EXIST - EXISTENCIA O NO DE CIERTOS REGISTROS
	//* 1- Retornan todas true si el usuario con el parámetro pasado existe
	//* 2- Retornan todas false si no existe

    function transferIDExists($transferID){
        return exists("transfers","transferID",$transferID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function transferUserExists($userID){
        return exists("transfers","userID",$userID);
    }

    // Aclaración: Devuelve true si el usuario con el ID que pasamos como parámetro efectuó al menos una orden,
    // caso contrario, devolverá false aunque el usuario si exista
    function transferSectorExists($sectorID){
        return exists("transfers","sectorID",$sectorID);
    }

    // Devuelve true si al menos una orden fue efectuada el dia que pasamos como parámetro,
    // caso contrario devuelve false
    function transferDateExists($transferDate){
        return exists("transfers","transferDate",$transferDate);
    }

    function transferDetailsExist($transferID){
        return exists("transferdetails","transferID",$transferID);
    }

    function transferLotesExist($transferID){
        return exists("transferlotes","transferID",$transferID);
    }

    //* FUNCIONES DELETE - ELIMINACIÓN DE DATOS
	//* 1- Retornan todas true si la consulta DELETE borra al menos un registro
	//* 2- Retornan todas false si no borra nada
	//* 3- Retornan todas un string si algó explotó (EL STRING ES EL ERROR)

    function deleteTransferByID($transferID){
        return deleteSomethingByParameter("transfers","transferID",$transferID);
    }

    function deleteTransfersByUser($userID){
        return deleteSomethingByParameter("transfers","userID",$userID);
    }

    function deleteTransfersBySector($sectorID){
        return deleteSomethingByParameter("transfers","sectorID",$sectorID);
    }

    function deleteTransfersByDate($transferDate){
        return deleteSomethingByParameter("transfers","transferDate",$transferDate);
    }

    function deleteTransferDetailsByTransferID($transferID){
        return deleteSomethingByParameter("transferdetails","transferID",$transferID);
    }

    function deleteTransferDetailByID($transferDetailID){
        return deleteSomethingByParameter("transferdetails","transferDetailID",$transferDetailID);
    }

    function deleteTransferLotesByTransferID($transferID){
        return deleteSomethingByParameter("transferlotes","transferID",$transferID);
    }

    function deleteTransferLotesByID($transferLoteID){
        return deleteSomethingByParameter("transferlotes","transferLoteID",$transferLoteID);
    }

    //*FUNCIONES UPDATE - ACTUALIZACIÓN DE DATOS

	// Actualiza los campos que le pasamos por un array en $data, de la orden que tiene el $transferID
	// TODO Ejemplo updateTransferByID(1,array('userID'=> '16','sectorID'=>1))
	// 1- Retorna true si actualizó algo
	// 2- Retorna false si no actualizó nada
	// 3- Retorna un string con un error si algó explotó (ESE STRING TIENE EL ERROR)
	function updateTransferByID($transferID,$data){
		return updateSomethingByID("transfers","transferID",$transferID,$data);
	}

    function updateTransferDetailByID($transferDetailID,$data){
        return updateSomethingByID("transferdetails","transferDetailID",$transferDetailID,$data);
    }

    function updateTransferLoteByID($transferLoteID,$data){
        return updateSomethingByID("transferlotes","transferLoteID",$transferLoteID,$data);
    }

    // TODO Pendiente: function updateTransferDetailsByTransferID()

    //*FUNCIONES INSERT - INSERCIÓN DE DATOS
    
    // Le ingresas un array con los campos en los que va a ingresar algo y el contenido
    // que vas a ingresar. Ejemplo:
    // TODO insertTransfer(array('userID'=>16,'sectorID'=>1))
    // 1- Devuelve true si el insert se hace correctamente
    // 2- Devuelve un string con el error si justamente hay algun error
    function insertTransfer($array){
        return insertSomething ("transfers",$array);
    }

    function insertTransferDetail($array){
        return insertSomething("transferdetails",$array);
    }

    function insertTransferLote($array){
        return insertSomething("transferlotes",$array);
    }

    function duplicateProducts($array){
        $cant = count($array);
        for($x=0; $x<$cant ; $x++){
            for ($i=0; $i < $cant-1; $i++) { 
                if( ($x!=$i) && ($array[$x]->productID == $array[$i]->productID) ){
                    $array[$x]->amount = strval($array[$x]->amount + $array[$i]->amount);
                    unset($array[$i]);
                    $array = array_values($array);
                    $x = 0;
                    $i = 0;
                }
                $cant = count($array);
            }
        }
        return $array;
    }

    function selectLotesFromAmount($productID,$amount){
        $lotes = getNonExpiredLotesByProductIDOrderByExpirationDate($productID);
        for($i=0;$i<count($lotes);$i++){
            if($lotes[$i]["realAmount"]<$amount){
                $response[] = $lotes[$i];
                $amount = $amount - $lotes[$i]["realAmount"];
            }else{
                $response[] = $lotes[$i];
                break;
            }
        }
        return $response;
    }
    //var_dump(getLotesByProductIDOrderByExpirationDate(1));
    
    //? SI QUIEREN PROBAR LA FUNCION DESCOMENTEN LAS 3 LINEAS DE ABAJO.
    //? Se supone que el primer lote del product 1 tiene 777 de amount. Entonces si llamo la funcion con amount 776, solo me devuelve ese lote, ya que es el unico q necesito usar
    //? En cambio si llamo la funcion con 778, como no me alcanza solo ese lote, me devuelve tambien el siguiente en terminos de fecha de vencimiento
    //var_dump(selectLotesFromAmount(1,776));
    //echo("<br>---------------------------------------<br>");
    //var_dump(selectLotesFromAmount(1,778));


    function turnTransferDetailsOnTransferLotes($products){
        $cantProducts = count($products);
        for($x=0; $x<$cantProducts ; $x++) {
            $lotes[$x]=selectLotesFromAmount($products[$x]["productID"],$products[$x]["amount"]);
            $amount[$x]=$products[$x]["amount"];
        }
        //var_dump($lotes);
        //var_dump($amount);
        for($x=0;$x<$cantProducts;$x++){
            for($y=0;$y<count($lotes[$x]);$y++){
                if($products[$x]["productID"]=$lotes[$x][$y]["productID"]){
                    $preresponse[$x][$y]["loteID"]=$lotes[$x][$y]["loteID"];
                    if($amount[$x]>$lotes[$x][$y]["realAmount"]){
                        $preresponse[$x][$y]["amount"]=$lotes[$x][$y]["realAmount"];
                        $amount[$x]=$amount[$x]-$lotes[$x][$y]["realAmount"];
                    }else{
                        $preresponse[$x][$y]["amount"]=$amount[$x];
                    }
                }
            }
        }
        $c=0;
        for($a=0;$a<count($preresponse);$a++){
            for($b=0;$b<count($preresponse[$a]);$b++){
                $response[$c]["loteID"]=$preresponse[$a][$b]["loteID"];
                $response[$c]["amount"]=$preresponse[$a][$b]["amount"];
                $c++;
            }
        }
        return $response;
    }

    function getTransferPDF($transferID){
        require_once '../class/dom.php';
        $data_transfer = getTransferByID($transferID);
        //var_dump($data_transfer);
        $data_transferdetails = getTransferDetails($transferID);
        //var_dump($data_transferdetails);
        $data_transferlote = getTransferLotes($transferID);
        //var_dump($data_transferlote);
        $data_transferperson = getUserTransferName($transferID);
        //var_dump($data_transferperson);
        $data_transfersector = getTransferSectorByID($transferID);
        //var_dump($data_transfersector);
        createTransferDOMPDF($data_transfer,$data_transferdetails,$data_transferlote,$data_transferperson,$data_transfersector);
    }
    //getTransferPDF(433);
?>