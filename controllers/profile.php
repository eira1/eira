<?php
    /* session_start();
    if (!isset($_SESSION['email'])){
        // Si el usuario no está logeado lo llevo a landing
        header("Location: landing");
    }else{ */

        require_once "models/model_users.php";

        $view = file_get_contents("views/profile.php");
        /* $userData = getUserByEmail($_SESSION['email']);

        $view=fillInputs($userData,$view); */

        // A partir de aca todo el codigo si el usuario está logeado
        $view = str_replace('{{NAV}}',file_get_contents("views/logged_nav_bar.php"),$view);
        $view = str_replace('{{FOOTER}}',file_get_contents("views/footer.php"),$view);
        echo($view);
    //}  

    function updateProfile() {
        $userEditableFields = getUserEditableFields();
        for ($i=0; $i < sizeof($userEditableFields);$i++) {
            if (isset($_POST['input-' . $userEditableFields[$i]])) {
                $data[$userEditableFields[$i]] = $_POST['input-' . $userEditableFields[$i]];
            }
        }
        //var_dump($data);
    }

    function fillInputs($userData,$view){
        if($userData['dni']!=0){
            $view = str_replace('{{DNI_VALUE}}',$userData['dni'],$view);
        }else {
            $view = str_replace('{{DNI_VALUE}}',"",$view);
        }

        if($userData['firstName']!=""){
            $view = str_replace('{{FIRST_NAME_VALUE}}',$userData['firstName'],$view);
        }else {
            $view = str_replace('{{FIRST_NAME_VALUE}}',"",$view);
        }

        if($userData['lastName']!=""){
            $view = str_replace('{{LAST_NAME_VALUE}}',$userData['lastName'],$view);
        }else {
            $view = str_replace('{{LAST_NAME_VALUE}}',"",$view);
        }

        if($userData['medicalLicense']!=0){
            $view = str_replace('{{MEDICAL_LICENSE_VALUE}}',$userData['medicalLicense'],$view);
        }else {
            $view = str_replace('{{MEDICAL_LICENSE_VALUE}}',"",$view);
        }

        if($userData['phoneNumber']!=0){
            $view = str_replace('{{PHONE_NUMBER_VALUE}}',$userData['phoneNumber'],$view);
        }else {
            $view = str_replace('{{PHONE_NUMBER_VALUE}}',"",$view);
        }

        if($userData['birthDate']!=""){
            $view = str_replace('{{BIRTH_DATE_VALUE}}',$userData['birthDate'],$view);
        }else {
            $view = str_replace('{{BIRTH_DATE_VALUE}}',"",$view);
        }

        return $view;
    }
?>