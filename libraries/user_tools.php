<?php

include_once 'db.php';


//=== Búsqueda ===============================================================================================================


/**
 ** Emm... Trae los datos del usuario... basándose en el userID.
 */
function getUserById($id) {
    return getSmthByParam('users', 'userID', $id);
}



/**
 ** OBVIAMENTE trae los datos del usuario basándose en el email.
 */
function getUserByEmail($email) {
    return getSmthByParam('users', 'userID', $email);
}



/**
 ** Devuelve si el email está registrado.
 */
function emailExists($email) {
    if (! getUserByEmail($email)) {
        return false;
    }
    else {
        return true;
    }
}



/**
 ** Trae el ID y código de activación de un usuario.
 * 
 * Si ya expiró su verificación, lo elimina de la base y devuelve null.
 * Retorna null si el código que recibe por parámetro no coincide con el del usuario.
 */
function findUnverifiedUser(string $email, string $code) {
    $userData = queryDB("SELECT userID, activationCode, activationExpiry < NOW() AS expired FROM users WHERE active = 0 AND email = '" . $email . "'")[0];

    if ($userData) {
        if ((int) $userData['expired'] === 1) {
            deleteUserById($userData['userID']);
            return null;
        }            
        if (password_verify($code, $userData['activationCode'])) {
            return $userData;
        }
        return null;
    }
}


//=== Misceláneos ============================================================================================================


/**
 ** Le pasás un ID, borra ese usuario.
 */
function deleteUserById($id) {
    queryDB("DELETE FROM users WHERE userID=" . $id);
}



/**
 ** Cambia el campo 'active' de un usuario a 1.
 */
function activateUser($id) {
    updateDB('users', 'userID', $id, array('active' => 1));
}


//=== Registro ===============================================================================================================


/**
 ** Registra al usuario.
 * 
 * Si no encuentra alguno de los campos requeridos, cancela el registro y devuelve false.
 * 
 * Cuando termina, envía un mail de verificación.
 * 
 * ! Falta testear
 * 
 * ? Sería buena idea separar esta función en varias más cortitas?
 */
function createUser($data) {
    $data['dni'] = intval($data['dni']);
    $data['pass'] = password_hash($data['pass']);
    $reqData = getUserRequiredFields();

    for ($i=0; $i < sizeof($reqData); $i++) {
        if (! key_exists($reqData[$i], $data) and $reqData[$i] != 'repass') {
            return false;
        }
    }

    $query = "INSERT INTO users (dni, firstName, lastName, email, pass, activationCode, activationExpiry) VALUES (";
    for ($i=0; $i < sizeof($reqData); $i++) {
        if ($reqData[$i] != 'repass') {
            if (is_numeric($data[$reqData[$i]])) {
                $query .= $data[$reqData[$i]] . ", ";
            }
            else {
                $query .= "'" . $data[$reqData[$i]] . "', ";
            }
            
        }
    }


    $activationCode = generateActivationCode();
    $hashCode = password_hash($activationCode, PASSWORD_DEFAULT);
    
    $query .= "'" . $hashCode . "', ";

    $activationExpiry = queryDB("SELECT GET_ACTIVATION_EXPIRY()")[0]['GET_ACTIVATION_EXPIRY()'];
    $query .= "'" . $activationExpiry . "')";

    queryDB($query);

    
    $editableFields = getUserEditableFields();
    for ($i=0; $i < sizeof($editableFields); $i++) { 
        if ( ! in_array($editableFields[$i], $reqData)) {
            $additionalData[$editableFields[$i]] = $data[$editableFields[$i]];
        }
    }

    if (isset($additionalData)) {
        updateDB('users', 'userID', $data['userID'], $additionalData);
    }


    sendActivationEmail($data['email'], $activationCode);

}



/**
 ** Envía un email para validar la cuenta de un usuario recién registrado.
 * 
 * Redirige a 'email_verify.php', y le manda el email y código por GET.
 * 
 * TODO: Aplicarle estilos y cambiar un poco el texto porque está refeo.
 */
function sendActivationEmail(string $email, string $code) {
    $msg = '

    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Verificación de correo para Eira</title>
        <style>
            a {
                background-color: red;
            }
        </style>
    </head>
    <body>
        <h1>Verificación de correo para Eira</h1>
        <a href="https://escuelarobertoarlt.com.ar/eira/email_verify.php?email=' . $email . '&code='. $code .'">Verificar</a>
    </body>
    </html>
    
    ';

    $msg = wordwrap($msg, 70, "\n\r");
    // Para enviar un correo HTML, debe establecerse la cabecera Content-type
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Cabeceras adicionales
    $headers .= 'From: Eira <no-reply@email.com>' . "\r\n";

    mail($email, "Verificación de cuenta", $msg, $headers);
}



/**
 ** Genera un código de activación al azar.
 */
function generateActivationCode() {
    return bin2hex(random_bytes(16));
}

?>
