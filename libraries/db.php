<?php

include_once 'constants.php';

/**
 ** Básicamente funciones para usar SQL.
 */



/**
 ** Se conecta a la base de datos usando las constantes
 ** que declaramos al principio.
 *
 * Devuelve un objeto mysqli
 */
function connectDB() {
    $db = new mysqli(DB_SERVER_NAME, DB_USERNAME, DB_PASS, DB_NAME);
    if ($db->connect_errno != 0) {
        echo $db->connect_error;
        return null;
    }
    return $db;
}



/**
 ** Recibe una consulta $query y la ejecuta.
 * 
 * Si esta empieza con "SELECT", devuelve lo que se pide,
 * si no devuelve null.
 * 
 * En caso de que sea un SELECT que no encontró nada,
 * devuelve false.
*/
function queryDB($query) {
    $db = connectDB();
    
    if ($db == null) {
        return null;
    }

    $result = $db->query($query);

    if (stripos($query, "SELECT") != 0) {
        return null;
    }

    if (! $result->num_rows) {
        return false;
    }

    while ($buffer = $result->fetch_array(MYSQLI_ASSOC)) {
        $resArray[] = $buffer;
    }
    return $resArray;
}



/**
 ** Obviamente es para la sentencia UPDATE
 *
 * Recibe el nombre de la tabla, el nombre de la FK,
 * el valor de la FK de la fila que estamos modificando y
 * los nuevos valores para agregar (modifica solo los campos
 * que le mandemos).
 * 
 * TODO: Capaz que debería hacer alguna validación antes, no sé :p
 */
function updateDB(string $table, string $idName, $id, $data) {
    $dataKeys = array_keys($data);

    for ($i=0; $i < sizeof($data); $i++) {
        if (is_numeric($data[$dataKeys[$i]]) or is_null($data[$dataKeys[$i]])) {
            queryDB("UPDATE ".$table." SET ".$dataKeys[$i]." = ".$data[$dataKeys[$i]]." WHERE ".$idName." = ".$id);
        }
        else {
            queryDB("UPDATE ".$table." SET ".$dataKeys[$i]." = '".$data[$dataKeys[$i]]."' WHERE ".$idName." = ".$id);
        }
    }
} 


/**
 ** Hace una búsqueda en la DB del primer registro que coincida con los criterios.
 * 
 * Recibe el nombre de la tabla, qué parámetro busca (normalmente es el ID) y
 * el valor de ese parámetro.
 * 
 * Devuelve un array asociativo.
 */
function getSmthByParam(string $tableName, string $paramName, $param) {
    $query = "SELECT * FROM ' . $tableName . ' WHERE ' . $paramName . ' = '" . $param ."'";
    $res = queryDB($query);
    if (! $res) {
        return false;
    }
    else {
        return $res[0]; 
    }
}
?>