<?php

    /**
     * * file_get_contents() lee un archivo entero y lo pasa a String
     * (Para Mauri)
    */

    $route = (array_values(array_filter(explode("/", $_SERVER["REQUEST_URI"]))));


    if (isset($route[1])){
        if(file_exists('controllers/'.$route[1].'.php')){
            include 'controllers/'.$route[1].'.php';
        }else{
            include 'controllers/404.php';
        }
    }else{
        include 'controllers/landing.php';
    }

?>