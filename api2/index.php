<?php
	header("Content-type: application/json");
	$body=array();
	define("URL_BASE","/eira/api2/");
	$request=array_filter(explode("/",str_replace(URL_BASE,"",$_SERVER["REQUEST_URI"])));
	if(!count($request)){
		$body=array("errno"=>400,"error"=>"Falta modelo");
	}else{
		$model = "model_".strtolower($request[0]);
		if(!file_exists("../models/".$model.".php")){
			$body=array("errno"=>400,"error"=>"No existe modelo");
		}else{
			if(!isset($request[1])){
				$body=array("errno"=>400,"error"=>"Falta funcion");
			}else{
				include_once "../models/".$model.".php";
				$function=$request[1];
				if(!function_exists($function)){
					$body = array('errno'=>400,"error"=>"No existe funcion");
				}else{
                    if(!isset($request[2])){
                        $body=array("errno"=>400,"Falta parametro");
                    }else{
                        $param=$request[2];
                        $body=$function($param);
                    }
				}
			}
		}
	}
	echo json_encode($body);

	// getFunctionParams($function);

	/*header("Content-Type:Application/Json");
	header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Credentials:true");
	header("Access-Control-Allow-Methods:PUT,POST,DELETE,GET");

	$body = array();

	define("URL_BASE","/eira/api2/");

	$parameters = explode("/",str_replace(URL_BASE,"",$_SERVER['REQUEST_URI']));
	$parameters = array_filter($parameters);
	if(!count($parameters)){
		$body = array("errno" => 400, "error" => "no esta declarado el modelo");
	} else {
		$model = "../models/model_".strtolower($parameters[0]).".php";
		if(!file_exists($model)){
			$body = array("errno" => 400, "error" => "no existe el modelo");
		} else if(!isset($parameters[1])){
			$body = array("errno" => 400, "error" => "no esta declarado la funcion");
		} else {
			include $model;
			$function = $parameters[1];
			if(!function_exists($function)){
				$body = array("errno" => 400, "error" => "no existe la funcion");
			} else {
				switch($_SERVER['REQUEST_METHOD']){
					case 'GET':
						if(!isset($parameters[2])){
							if(strpos($function,"ByID")){
								$body = array("errno" => 400, "error" => "parametro no declarado");
							} else {
								$body = $function();
							}
						} else {
							$pass = "";
							$data = $parameters[2];
							$data = explode("&",$data);
							for($x = 0; $x < count($data); $x++){
								$valueExist = strpos($data[$x],"=");
								if($valueExist===false){
									$body = array("errno" => 400, "error" => $data[$x]." esta vacio");
									break;
								} else {
									$key = explode("=",$data[$x]);
									if($key[1]==""){
										$body = array("errno" => 400, "error" => $key[0]." esta vacio");
										break;
									} else {
										$pass = $pass.$key[1].",";
									}
								}
							}
							$pass = substr($pass, 0, -1);
							if (isset($body["errno"])) {
								if($body["errno"]==400){
									break;
								}
							}
							$body = $function($pass);
						}
					break;
					case 'POST':
					break;
					case 'PUT':
					break;
					case 'DELETE':
					break;
					default:
						$body = array("errno" => 400, "error" => "metodo invalido");
					break;
				}
			}
		}
	}
	echo json_encode($body);*/
?>
