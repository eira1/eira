<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_permissions.php';

    $body = array();

    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo GET");
            } else if($_GET['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_GET['action']){
                    /*
                        PROPOSITO: mostrar los datos del permiso.
                        PARAMETROS: 
                            -permissionID : ID del permiso.
                        DEVUELVE: arreglo con los datos del permiso.
                    */
                    case 'getPermissionByID':
                        if(!isset($_GET['permissionID'])){
                            $body = array("errno" =>400, "error" => "permissionID no definido para getPermissionByID");
                        } else if($_GET['permissionID']==""){
                            $body = array("errno" =>400, "error" => "permissionID no tiene ningun valor");
                        } else if(exists('permissions','permissionID',$_GET['permissionID'])){
                            $body = getPermissionByID($_GET['permissionID']);
                        } else {
                            $body = array("errno" =>400, "error" => "El permiso no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo GET");
                    break;
                }
            }
        break;
    }

    echo json_encode($body);
?>