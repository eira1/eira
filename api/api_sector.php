<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_sectors.php';
    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET["action"])){
                $body = array("errno"=>400,"error"=>"Action no declarada para el metodo GET");
            }else{
                switch($_GET["action"]){
                    case 'getSectors':
                        $getSectors=getAllSectors();
                        if($getSectors===NULL){
                            $body=array();
                        }else{
                            $body=$getSectors;
                        }
                        break;
                    case 'getSectorByID':
                        if(!isset($_GET["sectorID"])){
                            $body = array("errno"=>400,"error"=>"sectorID no definido para getSectorByID");
                        }else if($_GET["sectorID"]==""){
                            $body = array("errno"=>400,"error"=>"sectorID vacio para getSectorByID");
                        }else{
                            $getSector=getSectorByID($_GET["sectorID"]);
                            if($getSector===NULL){
                                $body=array();
                            }else{
                                $body = $getSector;
                            }
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Action invalida para el metodo GET");
                }
            }
            break;
        case 'POST':
            if(!isset($_POST["action"])){
                $body= array("errno"=>400,"error"=>"Action no declarada para metodo POST");
            }else{
                switch($_POST["action"]){
                    case 'createSector':
                        $data_sector=array(
                            "name"=>"",
                            "description"=>""
                        );
                        foreach($data_sector as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para createSector");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." vacio para createSector");
                                break;
                            } else {
                                $data_sector[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertSector = insertSector($data_sector);
                        if($insertSector===TRUE){
                            $body=array("errno"=>200,"error"=>"Sector ingresado exitosamente");
                        }else{
                            $body=array("errno"=>400,"error"=>$insertSector);
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Accion invalida para el metodo POST");
                        break;
                }
            }
            break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);
            if(!isset($_PUT["action"])){
                $body = array("errno" => 400, "error" => "Action no declarada para el metodo PUT");
            } else {
                switch($_PUT["action"]){
                    case 'modifySector':
                        $data_editable = array();
                        foreach($_PUT as $key => $value){
                            if($key!="action" && $key!="sectorID"){
                                if($key=="name" || $key=="description"){
                                    $data_editable[$key] = $value;
                                }
                            } 
                        }
                        if(empty($data_editable)){
                            $body=array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: name");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                            break;
                            }
                        }
                        if(!isset($_PUT['sectorID'])){
                            $body = array("errno" => 400, "error" => "sectorID no declarado para modifySector");
                        } else {
                            if(sectorIDExists($_PUT['sectorID'])){
                                $updateSector=updateSectorByID($_PUT['sectorID'],$data_editable);
                                if($updateSector===true){
                                    $body = array("errno" => 200, "error" => "Sector modificado correctamente");
                                } else if ($updateSector===false) {
                                    $body = array("errno" => 300, "error" => "No se modifico nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $updateSector);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Sector no encontrado");
                            }
                        }
                        break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo PUT");
                        break;
            }
            }
            break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);
            if(!isset($_DELETE["action"])){
            $body = array("errno" => 400, "error" => "Action no declarada para el metodo DELETE");
            } else {
            switch($_DELETE["action"]){
                case 'deleteSector':
                if(!isset($_DELETE['sectorID'])){
                    $body = array("errno" => 400, "error" => "sectorID no declarado");
                } else {
                    if(sectorIDExists($_DELETE['sectorID'])){
                    $deleteSector=deleteSectorByID($_DELETE['sectorID']);
                    if($deleteSector===true){
                        $body = array("errno" => 200, "error" => "Sector eliminado");
                    } else if ($deleteSector===false) {
                        $body = array("errno" => 300, "error" => "No se elimino nada");
                    }else{
                        $body = array("errno" => 400, "error" => $deleteSector);
                    }
                    } else {
                    $body = array("errno" => 400, "error" => "Sector no encontrada");
                    }
                }
                break;
                default:
                $body=array("errno"=>400,"error"=>"Action invalida en metodo DELETE");
            }
            }
            break;
        default:
            $body = array("errno"=>400,"error"=>"Metodo no definido en api_sector.php");
            break;
    }
    echo json_encode($body);
?>