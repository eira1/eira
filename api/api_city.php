<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_cities.php';
    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET["action"])){
                $body = array("errno"=>400,"error"=>"Action no declarada para el metodo GET");
            }else{
                switch($_GET["action"]){
                    /*
                        PROPOSITO: mostrar los datos de todas las ciudades.
                        PARAMETROS: 
                            - no recibe parametros
                        DEVUELVE: arreglo con los datos de las ciudades.
                    */
                    case 'getCities':
                        $getCities=getAllCities();
                        if($getCities===NULL){
                            $body=array();
                        }else{
                            $body=$getCities;
                        }
                        break;

                    /*
                        PROPOSITO: mostrar los datos de una ciudad.
                        PARAMETROS: 
                            -cityID : ID de la ciudad.
                        DEVUELVE: arreglo con los datos de la ciudad.
                    */    
                    case 'getCityByID':
                        if(!isset($_GET["cityID"])){
                            $body = array("errno"=>400,"error"=>"cityID no definido para getCityByID");
                        }else if($_GET["cityID"]==""){
                            $body = array("errno"=>400,"error"=>"cityID vacio para getCityByID");
                        }else{
                            $getCity=getCityByID($_GET["cityID"]);
                            if($getCity===NULL){
                                $body=array();
                            }else{
                                $body = $getCity;
                            }
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Action invalida para el metodo GET");
                }
            }
            break;
        case 'POST':
            if(!isset($_POST["action"])){
                $body= array("errno"=>400,"error"=>"Action no declarada para metodo POST");
            }else{
                switch($_POST["action"]){
                    case 'createCity':
                        $data_city=array(
                            "name"=>""
                        );
                        foreach($data_city as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para createCity");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." vacio para createCity");
                                break;
                            } else {
                                $data_city[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertCity = insertCity($data_city);
                        if($insertCity===TRUE){
                            $body=array("errno"=>200,"error"=>"Ciudad ingresada exitosamente");
                        }else{
                            $body=array("errno"=>400,"error"=>$insertCity);
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Accion invalida para el metodo POST");
                        break;
                }
            }
            break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);
            if(!isset($_PUT["action"])){
                $body = array("errno" => 400, "error" => "Action no declarada para el metodo PUT");
            } else {
                switch($_PUT["action"]){
                    case 'modifyCity':
                        $data_editable = array();
                        foreach($_PUT as $key => $value){
                            if($key!="action" && $key!="cityID"){
                                if($key=="name"){
                                    $data_editable[$key] = $value;
                                }
                            } 
                        }
                        if(empty($data_editable)){
                            $body=array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: name");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                            break;
                            }
                        }
                        if(!isset($_PUT['cityID'])){
                            $body = array("errno" => 400, "error" => "cityID no declarado para modifyCity");
                        } else {
                            if(cityIDExists($_PUT['cityID'])){
                                $updateCity=updateCityByID($_PUT['cityID'],$data_editable);
                                if($updateCity===true){
                                    $body = array("errno" => 200, "error" => "Ciudad modificado correctamente");
                                } else if ($updateCity===false) {
                                    $body = array("errno" => 300, "error" => "No se modifico nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $updateCity);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Ciudad no encontrado");
                            }
                        }
                        break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo PUT");
                        break;
            }
            }
            break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);
            if(!isset($_DELETE["action"])){
            $body = array("errno" => 400, "error" => "Action no declarada para el metodo DELETE");
            } else {
            switch($_DELETE["action"]){
                case 'deleteCity':
                if(!isset($_DELETE['cityID'])){
                    $body = array("errno" => 400, "error" => "CityID no declarado");
                } else {
                    if(cityIDExists($_DELETE['cityID'])){
                    $deleteCity=deleteCityByID($_DELETE['cityID']);
                    if($deleteCity===true){
                        $body = array("errno" => 200, "error" => "Ciudad eliminada");
                    } else if ($deleteCity===false) {
                        $body = array("errno" => 300, "error" => "No se elimino nada");
                    }else{
                        $body = array("errno" => 400, "error" => $deleteCity);
                    }
                    } else {
                    $body = array("errno" => 400, "error" => "Ciudad no encontrada");
                    }
                }
                break;
                default:
                $body=array("errno"=>400,"error"=>"Action invalida en metodo DELETE");
            }
            }
            break;
        default:
            $body = array("errno"=>400,"error"=>"Metodo no definido en api_cities.php");
            break;
    }
    echo json_encode($body);
?>