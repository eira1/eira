<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_sections.php';

    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno"=>400, "error"=>"Action no declarada para el metodo GET");
            }else if($_GET['action']==""){
                $body = array("errno"=>400, "error"=>"Action no tiene ningun valor");
            }else{
                switch($_GET["action"]){
                    case "getSectionByID":
                        if(!isset($_GET['sectionID'])){
                            $body = array("errno" =>400, "error" => "sectionID no definida");
                        } else if($_GET['sectionID']==""){
                            $body = array("errno" =>400, "error" => "sectionID no tiene valor");
                        } else {
                            if(sectionIDExists($_GET['sectionID'])){
                                $body = getSectionByID($_GET['sectionID']);
                                $namesubcategory = getSubcategoryByID($body['subcategoryID']);
                                $body['subcategory'] = $namesubcategory['name'];
                                unset($body['subcategoryID']);
                            } else {
                                $body = array("errno" =>400, "error" => "la seccion no existe");
                            }
                        }
                        break;
                    default:
                        $body = array("errno" => 400, "error" => "action no valida para el metodo GET");
                        break;
                }
            }
            break;
        case 'POST':
            if(!isset($_POST['action'])){
                $body = array("errno"=>400, "error"=>"Action no declarada para el metodo POST");
            }else if($_POST['action']==""){
                $body = array("errno"=>400, "error"=>"Action no tiene ningun valor");
            }else{
                switch($_POST["action"]){
                    case "createSection":
                        $data_Section = array(
                            "subcategoryID" => "",
                            "name" => "",
                            "description" => ""
                        );
                        foreach($data_Section as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para insertSection");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_Section[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertSection = insertSection($data_Section);
                        if($insertSection === TRUE){
                            $body=array("errno"=>200,"error"=>"Section ingresado exitosamente");
                        } else {
                            $body=array("errno"=>400,"error"=>$insertSection);
                        }
                        break;
                    default:
                    $body = array("errno" => 400, "error" => "action no valida para el metodo POST");
                        break;
                }
            }
            break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);

            if(!isset($_PUT['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo PUT");
            } else if($_PUT['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_PUT['action']){
                    case "updateSectionByID":
                        $data_editable = array(
                            "subcategoryID" => "",
                            "name" => "",
                            "description" => ""
                        );
                        foreach($data_editable as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para updateSectionByID");
                                break;
                            } else if ($_PUT[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_editable[$key] = $_PUT[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['sectionID'])){
                            $body = array("errno" =>400, "error" => "sectionID no declarada");
                        } else if($_PUT['sectionID']==""){
                            $body = array("errno" =>400, "error" => "sectionID no tiene ningun valor");
                        } else if(sectionIDExists($_PUT['sectorID'])){
                            $updateSectionById = updateSectionById($_PUT['sectionID'],$data_editable);
                            if ($updateSectionByID === true){
                                $body = array("errno" =>200, "error" => "seccion modificada con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $updateSectionByID);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => $key."la seccion no existe");
                        }
                        break;
                    default:
                        $body = array("errno" => 400, "error" => "action no valida para el metodo PUT");
                        break;
                }
            }
            break;
        case 'DELETE':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);

            if(!isset($_DELETE['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo DELETE");
            } else if($_DELETE['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_DELETE['action']){
                    case 'deleteSectionByID':
                        if(!isset($_DELETE['sectionID'])){
                            $body = array("errno" =>400, "error" => "sectionID no declarada");
                        } else if($_DELETE['sectionID']==""){
                            $body = array("errno" =>400, "error" => "sectionID no tiene ningun valor");
                        } else if(sectionIDExists($_DELETE['sectionID'])){
                            $deleteSectionByID = deleteSectionByID($_DELETE['sectionID']);
                            if ($deleteSectionByID === true){
                                $body = array("errno" =>200, "error" => "seccion eliminada con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $deleteSectionByID);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => $key."la seccion no existe");
                        }
                        break;
                    default:
                    $body = array("errno" => 400, "error" => "action no valida para el metodo DELETE");
                        break;
                }
            }
            break;
    }

    echo json_encode($body);
?>