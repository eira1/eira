<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_subcategories.php';

    $body = array();

    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo GET");
            } else if($_GET['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_GET['action']){
                    /*
                        PROPOSITO: mostrar los datos de la subcategoria.
                        PARAMETROS: 
                            -subcategoryID : ID de la subcategoria.
                        DEVUELVE: arreglo con los datos de la subcategoria.
                    */
                    case 'getSubcategoryByID':
                        if(!isset($_GET['subcategoryID'])){
                            $body = array("errno" =>400, "error" => "subcategoryID no definido para getSubcategoryByID");
                        } else if($_GET['subcategoryID']==""){
                            $body = array("errno" =>400, "error" => "subcategoryID no tiene ningun valor");
                        } else if(subcategoryIDExists($_GET['subcategoryID'])){
                            $body = getSubcategoryByID($_GET['subcategoryID']);
                        } else {
                            $body = array("errno" =>400, "error" => "la subcategoria no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo GET");
                    break;
                }
            }
        break;
        case 'POST':
            if(!isset($_POST['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo POST");
            } else if($_POST['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_POST['action']){
                    /*
                        PROPOSITO: insertar una subcategoria.
                        PARAMETROS: 
                            -categoryID : ID de la categoria.
                            -name : nombre de la subcategoria.
                            -description : descripcion de la nueva subcategoria.
                        DEVUELVE: arreglo con el resultado de la insercion.
                    */
                    case 'createSubcategory':
                        $data_subcategory = array(
                            "categoryID" => "",
                            "name" => "",
                            "description" => ""
                        );
                        foreach($data_subcategory as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para insertSubcategory");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_subcategory[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertSubcategory = insertSubcategory($data_subcategory);
                        if($insertSubcategory===true){
                            $body = array("errno" => 200, "error" => "sub categoria creada con exito");
                        } else {
                            $body = array("errno" => 200, "error" => $insertSubcategory);
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo POST");
                    break;
                }
            }
        break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);
            if(!isset($_PUT['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo PUT");
            } else if($_PUT['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_PUT['action']){
                    /*
                        PROPOSITO: modificar los datos de una subcategoria.
                        PARAMETROS: 
                            -subcategoryID : ID de la subcategoria.
                        DEVUELVE: arreglo con la respuesta de la modificacion.
                    */
                    case 'modifySubcategoryByID':
                        $data_editable = array(
                            "categoryID" => "",
                            "name" => "",
                            "description" => ""
                        );
                        foreach($data_editable as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para modifySubcategoryByID");
                                break;
                            } else if($_PUT[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_editable[$key] = $_PUT[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['subcategoryID'])){
                            $body = array("errno" =>400, "error" => "subcategoryID no definida para modifySubcategoryByID");
                        } else if($_PUT['subcategoryID']==""){
                            $body = array("errno" =>400, "error" => "subcategoryID no tiene ningun valor");
                        } else if(subcategoryIDExists($_PUT['subcategoryID'])){
                            $updateSubcategoryByID = updateSubcategoryByID($_PUT['subcategoryID'],$data_editable);
                            if($updateSubcategoryByID === true){
                                $body = array("errno" =>200, "error" => "sub categoria modificada");
                            } else {
                                $body = array("errno" =>400, "error" => $updateSubcategoryByID);
                            }
                        } else {
                            $body = array("errno" =>400, "error" => "la sub categoria no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo PUT");
                    break;
                }
            }
        break;
        case 'DELETE':
            parse_str(file_get_contents('php://input'),$_DELETE);
            if(!isset($_DELETE['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo DELETE");
            } else if($_DELETE['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_DELETE['action']){
                    /*
                        PROPOSITO: eliminar una subcategoria.
                        PARAMETROS: 
                            -subcategoryID : ID de la subcategoria.
                        DEVUELVE: arreglo con la respuesta de la extraccion.
                    */
                    case 'deleteSubcategoryByID':
                        if(!isset($_DELETE['subcategoryID'])){
                            $body = array("errno" =>400, "error" => "subcategoryID no definido para deleteSubcategoryByID");
                        } else if($_DELETE['subcategoryID']==""){
                            $body = array("errno" =>400, "error" => "subcategoryID no tiene ningun valor");
                        } else if(subcategoryIDExists($_DELETE['subcategoryID'])){
                            $deleteSubcategoryByID = deleteSubcategoryByID($_DELETE['subcategoryID']);
                            if($deleteSubcategoryByID===true){
                                $body = array("errno" =>200, "error" => "sub categoria eliminado con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $deleteSubcategoryByID);
                            }
                        } else {
                            $body = array("errno" =>400, "error" => "la subcategoria no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo DELETE");
                    break;
                }
            }
        break;
    }

    echo json_encode($body);
?>