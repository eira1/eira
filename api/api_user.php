<?php
  // ! Honestamente, serviría un txt de guía para usar la API

  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include '../models/model_users.php';
    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno" =>400, "error" => "Action no declarada para el metodo GET");
            } else if($_GET['action']==""){
                $body = array("errno" =>400, "error" => "Action no tiene ningun valor");
            } else {
                switch($_GET["action"]){
                    /*
                        Recibe el token de un usuario;
                        Devuelve un mensaje si el token no se encuentra declarada o si no encuentra al usuario;
                        Devuelve los datos del usuario
                    */
                    case 'getDataByToken':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "Token no esta declarada");
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "El token no tiene ningun valor");
                        } else if(exists("users","token",$_GET['token'])){
                            $data_usuario = array(
                                'userID' =>"",
                                'dni' => "",
                                'firstName' => "",
                                'lastName' => "",
                                'medicalLicense' => "",
                                'phoneNumber' => "",
                                'email'=>"",
                                'birthDate'=>"",
                                'permissionID' => "",
                                'pass'=>"",
                                'active'=>"",
                                'activationCode'=>"",
                                'activationExpiry'=>"",
                                'activationTime'=>"",
                                'creationDate'=>"",
                                'photo'=>""
                            );
                            $data_A = array(getSomethingByParameter("users","token",$_GET['token'])[0])[0];
                            $body = array(array_intersect_key($data_A, $data_usuario))[0];
                        } else {
                            $body = array("errno" =>400, "error" => "Usuario no encontrado");
                        }
                    break;
                    /*
                        Recibe el token de un usuario;
                        Devuelve un mensaje si el token no se encuentra declarada o si no encuentra al usuario;
                        Devuelve los datos modificables del usuario;
                    */
                    case 'getModifiableDataByToken':
                        $body = array("errno" =>400, "error" => "mymodifiabledata");
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "Token no esta declarado");
                        } else if ($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "El token no tiene ningun valor");
                        } else if(exists("users","token",$_GET['token'])){
                            $data_editable = array(
                                'dni' => "",
                                'firstName' => "",
                                'lastName' => "",
                                'medicalLicense' => "",
                                'phoneNumber' => "",
                                'birthDate' => "",
                                'photo' => ""
                            );
                            $data_A = array(getSomethingByParameter("users","token",$_GET['token'])[0])[0];
                            $body = array(array_intersect_key($data_A,$data_editable))[0];
                        } else {
                            $body = array("errno" =>400, "error" => "Usuario no encontrado");
                        }
                    break;
                    case 'fileExists':
                      if(!isset($_GET["url"])){
                        $body=array("errno"=>400,"error"=>"Falta url para fileExists");
                      }else if($_GET["url"]==""){
                        $body=array("errno"=>400,"error"=>"url vacia para fileExists");
                      }else{
                        $fileExists=fileExists($_GET["url"]);
                        if($fileExists){
                          $body=array("errno"=>200,"error"=>"El archivo existe");
                        }else{
                          $body=array("errno"=>404,"error"=>"El archivo no existe");
                        }
                      }
                      break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo GET");
                }
            }
        break;
        case 'POST':
            if(!isset($_POST['action'])){
                $body = array("errno" =>400, "error" => "Action no declarada para el metodo POST");
            } else if($_POST['action']==""){
                $body = array("errno" =>400, "error" => "Action no tiene ningun valor");
            } else {
                switch($_POST["action"]){
                    /*
                    */
                    case 'register':
                        $data_usuario = array(
                            'email' => '',
                            'pass' => '',
                            'repass' => '',
                        );
                        foreach($data_usuario as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no esta declarada");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_usuario[$key] = $_POST[$key];
                            }
                        } 

                        if ($data_usuario['pass'] != $data_usuario['repass']){
                            $body = array("errno" => 400, "error" => "Las contraseñas no coinciden");
                            break;
                        }

                        if (!checkInput($data_usuario['pass'],CHECK__PASS)){
                          $body = array("errno" => 400, "error" => "La contraseña es inválida. Debe tener al menos una mayúscula, una minúscula, un número y un caracter especial (@#-_$%^&+=§!?).");
                        }
                        if(!checkInput($data_usuario['email'],CHECK__EMAIL)){
                          $body = array("errno" => 400, "error" => "El email es inválido");
                        }else{
                          $data_usuario['email']=strtolower($data_usuario['email']);
                        }

                        if (isset($body["errno"])) {
                          if($body["errno"]==400){
                              break;
                          }
                        }
                        $registerUser = createUser($data_usuario);
                        if($registerUser == 1){
                            updateTokenByEmail($_POST['email']);
                            $body = array("errno" => 200, "error" => "Registrado correctamente");
                        } else if ($registerUser==2) {
                            $body = array("errno" => 400, "error" => "El usuario ya existe");
                        } else if ($registerUser==0){
                            $body = array("errno" => 400, "error" => "La contraseña tiene menos de 8 caracteres");
                        }
                    break;
                    case 'login':
                        $data_usuario = array(
                            'email' => '',
                            'pass' => '',
                        );
                        foreach($data_usuario as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no esta declarada");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_usuario[$key] = $_POST[$key];
                            }
                        } 
                        /* if (!checkInput($data_usuario['pass'],CHECK__PASS)){
                          $body = array("errno" => 400, "error" => "La contraseña contiene caracteres no válidos");
                        } */
                        if (!checkInput($data_usuario['email'],CHECK__EMAIL)){
                          $body = array("errno" => 400, "error" => "El email contiene caracteres no válidos");
                        }else{
                          $data_usuario['email']=strtolower($data_usuario['email']);
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $userExists = loginUser($data_usuario['email'], $data_usuario['pass']);
                        if($userExists){
                            $body = array("errno" => 200, "error" => "Logeado correctamente", "token" => getTokenByEmail($data_usuario['email']));
                        } else {
                            $body = array("errno" => 400, "error" => "Error al loguear");
                        }
                    break;
                    case 'modifyUserPhoto':
                      if(!isset($_POST["token"])){
                        $body=array("errno"=>400,"error"=>"No envio token");
                      }else{
                          if(!isset($_FILES["photo"])){
                            $body=array("errno"=>400,"error"=>"No envio una foto");
                            break;
                          }else{
                            if($_FILES["photo"]["error"]!=0){
                              $body=array("errno"=>400,"error"=>$_FILES["photo"]["error"]);
                            }else{
                              $userID=getUserIDByToken($_POST["token"]);
                              $filename=$_FILES['photo']['name'];
                              $filetype=$_FILES['photo']['type'];
                              $filesize=$_FILES['photo']['size'];
                              $filetmp=$_FILES['photo']['tmp_name'];
                              if (!((strpos($filetype, "gif") || strpos($filetype, "jpeg") || strpos($filetype, "jpg") || strpos($filetype, "png")) && ($filesize < 2000000))) {
                                $body=array("errno"=>400,"error"=>'Error. La extensión o el tamaño de los archivos no es correcta.- Se permiten archivos .gif, .jpg, .png y .jpeg y de 200 kb como máximo.');
                              }
                              if(move_uploaded_file($filetmp,"../img/userphotos/".$userID.".".str_replace("image/","",$filetype))){
                                chmod("../img/userphotos/".$userID.".".str_replace("image/","",$filetype),447);
                                updateUserByID($userID,array('photo'=>$userID.".".str_replace("image/","",$filetype)));
                                $body=array("errno"=>200,"error"=>"Imagen subida con exito");
                              }else{
                                $body=array("errno"=>400,"error"=>"Ocurrio un error al intentar subir el archivo al servidor");
                                break;
                              }
                            }
                          }
                      }
                      break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo POST");
                }
            }
        break;
      //-------------------------------------MODIFICAR USUARIO----------------------------------------
      ////enviar los elementos por fetch (lista) (se envian por body) (enviar sin espacios)
      //! Lo estamos haciendo para mandarlos por JSON (ya lo hicimos xd)
      //! Lo estamos haciendo para que sea por token y no por userID
      //* Campos obligatorios: action, token.
      //* Campos opcionales: dni, firstName, lastName, medicalLicense, phoneNumber, birthDate, photo
      case 'PUT':
        //parse_str(file_get_contents('php://input'),$_PUT);
        //var_dump(file_get_contents("php://input"));
        $_PUT=json_decode(file_get_contents("php://input"));
        $_PUT=get_object_vars($_PUT);
        if(!isset($_PUT["action"])){
          $body = array("errno" => 400, "error" => "Action no declarada para metodo PUT");
        } else {
          switch($_PUT["action"]){
            case 'modifyUser':
              $data_editable = array(
                'dni' => "",
                'firstName' => "",
                'lastName' => "",
                'medicalLicense' => "",
                'phoneNumber' => "",
                'birthDate' => ""
              );
              foreach($_PUT as $key => $value){
                if(!isset($_PUT[$key])){
                  $body = array("errno" => 404, "error" => "No esta seteado el campo " .$key);
                }else{
                  if(($_PUT[$key])==""){
                    $body = array("errno" => 404, "error" => "Falta el campo ".$key);
                    break;
                  } else {
                    //if($key!="action" && $key!="token"){
                      //var_dump($value);
                      //if($key=="dni" || $key=="firstName" || $key="lastName" || $key="medicalLicense" || $key="phoneNumber" || $key="birthDate" || $key="photo"){
                        //$data_editable[$key] = $value;
                      //}
                    //}
                    if($key!="action" && $key!="token"){
                      $data_editable[$key] = $value;
                    }
                  }
                }
                // ! Comentamos todo, porque no hace falta chequear si faltan o no.
                // ! Al final si xd
              }
              if(empty($data_editable)){
                $body=array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: name");
              }
              // ! Creo que es al pedo verificar primero que sean todos alfanumericos
              /* foreach($data_editable as $key => $value){
                if($key!="birthDate"){
                  if(!checkInput($value,CHECK__ALPHANUMERIC)){
                    $body = array("errno" => 400, "error" => "El campo ".$key." es inválido");
                  }
                }
              } */
              if(!checkInput($data_editable["phoneNumber"],CHECK__NUMBERS)){
                $body = array("errno" => 400, "error" => "Debe ingresar solamente número en el campo teléfono");
              }
              if(!checkInput($data_editable["medicalLicense"],CHECK__NUMBERS)){
                $body = array("errno" => 400, "error" => "Debe ingresar solamente número en el campo matrícula");
              }
              if(!checkInput($data_editable["birthDate"],CHECK__DATE)){
                $body = array("errno" => 400, "error" => "El campo fecha de nacimiento es inválido");
              }
              if(!checkInput($data_editable["dni"],CHECK__NUMBERS)){
                $body = array("errno" => 400, "error" => "Debe ingresar solamente número en el campo DNI");
              }
              if(!checkInput($data_editable["lastName"],CHECK__LETTERS)){
                $body = array("errno" => 400, "error" => "Debe ingresar solamente letras en el campo apellido");
              } 
              if(!checkInput($data_editable["firstName"],CHECK__LETTERS)){
                $body = array("errno" => 400, "error" => "Debe ingresar solamente número en el campo nombre");
              }
              if (isset($body["errno"])) {
                if($body["errno"]==400){
                  break;
                }
              }
              if(!isset($_PUT['token'])){
                $body = array("errno" => 400, "error" => "Token no declarado");
              } else {
                if(userTokenExists($_PUT['token'])){
                  //var_dump($data_editable);
                  $updateUser=updateUserByToken($_PUT['token'],$data_editable);
                  if($updateUser===true){
                    //var_dump(updateUserByToken($_PUT['token'],$data_))
                    $body = array("errno" => 200, "error" => "Usuario modificado correctamente");
                  } else if ($updateUser===false) {
                    $body = array("errno" => 300, "error" => "No se modifico nada");
                  }else{
                    $body = array("errno" => 400, "error" => $updateUser);
                  }
                } else {
                  $body = array("errno" => 400, "error" => "Usuario no encontrado");
                }
              }
            break;
            default:
              $body = array("errno"=>400,"error"=>"Action invalida en metodo PUT");
          }
        }
      break;
      //-------------------------------------ELIMINAR USUARIO--------------------------------------
      //enviar los elementos por fetch (lista) (se envian por body) (enviar sin espacios)
      //! Lo mismo que modifyUser, lo modificamos para que sea que hay que mandar un JSON y fue optimizado un poquito
      //* Ya funciona bárbaro
      case 'DELETE':
        //parse_str(file_get_contents('php://input'),$_DELETE);
        $_DELETE=json_decode(file_get_contents("php://input"));
        $_DELETE=get_object_vars($_DELETE);
        if(!isset($_DELETE["action"])){
          $body = array("errno" => 400, "error" => "Action no declarada para metodo DELETE");
        } else {
          switch($_DELETE["action"]){
            case 'deleteUser':
              if(!isset($_DELETE['token'])){
                $body = array("errno" => 400, "error" => "Token no declarado");
              } else {
                if(userTokenExists($_DELETE['token'])){
                  $deleteUser=deleteUserByToken($_DELETE['token']);
                  if($deleteUser===true){
                    $body = array("errno" => 200, "error" => "Usuario eliminado");
                  } else if ($deleteUser===false) {
                    $body = array("errno" => 300, "error" => "No se elimino nada");
                  }else{
                    $body = array("errno" => 400, "error" => $deleteUser);
                  }
                } else {
                  $body = array("errno" => 400, "error" => "Usuario no encontrado");
                }
              }
            break;
            default:
              $body=array("errno"=>400,"error"=>"Action invalida en metodo DELETE");
          }
        }
      break;
      default:
        $body=array("errno"=>400,"error"=>"Metodo no usados en api_user.php");
    }
    echo json_encode($body);
?>