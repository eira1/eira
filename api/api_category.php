<?php

    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_categories.php';

    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo GET");
            } else if($_GET['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_GET['action']){
                    /*
                        PROPOSITO: mostrar los datos de una categoria.
                        PARAMETROS: 
                            -categoryID : ID de la categoria.
                        DEVUELVE: arreglo con los datos de la categoria.
                    */
                    case 'getCategoryByID':
                        if(!isset($_GET['categoryID'])){
                            $body = array("errno" =>400, "error" => "categoryID no definida");
                        } else if($_GET['categoryID']==""){
                            $body = array("errno" =>400, "error" => "categoryID no tiene valor");
                        } else {
                            if(categoryIDExists($_GET['categoryID'])){
                                $body = getCategoryByID($_GET['categoryID']);
                            } else {
                                $body = array("errno" =>400, "error" => "la category no existe");
                            }
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo GET");
                    break;
                }
            }
        break;
        case 'POST':
            if(!isset($_POST['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo POST");
            } else if($_POST['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_POST['action']){
                    /*
                        PROPOSITO: insertar una nueva categoria.
                        PARAMETROS: 
                            -name : nombre de la categoria.
                            -description: descripcion de la categoria.
                        DEVUELVE: arreglo con el resultado de la insercion.
                    */
                    case 'insertCategory':
                        $data_Category = array(
                            "name" => "",
                            "description" => ""
                        );
                        foreach($data_Category as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para insertCategory");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_Category[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertCategory = insertCategory($data_Category);
                        if($insertCategory===TRUE){
                            $body=array("errno"=>200,"error"=>"Categoria ingresada exitosamente");
                        } else {
                            $body=array("errno"=>400,"error"=>$insertCategory);
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo POST");
                    break;
                }
            }
        break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);

            if(!isset($_PUT['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo PUT");
            } else if($_PUT['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_PUT['action']){
                    /*
                        PROPOSITO: modificar los datos de la categoria.
                        PARAMETROS: 
                            -name : nombre de la categoria.
                            -description : descripcion de la categoria.
                            -categoryID : ID de la categoria.
                        DEVUELVE: arreglo con el resultado de la modificacion.
                    */
                    case 'updateCategoryById':
                        $data_editable = array(
                            "name" => "",
                            "description" => ""
                        );
                        foreach($data_editable as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para updateCategoryById");
                                break;
                            } else if ($_PUT[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                break;
                            } else {
                                $data_editable[$key] = $_PUT[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['categoryID'])){
                            $body = array("errno" =>400, "error" => "categoryID no declarada");
                        } else if($_PUT['categoryID']==""){
                            $body = array("errno" =>400, "error" => "categoryID no tiene ningun valor");
                        } else if(categoryIDExists($_PUT['categoryID'])){
                            $updateCategoryById = updateCategoryById($_PUT['categoryID'],$data_editable);
                            if ($updateCategoryByID === true){
                                $body = array("errno" =>200, "error" => "categoria modificada con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $updateCategoryByID);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => $key."la categoria no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo PUT");
                    break;
                }
            }
        break;
        case 'DELETE':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);

            if(!isset($_DELETE['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo DELETE");
            } else if($_DELETE['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_DELETE['action']){
                    /*
                        PROPOSITO: eliminar una categoria.
                        PARAMETROS: 
                            -supplierID : ID de la categoria.
                        DEVUELVE: arreglo con la respuesta del delete.
                    */
                    case 'deleteCategoryByID':
                        if(!isset($_DELETE['categoryID'])){
                            $body = array("errno" =>400, "error" => "categoryID no declarada");
                        } else if($_DELETE['categoryID']==""){
                            $body = array("errno" =>400, "error" => "categoryID no tiene ningun valor");
                        } else if(categoryIDExists($_DELETE['categoryID'])){
                            $deleteCategoryByID = deleteCategoryByID($_DELETE['categoryID']);
                            if ($deleteCategoryByID === true){
                                $body = array("errno" =>200, "error" => "categoria eliminada con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $deleteCategoryByID);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => $key."la categoria no existe");
                        }
                    break;
                }
            }
        break;
    }
    echo json_encode($body);
?>