<?php
    //var_dump($_POST);
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_products.php';
    $body = array();
    //var_dump($_SERVER["REQUEST_METHOD"]);
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            //var_dump($_GET);
            if(!isset($_GET["action"])){
                $body = array("errno"=>400,"error"=>"Action no declarada para el metodo GET");
            }else{
                switch($_GET["action"]){
                    case 'getProducts':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido getProductsByText");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio getProductsByText");
                        }else{
                            $getProducts=getAllProducts();
                            if($getProducts===NULL){
                                $body=array();
                            }else{
                                $body=$getProducts;
                            }
                        }
                        break;
                    case 'getProductByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido getProductsByText");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio getProductsByText");
                        }else if(!isset($_GET["productID"])){
                            $body = array("errno"=>400,"error"=>"productID no definido para getProductByID");
                        }else if($_GET["productID"]==""){
                            $body = array("errno"=>400,"error"=>"productID vacio para getProductByID");
                        }else{
                            $getProduct=getProductByID($_GET["productID"]);
                            if($getProduct===NULL){
                                $body=array();
                            }else{
                                $body = $getProduct;
                            }
                        }
                        break;
                    case 'getProductsByText':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido getProductsByText");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio getProductsByText");
                        }else if(!isset($_GET["text"])){
                            $body = array("errno"=>400,"error"=>"text no definido para getProductsByText");
                        }else if($_GET["text"]==""){
                            $body = array("errno"=>400,"error"=>"text vacio getProductsByText");
                        }else{
                            $getProducts=getProductsByText($_GET["text"]);
                            if($getProducts===NULL){
                                $body=array();
                            }else{
                                $body=$getProducts;
                            }
                        }
                        break;
                    case 'countProductsByText':
                        if(!isset($_GET["text"])){
                            $body = array("errno"=>400,"errno"=>"text no definido countProductsByText");
                        }else if($_GET["text"]==""){
                            $body = array("errno"=>400,"errno"=>"text vacio countProductsByText");
                        }else{
                            $countProducts=countProductsByText($_GET["text"]);
                            if($countProducts===NULL){
                                $body=array();
                            }else{
                                $body=$countProducts;
                            }
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Action invalida para el metodo GET");
                }
            }
            break;
        case 'POST':
            //var_dump($_POST);
            if(!isset($_POST["action"])){
                $body= array("errno"=>400,"error"=>"Action no declarada para metodo POST");
                //var_dump($_SERVER["REQUEST_METHOD"]);
            }else{
                switch($_POST["action"]){
                    case 'createProduct':
                        $data_product=array(
                            "sectionID"=>"",
                            "supplierID"=>"",
                            "name"=>"",
                            "description"=>"",
                            "price"=>"",
                            "photo"=>""
                        );
                        foreach($data_product as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para createProduct");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." vacio para createProduct");
                                break;
                            } else {
                                $data_product[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertProduct = insertProduct($data_product);
                        if($insertProduct===TRUE){
                            $body=array("errno"=>200,"error"=>"Producto ingresado exitosamente");
                        }else{
                            $body=array("errno"=>400,"error"=>$insertProduct);
                        }
                        break;
                    case 'modifyProductPhoto':
                        if(!isset($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No envio token");
                        }else{
                            if(!isset($_POST["productID"])){
                                $body=array("errno"=>400,"error"=>"No envio productID");
                            }else{
                                if(!isset($_FILES["photo"])){
                                    $body=array("errno"=>400,"error"=>"No envio una foto");
                                    break;
                                }else{
                                    if($_FILES["photo"]["error"]!=0){
                                        $body=array("errno"=>400,"error"=>$_FILES["photo"]["error"]);
                                    }else{
                                        $productID=$_POST["productID"];
                                        $filename=$_FILES['photo']['name'];
                                        $filetype=$_FILES['photo']['type'];
                                        $filesize=$_FILES['photo']['size'];
                                        $filetmp=$_FILES['photo']['tmp_name'];
                                        if (!((strpos($filetype, "gif") || strpos($filetype, "jpeg") || strpos($filetype, "jpg") || strpos($filetype, "png")) && ($filesize < 2000000))) {
                                            $body=array("errno"=>400,"error"=>'Error. La extensión o el tamaño de los archivos no es correcta.- Se permiten archivos .gif, .jpg, .png y .jpeg y de 200 kb como máximo.');
                                        }
                                        if(move_uploaded_file($filetmp,"../img/productphotos/".$productID.".".str_replace("image/","",$filetype))){
                                            chmod("../img/productphotos/".$productID.".".str_replace("image/","",$filetype),447);
                                            updateProductByID($productID,array('photo'=>$productID.".".str_replace("image/","",$filetype)));
                                            $body=array("errno"=>200,"error"=>"Imagen subida con exito");
                                        }else{
                                            $body=array("errno"=>400,"error"=>"Ocurrio un error al intentar subir el archivo al servidor");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Accion invalida para el metodo POST");
                        break;
                }
            }
            break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);
            if(!isset($_PUT["action"])){
                $body = array("errno" => 400, "error" => "Action no declarada para el metodo PUT");
            } else {
                switch($_PUT["action"]){
                    case 'modifyProduct':
                        $data_editable = array();
                        foreach($_PUT as $key => $value){
                            if($key!="action" && $key!="productID"){
                                if($key=="sectorID" || $key=="supplierID" || $key=="name" || $key=="description" || $key=="price" || $key=="photo"){
                                    $data_editable[$key] = $value;
                                }
                            } 
                        }
                        if(empty($data_editable)){
                            $body=array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: price");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                              break;
                            }
                        }
                        if(!isset($_PUT['productID'])){
                            $body = array("errno" => 400, "error" => "productID no declarado para modifyProduct");
                        } else {
                            if(productIDExists($_PUT['productID'])){
                                $updateProduct=updateProductByID($_PUT['productID'],$data_editable);
                                if($updateProduct===true){
                                    $body = array("errno" => 200, "error" => "Producto modificado correctamente");
                                } else if ($updateProduct===false) {
                                    $body = array("errno" => 300, "error" => "No se modifico nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $updateProduct);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Producto no encontrado");
                            }
                        }
                        break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo PUT");
                        break;
            }
            }
            break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);
            if(!isset($_DELETE["action"])){
              $body = array("errno" => 400, "error" => "Action no declarada para el metodo DELETE");
            } else {
              switch($_DELETE["action"]){
                case 'deleteProduct':
                    if(!isset($_DELETE['productID'])){
                        $body = array("errno" => 400, "error" => "productID no declarado");
                    } else {
                        if(productIDExists($_DELETE['productID'])){
                            $deleteProduct=deleteProductByID($_DELETE['productID']);
                            if($deleteProduct===true){
                                $body = array("errno" => 200, "error" => "Producto eliminado");
                            } else if ($deleteProduct===false) {
                                $body = array("errno" => 300, "error" => "No se elimino nada");
                            }else{
                                $body = array("errno" => 400, "error" => $deleteProduct);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => "Producto no encontrado");
                        }
                    }
                    break;
                default:
                  $body=array("errno"=>400,"error"=>"Action invalida en metodo DELETE");
              }
            }
            break;
        default:
            $body = array("errno"=>400,"error"=>"Metodo no definido en api_product.php");
            break;
    }
    echo json_encode($body);
?>