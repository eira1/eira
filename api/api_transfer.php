<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
    date_default_timezone_set('America/Argentina/Buenos_Aires');

    include_once '../models/model_transfers.php';
    include_once '../models/model_users.php';
    include_once '../models/model_products.php';
    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno"=>400, "error"=>"Action no declarada para el metodo GET");
            }else if($_GET['action']==""){
                $body = array("errno"=>400, "error"=>"Action no tiene ningun valor");
            }else{
                switch($_GET["action"]){
                    case 'getTransfers':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "Token no esta declarada");
                            // TODO break;
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "El token no tiene ningun valor");
                            // TODO break;
                        } else if(exists("users","token",$_GET['token'])){
                            $getTransfers=getAllTransfers();
                            if($getTransfers===NULL){
                                $body = array();
                            }else{
                                $body = $getTransfers;
                                foreach($body as $key => $value){
                                    $transferDetail = getTransferDetails($value['transferID']);
                                    $body[$key]["productList"] = $transferDetail;
                                }
                            }     
                        } else{
                            $body = array("errno" =>400, "error" => "No existe el usuario");
                            // TODO break;
                        }
                        break;
                    case 'getTransferByID':
                        if(!isset($_GET['transferID'])){
                            $body = array("errno"=>400,"error"=>"transferID no definido para getTransferByID");
                        }else if(trim($_GET["transferID"]) == ''){
                            $body = array("errno"=>400,"error"=>"transferID vacio para getTransferByID");
                        }else{
                            $getTransfers=getTransferByID($_GET['transferID']);
                            if($getTransfers===NULL){
                                $body = array();
                            }else{
                                $body = $getTransfers;
                            }
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Action invalida para metodo GET");
                        break;
                }
            }
            break;
        case 'POST':
            //var_dump(file_get_contents("php://input"));
            //var_dump($_POST);
            //$_POST=json_decode(file_get_contents("php://input"));
            //$_POST=get_object_vars($_POST);
            //var_dump($_POST);
            if(!isset($_POST["action"])){
                $body = array("errno"=>400, "error"=>"Action no declarada para el metodo POST");
            }else{
                switch($_POST["action"]){
                    case 'createTransfer': 
                        $data_transfer = array(
                            'token' => '',
                            'sectorID' => ''//,
                            //'person'=> ''
                        );                        
                        foreach($data_transfer as $key => $value){
                            if(!isset($_POST[$key])){
                                $body=array("errno"=>400,"error"=>"No esta seteado el campo ".$key);
                                break;
                            }else{
                                if($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => "Esta vacio el campo ".$key);
                                    break;
                                } else {
                                    $data_transfer[$key] = $_POST[$key];
                                }
                            }
                            
                        }
                        $data_transfer["transferDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago por acá por que si no sale error de que no le paso la fecha por POST
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_POST["productList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de productos (productList)");
                            break;
                        }else{
                            $_POST["productList"]=json_decode($_POST["productList"]);
                            if(empty($_POST["productList"])){
                                $body=array("errno"=>400,"error"=>"productList no puede estar vacio");
                            }else{
                                for ($i=0; $i < count($_POST['productList']) ; $i++) { 
                                    $_POST['productList'][$i]=get_object_vars($_POST['productList'][$i]);
                                    $data_transferdetail[$i] = array(
                                        'productID' => '',
                                        'amount' => '',
                                    );
                                    if(!isset($_POST["productList"][$i]["productID"])){
                                        $body=array("errno"=>400,"error"=>"No esta seteado el campo productID del producto n°".$i+1);
                                        break;
                                    }else if(!isset($_POST["productList"][$i]["amount"])){
                                        $body=array("errno"=>400,"error"=>"No esta seteado el campo productID del producto n°".$i+1);
                                        break;
                                    }else{
                                        if(($_POST['productList'][$i]["productID"])==""){
                                            $body = array("errno" => 400, "error" => "Esta vacio el campo productID del producto n°".$i+1);
                                            break;
                                        }else if (($_POST['productList'][$i]["amount"])==""){
                                            $body = array("errno" => 400, "error" => "Esta vacio el campo amount del producto n°".$i+1);
                                            break;
                                        } else {
                                            $stock = getProductStockByID($_POST["productList"][$i]["productID"]);
                                            if($_POST["productList"][$i]["amount"]>$stock){
                                                $body=array("errno"=>400,"error"=>"Esta intentando transferir una cantidad mas grande de la que hay en stock en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else if($_POST["productList"][$i]["amount"]<0){
                                                $body=array("errno"=>400,"error"=>"Esta intentando transferir una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else if($_POST["productList"][$i]["amount"]==0){
                                                $body=array("errno"=>400,"error"=>"Esta intentando transferir una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else{
                                                $data_transferdetail[$i]["productID"] = $_POST['productList'][$i]["productID"];
                                                $data_transferdetail[$i]["amount"] = $_POST['productList'][$i]["amount"];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $data_transfer['userID']=getUserIDByToken($data_transfer['token']);
                        unset($data_transfer['token']);
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $addTransfer = insertTransfer($data_transfer);
                        if($addTransfer==true){
                            $body = array("errno" => 200, "error" => "Traspaso agregado correctamente");
                        }else{
                            $body = array("errno" => 400, "error" => $addTransfer."linea 151");
                            break;
                            //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                        }
                        if(count($data_transferdetail)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de traspaso para agregar la orden");
                            deleteTransferByID(getTransferMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_transferdetail) ; $i++) {
                                $data_transferdetail[$i]["transferID"]=getTransferMaxID();
                                $addTransferDetail = insertTransferDetail($data_transferdetail[$i]);
                                if($addTransferDetail===true){
                                    $body = array("errno" => 200, "error" => "Traspaso y detalle del traspaso agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addTransferDetail."linea 169");
                                    break;
                                    //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                                }
                            }
                        }
                        break;
                        default:
                            $body = array("errno"=>400,"error"=>"Accion invalida para metodo POST");
                }
            }
            break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);
            if(!isset($_PUT["action"])){
                $body = array("errno" => 400, "error" => "Action no declarada para el metodo PUT");
            } else {
                switch($_PUT["action"]){
                    case 'modifyTransfer':
                        $data_editable = array(
                            'userID' => "",
                            'sectorID' => ""
                        );
                        foreach($_PUT as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 404, "error" => "No esta seteado el campo " .$key);
                            }else{
                                if(($_PUT[$key])==""){
                                    $body = array("errno" => 404, "error" => "Falta el campo ".$key);
                                    break;
                                } else {
                                    if($key!="action" && $key!="transferID"){
                                        if($key=="userID" || $key=="sectorID"){
                                            $data_editable[$key] = $value;
                                        }
                                    }
                                }
                            }
                        }
                        if(empty($data_editable)){
                            $body=array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: sectorID");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['transferID'])){
                            $body = array("errno" => 400, "error" => "transferID no declarado para modifyTransfer");
                        } else {
                            if(transferIDExists($_PUT['transferID'])){
                                $updateTransfer=updateTransferByID($_PUT['transferID'],$data_editable);
                                if($updateTransfer===true){
                                    $body = array("errno" => 200, "error" => "Traspaso modificado correctamente");
                                } else if ($updateTransfer===false) {
                                    $body = array("errno" => 300, "error" => "No se modifico nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $updateTransfer);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Traspaso no encontrado");
                            }
                        }
                        break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo PUT");
                    break;
            }
            }
            break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);
            if(!isset($_DELETE["action"])){
              $body = array("errno" => 400, "error" => "Action no declarada para el metodo DELETE");
            } else {
              switch($_DELETE["action"]){
                case 'deleteTransfer':
                    if(!isset($_DELETE['transferID'])){
                        $body = array("errno" => 400, "error" => "transferID no declarado");
                    } else {
                        if(transferIDExists($_DELETE['transferID'])){
                            $deleteTransfer=deleteTransferByID($_DELETE['transferID']);
                            if($deleteTransfer===true){
                                $body = array("errno" => 200, "error" => "Traspaso eliminado");
                            } else if ($deleteTransfer===false) {
                                $body = array("errno" => 300, "error" => "No se elimino nada");
                            }else{
                                $body = array("errno" => 400, "error" => $deleteTransfer);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => "Traspaso no encontrado");
                        }
                    }
                    break;
                default:
                  $body=array("errno"=>400,"error"=>"Action invalida en metodo DELETE");
              }
            }
            break;
        default: 
            $body = array("errno"=>400,"errno"=>"Metodo no definido para api_transfer.php");
            break;
    }
    echo json_encode($body);
?>