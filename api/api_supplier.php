<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    include_once '../models/model_suppliers.php';
    include_once '../models/model_cities.php';

    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo GET");
            } else if($_GET['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_GET['action']){
                    case 'getSuppliers':
                        $getSuppliers=getAllSuppliers();
                        if($getSuppliers===NULL){
                            $body=array();
                        }else{
                            $body=$getSuppliers;
                        }
                        break;
                    /*
                        PROPOSITO: mostrar los datos del proveedor
                        PARAMETROS: 
                            -supplierID : ID del proveedor.
                        DEVUELVE: arreglo con los datos del proveedor
                    */
                    case 'getSupplierByID':
                        if(!isset($_GET['supplierID'])){
                            $body = array("errno" =>400, "error" => "supplierID no definido para getSupplierByID");
                        } else if($_GET['supplierID']==""){
                            $body = array("errno" =>400, "error" => "supplierID no tiene ningun valor");
                        } else {
                            if(supplierIDExists($_GET['supplierID'])){
                                $body = getSupplierByID($_GET['supplierID']);
                                $namecity = getCityByID($body['cityID']);
                                /*! A revisar */
                                $body['city'] = $namecity['name'];
                                unset($body['cityID']);
                            } else {
                                $body = array("errno" =>400, "error" => "El proveedor no existe");
                            }
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo GET");
                    break;
                }
            }
        break;
        case 'POST':
            if(!isset($_POST['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo POST");
            } else if($_POST['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_POST['action']){
                    /*
                        PROPOSITO: insertar un proveedor.
                        PARAMETROS: 
                            -suppliername : nombre del proveedor.
                            -contactName : nombre de contacto.
                            -addressStreet : direccion.
                            -addressNumber : numero de la direccion.
                            -cityID : ID de la ciudad.
                            -postalCode : codigo postal.
                            -phoneNumber : numero de telefono.
                        DEVUELVE: arreglo con el resultado de la insercion.
                    */
                    case 'createSupplier':
                        $data_supplier = array(
                            "supplierName" => "",
                            "contactName" => "",
                            "addressStreet" => "",
                            "addressNumber" => "",
                            "cityID" => "",
                            "postalCode" => "",
                            "phoneNumber" => "",
                        );
                        foreach($data_supplier as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para insertSupplier");
                                break;
                            } else if ($_POST[$key]==""){
                                if ($key == "phoneNumber"){
                                    $data_supplier[$key] = $_POST[$key];
                                } else {
                                    $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                }
                                break;
                            } else {
                                $data_supplier[$key] = $_POST[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $insertSupplier = insertSupplier($data_supplier);
                        if($insertSupplier===TRUE){
                            $body=array("errno"=>200,"error"=>"Proveedor ingresado exitosamente");
                        } else {
                            $body=array("errno"=>400,"error"=>$insertSupplier);
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo POST");
                    break;
                }
            }
        break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);

            if(!isset($_PUT['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo PUT");
            } else if($_PUT['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_PUT['action']){
                    /*
                        PROPOSITO: modificar los datos de un proveedor.
                        PARAMETROS: 
                            -suppliername : nombre del proveedor.
                            -contactName : nombre de contacto.
                            -addressStreet : direccion.
                            -addressNumber : numero de la direccion.
                            -cityID : ID de la ciudad.
                            -postalCode : codigo postal.
                            -phoneNumber : numero de telefono.
                        DEVUELVE: arreglo con el resultado de la modificacion.
                    */
                    case 'modifySupplierByID':
                        $data_editable = array(
                            "supplierName" => "",
                            "contactName" => "",
                            "addressStreet" => "",
                            "addressNumber" => "",
                            "cityID" => "",
                            "postalCode" => "",
                            "phoneNumber" => "",
                        );
                        foreach($data_editable as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 400, "error" => $key." no definido para modifySupplierByID");
                                break;
                            } else if($_PUT[$key]==""){
                                if($key == "phoneNumber"){
                                    $data_editable[$key] = $_PUT[$key];
                                } else {
                                    $body = array("errno" => 400, "error" => $key." no tiene ningun valor");
                                }
                                break;
                            } else {
                                $data_editable[$key] = $_PUT[$key];
                            }
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }

                        if(!isset($_PUT['supplierID'])){
                            $body = array("errno" =>400, "error" => "supplierID no definida para modifySupplierByID");
                        } else if($_PUT['supplierID']==""){
                            $body = array("errno" =>400, "error" => "supplierID no tiene ningun valor");
                        } else if(supplierIDExists($_PUT['supplierID'])){
                            $updateSupplierByID = updateSupplierByID($_PUT['supplierID'],$data_editable);
                            if ($updateSupplierByID === true){
                                $body = array("errno" =>200, "error" => "proveedor modificado con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $updateSupplierByID);
                            }
                        } else {
                            $body = array("errno" =>400, "error" => "El proveedor no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo PUT");
                    break;
                }
            }
        break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);

            if(!isset($_DELETE['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el metodo DELETE");
            } else if($_DELETE['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningun valor");
            } else {
                switch($_DELETE['action']){
                    /*
                        PROPOSITO: eliminar un proveedor.
                        PARAMETROS: 
                            -supplierID : ID del proveedor.
                        DEVUELVE: arreglo con la respuesta del delete.
                    */
                    case 'deleteSupplierByID':
                        if(!isset($_DELETE['supplierID'])){
                            $body = array("errno" =>400, "error" => "supplierID no definida para modifySupplierByID");
                        } else if($_DELETE['supplierID']==""){
                            $body = array("errno" =>400, "error" => "supplierID no tiene ningun valor");
                        } else if(supplierIDExists($_DELETE['supplierID'])){
                            $deleteSupplierByID = deleteSupplierByID($_DELETE['supplierID']);
                            if($deleteSupplierByID===true){
                                $body = array("errno" =>200, "error" => "proveedor eliminado con exito");
                            } else {
                                $body = array("errno" =>400, "error" => $deleteSupplierByID);
                            }
                        } else {
                            $body = array("errno" =>400, "error" => "El proveedor no existe");
                        }
                    break;
                    default:
                        $body = array("errno" =>400, "error" => "action no valida para el metodo DELETE");
                    break;
                }
            }
        break;
    }
    echo json_encode($body);

?>