<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
    date_default_timezone_set('America/Argentina/Buenos_Aires');

    include_once '../models/model_orders.php';
    include_once '../models/model_users.php';
    $body = array();
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno"=>400, "error"=>"Action no declarada para el metodo GET");
            }else if($_GET['action']==""){
                $body = array("errno"=>400, "error"=>"Action no tiene ningun valor");
            }else{
                switch($_GET["action"]){
                    case 'getOrders':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "Token no esta declarada");
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "El token no tiene ningun valor");
                        } else if(exists("users","token",$_GET['token'])){
                            $getOrders=getAllOrders();
                            if($getOrders===NULL){
                                $body = array();
                            }else{
                                $body = $getOrders;
                                foreach($body as $key => $value){
                                    $orderDetail = getOrderDetails($value['orderID']);
                                    $body[$key]["productList"] = $orderDetail;
                                }
                            }     
                        } else{
                            $body = array("errno" =>400, "error" => "No existe el usuario");
                        }
                        break;
                    case 'getOrderByID':
                        if(!isset($_GET['orderID'])){
                            $body = array("errno"=>400,"error"=>"orderID no definido para getOrderByID");
                        }else if(trim($_GET["orderID"]) == ''){
                            $body = array("errno"=>400,"error"=>"orderID vacio para getOrderByID");
                        }else{
                            $getOrders=getOrderByID($_GET['orderID']);
                            if($getOrders===NULL){
                                $body = array();
                            }else{
                                $body = $getOrders;
                            }
                        }
                        break;
                    default:
                        $body=array("errno"=>400,"error"=>"Action invalida para metodo GET");
                        break;
                }
            }
            break;
        case 'POST':
            //$_POST=json_decode(file_get_contents("php://input"));
            //$_POST=get_object_vars($_POST);
            //var_dump($_POST);
            if(!isset($_POST["action"])){
                $body = array("errno"=>400, "error"=>"Action no declarada para el metodo POST");
            }else{
                switch($_POST["action"]){
                    case 'createOrder': 
                        $data_order = array(
                            'token' => '',
                            'supplierID' => ''
                        );                        
                        foreach($data_order as $key => $value){
                            if(!isset($_POST[$key])){
                                $body=array("errno"=>400,"error"=>"No esta seteado el campo ".$key);
                                break;
                            }else{
                                if($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => "Esta vacio el campo ".$key);
                                    break;
                                } else {
                                    $data_order[$key] = $_POST[$key];
                                }
                            }
                            
                        }
                        $data_order["orderDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago aca porque si no tira error de que no le paso la fecha por POST
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $_POST["productList"]=json_decode($_POST["productList"]);
                        if(!isset($_POST["productList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de productos (productList)");
                            break;
                        }else{
                            if(empty($_POST["productList"])){
                                $body=array("errno"=>400,"error"=>"productList no puede estar vacio");
                            }else{
                                for ($i=0; $i < count($_POST['productList']) ; $i++) { 
                                    $_POST['productList'][$i]=get_object_vars($_POST['productList'][$i]);
                                    $data_orderdetail[$i] = array(
                                        'productID' => '',
                                        'amount' => '',
                                    );
                                    /*foreach($data_orderdetail[$i] as $key => $value){
                                        if(!isset($_POST["productList"][$i][$key])){
                                            $body=array("errno"=>400,"error"=>"No esta seteado el campo ".$key." del producto n°".$i);
                                            break;
                                        }else{
                                            if(($_POST['productList'][$i][$key])==""){
                                                $body = array("errno" => 400, "error" => "Esta vacio el campo ".$key." del producto n°".$i);
                                                break;
                                            } else {
                                                $data_orderdetail[$i][$key] = $_POST['productList'][$i][$key];
                                            }
                                        }
                                    }*/
                                    if(!isset($_POST["productList"][$i]["productID"])){
                                        $body=array("errno"=>400,"error"=>"No esta seteado el campo productID del producto n°".$i);
                                        break;
                                    }else if(!isset($_POST["productList"][$i]["amount"])){
                                        $body=array("errno"=>400,"error"=>"No esta seteado el campo productID del producto n°".$i);
                                        break;
                                    }else{
                                        if(($_POST['productList'][$i]["productID"])==""){
                                            $body = array("errno" => 400, "error" => "Esta vacio el campo productID del producto n°".$i);
                                            break;
                                        }else if (($_POST['productList'][$i]["amount"])==""){
                                            $body = array("errno" => 400, "error" => "Esta vacio el campo amount del producto n°".$i);
                                            break;
                                        } else {
                                            $data_orderdetail[$i]["productID"] = $_POST['productList'][$i]["productID"];
                                            $data_orderdetail[$i]["amount"] = $_POST['productList'][$i]["amount"];
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $data_order['userID']=getUserIDByToken($data_order['token']);
                        unset($data_order['token']);
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $addTransfer = insertTransfer($data_order);
                        if($addTransfer==true){
                            $body = array("errno" => 200, "error" => "Traspaso agregado correctamente");
                        }else{
                            $body = array("errno" => 400, "error" => $addTransfer);
                            break;
                            //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                        }
                        //var_dump($data_orderdetail);
                        if(count($data_orderdetail)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de traspaso para agregar la orden");
                            deleteTransferByID(getTransferMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_orderdetail) ; $i++) {
                                $data_orderdetail[$i]["orderID"]=getTransferMaxID();
                                //var_dump($data_orderdetail[$i]); 
                                $addTransferDetail = insertTransferDetail($data_orderdetail[$i]);
                                //var_dump($addTransferDetail);
                                if($addTransferDetail===true){
                                    $body = array("errno" => 200, "error" => "Traspaso y detalle del traspaso agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addTransferDetail);
                                    break;
                                    //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                                }
                            }
                        }
                        break;
                        default:
                            $body = array("errno"=>400,"error"=>"Accion invalida para metodo POST");
                }
            }
            break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);
            if(!isset($_PUT["action"])){
                $body = array("errno" => 400, "error" => "Action no declarada para el metodo PUT");
            } else {
                switch($_PUT["action"]){
                    case 'modifyOrder':
                        $data_editable = array(
                            'userID' => "",
                            'supplierID' => ""
                        );
                        foreach($_PUT as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 404, "error" => "No esta seteado el campo " .$key);
                            }else{
                            if(($_PUT[$key])==""){
                                $body = array("errno" => 404, "error" => "Falta el campo ".$key);
                                break;
                            } else {
                            if($key!="action" && $key!="orderID"){
                                //var_dump($value);
                                if($key=="userID" || $key=="supplierID"){
                                    $data_editable[$key] = $value;
                                }
                            }
                            }
                            }  
                        }
                        if(empty($data_editable)){
                            $body=array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: supplierID");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['orderID'])){
                            $body = array("errno" => 400, "error" => "orderID no declarado para modifyOrder");
                        } else {
                        if(orderIDExists($_PUT['orderID'])){
                            $updateOrder=updateOrderByID($_PUT['orderID'],$data_editable);
                            if($updateOrder===true){
                                $body = array("errno" => 200, "error" => "Orden modificada correctamente");
                            } else if ($updateOrder===false) {
                                $body = array("errno" => 300, "error" => "No se modifico nada");
                            }else{
                                $body = array("errno" => 400, "error" => $updateOrder);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => "Orden no encontrada");
                        }
                        }
                        break;
                    default:
                        $body = array("errno"=>400,"error"=>"Action invalida en metodo PUT");
                        break;
            }
            }
            break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);
            if(!isset($_DELETE["action"])){
              $body = array("errno" => 400, "error" => "Action no declarada para el metodo DELETE");
            } else {
              switch($_DELETE["action"]){
                case 'deleteOrder':
                  if(!isset($_DELETE['orderID'])){
                    $body = array("errno" => 400, "error" => "orderID no declarado");
                  } else {
                    if(orderIDExists($_DELETE['orderID'])){
                      $deleteOrder=deleteOrderByID($_DELETE['orderID']);
                      if($deleteOrder===true){
                        $body = array("errno" => 200, "error" => "Orden eliminada");
                      } else if ($deleteOrder===false) {
                        $body = array("errno" => 300, "error" => "No se elimino nada");
                      }else{
                        $body = array("errno" => 400, "error" => $deleteOrder);
                      }
                    } else {
                      $body = array("errno" => 400, "error" => "Orden no encontrada");
                    }
                  }
                break;
                default:
                  $body=array("errno"=>400,"error"=>"Action invalida en metodo DELETE");
              }
            }
            break;
        default: 
            $body = array("errno"=>400,"errno"=>"Metodo no definido para api_order.php");
            break;
    }
    echo json_encode($body);
?>