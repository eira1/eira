<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');

    foreach (glob("../models/*.php") as $filename) {
        include_once $filename;
    }
    
    switch($_SERVER['REQUEST_METHOD']){
        case 'GET':
            if(!isset($_GET['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el método GET");
            } else if($_GET['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningún valor");
            } else {
                switch($_GET['action']){
                    /*
                        PROPOSITO: mostrar los datos de una categoría.
                        PARAMETROS: 
                            -categoryID : ID de la categoría.
                        DEVUELVE: arreglo con los datos de la categoría.
                    */
                    case 'getCategoryByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getCategoryByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getCategoryByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getCategoryByID");
                        }else if(!isset($_GET['categoryID'])){
                            $body = array("errno" =>400, "error" => "categoryID no definida para getCategoryByID");
                        }else if($_GET['categoryID']==""){
                            $body = array("errno" =>400, "error" => "categoryID vacia para getCategoryByID");
                        }else{
                            if(categoryIDExists($_GET['categoryID'])){
                                $body = getCategoryByID($_GET['categoryID']);
                            } else {
                                $body = array("errno" =>400, "error" => "la category no existe");
                            }
                        }
                        break;
                    case 'getConsumptions':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getConsumptions");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getConsumptions");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getConsumptions");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))<=2){
                            $body=array("errno"=>403,"error"=>"No puede ver los consumos o descartes sin ser encargado de un sector");
                        }else{
                            $sectorID=getUserSector($_GET["token"]);
                            $getConsumptions=getConsumptionsBySector($sectorID);
                            if($getConsumptions===NULL){
                                $body = array();
                            }else if($getConsumptions===false){
                                $body = array();
                            }else{
                                $body = $getConsumptions;
                                foreach($body as $key => $value){
                                    $consumptionDetail = getConsumptionDetails($value['consumptionID']);
                                    $body[$key]["productList"] = $consumptionDetail;
                                    $consumptionUserName = getUserConsumptionName($value['consumptionID']);
                                    $body[$key]["userFirstName"] = $consumptionUserName["firstName"];
                                    $body[$key]["userLastName"] = $consumptionUserName["lastName"];
                                }
                            }     
                        }
                        break;
                    case 'getConsumptionByID':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getConsumptionByID");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getConsumptionByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getConsumptionByID");
                        }else if(!isset($_GET['consumptionID'])){
                            $body = array("errno"=>400,"error"=>"consumptionID no definido para getConsumptionByID");
                        }else if(trim($_GET["consumptionID"]) == ''){
                            $body = array("errno"=>400,"error"=>"consumptionID vacío para getConsumptionByID");
                        }else{
                            $getConsumptions=getConsumptionByID($_GET['consumptionID']);
                            if($getConsumptions===NULL){
                                $body = array();
                            }else{
                                $body = $getConsumptions;
                            }
                        }
                        break;
                    /*
                        PROPOSITO: mostrar los datos de todas las ciudades.
                        PARAMETROS: 
                            - no recibe parametros
                        DEVUELVE: arreglo con los datos de las ciudades.
                    */
                    case 'getCities':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getCities");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getCities");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getCities");
                        }else{
                            $getCities=getAllCities();
                            if($getCities===NULL){
                                $body = array();
                            }else{
                                $body = $getCities;
                            }
                        }
                        break;

                    /*
                        PROPOSITO: mostrar los datos de una ciudad.
                        PARAMETROS: 
                            -cityID : ID de la ciudad.
                        DEVUELVE: arreglo con los datos de la ciudad.
                    */    
                    case 'getCityByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getCityByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getCityByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getCityByID");
                        }else if(!isset($_GET["cityID"])){
                            $body = array("errno"=>400,"error"=>"cityID no definido para getCityByID");
                        }else if($_GET["cityID"]==""){
                            $body = array("errno"=>400,"error"=>"cityID vacío para getCityByID");
                        }else{
                            $getCity=getCityByID($_GET["cityID"]);
                            if($getCity===NULL){
                                $body = array();
                            }else{
                                $body = $getCity;
                            }
                        }
                        break;
                    case 'getLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes sin ser farmaceutico");
                        }else{
                            $getLotes=getAllLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getLotesByProductID':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getLotesByProductID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getLotesByProductID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getLotesByProductID");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No puede ver los lotes sin ser farmaceutico");
                        }else if(!isset($_GET["productID"])){
                            $body = array("errno"=>400,"error"=>"productID no definido para getLotesByProductID");
                        }else if($_GET["productID"]==""){
                            $body = array("errno"=>400,"error"=>"productID vacío para getLotesByProductID");
                        }else{
                            $getLotes=getLotesByProductID($_GET["productID"]);
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getLoteByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getLoteByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getLoteByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getLoteByID");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No puede ver los lotes sin ser farmaceutico");
                        }else if(!isset($_GET["loteID"])){
                            $body = array("errno"=>400,"error"=>"loteID no definido para getLoteByID");
                        }else if($_GET["loteID"]==""){
                            $body = array("errno"=>400,"error"=>"loteID vacío para getLoteByID");
                        }else{
                            $getLote=getLoteByID($_GET["loteID"]);
                            if($getLote===NULL){
                                $body = array();
                            }else{
                                $body = $getLote;
                            }
                        }
                        break;
                    case 'getExpiredLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getExpiredLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getExpiredLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getExpiredLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes vencidos sin ser farmaceutico");
                        }else{
                            $getLotes=getExpiredLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getExpiredProductLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getExpiredProductLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getExpiredProductLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getExpiredProductLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes vencidos sin ser farmaceutico");
                        }else{
                            $getLotes=getExpiredProductLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getAboutToExpireProductLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getAboutToExpireProductLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getAboutToExpireProductLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getAboutToExpireProductLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes vencidos sin ser farmaceutico");
                        }else{
                            $getLotes=getAboutToExpireProductLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getUnexpiredLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getUnexpiredLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getUnexpiredLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getUnexpiredLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes no vencidos sin ser farmaceutico");
                        }else{
                            $getLotes=getUnexpiredLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getEmptyLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getEmptyLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getEmptyLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getEmptyLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes vacíos sin ser farmaceutico");
                        }else{
                            $getLotes=getEmptyLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                    case 'getNonEmptyLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getNonEmptyLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getNonEmptyLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getNonEmptyLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes no vacíos sin ser farmaceutico");
                        }else{
                            $getLotes=getNonEmptyLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getNonEmptyLotesByProductID':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getNonEmptyLotesByProductID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getNonEmptyLotesByProductID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getNonEmptyLotesByProductID");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes no vacíos sin ser farmaceutico");
                        }else if(!isset($_GET["productID"])){
                            $body = array("errno" =>400, "error" => "productID no definido para getNonEmptyLotesByProductID");
                        }else if($_GET["productID"]==""){
                            $body = array("errno"=>400,"error"=>"productID vacio para getNonEmptyLotesByProductID");
                        }else if(!productIDExists($_GET["productID"])){
                            $body = array("errno"=>400,"error"=>"No existe producto que tenga ese ID para getNonEmptyLotesByProductID");
                        }else{
                            $getLotes=getNonEmptyLotesByProductID($_GET["productID"]);
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                        break;
                    case 'getEmptyOrExpiredLotes':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getEmptyOrExpiredLotes");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getEmptyOrExpiredLotes");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getEmptyOrExpiredLotes");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los lotes vacíos o vencidos sin ser farmaceutico");
                        }else{
                            $getLotes=getEmptyOrExpiredLotes();
                            if($getLotes===NULL){
                                $body = array();
                            }else{
                                $body = $getLotes;
                            }
                        }
                    case 'getOrders':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "token no definido para getOrders");
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "token vacio para getOrders");
                        } else if(!userTokenExists($_GET["token"])){
                            $body = array("errno" =>400, "error" => "No existe usuario con ese token para getOrders");
                        } else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No puede ver ingresos a la farmacia sin ser farmacéutico");
                        }else{
                            $getOrders=getAllOrders();
                            if($getOrders===NULL){
                                $body = array();
                            }else{
                                $body = $getOrders;
                                foreach($body as $key => $value){
                                    $orderDetail = getOrderDetails($value['orderID']);
                                    $body[$key]["productList"] = $orderDetail;
                                    $orderUserName = getUserOrderName($value['orderID']);
                                    $body[$key]["userFirstName"] = $orderUserName["firstName"];
                                    $body[$key]["userLastName"] = $orderUserName["lastName"];
                                    $orderLote = getOrderLotes($value['orderID']);
                                    $body[$key]["loteList"] = $orderLote;
                                }
                            }     
                        }
                        break;
                    /*case 'getOrderPDF':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getOrderPDF");
                        }else if(trim($_POST["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getOrderPDF");
                        }else if(!userTokenExists($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getOrderPDF");
                        }else if(getUserByToken($_POST["token"])["permissionID"]!=1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido ver el PDF de un inrgeso sin ser farmacéutico");
                        }else if(!isset($_POST["orderID"])){
                            $body = array("errno"=>400,"error"=>"orderID no definido para getOrderPDF");
                        }else if(trim($_POST["orderID"])==""){
                            $body = array("errno"=>400,"error"=>"orderID vacio para getOrderPDF");
                        }else if(!orderIDExists($_POST["orderID"])){
                            $body = array("errno"=>400,"error"=>"No existe ingreso con ese orderID para getOrderPDF");
                        }else{
                            $getOrderPDF=getOrderPDF($_POST["orderID"]);
                            if($getOrderPDF){
                                $body=array("errno"=>200,"error"=>"Redirigido al PDF");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al redirigir al PDF");
                            }
                        }
                        break;*/
                    case 'getOrderByID':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "token no definido para getOrderByID");
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "token vacio para getOrderByID");
                        } else if(!userTokenExists($_GET["token"])){
                            $body = array("errno" =>400, "error" => "No existe usuario con ese token para getOrderByID");
                        }else if(!isset($_GET['orderID'])){
                            $body = array("errno"=>400,"error"=>"orderID no definido para getOrderByID");
                        }else if(trim($_GET["orderID"]) == ''){
                            $body = array("errno"=>400,"error"=>"orderID vacío para getOrderByID");
                        }else{
                            $getOrders=getOrderByID($_GET['orderID']);
                            if($getOrders===NULL){
                                $body = array();
                            }else{
                                $body = $getOrders;
                            }
                        }
                        break;
                    case 'getOrderPDF':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getOrderPDF");
                        }else if(trim($_GET["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getOrderPDF");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getOrderPDF");
                        }else if(getUserByToken($_GET["token"])["permissionID"]!=1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido ver el PDF de un inrgeso sin ser farmacéutico");
                        }else if(!isset($_GET["orderID"])){
                            $body = array("errno"=>400,"error"=>"orderID no definido para getOrderPDF");
                        }else if(trim($_GET["orderID"])==""){
                            $body = array("errno"=>400,"error"=>"orderID vacio para getOrderPDF");
                        }else if(!orderIDExists($_GET["orderID"])){
                            $body = array("errno"=>400,"error"=>"No existe ingreso con ese orderID para getOrderPDF");
                        }else{
                            $getOrderPDF=getOrderPDF($_GET["orderID"]);
                            if($getOrderPDF){
                                $body=array("errno"=>200,"error"=>"Redirigido al PDF");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al redirigir al PDF");
                            }
                        }
                        break;
                    case 'getPermissionByID':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "token no definido para getPermissionByID");
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "token vacio para getPermissionByID");
                        } else if(!userTokenExists($_GET["token"])){
                            $body = array("errno" =>400, "error" => "No existe usuario con ese token para getPermissionByID");
                        }else if(!isset($_GET['permissionID'])){
                            $body = array("errno" =>400, "error" => "permissionID no definido para getPermissionByID");
                        } else if($_GET['permissionID']==""){
                            $body = array("errno" =>400, "error" => "permissionID no tiene ningún valor");
                        } else if(exists('permissions','permissionID',$_GET['permissionID'])){
                            $body = getPermissionByID($_GET['permissionID']);
                        } else {
                            $body = array("errno" =>400, "error" => "El permiso no existe");
                        }
                        break;
                    case 'getEncargadoPermissions':
                        $body=getEncargadoPermissions();
                        break;
                    case 'getProducts':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getProducts");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getProducts");
                        }else if (!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getProducts");
                        }else{
                            $getProducts=getAllProducts();
                            if($getProducts===NULL){
                                $body = array();
                            }else{
                                $body = $getProducts;
                            }
                        }
                        break;
                    case 'getProductByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getProductByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getProductByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getProductByID");
                        }else if(!isset($_GET["productID"])){
                            $body = array("errno"=>400,"error"=>"productID no definido para getProductByID");
                        }else if($_GET["productID"]==""){
                            $body = array("errno"=>400,"error"=>"productID vacío para getProductByID");
                        }else{
                            $getProduct=getProductByID($_GET["productID"]);
                            if($getProduct===NULL){
                                $body = array();
                            }else{
                                $body = $getProduct;
                            }
                        }
                        break;
                    case 'getProductsByText':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getProductsByText");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getProductsByText");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getProductsByText");
                        }else if(!isset($_GET["text"])){
                            $body = array("errno"=>400,"error"=>"text no definido para getProductsByText");
                        }else if($_GET["text"]==""){
                            $body = array("errno"=>400,"error"=>"text vacío para getProductsByText");
                        }else{
                            $getProducts=getProductsByText($_GET["text"]);
                            if($getProducts===NULL){
                                $body = array();
                            }else{
                                $body = $getProducts;
                            }
                        }
                        break;
                    case 'getProductsBySector':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getProductsBySector");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getProductsBySector");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getProductsBySector");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))==1){
                            if(!isset($_GET["sectorID"])){
                                $body = array("errno"=>400,"error"=>"sectorID no definido para getProductsBySector");
                            }else if($_GET["sectorID"]==""){
                                $body = array("errno"=>400,"error"=>"sectorID vacío para getProductsBySector");
                            }
                            $getProducts = getProductsStockBySector($_GET["sectorID"]);
                            if($getProducts===NULL){
                                $body = array();
                            }else{
                                $body = $getProducts;
                            }
                        }else{
                            $sectorID = getSectorByPermissionID(getUserPermissionsByID(getUserIDByToken($_GET["token"])));
                            $getProducts=getProductsStockBySector($sectorID);
                            if($getProducts===NULL){
                                $body = array();
                            }else{
                                $body = $getProducts;
                            }
                        }
                        break;
                    case 'countProductsByText':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para countProductsByText");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para countProductsByText");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para countProductsByText");
                        }else if(!isset($_GET["text"])){
                            $body = array("errno"=>400,"errno"=>"text no definido para countProductsByText");
                        }else if($_GET["text"]==""){
                            $body = array("errno"=>400,"errno"=>"text vacío para countProductsByText");
                        }else{
                            $countProducts=countProductsByText($_GET["text"]);
                            if($countProducts===NULL){
                                $body = array();
                            }else{
                                $body = $countProducts;
                            }
                        }
                        break;
                    case 'getRemovals':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getRemovals");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getRemovals");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getRemovals");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver las eliminaciones de lotes sin ser farmaceutico");
                        }else{
                            $getRemovals=getAllRemovals();
                            if($getRemovals===NULL){
                                $body = array();
                            }else{
                                $body = $getRemovals;
                            }
                        }
                        break;
                    case 'getRemovalByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno" =>400, "error" => "token no definido para getRemovalByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getRemovalByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario que tenga ese token para getRemovalByID");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))!=1){
                            $body = array("errno"=>403,"error"=>"No puede ver las eliminaciones sin ser farmaceutico");
                        }else if(!isset($_GET["removalID"])){
                            $body = array("errno"=>400,"error"=>"removalID no definido para getRemovalByID");
                        }else if($_GET["removalID"]==""){
                            $body = array("errno"=>400,"error"=>"removalID vacío para getRemovalByID");
                        }else{
                            $getRemoval=getRemovalByID($_GET["removalID"]);
                            if($getRemoval===NULL){
                                $body = array();
                            }else{
                                $body = $getRemoval;
                            }
                        }
                        break;
                    case 'getRequests':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getRequests");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getRequests");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getRequests");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))==1){
                            // ? En el caso de que el usuario sea farmaceutico, le devolvemos todos los pedidos
                            $getRequests=getAllPendingRequests();
                            if($getRequests===NULL){
                                $body = array();
                            }else{
                                $body = $getRequests;
                                foreach($body as $key => $value){
                                    $requestDetail = getRequestDetails($value['requestID']);
                                    $body[$key]["productList"] = $requestDetail;
                                    $requestUserName = getUserRequestName($value['requestID']);
                                    $body[$key]["userFirstName"] = $requestUserName["firstName"];
                                    $body[$key]["userLastName"] = $requestUserName["lastName"];
                                }
                            }     
                        }else{
                            // ? En el caso de que sea encargado de un sector, le devolvemos los encargos que ya hizo su sector
                            $sectorID = getSectorByPermissionID(getUserPermissionsByID(getUserIDByToken($_GET["token"])));
                            $getRequests = getAllSectorRequests($sectorID);
                            if($getRequests===NULL){
                                $body = array();
                            }else{
                                $body = $getRequests;
                                foreach($body as $key => $value){
                                    $requestDetail = getRequestDetails($value['requestID']);
                                    $body[$key]["productList"] = $requestDetail;
                                    $requestUserName = getUserRequestName($value['requestID']);
                                    $body[$key]["userFirstName"] = $requestUserName["firstName"];
                                    $body[$key]["userLastName"] = $requestUserName["lastName"];
                                }
                            }
                        }
                        break;
                    case 'getPendingRequests':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getPendingRequests");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getPendingRequests");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getRequests");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))==1){
                            // ? En el caso de que el usuario sea farmaceutico, le devolvemos todos los pedidos
                            $getRequests=getAllPendingRequests();
                            if($getRequests===NULL){
                                $body = array();
                            }else{
                                $body = $getRequests;
                                foreach($body as $key => $value){
                                    $requestDetail = getRequestDetails($value['requestID']);
                                    $body[$key]["productList"] = $requestDetail;
                                    $requestUserName = getUserRequestName($value['requestID']);
                                    $body[$key]["userFirstName"] = $requestUserName["firstName"];
                                    $body[$key]["userLastName"] = $requestUserName["lastName"];
                                }
                            }     
                        }else{
                            $body = array("errno"=>400,"error"=>"No puede ver los pedidos pendientes como encargado");
                        }
                    case 'getNonPendingRequests':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getNonPendingRequests");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getNonPendingRequests");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getRequests");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))==1){
                            // ? En el caso de que el usuario sea farmaceutico, le devolvemos todos los pedidos
                            $getRequests=getAllNonPendingRequests();
                            if($getRequests===NULL){
                                $body = array();
                            }else{
                                $body = $getRequests;
                                foreach($body as $key => $value){
                                    $requestDetail = getRequestDetails($value['requestID']);
                                    $body[$key]["productList"] = $requestDetail;
                                    $requestUserName = getUserRequestName($value['requestID']);
                                    $body[$key]["userFirstName"] = $requestUserName["firstName"];
                                    $body[$key]["userLastName"] = $requestUserName["lastName"];
                                }
                            }     
                        }else{
                            $body = array("errno"=>400,"error"=>"No puede ver los pedidos no pendientes como encargado");
                        }
                        break;
                    case 'getRequestByID':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getRequestByID");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getRequestByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getRequestByID");
                        }else if(!isset($_GET['requestID'])){
                            $body = array("errno"=>400,"error"=>"requestID no definido para getRequestByID");
                        }else if(trim($_GET["requestID"]) == ''){
                            $body = array("errno"=>400,"error"=>"requestID vacío para getRequestByID");
                        }else{
                            $getRequests=getRequestByID($_GET['requestID']);
                            if($getRequests===NULL){
                                $body = array();
                            }else{
                                $body = $getRequests;
                            }
                        }
                        break;
                    case "getSectionByID":
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getSectionByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getSectionByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para para getSectionByID");
                        }else if(!isset($_GET['sectionID'])){
                            $body = array("errno" =>400, "error" => "sectionID no definida para getSectionByID");
                        } else if($_GET['sectionID']==""){
                            $body = array("errno" =>400, "error" => "sectionID no tiene valor para getSectionByID");
                        } else {
                            if(sectionIDExists($_GET['sectionID'])){
                                $body = getSectionByID($_GET['sectionID']);
                                $namesubcategory = getSubcategoryByID($body['subcategoryID']);
                                $body['subcategory'] = $namesubcategory['name'];
                                unset($body['subcategoryID']);
                            } else {
                                $body = array("errno" =>400, "error" => "la seccion no existe");
                            }
                        }
                        break;
                    case 'getSectors':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getSectors");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getSectors");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para para getSectors");
                        }else{
                            $getSectors=getAllSectors();
                            if($getSectors===NULL){
                                $body =array();
                            }else{
                                $body =$getSectors;
                            }
                        }
                        break;
                    case 'getSectorByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getSectorByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getSectorByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para para getSectorByID");
                        }else if(!isset($_GET["sectorID"])){
                            $body = array("errno"=>400,"error"=>"sectorID no definido para getSectorByID");
                        }else if($_GET["sectorID"]==""){
                            $body = array("errno"=>400,"error"=>"sectorID vacío para getSectorByID");
                        }else{
                            $getSector=getSectorByID($_GET["sectorID"]);
                            if($getSector===NULL){
                                $body =array();
                            }else{
                                $body = $getSector;
                            }
                        }
                        break;
                    case 'getSubcategoryByID':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getSubcategoryByID");
                        }else if($_GET["token"]==""){
                            $body = array("errno"=>400,"error"=>"token vacío para getSubcategoryByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para para getSubcategoryByID");
                        }else if(!isset($_GET['subcategoryID'])){
                            $body = array("errno" =>400, "error" => "subcategoryID no definido para getSubcategoryByID");
                        } else if($_GET['subcategoryID']==""){
                            $body = array("errno" =>400, "error" => "subcategoryID no tiene ningún valor para getSubcategoryByID");
                        } else if(!subcategoryIDExists($_GET['subcategoryID'])){
                            $body = array("errno" =>400, "error" => "la subcategoría no existe para getSubcategoryByID");
                        } else {
                            $body = getSubcategoryByID($_GET['subcategoryID']);
                        }
                        break;
                    case 'getSuppliers':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getSuppliers");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getSuppliers");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getSuppliers");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))>=2){
                            $body = array("errno"=>403,"error"=>"No tiene permitido ver los proveedores sin ser farmacéutico");
                        }else{
                            $getSuppliers=getAllSuppliers();
                            if($getSuppliers===NULL){
                                $body=array();
                            }else{
                                $body=$getSuppliers;
                            }
                        }
                        break;
                    /*
                        PROPOSITO: mostrar los datos del proveedor
                        PARAMETROS: 
                            -supplierID : ID del proveedor.
                        DEVUELVE: arreglo con los datos del proveedor
                    */
                    case 'getSupplierByID':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getSupplierByID");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getSupplierByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getSupplierByID");
                        }else if(!isset($_GET['supplierID'])){
                            $body = array("errno" =>400, "error" => "supplierID no definido para getSupplierByID");
                        } else if($_GET['supplierID']==""){
                            $body = array("errno" =>400, "error" => "supplierID no tiene ningún valor para getSupplierByID");
                        } else {
                            if(!supplierIDExists($_GET['supplierID'])){
                                $body = array("errno" =>400, "error" => "El proveedor no existe");
                            } else {
                                $body = getSupplierByID($_GET['supplierID']);
                                $namecity = getCityByID($body['cityID']);
                                //! A revisar 
                                $body['city'] = $namecity['name'];
                                unset($body['cityID']);
                            }
                        }
                        break;
                    case 'getTransfers':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getTransfers");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getTransfers");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getTransfers");
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))==1){
                            $getTransfers=getAllTransfers();
                            if($getTransfers===NULL){
                                $body = array();
                            }else{
                                $body = $getTransfers;
                                foreach($body as $key => $value){
                                    $transferDetail = getTransferDetails($value['transferID']);
                                    $body[$key]["productList"] = $transferDetail;
                                    $transferUserName = getUserTransferName($value['transferID']);
                                    $body[$key]["userFirstName"] = $transferUserName["firstName"];
                                    $body[$key]["userLastName"] = $transferUserName["lastName"];
                                    $transferLote = getTransferLotes($value['transferID']);
                                    $body[$key]["loteList"] = $transferLote;
                                }
                            }     
                        }else if(getUserPermissionsByID(getUserIDByToken($_GET["token"]))>=3){
                            $getTransfers=getAllTransfersBySector(getUserSector($_GET["token"]));
                            if($getTransfers===NULL){
                                $body = array();
                            }else{
                                $body = $getTransfers;
                                foreach($body as $key => $value){
                                    $transferDetail = getTransferDetails($value['transferID']);
                                    $body[$key]["productList"] = $transferDetail;
                                    $transferUserName = getUserTransferName($value['transferID']);
                                    $body[$key]["userFirstName"] = $transferUserName["firstName"];
                                    $body[$key]["userLastName"] = $transferUserName["lastName"];
                                    $transferLote = getTransferLotes($value['transferID']);
                                    $body[$key]["loteList"] = $transferLote;
                                }
                            }
                        }else{
                            $body = array("errno"=>403,"error"=>"No puede ver las transferencias sin ser farmacéutico o encargado de un sector");
                        }
                        break;
                    case 'getTransferByID':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para getTransferByID");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para getTransferByID");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para getTransferByID");
                        }else if(!isset($_GET['transferID'])){
                            $body = array("errno"=>400,"error"=>"transferID no definido para getTransferByID");
                        }else if(trim($_GET["transferID"]) == ''){
                            $body = array("errno"=>400,"error"=>"transferID vacío para getTransferByID");
                        }else{
                            $getTransfers=getTransferByID($_GET['transferID']);
                            if($getTransfers===NULL){
                                $body = array();
                            }else{
                                $body = $getTransfers;
                            }
                        }
                        break;
                        case 'getTransferPDF':
                            if(!isset($_GET["token"])){
                                $body = array("errno"=>400,"error"=>"token no definido para getTransferPDF");
                            }else if(trim($_GET["token"])==""){
                                $body = array("errno"=>400,"error"=>"token vacio para getTransferPDF");
                            }else if(!userTokenExists($_GET["token"])){
                                $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getTransferPDF");
                            }else if(getUserByToken($_GET["token"])["permissionID"]!=1){
                                $body=array("errno"=>403,"error"=>"No tiene permitido ver el PDF de un egreso sin ser farmacéutico");
                            }else if(!isset($_GET["transferID"])){
                                $body = array("errno"=>400,"error"=>"transferID no definido para getTransferPDF");
                            }else if(trim($_GET["transferID"])==""){
                                $body = array("errno"=>400,"error"=>"transferID vacio para getTransferPDF");
                            }else if(!transferIDExists($_GET["transferID"])){
                                $body = array("errno"=>400,"error"=>"No existe egreso con ese transferID para getTransferPDF");
                            }else{
                                $getTransferPDF=getTransferPDF($_GET["transferID"]);
                                if($getTransferPDF){
                                    $body=array("errno"=>200,"error"=>"Redirigido al PDF");
                                }else{
                                    $body=array("errno"=>400,"error"=>"Ocurrió un error al redirigir al PDF");
                                }
                            }
                            break;
                    case 'getUserDataByToken':
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "Token no está declarada");
                        } else if($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "El token no tiene ningún valor");
                        } else if(exists("users","token",$_GET['token'])){
                            $data_usuario = array(
                                'userID' =>"",
                                'dni' => "",
                                'firstName' => "",
                                'lastName' => "",
                                'medicalLicense' => "",
                                'phoneNumber' => "",
                                'email'=>"",
                                'birthDate'=>"",
                                'permissionID' => "",
                                'pass'=>"",
                                'active'=>"",
                                'activationCode'=>"",
                                'activationExpiry'=>"",
                                'activationTime'=>"",
                                'creationDate'=>"",
                                'photo'=>""
                            );
                            $data_A = array(getSomethingByParameter("users","token",$_GET['token'])[0])[0];
                            $body = array(array_intersect_key($data_A, $data_usuario))[0];
                        } else {
                            $body = array("errno" =>400, "error" => "Usuario no encontrado");
                        }
                    break;
                    /*
                        Recibe el token de un usuario;
                        Devuelve un mensaje si el token no se encuentra declarada o si no encuentra al usuario;
                        Devuelve los datos modificables del usuario;
                    */
                    case 'getUserModifiableDataByToken':
                        $body = array("errno" =>400, "error" => "mymodifiabledata");
                        if(!isset($_GET['token'])){
                            $body = array("errno" =>400, "error" => "Token no está declarado");
                        } else if ($_GET['token']==""){
                            $body = array("errno" =>400, "error" => "El token no tiene ningún valor");
                        } else if(exists("users","token",$_GET['token'])){
                            $data_editable = array(
                                'dni' => "",
                                'firstName' => "",
                                'lastName' => "",
                                'medicalLicense' => "",
                                'phoneNumber' => "",
                                'birthDate' => "",
                                'photo' => ""
                            );
                            $data_A = array(getSomethingByParameter("users","token",$_GET['token'])[0])[0];
                            $body = array(array_intersect_key($data_A,$data_editable))[0];
                        } else {
                            $body = array("errno" =>400, "error" => "Usuario no encontrado");
                        }
                        break;
                    case 'getUnverifiedUsers':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getUnverifiedUsers");
                        }else if(trim($_GET["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getUnverifiedUsers");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getUnverifiedUsers");
                        }else if(getUserByToken($_GET["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido ver getUnverifiedUsers como usuario normal");
                        }else{
                            $getUnverifiedUsers=getUnverifiedUsers();
                            if($getUnverifiedUsers===NULL){
                                $body=array();
                            }else{
                                $body=$getUnverifiedUsers;
                            }
                        }
                        break;
                    case 'getVerifiedUsers':
                        if(!isset($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para getVerifiedUsers");
                        }else if(trim($_GET["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para getVerifiedUsers");
                        }else if(!userTokenExists($_GET["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getVerifiedUsers");
                        }else if(getUserByToken($_GET["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido ver getVerifiedUsers como usuario normal");
                        }else{
                            $getVerifiedUsers=getVerifiedUsers();
                            if($getVerifiedUsers===NULL){
                                $body=array();
                            }else{
                                $body=$getVerifiedUsers;
                            }
                        }
                        break;
                    case 'fileExists':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para fileExists");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para fileExists");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para fileExists");
                        }else if(!isset($_GET["url"])){
                            $body =array("errno"=>400,"error"=>"Falta url para fileExists");
                        }else if($_GET["url"]==""){
                            $body =array("errno"=>400,"error"=>"url vacía para fileExists");
                        }else{
                            $fileExists=fileExists($_GET["url"]);
                            if($fileExists){
                                $body =array("errno"=>200,"error"=>"El archivo existe");
                            }else{
                                $body =array("errno"=>404,"error"=>"El archivo no existe");
                            }
                        }
                        break;
                    // ! Empiezo con una maqueta para obtener las estadisticas de gasto sobre productos
                    // ! Por default devuelve la estadistica mensual, en caso de necesitar un año o X meses
                    // ! Debera enviar un segundo parametro months con la respectiva cantidad de meses
                    // ! Todavía no existe la función getStatisticsByProductID
                    // ! Por ahora esta desarrollada para devolver el promedio de productos gastados por día
                    case 'getTransfersStatistics':
                        if(!isset($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"token no definido para obtener estadísticas");
                        }else if($_GET["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para obtener estadísticas");
                        }else if(!userTokenExists($_GET["token"])){
                            $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para obtener estadísticas");
                        }else if(isset($_GET["productID"])){
                            $productID = $_GET["productID"];
                            if(!isset($_GET['finishDate']) || !isset($_GET['startDate'])){
                                $body=array("errno"=>400,"error"=>"Debe ingresar una fecha de inicio, y una fecha final.");
                                break;
                            }else{
                                $startDate = $_GET['startDate'];
                                $finishDate = $_GET['finishDate'];
                                $statistics = getStatisticsByProductID($productID,$startDate,$finishDate);
                                $body = array("errno"=>200,"statistics"=>$statistics);
                            }
                        }else{
                            $body=array("errno"=>400,"error"=>"producto vacio para obtener estadísticas");
                        }
                        break;
                        case 'getMostSpentProducts':
                            if(!isset($_GET["token"])){
                                $body=array("errno"=>400,"error"=>"token no definido para obtener estadísticas");
                            }else if($_GET["token"]==""){
                                $body=array("errno"=>400,"error"=>"token vacio para obtener estadísticas");
                            }else if(!userTokenExists($_GET["token"])){
                                $body=array("errno"=>400,"error"=>"No hay usuarios con ese token para obtener estadísticas");
                            }else{
                                $body = array("errno" => 200, "statistics" => getStatisticsMostSpentProducts(),"statisticsY"=>getStatisticsY());
                            }
                            break;
                        //----------------------------------------------------------------------------------------------------------------------------------------------------------------
                    default:
                        $body = array("errno" =>400, "error" => "Action inválida para el método GET");
                    break;
                }
            }
        break;
        case 'POST':
            if(!isset($_POST['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el método POST");
            } else if($_POST['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningún valor");
            } else {
                switch($_POST['action']){
                    /*
                        PROPOSITO: insertar una nueva categoría.
                        PARAMETROS: 
                            -name : nombre de la categoría.
                            -description: descripcion de la categoría.
                        DEVUELVE: arreglo con el resultado de la insercion.
                    */
                    case 'createCategory':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createCategory");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createCategory");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createCategory");
                        }else if(getUserByToken($_POST["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear categoria como usuario normal");
                        }else{
                            $data_Category = array(
                                "name" => "",
                                "description" => ""
                            );
                            foreach($data_Category as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para insertCategory");
                                    break;
                                } else if ($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    break;
                                } else {
                                    $data_Category[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertCategory = insertCategory($data_Category);
                            if($insertCategory===TRUE){
                                $body =array("errno"=>200,"error"=>"categoría ingresada exitosamente");
                            } else {
                                $body =array("errno"=>400,"error"=>$insertCategory);
                            }
                        }
                        break;
                    case 'createCity':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createCity");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createCity");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createCity");
                        }else if(getUserByToken($_POST["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear categoria como usuario normal");
                        }else{
                            $data_city=array(
                                "name"=>""
                            );
                            foreach($data_city as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para createCity");
                                    break;
                                } else if ($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." vacío para createCity");
                                    break;
                                } else {
                                    $data_city[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertCity = insertCity($data_city);
                            if($insertCity===TRUE){
                                $body =array("errno"=>200,"error"=>"Ciudad ingresada exitosamente");
                            }else{
                                $body =array("errno"=>400,"error"=>$insertCity);
                            }
                        }
                        break;
                    case 'createConsumption':
                        $data_consumption = array(
                            'token' => ''
                        );                        
                        foreach($data_consumption as $key => $value){
                            if(!isset($_POST[$key])){
                                $body =array("errno"=>400,"error"=>"No está seteado el campo ".$key);
                                break;
                            }else{
                                if($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => "Está vacío el campo ".$key);
                                    break;
                                } else {
                                    $data_consumption[$key] = $_POST[$key];
                                }
                            }
                            
                        }
                        //!No chequeado
                        if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createConsumption");
                        }
                        //!No chequado
                        $data_consumption["consumptionDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago aca porque si no tira error de que no le paso la fecha por POST
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $_POST["productList"]=json_decode($_POST["productList"]);
                        $_POST["productList"]=duplicateProducts($_POST["productList"]);
                        if(!isset($_POST["productList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de productos (productList)");
                            break;
                        }else{
                            if(empty($_POST["productList"])){
                                $body =array("errno"=>400,"error"=>"productList no puede estar vacío");
                            }else{
                                for ($i=0; $i < count($_POST['productList']) ; $i++) { 
                                    $_POST['productList'][$i]=get_object_vars($_POST['productList'][$i]);
                                    $data_consumptiondetail[$i] = array(
                                        'productID' => '',
                                        'amount' => '',
                                    );
                                    if(!isset($_POST["productList"][$i]["productID"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i);
                                        break;
                                    }else if(!isset($_POST["productList"][$i]["amount"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i);
                                        break;
                                    }else{
                                        if(($_POST['productList'][$i]["productID"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo productID del producto n°".$i);
                                            break;
                                        }else if (($_POST['productList'][$i]["amount"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo amount del producto n°".$i);
                                            break;
                                        } else {
                                            $sectorID=getUserSector($_POST["token"]);
                                            $productID=$_POST['productList'][$i]["productID"];
                                            $stock = getProductStockAmountBySectorAndProduct($sectorID,$productID);
                                            if($_POST["productList"][$i]["amount"]>$stock){
                                                $body =array("errno"=>400,"error"=>"Está intentando registrar el consumo o descarte de una cantidad mas grande de la que hay en stock en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else if($_POST["productList"][$i]["amount"]<0){
                                                $body =array("errno"=>400,"error"=>"Está intentando registrar el consumo o descarte de una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else if($_POST["productList"][$i]["amount"]==0){
                                                $body =array("errno"=>400,"error"=>"Está intentando registrar el consumo o descarte de una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else{
                                                $data_consumptiondetail[$i]["productID"] = $_POST['productList'][$i]["productID"];
                                                $data_consumptiondetail[$i]["amount"] = $_POST['productList'][$i]["amount"];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $data_consumption['userID']=getUserIDByToken($data_consumption['token']);
                        $data_consumption['sectorID']=getUserSector($_POST["token"]);
                        unset($data_consumption['token']);
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $addConsumption = insertConsumption($data_consumption);
                        if($addConsumption==true){
                            $body = array("errno" => 200, "error" => "Consumo o descarte agregado correctamente");
                        }else{
                            $body = array("errno" => 400, "error" => $addConsumption);
                            break;
                            //Se guarda el error en un string si hay error en insertConsumption, por eso muestro $addConsumption
                        }
                        if(count($data_consumptiondetail)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de consumo o descarte para agregar la orden");
                            deleteConsumptionByID(getConsumptionMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_consumptiondetail) ; $i++) {
                                $data_consumptiondetail[$i]["consumptionID"]=getConsumptionMaxID();
                                $addConsumptionDetail = insertConsumptionDetail($data_consumptiondetail[$i]);
                                if($addConsumptionDetail===true){
                                    $body = array("errno" => 200, "error" => "Consumo o descarte y detalle del consumo o descarte agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addConsumptionDetail);
                                    break;
                                    //Se guarda el error en un string si hay error en insertConsumptionDetail, por eso muestro $addConsumptionDetail
                                }
                            }
                        }
                        break;
                    case 'createOrder': 
                        $data_order = array(
                            'token' => '',
                            'supplierID' => ''
                        );                        
                        foreach($data_order as $key => $value){
                            if(!isset($_POST[$key])){
                                $body =array("errno"=>400,"error"=>"No está seteado el campo ".$key);
                                break;
                            }else{
                                if($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => "Está vacío el campo ".$key);
                                    break;
                                } else {
                                    $data_order[$key] = $_POST[$key];
                                }
                            }
                            
                        }
                        if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createOrder");
                        }
                        if(getUserByToken($_POST["token"])["permissionID"]!=1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear ordenes y lotes de ordenes sin ser farmacéutico");
                        }
                        $data_order["orderDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago aca porque si no tira error de que no le paso la fecha por POST
                        if(isset($body["errno"])){
                            if($body["errno"]>=400){
                                break;
                            }
                        }
                        //!$_POST["productList"]=json_decode($_POST["productList"]);
                        //!$_POST["productList"]=duplicateProducts($_POST["productList"]);
                        /*if(!isset($_POST["productList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de productos (productList)");
                            break;
                        }else{
                            if(empty($_POST["productList"])){
                                $body =array("errno"=>400,"error"=>"productList no puede estar vacío");
                            }else{
                                for ($i=0; $i < count($_POST['productList']) ; $i++) { 
                                    $_POST['productList'][$i]=get_object_vars($_POST['productList'][$i]);
                                    $data_orderdetail[$i] = array(
                                        'productID' => '',
                                        'amount' => '',
                                    );
                                    //foreach($data_orderdetail[$i] as $key => $value){
                                    //    if(!isset($_POST["productList"][$i][$key])){
                                    //        $body =array("errno"=>400,"error"=>"No está seteado el campo ".$key." del producto n°".$i);
                                    //        break;
                                    //    }else{
                                    //        if(($_POST['productList'][$i][$key])==""){
                                    //            $body = array("errno" => 400, "error" => "Está vacío el campo ".$key." del producto n°".$i);
                                    //            break;
                                    //        } else {
                                    //            $data_orderdetail[$i][$key] = $_POST['productList'][$i][$key];
                                    //        }
                                    //    }
                                    //}
                                    if(!isset($_POST["productList"][$i]["productID"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i);
                                        break;
                                    }else if(!isset($_POST["productList"][$i]["amount"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i);
                                        break;
                                    }else{
                                        if(($_POST['productList'][$i]["productID"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo productID del producto n°".$i);
                                            break;
                                        }else if (($_POST['productList'][$i]["amount"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo amount del producto n°".$i);
                                            break;
                                        } else {
                                            $data_orderdetail[$i]["productID"] = $_POST['productList'][$i]["productID"];
                                            $data_orderdetail[$i]["amount"] = $_POST['productList'][$i]["amount"];
                                        }
                                    }
                                }
                            }
                        }*/
                        if(!isset($_POST["loteList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de lotes (loteList)");
                            break;
                        }else{
                            $_POST["loteList"]=json_decode($_POST["loteList"]);
                            if(empty($_POST["loteList"])){
                                $body =array("errno"=>400,"error"=>"No cargo ningún lote");
                            }else{
                                //Asigno la cantidad de lotes a una variable
                                $cant = count($_POST['loteList']);

                                for ($i=0; $i < $cant ; $i++) { 
                                    $_POST['loteList'][$i]=get_object_vars($_POST['loteList'][$i]);
                                    $data_orderlote[$i] = array(
                                        'productID' => '',
                                        'loteCode' => '',
                                        'expirationDate' => '',
                                        'totalAmount'=>''
                                    );
                                    //var_dump($_POST);
                                    if(!isset($_POST["loteList"][$i]["productID"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i/* +1 */);
                                        break;
                                    }else if(!isset($_POST["loteList"][$i]["loteCode"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo loteCode del producto n°".$i/* +1 */);
                                        break;
                                    }else if(!isset($_POST["loteList"][$i]["expirationDate"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo expirationDate del producto n°".$i/* +1 */);
                                        break;
                                    }else if(!isset($_POST["loteList"][$i]["totalAmount"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo totalAmount del producto n°".$i/* +1 */);
                                        break;
                                    }else{
                                        if(trim($_POST['loteList'][$i]["productID"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo productID del producto n°".$i/* +1 */);
                                            break;
                                        }else if (trim($_POST['loteList'][$i]["loteCode"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo loteCode del producto n°".$i/* +1 */);
                                            break;
                                        }else if (trim($_POST['loteList'][$i]["expirationDate"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo expirationDate del producto n°".$i/* +1 */);
                                            break;
                                        }else if (trim($_POST['loteList'][$i]["totalAmount"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo totalAmount del producto n°".$i/* +1 */);
                                            break;
                                        } else {
                                            if($_POST["loteList"][$i]["totalAmount"]<0){
                                                $body =array("errno"=>400,"error"=>"Está intentando ingresar una cantidad negativa en el lote ".$_POST["loteList"][$i]["loteCode"]);
                                            }else if($_POST["loteList"][$i]["totalAmount"]==0){
                                                $body =array("errno"=>400,"error"=>"Está intentando ingresar una cantidad nula en el lote ".$_POST["loteList"][$i]["loteCode"]);
                                            }else if($_POST["loteList"][$i]["totalAmount"] >= 100000){
                                                $body =array("errno"=>400,"error"=>"No puede generar un ingreso con una cantidad superior a 100000 lote ".$_POST["loteList"][$i]["loteCode"]);
                                                break;
                                            }else if(strlen($_POST["loteList"][$i]["loteCode"]) >= 8){
                                                $body =array("errno"=>400,"error"=>"El código de lote no puede superar los 8 carácteres lote ".$_POST["loteList"][$i]["loteCode"]);
                                                break;
                                            }else{
                                                $data_orderlote[$i]["productID"] = $_POST['loteList'][$i]["productID"];
                                                $data_orderlote[$i]["loteCode"] = strtoupper($_POST['loteList'][$i]["loteCode"]);
                                                $data_orderlote[$i]["expirationDate"] = $_POST['loteList'][$i]["expirationDate"];
                                                $data_orderlote[$i]["totalAmount"] = $_POST['loteList'][$i]["totalAmount"];
                                                $data_orderlote[$i]["realAmount"]= $_POST['loteList'][$i]["totalAmount"];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        //?---------------------------------------------------------------
                        $data_orderdetail=turnOrderLotesOnOrderDetails($data_orderlote);
                        //?---------------------------------------------------------------
                        $data_order['userID']=getUserIDByToken($data_order['token']);
                        unset($data_order['token']);
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $addOrder = insertOrder($data_order);
                        if($addOrder==true){
                            $body = array("errno" => 200, "error" => "Orden agregada correctamente");
                        }else{
                            $body = array("errno" => 400, "error" => $addOrder . "linea 1232");
                            break;
                            //Se guarda el error en un string si hay error en insertOrder, por eso muestro $addorder
                        }
                        if(count($data_orderlote)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de lote para agregar la orden");
                            deleteOrderByID(getOrderMaxID());
                            break;
                        }else{
                            for($y=0;$y<count($data_orderlote);$y++){
                                $addLote=insertLote($data_orderlote[$y]);
                                if($addLote===true){
                                    $body = array("errno" => 200, "error" => "Orden y lotes de la orden agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addLote . "linea 1246");
                                    break;
                                }
                                $newLoteID=getLotesMaxID();
                                $data_orderlote[$y]["orderID"]=getOrderMaxID();
                                $addOrderLote=insertOrderLote(array("orderID"=>$data_orderlote[$y]["orderID"],"loteID"=>$newLoteID));
                                if($addOrderLote===true){
                                    $body = array("errno" => 200, "error" => "Orden y detalles de lotes de la orden agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addOrderLote . "linea 1255");
                                    break;
                                }
                            }
                        }
                        if(count($data_orderdetail)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de orden para agregar la orden");
                            deleteOrderByID(getOrderMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_orderdetail) ; $i++) {
                                $data_orderdetail[$i]["orderID"]=getOrderMaxID();
                                $addOrderDetail = insertOrderDetail($data_orderdetail[$i]);
                                if($addOrderDetail===true){
                                    $body = array("errno" => 200, "error" => "Orden y detalle de la orden y lotes de la orden agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addOrderDetail. "linea 1271");
                                    break;
                                }
                            }
                        }
                        break;
                    case 'createOrderLotes': // Recibe token, orderID, y loteList
                        /*if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createOrderLotes");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createOrderLotes");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createOrderLotes");
                        }else if(getUserByToken($_POST["token"])["permissionID"]!=1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear lotes y lotes de ordenes sin ser farmacéutico");
                        }else{
                            if(!isset($_POST["orderID"])){
                                $body = array("errno"=>400,"error"=>"orderID indefinido para createOrderLotes");
                            }else if($_POST["orderID"]==""){
                                $body=array("errno"=>400,"error"=>"orderID vacio para createOrderLotes");
                            }else
                            if(!isset($_POST["loteList"])){
                                $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de lotes (loteList)");
                                break;
                            }else{
                                $_POST["loteList"]=json_decode($_POST["loteList"]);
                                if(empty($_POST["loteList"])){
                                    $body =array("errno"=>400,"error"=>"No cargo ningún lote");
                                }else{
                                    //Asigno la cantidad de productos a una variable
                                    $cant = count($_POST['loteList']);
    
                                    for ($i=0; $i < $cant ; $i++) { 
                                        $_POST['loteList'][$i]=get_object_vars($_POST['loteList'][$i]);
                                        $data_orderlote[$i] = array(
                                            'productID' => '',
                                            'loteCode' => '',
                                            'expirationDate' => '',
                                            'totalAmount'=>''
                                        );
                                        if(!isset($_POST["loteList"][$i]["loteID"])){
                                            $body =array("errno"=>400,"error"=>"No está seteado el campo loteID del producto n°".$i+1);
                                            break;
                                        }else if(!isset($_POST["loteList"][$i]["loteCode"])){
                                            $body =array("errno"=>400,"error"=>"No está seteado el campo loteCode del producto n°".$i+1);
                                            break;
                                        }else if(!isset($_POST["loteList"][$i]["expirationDate"])){
                                            $body =array("errno"=>400,"error"=>"No está seteado el campo loteCode del producto n°".$i+1);
                                            break;
                                        }else if(!isset($_POST["loteList"][$i]["totalAmount"])){
                                            $body =array("errno"=>400,"error"=>"No está seteado el campo totalAmount del producto n°".$i+1);
                                            break;
                                        }else{
                                            if(($_POST['loteList'][$i]["loteID"])==""){
                                                $body = array("errno" => 400, "error" => "Está vacío el campo loteID del producto n°".$i+1);
                                                break;
                                            }else if (($_POST['loteList'][$i]["loteCode"])==""){
                                                $body = array("errno" => 400, "error" => "Está vacío el campo loteCode del producto n°".$i+1);
                                                break;
                                            }else if (($_POST['productList'][$i]["expirationDate"])==""){
                                                $body = array("errno" => 400, "error" => "Está vacío el campo expirationDate del producto n°".$i+1);
                                                break;
                                            }else if (($_POST['productList'][$i]["totalAmount"])==""){
                                                $body = array("errno" => 400, "error" => "Está vacío el campo totalAmount del producto n°".$i+1);
                                                break;
                                            } else {
                                                if($_POST["loteList"][$i]["totalAmount"]<0){
                                                    $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad negativa en el producto ".getLoteCodeByID($_POST["loteList"][$i]["loteID"]));
                                                }else if($_POST["productList"][$i]["amount"]==0){
                                                    $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad negativa en el producto ".getLoteCodeByID($_POST["loteList"][$i]["loteID"]));
                                                }else{
                                                    $data_orderlote[$i]["loteID"] = $_POST['loteList'][$i]["loteID"];
                                                    $data_orderlote[$i]["loteCode"] = $_POST['loteList'][$i]["loteCode"];
                                                    $data_orderlote[$i]["expirationDate"] = $_POST['loteList'][$i]["expirationDate"];
                                                    $data_orderlote[$i]["totalAmount"] = $_POST['loteList'][$i]["totalAmount"];
                                                }
                                            }
                                        }
                                    }

                                    for($y=0;$y<count($data_orderlote);$y++){
                                        insertLote($data_orderlote[$i]);
                                        $newLoteID=getLotesMaxID();
                                        insertOrderLote(array("orderID"=>$_POST["orderID"],"loteID"=>$newLoteID));
                                    }
                                }
                            }
                        }*/
                        break;
                    case 'createProduct':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createProduct");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createProduct");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createProduct");
                        }else if(getUserByToken($_POST["token"])["permissionID"]!=2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear producto como usuario normal");
                        }else{
                            $data_product=array(
                                "sectionID"=>"",
                                "supplierID"=>"",
                                "name"=>"",
                                "description"=>"",
                                "price"=>"",
                                "photo"=>""
                            );
                            foreach($data_product as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para createProduct");
                                    break;
                                } else if ($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." vacío para createProduct");
                                    break;
                                } else {
                                    $data_product[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertProduct = insertProduct($data_product);
                            if($insertProduct===TRUE){
                                $body =array("errno"=>200,"error"=>"Producto ingresado exitosamente");
                                $newProductID=getProductsMaxID();
                                insertProductsStockRowByProductID($newProductID);
                            }else{
                                $body =array("errno"=>400,"error"=>$insertProduct);
                            }
                        }
                        break;
                    case 'modifyProductPhoto':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifyProductPhoto");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifyProductPhoto");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifyProductPhoto");
                        }else if(getUserByToken($_POST["token"])["permissionID"]!=2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar fotos de producto como usuario normal");
                        }else{
                            if(!isset($_POST["productID"])){
                                $body =array("errno"=>400,"error"=>"No envio productID");
                            }else{
                                if(!isset($_FILES["photo"])){
                                    $body =array("errno"=>400,"error"=>"No envio una foto");
                                    break;
                                }else{
                                    if($_FILES["photo"]["error"]!=0){
                                        $body =array("errno"=>400,"error"=>$_FILES["photo"]["error"]);
                                    }else{
                                        $productID=$_POST["productID"];
                                        $filename=$_FILES['photo']['name'];
                                        $filetype=$_FILES['photo']['type'];
                                        $filesize=$_FILES['photo']['size'];
                                        $filetmp=$_FILES['photo']['tmp_name'];
                                        if (!((strpos($filetype, "gif") || strpos($filetype, "jpeg") || strpos($filetype, "jpg") || strpos($filetype, "png")) && ($filesize < 2000000))) {
                                            $body =array("errno"=>400,"error"=>'Error. La extensión o el tamaño de los archivos no es correcta.- Se permiten archivos .gif, .jpg, .png y .jpeg y de 200 kb como máximo.');
                                        }
                                        if(move_uploaded_file($filetmp,"../img/productphotos/".$productID.".".str_replace("image/","",$filetype))){
                                            chmod("../img/productphotos/".$productID.".".str_replace("image/","",$filetype),447);
                                            updateProductByID($productID,array('photo'=>$productID.".".str_replace("image/","",$filetype)));
                                            $body =array("errno"=>200,"error"=>"Imagen subida con éxito");
                                        }else{
                                            $body =array("errno"=>400,"error"=>"Ocurrio un error al intentar subir el archivo al servidor");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "createSection":
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createSection");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createSection");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createSection");
                        }else if(getUserByToken($_POST["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear secciones como usuario normal");
                        }else{
                            $data_Section = array(
                                "subcategoryID" => "",
                                "name" => "",
                                "description" => ""
                            );
                            foreach($data_Section as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para insertSection");
                                    break;
                                } else if ($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    break;
                                } else {
                                    $data_Section[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertSection = insertSection($data_Section);
                            if($insertSection === TRUE){
                                $body =array("errno"=>200,"error"=>"Section ingresado exitosamente");
                            } else {
                                $body =array("errno"=>400,"error"=>$insertSection);
                            }
                        }
                        break;
                    case 'createSector':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createSector");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createSector");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createSector");
                        }else if(getUserByToken($_POST["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear sectores como usuario normal");
                        }else{
                            $data_sector=array(
                                "name"=>"",
                                "description"=>""
                            );
                            foreach($data_sector as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para createSector");
                                    break;
                                } else if ($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." vacío para createSector");
                                    break;
                                } else {
                                    $data_sector[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertSector = insertSector($data_sector);
                            if($insertSector===TRUE){
                                $body =array("errno"=>200,"error"=>"Sector ingresado exitosamente");
                                $newSectorID=getSectorsMaxID();
                                insertProductsStockRowBySectorID($newSectorID);
                            }else{
                                $body =array("errno"=>400,"error"=>$insertSector);
                            }
                        }
                        break;
                    case 'createSubcategory':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createSubcategory");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createSubcategory");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createSubcategory");
                        }else if(getUserByToken($_POST["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido crear subcategorias como usuario normal");
                        }else{
                            $data_subcategory = array(
                                "categoryID" => "",
                                "name" => "",
                                "description" => ""
                            );
                            foreach($data_subcategory as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para insertSubcategory");
                                    break;
                                } else if ($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    break;
                                } else {
                                    $data_subcategory[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertSubcategory = insertSubcategory($data_subcategory);
                            if($insertSubcategory===true){
                                $body = array("errno" => 200, "error" => "sub categoría creada con éxito");
                            } else {
                                $body = array("errno" => 200, "error" => $insertSubcategory);
                            }
                        }
                        break;
                    case 'createSupplier':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para createSupplier");
                        }else if($_POST["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para createSupplier");
                        }else if(!userTokenExists($_POST["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para createSupplier");
                        }else if(getUserByToken($_POST["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido agregar proveedores como usuario normal");
                        }else{
                            $data_supplier = array(
                                "supplierName" => "",
                                "contactName" => "",
                                "addressStreet" => "",
                                "addressNumber" => "",
                                "cityID" => "",
                                "postalCode" => "",
                                "phoneNumber" => "",
                            );
                            foreach($data_supplier as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para insertSupplier");
                                    break;
                                } else if ($_POST[$key]==""){
                                    if ($key == "phoneNumber"){
                                        $data_supplier[$key] = $_POST[$key];
                                    } else {
                                        $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    }
                                    break;
                                } else {
                                    $data_supplier[$key] = $_POST[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $insertSupplier = insertSupplier($data_supplier);
                            if($insertSupplier===TRUE){
                                $body =array("errno"=>200,"error"=>"Proveedor ingresado exitosamente");
                            } else {
                                $body =array("errno"=>400,"error"=>$insertSupplier);
                            }
                        }
                        break;
                    case 'createTransfer': 
                        $data_transfer = array(
                            'token' => '',
                            'sectorID' => ''
                        );                        
                        foreach($data_transfer as $key => $value){
                            if(!isset($_POST[$key])){
                                $body =array("errno"=>400,"error"=>"No está seteado el campo ".$key);
                                break;
                            }else{
                                if($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => "Está vacío el campo ".$key);
                                    break;
                                } else {
                                    $data_transfer[$key] = $_POST[$key];
                                }
                            }
                        }
                        $data_transfer["transferDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago por acá por que si no sale error de que no le paso la fecha por POST
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_POST["productList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de productos (productList)");
                            break;
                        }else{
                            $_POST["productList"]=json_decode($_POST["productList"]);
                            if(empty($_POST["productList"])){
                                $body =array("errno"=>400,"error"=>"No cargo ningún producto");
                            }else{
                                $_POST["productList"]=duplicateProducts($_POST["productList"]);
                                //Asigno la cantidad de productos a una variable
                                $cant = count($_POST['productList']);

                                for ($i=0; $i < $cant ; $i++) { 
                                    $_POST['productList'][$i]=get_object_vars($_POST['productList'][$i]);
                                    $data_transferdetail[$i] = array(
                                        'productID' => '',
                                        'amount' => '',
                                    );
                                    if(!isset($_POST["productList"][$i]["productID"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i+1);
                                        break;
                                    }else if(!isset($_POST["productList"][$i]["amount"])){
                                        $body =array("errno"=>400,"error"=>"No está seteado el campo amount del producto n°".$i+1);
                                        break;
                                    }else{
                                        if(($_POST['productList'][$i]["productID"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo productID del producto n°".$i+1);
                                            break;
                                        }else if (($_POST['productList'][$i]["amount"])==""){
                                            $body = array("errno" => 400, "error" => "Está vacío el campo amount del producto n°".$i+1);
                                            break;
                                        } else {
                                            $stock = getProductStockByID($_POST["productList"][$i]["productID"]);
                                            if($_POST["productList"][$i]["amount"]>$stock){
                                                $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad mas grande de la que hay en stock en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else if($_POST["productList"][$i]["amount"]<0){
                                                $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else if($_POST["productList"][$i]["amount"]==0){
                                                $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                            }else{
                                                $data_transferdetail[$i]["productID"] = $_POST['productList'][$i]["productID"];
                                                $data_transferdetail[$i]["amount"] = $_POST['productList'][$i]["amount"];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($body["errno"])){
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        $data_transfer['userID']=getUserIDByToken($data_transfer['token']);
                        unset($data_transfer['token']);
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        //!---------------- A partir de aca transfers -------------
                        $addTransfer = insertTransfer($data_transfer);
                        if($addTransfer==true){
                            $body = array("errno" => 200, "error" => "Traspaso agregado correctamente");
                        }else{
                            $body = array("errno" => 400, "error" => $addTransfer."linea 1605");
                            break;
                            //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                        }
                        $data_transferlote=turnTransferDetailsOnTransferLotes($data_transferdetail);
                        //!---------------- A partir de aca transferdetails -------------
                        if(count($data_transferdetail)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de traspaso para agregar el traspaso");
                            deleteTransferByID(getTransferMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_transferdetail) ; $i++) {
                                $data_transferdetail[$i]["transferID"]=getTransferMaxID();
                                $addTransferDetail = insertTransferDetail($data_transferdetail[$i]);
                                if($addTransferDetail===true){
                                    $body = array("errno" => 200, "error" => "Traspaso y detalle del traspaso agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addTransferDetail."linea 1622");
                                    break;
                                    //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                                }
                            }
                        }
                        //!---------------- A partir de aca transferlotes -------------
                        $msg="Traspaso y detalle del traspaso con lotes agregados correctamente. Sacamos <br>";
                        if(count($data_transferlote)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un lote de traspaso para agregar el traspaso");
                            deleteTransferByID(getTransferMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_transferlote) ; $i++) {
                                $data_transferlote[$i]["transferID"]=getTransferMaxID();
                                $addTransferLote = insertTransferLote($data_transferlote[$i]);
                                $msg.="* ".$data_transferlote[$i]["amount"]." productos del lote ".getLoteCodeByID($data_transferlote[$i]["loteID"])."<br>";
                                if($addTransferLote===true){
                                    $body = array("errno" => 200, "error" => $msg);
                                }else{
                                    $body = array("errno" => 400, "error" => $addTransferLote."linea 1826");
                                    break;
                                    //Se guarda el error en un string si hay error en insertTransfer, por eso muestro $addTransfer
                                }
                            }
                        }
                        break;
                        /*case 'getTransferPDF':
                            if(!isset($_POST["token"])){
                                $body = array("errno"=>400,"error"=>"token no definido para getTransferPDF");
                            }else if(trim($_POST["token"])==""){
                                $body = array("errno"=>400,"error"=>"token vacio para getTransferPDF");
                            }else if(!userTokenExists($_POST["token"])){
                                $body = array("errno"=>400,"error"=>"No existe usuario con ese token para getTransferPDF");
                            }else if(getUserByToken($_POST["token"])["permissionID"]!=1){
                                $body=array("errno"=>403,"error"=>"No tiene permitido ver el PDF de un egreso sin ser farmacéutico");
                            }else if(!isset($_POST["transferID"])){
                                $body = array("errno"=>400,"error"=>"transferID no definido para getTransferPDF");
                            }else if(trim($_POST["transferID"])==""){
                                $body = array("errno"=>400,"error"=>"transferID vacio para getTransferPDF");
                            }else if(!transferIDExists($_POST["transferID"])){
                                $body = array("errno"=>400,"error"=>"No existe egreso con ese transferID para getTransferPDF");
                            }else{
                                $getTransferPDF=getTransferPDF($_POST["transferID"]);
                                if($getTransferPDF){
                                    $body=array("errno"=>200,"error"=>"Redirigido al PDF");
                                }else{
                                    $body=array("errno"=>400,"error"=>"Ocurrió un error al redirigir al PDF");
                                }
                            }
                            break;*/
                        case 'createRequest':
                            $data_request = array(
                                'token' => ''
                            );                        
                            foreach($data_request as $key => $value){
                                if(!isset($_POST[$key])){
                                    $body =array("errno"=>400,"error"=>"No está seteado el campo ".$key);
                                    break;
                                }else{
                                    if($_POST[$key]==""){
                                        $body = array("errno" => 400, "error" => "Está vacío el campo ".$key);
                                        break;
                                    } else {
                                        $data_request[$key] = $_POST[$key];
                                    }
                                }
                            }
                            $data_request['sectorID'] = getUserSector($data_request['token']);
                            if(getUserPermissionsByID(getUserIDByToken($_POST["token"]))<=2){
                                $body = array("errno"=>403,"error"=>"Solo los encargados de sectores pueden realizar pedidos");
                                break;
                            }
                            $data_request["requestDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago por acá por que si no sale error de que no le paso la fecha por POST
                            if(isset($_POST["personName"])){
                                if(!checkInput($_POST["personName"],CHECK__LETTERS)){
                                    $body = array("errno" => 400, "error" => "Debe ingresar solamente letras en el campo persona");
                                    $data_request["requestPerson"]=$_POST["personName"];
                                }
                            }
                            if(isset($_POST["personDNI"])){
                                $data_request["requestPersonDNI"]=$_POST["personDNI"];
                            }
                            if(isset($_POST["details"])){
                                $data_request["details"]=$_POST["details"];
                            }
                            if(isset($body["errno"])){
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            if(!isset($_POST["productList"])){
                                $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de productos (productList)");
                                break;
                            }else{
                                $_POST["productList"]=json_decode($_POST["productList"]);
                                if(empty($_POST["productList"])){
                                    $body =array("errno"=>400,"error"=>"No cargo ningún producto");
                                }else{
                                    //Asigno la cantidad de productos a una variable
                                    $_POST["productList"]=duplicateProducts($_POST["productList"]);
                                    $cant = count($_POST['productList']);
                                    for ($i=0; $i < $cant ; $i++) { 
                                        $_POST['productList'][$i]=get_object_vars($_POST['productList'][$i]);
                                        $data_requestdetail[$i] = array(
                                            'productID' => '',
                                            'amount' => ''
                                        );
                                        if(!isset($_POST["productList"][$i]["productID"])){
                                            $body =array("errno"=>400,"error"=>"No está seteado el campo productID del producto n°".$i+1);
                                            break;
                                        }else if(!isset($_POST["productList"][$i]["amount"])){
                                            $body =array("errno"=>400,"error"=>"No está seteado el campo amount del producto n°".$i+1);
                                            break;
                                        }else{
                                            if(($_POST['productList'][$i]["productID"])==""){
                                                $body = array("errno" => 400, "error" => "Está vacío el campo productID del producto n°".$i+1);
                                                break;
                                            }else if (($_POST['productList'][$i]["amount"])==""){
                                                $body = array("errno" => 400, "error" => "Está vacío el campo amount del producto n°".$i+1);
                                                break;
                                            } else {
                                                /*$stock = getProductStockByID($_POST["productList"][$i]["productID"]);
                                                if($_POST["productList"][$i]["amount"]>$stock){
                                                    $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad mas grande de la que hay en stock en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                                }else if($_POST["productList"][$i]["amount"]<0){
                                                    $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                                }else if($_POST["productList"][$i]["amount"]==0){
                                                    $body =array("errno"=>400,"error"=>"Está intentando transferir una cantidad negativa en el producto ".getProductNameByID($_POST["productList"][$i]["productID"]));
                                                }else{*/
                                                    $data_requestdetail[$i]["productID"] = $_POST['productList'][$i]["productID"];
                                                    $data_requestdetail[$i]["amount"] = $_POST['productList'][$i]["amount"];
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            if(isset($body["errno"])){
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $data_request['userID']=getUserIDByToken($data_request['token']);
                            unset($data_request['token']);
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            $addRequest = insertRequest($data_request);

                            if($addRequest==true){
                                $body = array("errno" => 200, "error" => "Pedido agregado correctamente");
                            }else{
                                $body = array("errno" => 400, "error" => $addRequest."linea 1731");
                                break;
                                //Se guarda el error en un string si hay error en insertRequest, por eso muestro $addRequest
                            }
                            if(count($data_requestdetail)<=0){
                                $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de pedido para agregar el pedido");
                                deleteRequestByID(getRequestMaxID());
                                break;
                            }else{
                                for ($i=0; $i <count($data_requestdetail) ; $i++) {
                                    $data_requestdetail[$i]["requestID"]=getRequestMaxID();
                                    $addRequestDetail = insertRequestDetail($data_requestdetail[$i]);
                                    if($addRequestDetail===true){
                                        $body = array("errno" => 200, "error" => "Pedido y detalle del pedido agregados correctamente");
                                    }else{
                                        $body = array("errno" => 400, "error" => $addRequestDetail."linea 1746");
                                        break;
                                        //Se guarda el error en un string si hay error en insertRequestDetail, por eso muestro $addRequestDetail
                                    }
                                }
                            }
                            break;
                    case 'createRemoval':
                        $data_removal = array(
                            'token' => ''
                        );                        
                        foreach($data_removal as $key => $value){
                            if(!isset($_POST[$key])){
                                $body =array("errno"=>400,"error"=>"No está seteado el campo ".$key);
                                break;
                            }else{
                                if($_POST[$key]==""){
                                    $body = array("errno" => 400, "error" => "Está vacío el campo ".$key);
                                    break;
                                } else {
                                    $data_removal[$key] = $_POST[$key];
                                }
                            }
                        }
                        if(getUserPermissionsByID(getUserIDByToken($_POST["token"]))>=2){
                            $body = array("errno"=>403,"error"=>"Solo los farmaceuticos pueden realizar eliminaciones");
                            break;
                        }
                        $data_removal["removalDate"]=date("Y-m-d H:i:s",time()); // ? Lo hago por acá por que si no sale error de que no le paso la fecha por POST
                        if(isset($body["errno"])){
                            if($body["errno"]>=400){
                                break;
                            }
                        }
                        if(!isset($_POST["loteList"])){
                            $body = array("errno"=>400,"error"=>"Debe ingresar un array con una lista de lotes (loteList)");
                            break;
                        }else{
                            $_POST["loteList"]=json_decode($_POST["loteList"]);
                            if(empty($_POST["loteList"])){
                                $body =array("errno"=>400,"error"=>"No cargo ningún lote");
                            }else{
                                $cant = count($_POST['loteList']);
                                for ($i=0; $i < $cant ; $i++) { 
                                    $_POST['loteList'][$i]=get_object_vars($_POST['loteList'][$i]);
                                    $data_removaldetail[$i] = array(
                                        'loteID' => '',
                                    );
                                    $loteNro = $i+1;
                                    if(!isset($_POST["loteList"][$i]["loteID"])){
                                        $body =array("errno"=>400,"error"=>"No esta seteado el campo loteID del lote n°".strval($loteNro));
                                        break;
                                    }else{
                                        if(trim($_POST['loteList'][$i]["loteID"])==""){
                                            $body = array("errno" => 400, "error" => "Esta vacio el campo loteID del lote n°".strval($loteNro));
                                            break;
                                        } else {
                                            $data_removaldetail[$i]["loteID"] = $_POST['loteList'][$i]["loteID"];
                                        }
                                    }
                                }
                            }
                        }
                        if(isset($body["errno"])){
                            if($body["errno"]>=400){
                                break;
                            }
                        }
                        $data_removal['userID']=getUserIDByToken($data_removal['token']);
                        unset($data_removal['token']);
                        $addRemoval = insertRemoval($data_removal);
                        if($addRemoval==true){
                            $body = array("errno" => 200, "error" => "Eliminación agregada correctamente");
                        }else{
                            $body = array("errno" => 400, "error" => $addRemoval."linea 1933");
                            break;
                            //Se guarda el error en un string si hay error en insertRequest, por eso muestro $addRequest
                        }
                        if(count($data_removaldetail)<=0){
                            $body = array("errno"=>400,"error"=>"Debe ingresar al menos un detalle de eliminacion para agregar la eliminacion");
                            deleteRemovalByID(getRemovalMaxID());
                            break;
                        }else{
                            for ($i=0; $i <count($data_removaldetail) ; $i++) {
                                $data_removaldetail[$i]["removalID"]=getRemovalMaxID();
                                $addRemovalDetail = insertRemovalDetail($data_removaldetail[$i]);
                                if($addRemovalDetail===true){
                                    $body = array("errno" => 200, "error" => "Eliminación y detalle de la eliminación agregados correctamente");
                                }else{
                                    $body = array("errno" => 400, "error" => $addRemovalDetail."linea 1948");
                                    break;
                                    //Se guarda el error en un string si hay error en insertRemovalDetail, por eso muestro $addRemovalDetail
                                }
                            }
                        }
                        break;
                    case 'register':
                        $data_usuario = array(
                            'firstName' => '',
                            'lastName' => '',
                            'email' => '',
                            'pass' => '',
                            'repass' => '',
                            "permissionID"=>''
                        );
                        foreach($data_usuario as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no está declarada");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                break;
                            } else {
                                $data_usuario[$key] = $_POST[$key];
                            }
                        } 

                        if ($data_usuario['pass'] != $data_usuario['repass']){
                            $body = array("errno" => 400, "error" => "Las contraseñas no coinciden");
                            break;
                        }

                        if (!checkInput($data_usuario['pass'],CHECK__PASS)){
                          $body = array("errno" => 400, "error" => "La contraseña es inválida. Debe tener al menos una mayúscula, una minúscula, un número y un caracter especial (@#-_$%^&+=§!?).");
                        }
                        if(!checkInput($data_usuario['email'],CHECK__EMAIL)){
                          $body = array("errno" => 400, "error" => "El email es inválido");
                        }else{
                          $data_usuario['email']=strtolower($data_usuario['email']);
                        }
                        if(!checkInput($data_usuario["lastName"],CHECK__LETTERS)){
                            $body = array("errno" => 400, "error" => "Debe ingresar solamente letras en el campo apellido");
                        } 
                        if(!checkInput($data_usuario["firstName"],CHECK__LETTERS)){
                            $body = array("errno" => 400, "error" => "Debe ingresar solamente letras en el campo nombre");
                        } 

                        if (isset($body["errno"])) {
                          if($body["errno"]==400){
                              break;
                          }
                        }
                        $registerUser = createUser($data_usuario);
                        $creationDate=date("Y-m-d H:i:s",time());
                        $setCreationDate = setCreationDate($data_usuario["email"],$creationDate);
                        if($registerUser == 1){
                            updateTokenByEmail($_POST['email']);
                            $body = array("errno" => 200, "error" => "Registrado correctamente");
                        } else if ($registerUser==2) {
                            $body = array("errno" => 400, "error" => "El usuario ya existe");
                        }else if($registerUser==3){
                            $body = array("errno" => 400, "error" => "Ocurrió un error al enviar el mail");
                        } else if ($registerUser==0){
                            $body = array("errno" => 400, "error" => "La contraseña tiene menos de 8 caracteres");
                        }
                    break;
                    case 'login':
                        $data_usuario = array(
                            'email' => '',
                            'pass' => '',
                        );
                        foreach($data_usuario as $key => $value){
                            if(!isset($_POST[$key])){
                                $body = array("errno" => 400, "error" => $key." no está declarada");
                                break;
                            } else if ($_POST[$key]==""){
                                $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                break;
                            } else {
                                $data_usuario[$key] = $_POST[$key];
                            }
                        } 
                        /* if (!checkInput($data_usuario['pass'],CHECK__PASS)){
                          $body = array("errno" => 400, "error" => "La contraseña contiene caracteres no válidos");
                        } */
                        if (!checkInput($data_usuario['email'],CHECK__EMAIL)){
                          $body = array("errno" => 400, "error" => "El email contiene caracteres no válidos");
                        }else{
                          $data_usuario['email']=strtolower($data_usuario['email']);
                        }
                        if (!checkInput($data_usuario['pass'],CHECK__PASS__LOGIN)){
                            $body = array("errno" => 400, "error" => "Contraseña inválida");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            } 
                        }
                        $loginUser = loginUser($data_usuario['email'], $data_usuario['pass']);
                        if($loginUser==4){
                            $body = array("errno" => 400, "error" => "El email no coincide con los registros en nuestra base de datos");
                        }else if($loginUser==3){
                            $body = array("errno" => 400, "error" => "El usuario no está activado. Revise su mail");
                        }else if($loginUser==2){
                            $body = array("errno" => 400, "error" => "El usuario no está verificado. Notifique a un administrador para que lo valide");
                        }else if($loginUser==1){
                            $body = array("errno" => 400, "error" => "Ha ingresado incorrectamente la contraseña");
                        }else if($loginUser==0){
                            $body = array("errno" => 200, "error" => "Usuario logeado correctamente", "token" => getUserByEmail($data_usuario['email'])["token"],"permission"=> getUserByEmail($data_usuario['email'])["permissionID"]);
                        }else{
                            $body = array("errno" => 400, "error" => "Ocurrio un error inesperado al iniciar sesión");
                        }

                    break;
                    case 'modifyUserPhoto':
                      if(!isset($_POST["token"])){
                        $body =array("errno"=>400,"error"=>"No envio token");
                      }else{
                          if(!isset($_FILES["photo"])){
                            $body =array("errno"=>400,"error"=>"No envio una foto");
                            break;
                          }else{
                            if($_FILES["photo"]["error"]!=0){
                              $body =array("errno"=>400,"error"=>$_FILES["photo"]["error"]);
                            }else{
                              $currentImgFile = array(
                                "gif",
                                "jpg",
                                "jpeg",
                                "png"
                              );
                              $userID=getUserIDByToken($_POST["token"]);
                              $filename=$_FILES['photo']['name'];
                              $filetype=$_FILES['photo']['type'];
                              $filesize=$_FILES['photo']['size'];
                              $filetmp=$_FILES['photo']['tmp_name'];
                              if (!((strpos($filetype, "gif") || strpos($filetype, "jpeg") || strpos($filetype, "jpg") || strpos($filetype, "png")) && ($filesize < 2000000))) {
                                $body =array("errno"=>400,"error"=>'Error. La extensión o el tamaño de los archivos no es correcta.- Se permiten archivos .gif, .jpg, .png y .jpeg y de 200 kb como máximo.');
                              }                              
                              if(move_uploaded_file($filetmp,"../img/userphotos/".$userID.".".str_replace("image/","",$filetype))){
                                foreach($currentImgFile as $key => $type){
                                    if(str_replace("image/","",$filetype)!=$type){
                                        if(file_exists("../img/userphotos/".$userID.".".$type)){
                                            unlink("../img/userphotos/".$userID.".".$type);
                                        }                                        
                                    }
                                } 
                                chmod("../img/userphotos/".$userID.".".str_replace("image/","",$filetype),447);
                                updateUserByID($userID,array('photo'=>$userID.".".str_replace("image/","",$filetype)));
                                $body =array("errno"=>200,"error"=>"Imagen subida con éxito");
                              }else{
                                $body =array("errno"=>400,"error"=>"Ocurrio un error al intentar subir el archivo al servidor");
                                break;
                              }
                            }
                          }
                      }
                      break;
                    case 'verifyUser':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para verifyUser");
                        }else if(trim($_POST["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para verifyUser");
                        }else if(!userTokenExists($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para verifyUser");
                        }else if(getUserByToken($_POST["token"])["permissionID"]!=2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido verificar a un usuario como usuario normal");
                        }else if(!isset($_POST["userID"])){
                            $body = array("errno"=>400,"error"=>"userID no definido para verifyUser");
                        }else if(trim($_POST["userID"])==""){
                            $body = array("errno"=>400,"error"=>"userID vacio para verifyUser");
                        }else if(!userIDExists($_POST["userID"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese userID para verifyUser");
                        }else{
                            $verifyUser=verifyUserByID($_POST["userID"]);
                            if($verifyUser){
                                $body=array("errno"=>200,"error"=>"Usuario verificado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al verificar");
                            }
                        }
                        break;
                    case 'unverifyUser':
                        if(!isset($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para unverifyUser");
                        }else if(trim($_POST["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para unverifyUser");
                        }else if(!userTokenExists($_POST["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para unverifyUser");
                        }else if(getUserByToken($_POST["token"])["permissionID"]!=2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido sacar la verificación a un usuario como usuario normal");
                        }else if(!isset($_POST["userID"])){
                            $body = array("errno"=>400,"error"=>"userID no definido para unverifyUser");
                        }else if(trim($_POST["userID"])==""){
                            $body = array("errno"=>400,"error"=>"userID vacio para unverifyUser");
                        }else if(!userIDExists($_POST["userID"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese userID para unverifyUser");
                        }else{
                            $unverifyUser=unverifyUserByID($_POST["userID"]);
                            if($unverifyUser){
                                $body=array("errno"=>200,"error"=>"Usuario desverificado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al desverificar");
                            }
                        }
                        break;
                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    default:
                        $body = array("errno" =>400, "error" => "action no válida para el método POST");
                    break;
                }
            }
        break;
        case 'PUT':
            $_PUT=json_decode(file_get_contents("php://input"));
            $_PUT=get_object_vars($_PUT);

            if(!isset($_PUT['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el método PUT");
            } else if($_PUT['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningún valor");
            } else {
                switch($_PUT['action']){
                    /*
                        PROPOSITO: modificar los datos de la categoría.
                        PARAMETROS: 
                            -name : nombre de la categoría.
                            -description : descripcion de la categoría.
                            -categoryID : ID de la categoría.
                        DEVUELVE: arreglo con el resultado de la modificacion.
                    */
                    case 'modifyCategory':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifyCategory");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifyCategory");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifyCategory");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar categorías como usuario normal");
                        }else{
                            $data_editable = array(
                                "name" => "",
                                "description" => ""
                            );
                            foreach($data_editable as $key => $value){
                                if(!isset($_PUT[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para updateCategoryById");
                                    break;
                                } else if ($_PUT[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    break;
                                } else {
                                    $data_editable[$key] = $_PUT[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            if(!isset($_PUT['categoryID'])){
                                $body = array("errno" =>400, "error" => "categoryID no declarada");
                            } else if($_PUT['categoryID']==""){
                                $body = array("errno" =>400, "error" => "categoryID no tiene ningún valor");
                            } else if(categoryIDExists($_PUT['categoryID'])){
                                $updateCategoryById = updateCategoryById($_PUT['categoryID'],$data_editable);
                                if ($updateCategoryByID === true){
                                    $body = array("errno" =>200, "error" => "categoría modificada con éxito");
                                } else {
                                    $body = array("errno" =>400, "error" => $updateCategoryByID);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => $key."la categoría no existe");
                            }
                        }
                        break;
                    case 'modifyCity':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifyCity");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifyCity");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifyCity");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar ciudades como usuario normal");
                        }else{
                            $data_editable = array();
                            foreach($_PUT as $key => $value){
                                if($key!="action" && $key!="cityID"){
                                    if($key=="name"){
                                        $data_editable[$key] = $value;
                                    }
                                } 
                            }
                            if(empty($data_editable)){
                                $body =array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: nombre");
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                break;
                                }
                            }
                            if(!isset($_PUT['cityID'])){
                                $body = array("errno" => 400, "error" => "cityID no declarado para modifyCity");
                            } else {
                                if(cityIDExists($_PUT['cityID'])){
                                    $updateCity=updateCityByID($_PUT['cityID'],$data_editable);
                                    if($updateCity===true){
                                        $body = array("errno" => 200, "error" => "Ciudad modificado correctamente");
                                    } else if ($updateCity===false) {
                                        $body = array("errno" => 300, "error" => "No se modificó nada");
                                    }else{
                                        $body = array("errno" => 400, "error" => $updateCity);
                                    }
                                } else {
                                    $body = array("errno" => 400, "error" => "Ciudad no encontrada");
                                }
                            }
                        }
                        break;
                    //* Dejenmelo panas atte mauri
                    case 'modifyOrderDetails':
                        $data_editable = array(
                            'productID' => "",
                            'loteID' => "",
                            'amount' => "",
                            'supposedAmount' => ""
                        );
                        foreach($_PUT as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 400, "error" => "No está seteado el campo " .$key);
                            }else{
                            if(($_PUT[$key])==""){
                                $body = array("errno" => 400, "error" => "Falta el campo ".$key);
                                break;
                            } else {
                            if($key!="action" && $key!="orderID"){
                                if($key=="userID" || $key=="supplierID"){
                                    $data_editable[$key] = $value;
                                }
                            }
                            }
                            }  
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['orderID'])){
                            $body = array("errno" => 400, "error" => "orderID no declarado para modifyOrder");
                        } else {
                        if(orderIDExists($_PUT['orderID'])){
                            $updateOrder=updateOrderByID($_PUT['orderID'],$data_editable);
                            if($updateOrder===true){
                                $body = array("errno" => 200, "error" => "Orden modificada correctamente");
                            } else if ($updateOrder===false) {
                                $body = array("errno" => 300, "error" => "No se modificó nada");
                            }else{
                                $body = array("errno" => 400, "error" => $updateOrder);
                            }
                        } else {
                            $body = array("errno" => 400, "error" => "Orden no encontrada");
                        }
                        }
                        break;
                        case 'modifyOrder':
                            $data_editable = array(
                                'userID' => "",
                                'supplierID' => ""
                            );
                            foreach($_PUT as $key => $value){
                                if(!isset($_PUT[$key])){
                                    $body = array("errno" => 404, "error" => "No está seteado el campo " .$key);
                                }else{
                                if(($_PUT[$key])==""){
                                    $body = array("errno" => 404, "error" => "Falta el campo ".$key);
                                    break;
                                } else {
                                if($key!="action" && $key!="orderID"){
                                    if($key=="userID" || $key=="supplierID"){
                                        $data_editable[$key] = $value;
                                    }
                                }
                                }
                                }  
                            }
                            if(empty($data_editable)){
                                $body =array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: proveedor");
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            if(!isset($_PUT['orderID'])){
                                $body = array("errno" => 400, "error" => "orderID no declarado para modifyOrder");
                            } else {
                            if(orderIDExists($_PUT['orderID'])){
                                $updateOrder=updateOrderByID($_PUT['orderID'],$data_editable);
                                if($updateOrder===true){
                                    $body = array("errno" => 200, "error" => "Orden modificada correctamente");
                                } else if ($updateOrder===false) {
                                    $body = array("errno" => 300, "error" => "No se modificó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $updateOrder);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Orden no encontrada");
                            }
                            }
                            break;
                    case 'modifyProduct':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifyProduct");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifyProduct");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifyProduct");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]!=2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar productos como usuario normal");
                        }else{
                            $data_editable = array();
                            foreach($_PUT as $key => $value){
                                if($key!="action" && $key!="productID"){
                                    if($key=="name" || $key=="description" || $key=="price" || $key=="criticalStock" || $key=="concentration" || $key=="code" || $key=="formID"){
                                        $data_editable[$key] = $value;
                                    }
                                } 
                            }
                            if(empty($data_editable)){
                                $body =array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: precio");
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                break;
                                }
                            }
                            if(!isset($_PUT['productID'])){
                                $body = array("errno" => 400, "error" => "productID no declarado para modifyProduct");
                            } else {
                                if(productIDExists($_PUT['productID'])){
                                    $updateProduct=updateProductByID($_PUT['productID'],$data_editable);
                                    if($updateProduct===true){
                                        $body = array("errno" => 200, "error" => "Producto modificado correctamente");
                                    } else if ($updateProduct===false) {
                                        $body = array("errno" => 300, "error" => "No se modificó nada");
                                    }else{
                                        $body = array("errno" => 400, "error" => $updateProduct);
                                    }
                                } else {
                                    $body = array("errno" => 400, "error" => "Producto no encontrado");
                                }
                            }
                        }
                        break;
                    case "modifySection":
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifySection");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifySection");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifySection");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar secciones como usuario normal");
                        }else{
                            $data_editable = array(
                                "subcategoryID" => "",
                                "name" => "",
                                "description" => ""
                            );
                            foreach($data_editable as $key => $value){
                                if(!isset($_PUT[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para updateSectionByID");
                                    break;
                                } else if ($_PUT[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    break;
                                } else {
                                    $data_editable[$key] = $_PUT[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            if(!isset($_PUT['sectionID'])){
                                $body = array("errno" =>400, "error" => "sectionID no declarada");
                            } else if($_PUT['sectionID']==""){
                                $body = array("errno" =>400, "error" => "sectionID no tiene ningún valor");
                            } else if(sectionIDExists($_PUT['sectorID'])){
                                $updateSectionById = updateSectionById($_PUT['sectionID'],$data_editable);
                                if ($updateSectionByID === true){
                                    $body = array("errno" =>200, "error" => "seccion modificada con éxito");
                                } else {
                                    $body = array("errno" =>400, "error" => $updateSectionByID);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => $key."la seccion no existe");
                            }
                        }
                        break;
                    case 'modifySector':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifySector");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifySector");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifySector");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar sectores como usuario normal");
                        }else{
                            $data_editable = array();
                            foreach($_PUT as $key => $value){
                                if($key!="action" && $key!="sectorID"){
                                    if($key=="name" || $key=="description"){
                                        $data_editable[$key] = $value;
                                    }
                                } 
                            }
                            if(empty($data_editable)){
                                $body =array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: nombre");
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                break;
                                }
                            }
                            if(!isset($_PUT['sectorID'])){
                                $body = array("errno" => 400, "error" => "sectorID no declarado para modifySector");
                            } else {
                                if(sectorIDExists($_PUT['sectorID'])){
                                    $updateSector=updateSectorByID($_PUT['sectorID'],$data_editable);
                                    if($updateSector===true){
                                        $body = array("errno" => 200, "error" => "Sector modificado correctamente");
                                    } else if ($updateSector===false) {
                                        $body = array("errno" => 300, "error" => "No se modificó nada");
                                    }else{
                                        $body = array("errno" => 400, "error" => $updateSector);
                                    }
                                } else {
                                    $body = array("errno" => 400, "error" => "Sector no encontrado");
                                }
                            }
                        }
                        break;
                    case 'modifySubcategory':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifySubcategory");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifySubcategory");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifySubcategory");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar subcategorías como usuario normal");
                        }else{
                            $data_editable = array(
                                "categoryID" => "",
                                "name" => "",
                                "description" => ""
                            );
                            foreach($data_editable as $key => $value){
                                if(!isset($_PUT[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para modifySubcategoryByID");
                                    break;
                                } else if($_PUT[$key]==""){
                                    $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    break;
                                } else {
                                    $data_editable[$key] = $_PUT[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }
                            if(!isset($_PUT['subcategoryID'])){
                                $body = array("errno" =>400, "error" => "subcategoryID no definida para modifySubcategoryByID");
                            } else if($_PUT['subcategoryID']==""){
                                $body = array("errno" =>400, "error" => "subcategoryID no tiene ningún valor");
                            } else if(subcategoryIDExists($_PUT['subcategoryID'])){
                                $updateSubcategoryByID = updateSubcategoryByID($_PUT['subcategoryID'],$data_editable);
                                if($updateSubcategoryByID === true){
                                    $body = array("errno" =>200, "error" => "sub categoría modificada");
                                } else {
                                    $body = array("errno" =>400, "error" => $updateSubcategoryByID);
                                }
                            } else {
                                $body = array("errno" =>400, "error" => "la sub categoría no existe");
                            }
                        }
                        break;
                    case 'modifySupplier':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para modifySupplier");
                        }else if($_PUT["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para modifySupplier");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para modifySupplier");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido modificar proveedores como usuario normal");
                        }else{
                            $data_editable = array(
                                "supplierName" => "",
                                "contactName" => "",
                                "addressStreet" => "",
                                "addressNumber" => "",
                                "cityID" => "",
                                "postalCode" => "",
                                "phoneNumber" => "",
                            );
                            foreach($data_editable as $key => $value){
                                if(!isset($_PUT[$key])){
                                    $body = array("errno" => 400, "error" => $key." no definido para modifySupplierByID");
                                    break;
                                } else if($_PUT[$key]==""){
                                    if($key == "phoneNumber"){
                                        $data_editable[$key] = $_PUT[$key];
                                    } else {
                                        $body = array("errno" => 400, "error" => $key." no tiene ningún valor");
                                    }
                                    break;
                                } else {
                                    $data_editable[$key] = $_PUT[$key];
                                }
                            }
                            if (isset($body["errno"])) {
                                if($body["errno"]==400){
                                    break;
                                }
                            }

                            if(!isset($_PUT['supplierID'])){
                                $body = array("errno" =>400, "error" => "supplierID no definida para modifySupplierByID");
                            } else if($_PUT['supplierID']==""){
                                $body = array("errno" =>400, "error" => "supplierID no tiene ningún valor");
                            } else if(supplierIDExists($_PUT['supplierID'])){
                                $updateSupplierByID = updateSupplierByID($_PUT['supplierID'],$data_editable);
                                if ($updateSupplierByID === true){
                                    $body = array("errno" =>200, "error" => "proveedor modificado con éxito");
                                } else {
                                    $body = array("errno" =>400, "error" => $updateSupplierByID);
                                }
                            } else {
                                $body = array("errno" =>400, "error" => "El proveedor no existe");
                            }
                        }
                        break;
                    case 'requestGiven':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para requestGiven");
                        }else if(trim($_PUT["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para requestGiven");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para requestGiven");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1 OR getUserByToken($_PUT["token"])["permissionID"]==2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido poner como enviado a un pedido sin ser encargado de un sector");
                        }else if(!isset($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"requestID no definido para requestGiven");
                        }else if(trim($_PUT["requestID"])==""){
                            $body = array("errno"=>400,"error"=>"requestID vacio para requestGiven");
                        }else if(!requestIDExists($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"No existe pedido con ese requestID para requestGiven");
                        }else{
                            $requestGiven=requestGiven($_PUT["requestID"]);
                            if($requestGiven){
                                $body=array("errno"=>200,"error"=>"Pedido marcado como enviado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al marcar el pedido como enviado");
                            }
                        }
                        break;
                    case 'requestNotGiven':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para requestNotGiven");
                        }else if(trim($_PUT["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para requestNotGiven");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para requestNotGiven");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]==1 OR getUserByToken($_PUT["token"])["permissionID"]==2){
                            $body=array("errno"=>403,"error"=>"No tiene permitido poner como enviado a un pedido sin ser encargado de un sector");
                        }else if(!isset($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"requestID no definido para requestNotGiven");
                        }else if(trim($_PUT["requestID"])==""){
                            $body = array("errno"=>400,"error"=>"requestID vacio para requestNotGiven");
                        }else if(!requestIDExists($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"No existe pedido con ese requestID para requestNotGiven");
                        }else{
                            $requestNotGiven=requestNotGiven($_PUT["requestID"]);
                            if($requestNotGiven){
                                $body=array("errno"=>200,"error"=>"Pedido marcado como no enviado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al marcar el pedido como no enviado");
                            }
                        }
                        break;
                    case 'modifyTransfer':
                        $data_editable = array(
                            'userID' => "",
                            'sectorID' => ""
                        );
                        foreach($_PUT as $key => $value){
                            if(!isset($_PUT[$key])){
                                $body = array("errno" => 404, "error" => "No está seteado el campo " .$key);
                            }else{
                                if(($_PUT[$key])==""){
                                    $body = array("errno" => 404, "error" => "Falta el campo ".$key);
                                    break;
                                } else {
                                    if($key!="action" && $key!="transferID"){
                                        if($key=="userID" || $key=="sectorID"){
                                            $data_editable[$key] = $value;
                                        }
                                    }
                                }
                            }
                        }
                        if(empty($data_editable)){
                            $body =array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: sector");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                                break;
                            }
                        }
                        if(!isset($_PUT['transferID'])){
                            $body = array("errno" => 400, "error" => "transferID no declarado para modifyTransfer");
                        } else {
                            if(transferIDExists($_PUT['transferID'])){
                                $updateTransfer=updateTransferByID($_PUT['transferID'],$data_editable);
                                if($updateTransfer===true){
                                    $body = array("errno" => 200, "error" => "Traspaso modificado correctamente");
                                } else if ($updateTransfer===false) {
                                    $body = array("errno" => 300, "error" => "No se modificó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $updateTransfer);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Traspaso no encontrado");
                            }
                        }
                        break;
                    case 'modifyUser':
                        $data_editable = array(
                            'dni' => "",
                            'firstName' => "",
                            'lastName' => "",
                            'medicalLicense' => "",
                            'phoneNumber' => "",
                            'birthDate' => ""
                        );
                        foreach($_PUT as $key => $value){
                            if(!isset($_PUT[$key])){
                            $body = array("errno" => 404, "error" => "No está seteado el campo " .$key);
                            }else{
                            if(($_PUT[$key])==""){
                                $body = array("errno" => 404, "error" => "Falta el campo ".getUserFieldsTranslations()[$key]);
                                break 2;
                            } else {
                                if($key!="action" && $key!="token"){
                                $data_editable[$key] = $value;
                                }
                            }
                            }
                            // ! Comentamos todo, porque no hace falta chequear si faltan o no.
                            // ! Al final si xd
                        }
                        if(empty($data_editable)){
                            $body =array("errno"=>400,"error"=>"Debe modificar al menos un campo, por ejemplo: nombre");
                        }
                        // ! Creo que es al pedo verificar primero que sean todos alfanumericos
                        /* foreach($data_editable as $key => $value){
                            if($key!="birthDate"){
                            if(!checkInput($value,CHECK__ALPHANUMERIC)){
                                $body = array("errno" => 400, "error" => "El campo ".$key." es inválido");
                            }
                            }
                        } */
                        if(!checkInput($data_editable["phoneNumber"],CHECK__PHONE__NUMBER)){
                            $body = array("errno" => 400, "error" => "Debe ingresar solamente números en el campo teléfono");
                        }else{
                            $data_editable["phoneNumber"]=str_replace(' ', '',$data_editable["phoneNumber"]);
                            $data_editable["phoneNumber"]=str_replace('-', '',$data_editable["phoneNumber"]);
                        }
                        if(!checkInput($data_editable["medicalLicense"],CHECK__NUMBERS)){
                            $body = array("errno" => 400, "error" => "Debe ingresar solamente números en el campo matrícula");
                        }
                        if(strlen($data_editable["medicalLicense"])!=6){
                            $body = array("errno" => 400, "error" => "La matrícula médica debe estar compuesta por 6 carácteres");
                        }
                        if(!checkInput($data_editable["birthDate"],CHECK__DATE__BIRTH)){
                            $body = array("errno" => 400, "error" => "La edad mínima es 18");
                        }
                        if(!checkInput($data_editable["birthDate"],CHECK__DATE)){
                            $body = array("errno" => 400, "error" => "El campo fecha de nacimiento es inválido");
                        }
                        if(!checkInput($data_editable["dni"],CHECK__NUMBERS)){
                         $body = array("errno" => 400, "error" => "Debe ingresar solamente números en el campo DNI");
                        }
                        if(!checkInput($data_editable["lastName"],CHECK__LETTERS)){
                            $body = array("errno" => 400, "error" => "Debe ingresar solamente letras en el campo apellido");
                        } 
                        if(!checkInput($data_editable["firstName"],CHECK__LETTERS)){
                            $body = array("errno" => 400, "error" => "Debe ingresar solamente letras en el campo nombre");
                        }
                        if (isset($body["errno"])) {
                            if($body["errno"]==400){
                            break;
                            }
                        }
                        if(!isset($_PUT['token'])){
                            $body = array("errno" => 400, "error" => "Token no declarado");
                        } else {
                            if(userTokenExists($_PUT['token'])){
                            $updateUser=updateUserByToken($_PUT['token'],$data_editable);
                            if($updateUser===true){
                                $body = array("errno" => 200, "error" => "Usuario modificado correctamente");
                            } else if ($updateUser===false) {
                                $body = array("errno" => 300, "error" => "No se modificó nada");
                            }else{
                                $body = array("errno" => 400, "error" => $updateUser);
                            }
                            } else {
                            $body = array("errno" => 400, "error" => "Usuario no encontrado");
                            }
                        }
                        break;
                        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    case 'sendModificationMail':
                        if(!isset($_PUT["email"])){
                            $body = array("errno"=>400,"error"=>"email no definido para sendModificationMail");
                        }else if(trim($_PUT["email"])==""){
                            $body = array("errno"=>400,"error"=>"email vacio para sendModificationMail");
                        }else if(!userEmailExists($_PUT["email"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese email para sendModificationMail");
                        }else{
                            $userEmail = $_PUT["email"];
                            $sendMail=sendModificationMail($userEmail);
                            if($sendMail){
                                $body=array("errno"=>200,"error"=>"Mail de modificación enviado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Hubo un error al mandar el mail");
                            }
                        }
                        break;
                    case 'modifyUserPassword':
                        if(!isset($_PUT["email"])){
                            $body = array("errno"=>400,"error"=>"email no definido para modifyUserPassword");
                        }else if(trim($_PUT["email"])==""){
                            $body = array("errno"=>400,"error"=>"email vacio para modifyUserPassword");
                        }else if(!userEmailExists($_PUT["email"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese email para modifyUserPassword");
                        }else if(!isset($_PUT["password"])){
                            $body = array("errno"=>400,"error"=>"password no definido para modifyUserPassword");
                        }else if(trim($_PUT["password"])==""){
                            $body = array("errno"=>400,"error"=>"password vacio para modifyUserPassword");
                        }else if(!isset($_PUT["repassword"])){
                            $body = array("errno"=>400,"error"=>"repassword no definido para modifyUserPassword");
                        }else if(trim($_PUT["repassword"])==""){
                            $body = array("errno"=>400,"error"=>"repassword no definido para modifyUserPassword");
                        }else if(!isset($_PUT["modificationCode"])){
                            $body = array("errno"=>400,"error"=>"modificationCode no definido para modifyUserPassword");
                        }elseif(trim($_PUT["modificationCode"])==""){
                            $body = array("errno"=>400,"error"=>"modificationCode no definido para modifyUserPassword");
                        }else{
                            $modificationCode=getUserModificationCodeByEmail($_PUT["email"]);
                            if($_PUT["modificationCode"]!=$modificationCode){
                                $body = array("errno"=>400,"error"=>"El código de modificación adjunto no coincide con los registros de la base de datos");
                            }
                            if (!checkInput($_PUT['password'],CHECK__PASS)){
                                $body = array("errno" => 400, "error" => "La contraseña es inválida. Debe tener al menos una mayúscula, una minúscula, un número y un caracter especial (@#-_$%^&+=§!?).");
                            }
                            if (!checkInput($_PUT['repassword'],CHECK__PASS)){
                                $body = array("errno" => 400, "error" => "La contraseña es inválida. Debe tener al menos una mayúscula, una minúscula, un número y un caracter especial (@#-_$%^&+=§!?).");
                            }
                            if($_PUT["password"]!=$_PUT["repassword"]){
                                $body = array("errno"=>400,"error"=>"Las contraseñas no coinciden");
                            }
                            if(isset($body)){
                                break;
                            }
                            $updatePassword=updatePassByEmail($_PUT["email"],$_PUT["password"]);
                            if($updatePassword){
                                $body=array("errno"=>200,"error"=>"Contraseña cambiada correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al cambiar la contraseña");
                            }
                        }
                        break;
                    case 'acceptRequest':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para acceptRequest");
                        }else if(trim($_PUT["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para acceptRequest");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para acceptRequest");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]!=1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido aceptar un pedido sin ser farmacéutico");
                        }else if(!isset($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"requestID no definido para acceptRequest");
                        }else if(trim($_PUT["requestID"])==""){
                            $body = array("errno"=>400,"error"=>"requestID vacio para acceptRequest");
                        }else if(!requestIDExists($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"No existe pedido con ese requestID para acceptRequest");
                        }else{
                            $acceptRequest=acceptRequest($_PUT["requestID"]);
                            if($acceptRequest){
                                $body=array("errno"=>200,"error"=>"Pedido marcado como aceptado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al marcar el pedido como aceptado");
                            }
                        }
                        break;
                    case 'rejectRequest':
                        if(!isset($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"token no definido para rejectRequest");
                        }else if(trim($_PUT["token"])==""){
                            $body = array("errno"=>400,"error"=>"token vacio para rejectRequest");
                        }else if(!userTokenExists($_PUT["token"])){
                            $body = array("errno"=>400,"error"=>"No existe usuario con ese token para rejectRequest");
                        }else if(getUserByToken($_PUT["token"])["permissionID"]!=1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido rechazar un pedido sin ser farmacéutico");
                        }else if(!isset($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"requestID no definido para rejectRequest");
                        }else if(trim($_PUT["requestID"])==""){
                            $body = array("errno"=>400,"error"=>"requestID vacio para rejectRequest");
                        }else if(!requestIDExists($_PUT["requestID"])){
                            $body = array("errno"=>400,"error"=>"No existe pedido con ese requestID para rejectRequest");
                        }else{
                            $rejectRequest=rejectRequest($_PUT["requestID"]);
                            if($rejectRequest){
                                $body=array("errno"=>200,"error"=>"Pedido marcado como rechazado correctamente");
                            }else{
                                $body=array("errno"=>400,"error"=>"Ocurrió un error al marcar el pedido como rechazado");
                            }
                        }
                        break;
                    default:
                        $body = array("errno" =>400, "error" => "action no válida para el método PUT");
                    break;
                }
            }
        break;
        case 'DELETE':
            $_DELETE=json_decode(file_get_contents("php://input"));
            $_DELETE=get_object_vars($_DELETE);

            if(!isset($_DELETE['action'])){
                $body = array("errno" =>400, "error" => "action no declarada para el método DELETE");
            } else if($_DELETE['action']==""){
                $body = array("errno" =>400, "error" => "action no tiene ningún valor");
            } else {
                switch($_DELETE['action']){
                    case 'deleteCity':
                        if(!isset($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para deleteCity");
                        }else if($_DELETE["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para deleteCity");
                        }else if(!userTokenExists($_DELETE["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para deleteCity");
                        }else if(getUserByToken($_DELETE["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido eliminar ciudades como usuario normal");
                        }else if(!isset($_DELETE['cityID'])){
                            $body = array("errno" => 400, "error" => "CityID no declarado");
                        } else {
                            if(cityIDExists($_DELETE['cityID'])){
                            $deleteCity=deleteCityByID($_DELETE['cityID']);
                            if($deleteCity===true){
                                $body = array("errno" => 200, "error" => "Ciudad eliminada");
                            } else if ($deleteCity===false) {
                                $body = array("errno" => 300, "error" => "No se eliminó nada");
                            }else{
                                $body = array("errno" => 400, "error" => $deleteCity);
                            }
                            } else {
                            $body = array("errno" => 400, "error" => "Ciudad no encontrada");
                            }
                        }
                        break;
                    case 'deleteOrder':
                        /*if(!isset($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para deleteOrder");
                        }else if($_DELETE["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para deleteOrder");
                        }else if(!userTokenExists($_DELETE["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para deleteOrder");
                        }else if(!isset($_DELETE['orderID'])){
                            $body = array("errno" => 400, "error" => "orderID no declarado");
                        }else if(getUserPermissionsByID(getUserIDByToken($_DELETE["token"]))!=1){
                            $body = array("errno" => 400, "error" => "No tiene los permisos necesarios para esta operación");
                        } else {
                            if(orderIDExists($_DELETE['orderID'])){
                                $deleteOrder=deleteOrderByID($_DELETE['orderID']);
                                if($deleteOrder===true){
                                    $body = array("errno" => 200, "error" => "Orden eliminada");
                                } else if ($deleteOrder===false) {
                                    $body = array("errno" => 300, "error" => "No se eliminó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $deleteOrder);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Orden no encontrada");
                            }
                        }*/
                        break;
                    case 'deleteRemoval':
                        if(!isset($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para deleteRemoval");
                        }else if($_DELETE["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para deleteRemoval");
                        }else if(!userTokenExists($_DELETE["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para deleteRemoval");
                        }else if(!isset($_DELETE['removalID'])){
                            $body = array("errno" => 400, "error" => "removalID no declarado");
                        }else if (trim($_DELETE["removalID"])==""){
                            $body = array("errno" => 400, "error" => "removalID vacío");
                        }else if(getUserPermissionsByID(getUserIDByToken($_DELETE["token"]))!=1){
                            $body = array("errno" => 400, "error" => "No tiene los permisos necesarios para esta operación");
                        } else {
                            if(removalIDExists($_DELETE['removalID'])){
                                $deleteRemoval=deleteRemovalByID($_DELETE['removalID']);
                                if($deleteRemoval===true){
                                    $body = array("errno" => 200, "error" => "Eliminación anulada");
                                } else if ($deleteRemoval===false) {
                                    $body = array("errno" => 300, "error" => "No se anuló nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $deleteRemoval);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Eliminación no encontrada");
                            }
                        }
                        break;
                    case 'deleteProduct':
                        if(!isset($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para deleteProduct");
                        }else if($_DELETE["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para deleteProduct");
                        }else if(!userTokenExists($_DELETE["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para deleteProduct");
                        }else if(getUserByToken($_DELETE["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido eliminar productos como usuario normal");
                        }else if(!isset($_DELETE['productID'])){
                            $body = array("errno" => 400, "error" => "productID no declarado");
                        } else {
                            if(productIDExists($_DELETE['productID'])){
                                $deleteProduct=deleteProductByID($_DELETE['productID']);
                                if($deleteProduct===true){
                                    $body = array("errno" => 200, "error" => "Producto eliminado");
                                } else if ($deleteProduct===false) {
                                    $body = array("errno" => 300, "error" => "No se eliminó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $deleteProduct);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Producto no encontrado");
                            }
                        }
                        break;
                    case 'deleteSector':
                        /*if(!isset($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para deleteSector");
                        }else if($_DELETE["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para deleteSector");
                        }else if(!userTokenExists($_DELETE["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para deleteSector");
                        }else if(getUserByToken($_DELETE["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido eliminar sectores como usuario normal");
                        }else if(!isset($_DELETE['sectorID'])){
                            $body = array("errno" => 400, "error" => "sectorID no declarado");
                        } else {
                            if(sectorIDExists($_DELETE['sectorID'])){
                                $deleteSector=deleteSectorByID($_DELETE['sectorID']);
                                if($deleteSector===true){
                                    $body = array("errno" => 200, "error" => "Sector eliminado");
                                } else if ($deleteSector===false) {
                                    $body = array("errno" => 300, "error" => "No se eliminó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $deleteSector);
                                }
                            } else {
                            $body = array("errno" => 400, "error" => "Sector no encontrado");
                            }
                        }*/
                        break;
                    case 'deleteSupplier':
                        if(!isset($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"token indefinido para deleteSupplier");
                        }else if($_DELETE["token"]==""){
                            $body=array("errno"=>400,"error"=>"token vacio para deleteSupplier");
                        }else if(!userTokenExists($_DELETE["token"])){
                            $body=array("errno"=>400,"error"=>"No existe usuario con ese token para deleteSupplier");
                        }else if(getUserByToken($_DELETE["token"])["permissionID"]==1){
                            $body=array("errno"=>403,"error"=>"No tiene permitido eliminar proveedores como usuario normal");
                        }else if(!isset($_DELETE['supplierID'])){
                            $body = array("errno" =>400, "error" => "supplierID no definida para modifySupplierByID");
                        } else if($_DELETE['supplierID']==""){
                            $body = array("errno" =>400, "error" => "supplierID no tiene ningún valor");
                        } else if(supplierIDExists($_DELETE['supplierID'])){
                            $deleteSupplierByID = deleteSupplierByID($_DELETE['supplierID']);
                            if($deleteSupplierByID===true){
                                $body = array("errno" =>200, "error" => "proveedor eliminado con éxito");
                            } else {
                                $body = array("errno" =>400, "error" => $deleteSupplierByID);
                            }
                        } else {
                            $body = array("errno" =>400, "error" => "El proveedor no existe");
                        }
                        break;
                    case 'deleteTransfer':
                        /*if(!isset($_DELETE['transferID'])){
                            $body = array("errno" => 400, "error" => "transferID no declarado");
                        } else {
                            if(transferIDExists($_DELETE['transferID'])){
                                $deleteTransfer=deleteTransferByID($_DELETE['transferID']);
                                if($deleteTransfer===true){
                                    $body = array("errno" => 200, "error" => "Traspaso eliminado");
                                } else if ($deleteTransfer===false) {
                                    $body = array("errno" => 300, "error" => "No se eliminó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $deleteTransfer);
                                }
                            } else {
                                $body = array("errno" => 400, "error" => "Traspaso no encontrado");
                            }
                        }*/
                        break;
                    case 'deleteUser':
                        if(!isset($_DELETE['token'])){
                            $body = array("errno" => 400, "error" => "Token no declarado");
                        } else {
                            if(userTokenExists($_DELETE['token'])){
                                $deleteUser=deleteUserByToken($_DELETE['token']);
                                if($deleteUser===true){
                                    $body = array("errno" => 200, "error" => "Usuario eliminado");
                                } else if ($deleteUser===false) {
                                    $body = array("errno" => 300, "error" => "No se eliminó nada");
                                }else{
                                    $body = array("errno" => 400, "error" => $deleteUser);
                                }
                            } else {
                            $body = array("errno" => 400, "error" => "Usuario no encontrado");
                            }
                        }
                        break;
                    case 'deleteUserByID':
                        if(!isset($_DELETE['token'])){
                            $body = array("errno" => 400, "error" => "Token no declarado para deleteUserByID");
                        } else if(trim($_DELETE["token"])=="") {
                            $body = array("errno"=>400,"error"=>"Token vacio para deleteUserByID");
                        } else if(!userTokenExists($_DELETE["token"])){
                            $body = array("errno"=>400,"error"=>"Ningun usuario tiene ese token para deleteUserByID");
                        } else if (getUserByToken($_DELETE["token"])["permissionID"]==1){
                            $body = array("errno"=>403,"error"=>"No tiene permitido eliminar otros usuarios como usuario normal");
                        }else if(!isset($_DELETE["userID"])){
                            $body = array("errno"=>400,"error"=>"userID no declarado para deleteUserByID");
                        }else if(trim($_DELETE["userID"])==""){
                            $body = array("errno"=>400,"error"=>"userID vacio para deleteUserByID");
                        }else if(!userIDExists($_DELETE["userID"])){
                            $body = array("errno"=>400,"error"=>"Ningun usuario tiene ese userID para deleteUserByID");
                        }else{
                            $deleteUser = deleteUserByID($_DELETE["userID"]);
                            if($deleteUser===true){
                                $body = array("errno" => 200, "error" => "Usuario eliminado");
                            } else if ($deleteUser===false) {
                                $body = array("errno" => 300, "error" => "No se eliminó nada");
                            }else{
                                $body = array("errno" => 400, "error" => $deleteUser);
                            }
                        }
                        break;
                        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    default:
                        $body =array("errno"=>400,"error"=>"Action inválida en método DELETE");
                    
                }
            }
        break;
    }
    echo json_encode($body);


?>