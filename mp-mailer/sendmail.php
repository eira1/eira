<?php

/*  Requiere por metodo post:

    send: valor "mail"
    destinatario: email del receptor
    motivo: motivo del mensaje que verá el receptor
    contenido: mensaje que verá el receptor, acepta etiquetas html
*/

include_once 'credenciales.php';
include_once 'Mailer/src/PHPMailer.php';
include_once 'Mailer/src/SMTP.php';
include_once 'Mailer/src/Exception.php';

$mail = new PHPMailer\PHPMailer\PHPMailer();
/*     $_POST["send"]="mail";
    $_POST["destinatario"]="manu44rafa44@gmail.com";
    $_POST["motivo"]="hola papi";
    $_POST["contenido"]="hola papi pepo"; */
if(isset($_POST["send"])){

    if($_POST["send"]=="mail"){

        $mail->isSMTP();
        $mail->SMTPDebug = 0 ;
        $mail->Host = HOST__MAIL;
        $mail->Port = PORT;
        $mail->SMTPAuth = SMTP_AUTH; //
        $mail->SMTPSecure = SMTP_SECURE;
        $mail->Username = REMITENTE;
        $mail->Password = PASSWORD;

        $mail->setFrom(REMITENTE, NOMBRE);
        $mail->addAddress($_POST["destinatario"]);

        $mail->isHTML(true);

        $mail->Subject = utf8_decode($_POST["motivo"]);
        $mail->Body = utf8_decode($_POST["contenido"]);

        if(!$mail->send()){
            error_log("Mailer no se pudo enviar el correo!" );
			$body = array("errno" => 400, "error" => "No se pudo enviar.");
        }else{
			$body = array("errno" => 200, "error" => "Enviado con exito.");
		}   
    }else{
		$body= array("errno" => 400, "error" => "falta accion mail");
	}
}else{

	$body = array("errno" => 400, "error" => "falta variable send");
}

header("Content-Type: application/json");

echo json_encode($body);
 
?>


