<?php
/*  Requiere por metodo post:

    send: valor "mail"
    destinatario: email del receptor
    motivo: motivo del mensaje que verá el receptor
    contenido: mensaje que verá el receptor, acepta etiquetas html
*/

include_once 'credenciales.php';
include_once 'Mailer/src/PHPMailer.php';
include_once 'Mailer/src/SMTP.php';
include_once 'Mailer/src/Exception.php';

$mail = new PHPMailer\PHPMailer\PHPMailer();
/*     $_POST["send"]="mail";
    $_POST["destinatario"]="manu44rafa44@gmail.com";
    $_POST["motivo"]="hola papi";
    $_POST["contenido"]="hola papi pepo"; */

        $mail->isSMTP();
        $mail->SMTPDebug = 0 ;
        $mail->Host = HOST__MAIL;
        $mail->Port = PORT;
        $mail->SMTPAuth = SMTP_AUTH; //
        $mail->SMTPSecure = SMTP_SECURE;
        $mail->Username = REMITENTE;
        $mail->Password = PASSWORD;

        $mail->setFrom(REMITENTE, NOMBRE);
        $mail->addAddress($destinatario);

        $mail->isHTML(true);

        $mail->Subject = utf8_decode($motivo);
        $mail->Body = utf8_decode($contenido);

        if(!$mail->send()){
            error_log("Mailer no se pudo enviar el correo!" );
			$return = false;
        }else{
			$return = true;
		} 
?>