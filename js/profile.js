if (localStorage.getItem('token') === null) {
    window.location.replace('/eira/login');
}


/**
 * TODO: Hacer verificaciones para celular.
 * TODO: Crear el Delete.
 */


document.addEventListener('DOMContentLoaded', () => {

    hideElement(msg_success);
    hideElement(msg_error);

    btn_update.onclick = (e) => {
        e.preventDefault();
        updateData();
    }

    getUserData().then(data => {
        const userData = data;
        const inputList = document.querySelectorAll('.input-info > input');

        inputList.forEach(inp => {
            let inputName = getInputName(inp);
            inp.value = userData[inputName];
        });
        if (input_dni.value == 0) {
            input_dni.value = "";
        }

    });

    btn_dark_mode.checked = localStorage.getItem('darkMode') == 'true';
    btn_dark_mode.onclick = () => {
        localStorage.setItem('darkMode', btn_dark_mode.checked);
        updateDarkMode();
    }
});



function updateData() {
    const inputList = document.querySelectorAll('.input-info > input');
    let sendedData = {
        "action": 'modifyUser',
		"token": localStorage.getItem('token')
    };

    loading_screen.style.opacity = 1;
	loading_screen.style.pointerEvents = 'all';

	hideElement(msg_error);
	hideElement(msg_success);

    inputList.forEach(inp => {
        const inputName = getInputName(inp);
		sendedData[inputName] = inp.value;
    });

    put(sendedData).then(data => {

		loading_screen.style.opacity = null;
		loading_screen.style.pointerEvents = null;
		window.scrollTo(0, 0);

		if (data === undefined) {
			showElement(msg_error);
			msg_error.textContent = CONN_ERROR_MSG;

			return;
		}
	
		if (data.errno == 200) {

			showElement(msg_success);
			const msg = "¡Datos modificados con éxito!";
			msg_success.textContent = msg;
		}
		else {
			showElement(msg_error);
			msg_error.textContent = data.error;
		}
	});
}


function getInputName(inp) {
    return inp.id.replace('input_', '');
}
	