if (localStorage.getItem('token') === null) {
    window.location.replace('/eira/login');
}

const USER_PERMISSION_SECTOR_MANAGER = 3;
const MOBILE_WIDTH = 575;

var productList;
var selectedProducts = [];

const LIST_TYPES = {
    product: 'product',
    transfer: 'transfer',
    order: 'transfer',
    consumption: 'transfer',
    request: 'request',
    expiredLote: 'lote'
};

const VALID_PDF_LISTS = [
    'transfer',
    'order'
];


document.addEventListener('DOMContentLoaded', initPanel);

async function initPanel() {
    setElementOn(loading_screen, true);
    await loadAdditionalCode();
    
    const currentWorkZone = sessionStorage.getItem('panel__currentWorkZone');
    switch (localStorage.permission) {
        //* Ojo que los permisos están como String
        case '1':
            addSidePanelOption('notification', 'Novedades', 'circle-exclamation');
            addSidePanelOption('transfer', 'Egresos de productos', 'arrow-right-arrow-left');
            addSidePanelOption('order', 'Ingresos de productos', 'truck-arrow-right');
            addSidePanelOption('product', 'Productos', 'pills');
            addSidePanelOption('request', 'Peticiones', 'hand-holding-medical');
            addSidePanelOption('expiredLote', 'Lotes vencidos', 'calendar-xmark');
            //addSidePanelOption('stat', 'Estadísticas', 'chart-pie');
            selectWorkZoneList(notification_list);
            break;
        case '2':
            addSidePanelOption('unverifiedUser', 'Verificación de usuarios', 'circle-check');
            selectWorkZoneList(unverifiedUser_list);
            break;
        default:
            addSidePanelOption('request', 'Peticiones', 'hand-holding-medical');
            addSidePanelOption('transfer', 'Ingresos de productos', 'truck-arrow-right');
            addSidePanelOption('consumption', 'Consumos', 'vial-circle-check');
            addSidePanelOption('product', 'Productos', 'pills');
            selectWorkZoneList(request_list);
            break;
    }
    //* Esto no lo borres
    /* const notAmount = document.createElement('div');
    notAmount.id = 'notification_amount';
    document.querySelector('#btn_notifications').appendChild(notAmount); */
    if (currentWorkZone !== null) {
        selectWorkZoneList(document.getElementById(currentWorkZone));
    }

    product_list.querySelector('.filters > .search').oninput = () => {
        filterProductList(product_list, product_list.querySelector('.filters > .search').value);
    }
    document.querySelectorAll('.new_container > .details > .product_list').forEach(list => {
        list.ondragover = (e) => {
            e.preventDefault();
        }
        list.ondraleave = (e) => {
            e.preventDefault();
        }
    });


    window.onmousedown = (e) => {
        if (defaultClickEvent(e)) {
            document.querySelectorAll('.popup').forEach(popup => {
                if (! popup.contains(e.target)) {
                    setElementOn(popup, false);
                }
            });
            checkPopups();
        }
    }

    await updateWorkZones();
    setInterval(updateWorkZones, 60000);

    initWorkZone();
    
    setElementOn(loading_screen, false);
}



function selectWorkZoneList(list) {
    const btn = document.querySelector(`#btn_${list.id.split('_',)[0]}s`);

    hideWorkZoneLists();
    unselectSidePanelOptions();
    btn.classList.add('selected');
    list.classList.remove('hidden');

    sessionStorage.setItem('panel__currentWorkZone', list.id);
}
function unselectSidePanelOptions() {
    side_panel.querySelectorAll('.option').forEach(option => {
        option.classList.remove('selected');
    });
}
function hideWorkZoneLists() {
    const lists = document.querySelectorAll('.work_zone_list');

    lists.forEach(list => {
        list.classList.add('hidden');
    });
}


async function updateWorkZones() {
    if (localStorage.getItem('permission') == '1') {
        await updateProductList();
        await updateGenericList('transfer');
        await updateGenericList('order');
        await updateGenericList('request');
        await updateGenericList('expiredLote');
    }
    if (localStorage.getItem('permission') == '2') {
        await updateList('unverifiedUser');
    }
    if (userIsSectorManager()) {
        await updateProductList();
        await updateGenericList('request');
        await updateGenericList('transfer');
        await updateGenericList('consumption');
    }
}


async function loadAdditionalCode() {
	await loadSuperPopup();
    await loadHTML('/eira/views/tpl__work_zone.php', 'main');
    await loadHTML('/eira/views/tpl__panel_templates.php', 'body');

    if (localStorage.getItem('permission') == '1') {
        await loadHTML('/eira/views/tpl__new_transfer.php', 'main');
        await loadHTML('/eira/views/tpl__new_order.php', 'main');
        await loadHTML('/eira/views/tpl__product_details.php', 'main');
    }
    if (userIsSectorManager()) {
        await loadHTML('/eira/views/tpl__new_request.php','body');
        await loadHTML('/eira/views/tpl__new_consumption.php', 'main');
        await loadHTML('/eira/views/tpl__product_details.php', 'main');
    }

    enableBtnQuit();
}


function initWorkZone() {
    if (localStorage.getItem('permission') == '1') {
        initNewList('transfer');
        initNewLoteList('order');

        showElement(btn_show_new_transfer);

        product_list.querySelector('.filters > .search').oninput = () => {
            filterProductList(product_list, product_list.querySelector('.filters > .search').value);
        }

        /* const params = {
            action: 'getTransfersStatistics',
            token: localStorage.getItem('token'),
            productID: 1,
            startDate: '2000-01-01',
            finishDate: '2050-01-01'
        };
        get(params).then(data => {
            console.log(data);
        }); */
    }

    if (userIsSectorManager()) {
        initNewList('request');
        initNewList('consumption');

        showElement(btn_show_new_request);

        product_list.querySelector('.filters > .search').oninput = () => {
            filterProductList(product_list, product_list.querySelector('.filters > .search').value);
        }
        
    }
}

function initNewList(listName) {
    const btnShow = document.querySelector(`#btn_show_new_${listName}`);
    const container = document.querySelector(`#new_${listName}_container`);
    const nproductList = document.querySelector(`#new_${listName}_product_list`);
    const search = document.querySelector(`#new_${listName}_search_products`);
    const btnAddCard = document.querySelector(`#new_${listName}_details_add_product_card`);
    const detailsList = document.querySelector(`#new_${listName}_details_product_list`);
    const btnClean = document.querySelector(`#btn_clean_${listName}_details_list`);
    const btnCreate = document.querySelector(`#btn_new_${listName}`);
    const detailsProductList = document.querySelector(`#new_${listName}_details_product_list`);

    if(typeof window[`updateNew${capitalize(listName)}Screen`] === 'function') {
        window[`updateNew${capitalize(listName)}Screen`]();
    }

    btnShow.onclick = () => {
        setElementOn(container, true);
        updateNewListProductList(nproductList);
        if(typeof window[`updateNew${capitalize(listName)}Screen`] === 'function') {
            window[`updateNew${capitalize(listName)}Screen`]();
        }
        checkPopups();
    }
    search.oninput = () => {
        updateNewListProductList(nproductList);
    }
    btnClean.onclick = () => {
        clearList(detailsList);
        selectedProducts = [];
        onNewListChanged(detailsList);
    }
    btnCreate.onclick = () => {
        createFromNewList(listName, getNewListAdditionalParams(listName));
    }

    detailsProductList.ondrop = (e) => {
        e.preventDefault();
        if (e.dataTransfer.getData('object') != 'New product card') {
            return;
        }
        const data = {
            'productID': e.dataTransfer.getData('productID'),
            'name': e.dataTransfer.getData('productName'),
            'amount': e.dataTransfer.getData('productAmount')
        }
        addNewListDetailsProductCard(data, listName);
    }

    btnAddCard.onchange = () => {
        const productData = productList.find(item => item.code == btnAddCard.value);
        if (! productData) {
            return;
        }

        let name = productData.name;
        if (productData.concentration != 'NULL' && productData.concentration != undefined) {
            name += ` - ${productData.concentration}`;
        }

        const data = {
            'productID': productData.productID,
            'name': name,
            'amount': productData.currentAmount
        }
        addNewListDetailsProductCard(data, listName);
        btnAddCard.value = '';
    }
    
    container.addEventListener('popup_closed', () => {
        cleanNewScreen(listName);
        container.querySelectorAll('.input-info > input').forEach(input => {
            input.value = '';
        });
    });
}


function initNewLoteList(listName) {
    const btnShow = document.querySelector(`#btn_show_new_${listName}`);
    const container = document.querySelector(`#new_${listName}_container`);
    const detailsList = document.querySelector(`#new_${listName}_details_lote_list`);
    const btnClean = document.querySelector(`#btn_clean_${listName}_details_list`);
    const btnCreate = document.querySelector(`#btn_new_${listName}`);

    btnShow.onclick = () => {
        setElementOn(container, true);
        if(typeof window[`updateNew${capitalize(listName)}Screen`] === 'function') {
            window[`updateNew${capitalize(listName)}Screen`]();
        }
        checkPopups();
    }
    btnClean.onclick = () => {
        clearList(detailsList);
        addNewLoteCard(listName);
    }
    btnCreate.onclick = () => {
        createFromNewLoteList(listName, getNewListAdditionalParams(listName));
    }

    addNewLoteCard('order');
}


function initProductDetails() {
    product_details.querySelector('input[type="button"]').onclick = () => {
        let newProductData = {};
    }
}



function getNewListAdditionalParams(listName) {
    switch (listName) {
        case 'transfer':
            return {
            'sectorID': new_transfer_sector.value,
            /* 'person': new_transfer_person.value */
            };
        case 'order':
            return {
                'supplierID': new_order_supplier.value,
            };
        case 'request':
            return {
                'personName': new_request_patientName.value,
                'personDNI': new_request_patientDNI.value,
                'details': new_request_detailsText.value
            };
        default:
            return {};
    }
}


/* Get Lists ==================================================================================== */
/**
 * @param {string} action Action a ejecutar en la api
 * @returns {Array}
 */
async function getListFromServer(action) {
    try {
		const response = await fetch(`https://escuelarobertoarlt.com.ar/eira/api/?action=${action}&token=${localStorage.getItem('token')}`);
		const data = await response.json();

		return data;
	} catch (error) {
		console.error(error);
	}
}

async function getProductList() {
    if (localStorage.permission == '1') {
        return await getListFromServer('getProducts');
    }
    else {
        let data = await getListFromServer('getProductsBySector');
        return data;
    }
}
async function getTransferList() {
    const data = await getListFromServer('getTransfers');
    return data;
}
async function getOrderList() {
    const data = await getListFromServer('getOrders');
    return data;
}
async function getConsumptionList() {
    const data = await getListFromServer('getConsumptions');
    return data;
}






function clearList(list) {
    list.querySelectorAll('.card').forEach(card => {
        list.removeChild(card);
    });
}





/* Product List ================================================================================= */
async function updateProductList() {

    const data = await getProductList();

    clearList(product_list);
    if (data === undefined) {
        return;
    }
    productList = data;
    
    
    filterProductList(product_list, product_list.querySelector('.filters > .search').value);
    /* data.forEach(product => {
        addProductCard(product, product_list);
    }); */

    setProductListOnclick();

    document.querySelectorAll('.new_products > .product_list').forEach(list => {
        updateNewListProductList(list);
    });

    updateProductDatalist();
    if (localStorage.permission == 1) {
        updateNotificationList();
    }
}


function updateProductDatalist() {
    product_datalist.querySelectorAll('option').forEach(option => {
        option.remove();
    });
    productList.forEach(product => {
        const option = document.createElement('option');
        option.value = product.code;
        option.textContent = product.name;
        if (product.concentration != 'NULL' && product.concentration != undefined) {
            option.textContent += ` - ${product.concentration}`;
        }
        option.textContent += ` | Cantidad: ${product.currentAmount}`;
        product_datalist.appendChild(option);
    });
}


function addProductCard(data, list) {
    const tpl = tpl__product_card.content;
    const clone = tpl.cloneNode(true);
    
    const card = clone.querySelector('.card');
    card.setAttribute('productID', data['productID']);
    card.setAttribute('code', data['code']);
    card.setAttribute('concentration', data['concentration']);
    card.setAttribute('form', data['form']);
    card.setAttribute('amount', data['currentAmount']);
    card.setAttribute('desc', data['description']);
    if (! userIsSectorManager()) {
        card.setAttribute('expiredAmount', data['expiredAmount']);
        card.setAttribute('criticalStock', data['criticalStock']);

        clone.querySelector('.expired_amount').innerHTML = `<i class="fa-solid fa-boxes-stacked"></i> ${data['expiredAmount']}`;
        clone.querySelector('.expired_amount').title = `Vencidos: ${data['expiredAmount']}`;
    }
    else {
        clone.querySelector('.expired_amount').remove();
    }

    clone.querySelector('.name').textContent = `${data['name']}`;
    if (data['concentration']) {
        clone.querySelector('.name').textContent += ` - ${data['concentration']}`;
    }
    clone.querySelector('.name').title = data['name'];
    clone.querySelector('.amount').innerHTML = `<i class="fa-solid fa-boxes-stacked"></i> ${data['currentAmount']}`;
    clone.querySelector('.amount').title = `Cantidad disponible: ${data['currentAmount']}`;
    clone.querySelector('img').src = `${PRODUCT_PHOTO_PATH}${data['photo']}`;
    list.appendChild(clone);
}


function setProductListOnclick() {
    product_list.querySelectorAll('.card').forEach(card => {
        card.onclick = () => {
            setElementOn(product_details, true);
            product_details.setAttribute('productID', card.getAttribute('productID'));
            product_details.querySelector('.img').style.backgroundImage = `url(${card.querySelector('img').src})`;
            product_details__name.value = card.querySelector('.name').textContent;
            product_details__code.value = card.getAttribute('code');
            product_details__form.value = card.getAttribute('form');
            product_details__amount.value = card.getAttribute('amount');
            
            product_details__desc.value = card.getAttribute('desc');
            
            if (localStorage.permission != 2) {
                product_details.querySelectorAll('input').forEach(inp => {
                    inp.disabled = true;
                });
                product_details__desc.disabled = true;
                product_details.querySelector('input[type="button"]').title = "No tiene permisos para modificar los productos.";
            }
            else {
                product_details.querySelectorAll('input').forEach(inp => {
                    inp.disabled = false;
                });
                product_details__desc.disabled = false;
                product_details.querySelector('input[type="button"]').title = "Guardar cambios";
            }

            if (userIsSectorManager()) {
                hideElement(product_details__lotes_container);
                hideElement(product_details__criticalStock.parentElement);
                hideElement(product_details__expiredAmount.parentElement);
            }
            else {
                product_details__criticalStock.value = card.getAttribute('criticalStock');
                product_details__expiredAmount.value = card.getAttribute('expiredAmount');
                updateProductDetailsLoteList();
            }
            checkPopups();
        }
    });
}


/* Transfer List =================================================================================== */


function addTransferCard(data, list, first = false) {
    const listName = list.id.split('_')[0];
    const tpl = tpl__transfer_card.content;
    const clone = tpl.cloneNode(true);
    let date = data[`${listName}Date`];
    if ('sector' in data) {
        clone.querySelector('.sector').textContent = data['sector'];
    }
    else {
        clone.querySelector('.sector').textContent = data['supplier'];
    }
    date = formatDate(date);
    clone.querySelector('.date').textContent = date;
    clone.querySelector('.card').setAttribute(`${listName}ID`, data[`${listName}ID`]);

    clone.querySelector('.username').innerHTML = `por <span class="bold">${data['userFirstName']} ${data['userLastName']}</span>`;
    /* if ('person' in data) {
        clone.querySelector('.username').innerHTML += `, a <span class="bold">${data['person']}</span>`;
    } */

    let prodCount = 0;

    data['productList'].forEach( product => {
        addTransferDetail(clone, product, prodCount < 5);
        prodCount++;
    });
    if (prodCount >= 5) {
        const elem = document.createElement('div');
        elem.classList.add('transfer_detail');
        elem.classList.add('text-button');
        elem.textContent = '...';
        elem.onclick = () => {
            elem.parentElement.querySelectorAll('.transfer_detail.hidden').forEach(detail => {
                detail.classList.remove('hidden');
            });
            elem.classList.add('hidden');
        }
        clone.querySelector('.transfer_details').appendChild(elem);
    }

    if (! userIsSectorManager()) {
        if (VALID_PDF_LISTS.includes(listName)) {
            const btnPDF = clone.querySelector('.create_pdf');
            showElement(btnPDF);
            btnPDF.onclick = () => {
                const params = `?action=get${capitalize(listName)}PDF&${listName}ID=${data[`${listName}ID`]}&token=${localStorage.getItem('token')}`;
                window.open(`/eira/api/${params}`, '_blank');
            }
        }
    }
    else {
        clone.querySelector('h3.sector').remove();
    }
    
    if (first) {
        list.prepend(clone);
    }
    else {
        list.appendChild(clone);
    }
}
function addTransferDetail(card, details, visible = true) {
    const tpl = tpl__transfer_detail.content;
    const clone = tpl.cloneNode(true);
    clone.querySelector('.transfer_detail_product').textContent = details['name'];
    clone.querySelector('.transfer_detail_amount').textContent = 'x' + details['amount'];
    if (! visible) {
        clone.querySelector('.transfer_detail').classList.add('hidden');
    }
    card.querySelector('.transfer_details').appendChild(clone);
}



function addLoteCard(data, list, first = false) {
    const listName = list.id.split('_')[0];
    const tpl = tpl__lote_card.content;
    const clone = tpl.cloneNode(true);
    //const card = clone.querySelector('.card.lote');
    let date = data.expirationDate;
    date = formatDate(date, false);

    clone.querySelector('.card').setAttribute(`${listName}ID`, data[`loteID`]);
    clone.querySelector('.loteCode').textContent = `Lote: ${data.loteCode}`;
    clone.querySelector('.date').textContent = `Vencimiento: ${date}`;
    clone.querySelector('.product > .name').textContent = data.name;
    clone.querySelector('.product > .amount').textContent = `x${data.realAmount}`;

    clone.querySelector('.btn_delete').onclick = () => {
        const msg = 'Está seguro de que quiere descartar este lote (ya no aparecerá en la lista). Se recomienda hacerlo una vez que el lote físico haya sido descartado.';
        showSuperPopup('Descartar lote', msg, ['error'], 'Descartar', 'Cancelar').then(response => {
            if (response == 1) {
                const loteList = JSON.stringify([{loteID: data.loteID}]);
                const params = {
                    action: 'createRemoval',
                    token: localStorage.getItem('token'),
                    loteList: loteList
                }
                post(params).then(() => {
                    updateGenericList(listName);
                    updateProductList();
                });
            }
        });
    }

    if (first) {
        list.prepend(clone);
    }
    else {
        list.appendChild(clone);
    }
}





/* New transfer ================================================================================= */


function onNewListChanged(list) {
    const prodList = document.querySelector(`#${list.id.replace('details_', '')}`);
    if (list.querySelectorAll('.card.new_list__product').length == 0) {
        list.querySelector('p.details_msg').classList.add('on');
    }
    else {
        list.querySelector('p.details_msg').classList.remove('on');
    }
    updateNewListProductList(prodList);
}



function addNewListDetailsProductCard(data, listName, defaultData = null) {
    const list = document.querySelector(`#new_${listName}_details_product_list`);

    const tpl = tpl__new_list_product.content;
    const clone = tpl.cloneNode(true);
    clone.querySelector('.card').setAttribute('productID', data.productID);
    clone.querySelector('p.product').textContent = data.name;
    //clone.querySelector('input.product').value = data.code;
    clone.querySelector('.card').setAttribute('amount', data.amount);
    clone.querySelector('.btn_delete').addEventListener('click', deleteNewListDetailsProductCard);

    if (defaultData != null) {
        clone.querySelector('input.amount').value = defaultData.amount;
    }

    selectedProducts.push(data.productID);
    list.prepend(clone);
    onNewListChanged(list);

}
function deleteNewListDetailsProductCard(e) {
    const card = e.currentTarget.parentElement;
    const list = card.parentElement;
    card.remove();
    selectedProducts = selectedProducts.filter(e => e !== card.getAttribute('productID'));
    onNewListChanged(list);
}

function addNewTransferProductCard(data, list) {
    const tpl = tpl__new_list_product.content;
    const clone = tpl.cloneNode(true);
    clone.querySelector('.card').setAttribute('productID', data.productID);
    clone.querySelector('p').textContent = data.name;
    clone.querySelector('.card').setAttribute('amount', data.amount);
    const btnDelete = clone.querySelector('.btn_delete');
    btnDelete.onclick = () => {
        btnDelete.parentElement.remove();
        selectedProducts = selectedProducts.filter(e => e !== data.productID);
        onNewListChanged(list);
    }
    selectedProducts.push(data.productID);
    list.appendChild(clone);
    onNewListChanged(list);
}


function addNewLoteCard(listName) {
    const list = document.querySelector(`#new_${listName}_details_lote_list`);

    const tpl = tpl__new_list_lote.content;
    const clone = tpl.cloneNode(true);
    const card = clone.querySelector('.card.new_list__lote');
    
    const btnDelete = clone.querySelector('.btn_delete');
    btnDelete.onclick = () => {
        btnDelete.parentElement.remove();
        disableElement(list.firstElementChild.querySelector('.btn_delete'), true);

    }

    const inpProduct = clone.querySelector('.product');
    const inpCode = clone.querySelector('.code');
    const inpAmount = clone.querySelector('.amount');
    const inpExpirationDate = clone.querySelector('.expirationDate');
    inpProduct.onchange = () => { 
        if (list.firstElementChild == card) {
            addNewLoteCard(listName);
            const firstCard  = list.firstElementChild;
            disableElement(btnDelete, false);
            disableElement(firstCard.querySelector('.btn_delete'), true);
            
            inpCode.disabled = false;
            inpAmount.disabled = false;
            inpExpirationDate.disabled = false;
            inpExpirationDate.setAttribute('type', 'date');
        }
    }
    
    list.prepend(clone);
}


function updateNewTransferScreen() {
    getSectorList().then(data => {
        new_transfer_sector.querySelectorAll('option').forEach(option => {
            new_transfer_sector.removeChild(option);
        });

        data.forEach(sector => {
            const option = new Option(sector['name'], sector['sectorID']);
            new_transfer_sector.appendChild(option);
        });
    });
    updateNewListProductList(new_transfer_product_list);
}
function updateNewOrderScreen() {
    getSupplierList().then(data => {
        new_order_supplier.querySelectorAll('option').forEach(option => {
            new_order_supplier.removeChild(option);
        });

        data.forEach(supplier => {
            const option = new Option(supplier['supplierName'], supplier['supplierID']);
            new_order_supplier.appendChild(option);
        });
    });
}


async function getSectorList() {
    const params = {
        'action': 'getSectors',
        'token': localStorage.getItem('token')
    }
    return await get(params);
}
async function getSupplierList() {
    const params = {
        'action': 'getSuppliers',
        'token': localStorage.getItem('token')
    }
    return await get(params);
}


function filterProductList(list, name) {
    clearList(list);
    const data = getProductListByName(name);

    if (data == false) {
        return;
    }
    data.forEach(product => {
        if (! selectedProducts.includes(product['productID'])) {
            addProductCard(product, list);
        }
    });

    if (list == product_list) {
        setProductListOnclick();
    }
}


/* function updateNewTransferProductList() {
    const prodName = new_transfer_search_products.value;
    clearList(new_transfer_product_list);

    const data = getProductListByName(prodName);

    if (data == false) {
        return;
    }
    data.forEach(product => {
        if (! selectedProducts.includes(product['productID'])) {
            addProductCard(product, new_transfer_product_list, true);
        }
        
    });
    new_transfer_product_list.querySelectorAll('.card.product').forEach(product => {
        product.setAttribute('draggable', 'true');
        product.title = '';
        product.ondragstart = (e) => {
            e.dataTransfer.setData('object', 'New transfer product card');
            e.dataTransfer.setData('productID', e.target.getAttribute('productID'));
            e.dataTransfer.setData('productName', e.target.querySelector('.name').textContent);
            e.dataTransfer.setData('productAmount', e.target.getAttribute('amount'));
            //e.dataTransfer.setData('productAmount', e.target.querySelector('.amount').textContent.trim());
        }
    });
} */
function updateNewListProductList(list) {
    const prodName = list.parentElement.querySelector('.search').value;
    clearList(list);

    const listName = list.id.split('_')[1];
    const data = getProductListByName(prodName);

    if (data == false) {
        return;
    }
    data.forEach(product => {
        if (! selectedProducts.includes(product['productID'])) {
            addProductCard(product, list, true);
        }
    });
    list.querySelectorAll('.card.product').forEach(product => {
        product.setAttribute('draggable', 'true');
        product.title = '';
        const detailsProductList = document.querySelector('.popup.on > .details > .product_list');
        product.ondragstart = (e) => {
            setElementOn(detailsProductList, true);
            e.dataTransfer.setData('object', 'New product card');
            e.dataTransfer.setData('productID', e.target.getAttribute('productID'));
            e.dataTransfer.setData('productName', e.target.querySelector('.name').textContent);
            e.dataTransfer.setData('productAmount', e.target.getAttribute('amount'));
        }
        product.onclick = (e) => {
            const data = {
                'productID': product.getAttribute('productID'),
                'name': product.querySelector('.name').textContent,
                'amount': product.getAttribute('amount')
            }
            addNewListDetailsProductCard(data, listName);
        }
        product.ondragend = () => {
            setElementOn(detailsProductList, false);
        }
    });
}


function updateOrderList() {
    
}



function getProductListByName(name) {
    name = name.toLowerCase();

    let newList = [];
    productList.forEach(product => {
        if (product['name'].toLowerCase().includes(name)) {
            newList.push(product);
        }
    });

    if (newList.length == 0) {
        return false;
    }
    return newList;
}

function createFromNewList(listName, additionalParams) {
    console.log(additionalParams);
    const list = document.querySelector(`#new_${listName}_details_product_list`);

    if (! verifyNewList(listName)) {
        return;
    }
    const verifyFunctionName = `verifyNew${capitalize(listName)}`;
    if (typeof window[verifyFunctionName] === 'function') {
        if (! window[verifyFunctionName]()) {
            return;
        }
    }

    let productList = [];

    list.querySelectorAll('.card.new_list__product').forEach(product => {
        let productObject = {};
        productObject['productID'] = product.getAttribute('productID');
        productObject['amount'] = product.querySelector('input[type="number"]').value;

        productList.push(productObject);
    });

    const strProductList = JSON.stringify(productList);

    let params = {
		'action': `create${capitalize(listName)}`,
        'token': localStorage.getItem('token'),
        'productList': strProductList,
	};
    params = {...params, ...additionalParams};

    showSuperPopup('Confirmar la operación', 'Está por generar un registro nuevo. ¿Continuar?', [], 'Continuar', 'Cancelar').then(response => {
        if (response == 1) {
            post(params).then(data => {
                updateProductList();
                if (data.errno == 200) {
                    updateGenericList(listName);
                    clearList(list);
                    
                    selectedProducts = [];
                    showSuperPopup('¡Listo!', data.error, ['success']);
                }
                else {
                    showSuperPopup('¡Error!', data.error, ['error']);
                }
            });
        }
    });
    
}


function verifyNewList(listName) {
    const verifiedList = document.querySelector(`#new_${listName}_details_product_list`);
    const list = verifiedList.querySelectorAll('.card');
    
    if (list.length == 0) {
        const msg = 'Seleccione al menos un producto.';
        showSuperPopup('¡Error!', msg, ['error']);
        return false;
    }

    let error = false;
    list.forEach(product => {
        if (! error) {
            const inp = product.querySelector('input[type="number"]');
            if (inp.value.trim() == '') {
                const msg = `Debe ingresar una cantidad: "${product.querySelector('p').textContent}"`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }
            
            const value = parseInt(inp.value);
            if (value <= 0) {
                const msg = `No se puede transferir una cantidad inferior a 1: "${product.querySelector('p').textContent}"`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }
        }
    });
    return !error;
}

function verifyNewTransfer() {
    const list = new_transfer_details_product_list.querySelectorAll('.card');

    /* if (new_transfer_person.value.trim() == '') {
        const msg = 'Debe ingresar qué persona se llevó los productos.';
        showSuperPopup('¡Error!', msg, ['error']);
        return false;
    } */

    let error = false;
    list.forEach(product => {
        if (! error) {
            const inp = product.querySelector('input[type="number"]');
            const value = parseInt(inp.value);

            const maxValue = parseInt(product.getAttribute('amount'));
            /* let maxValue;
            if (window.innerWidth > MOBILE_WIDTH) {
                maxValue = parseInt(product.getAttribute('amount'));
            }
            else {
                maxValue = parseInt(productList.find(item => item.code == lote.querySelector('.product').value).productID);
            } */

            if (value > maxValue) {
                const msg = `No se puede transferir una cantidad superior al stock actual: "${product.querySelector('p').textContent}"`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }
        }
    });
    return !error;
}

// Orders

function createFromNewLoteList(listName, additionalParams) {
    const list = document.querySelector(`#new_${listName}_details_lote_list`);

    if (! verifyNewLoteList(listName)) {
        return;
    }
    const verifyFunctionName = `verifyNew${capitalize(listName)}`;
    if (typeof window[verifyFunctionName] === 'function') {
        if (! window[verifyFunctionName]()) {
            return;
        }
    }

    let loteList = [];

    list.querySelectorAll('.card.new_list__lote').forEach(lote => {
        if (lote.querySelector('.product').value.trim() == '') {
            return;
        }
        let loteObject = {};

        

        loteObject['productID'] = productList.find(item => item.code == lote.querySelector('.product').value).productID;
        loteObject['loteCode'] = lote.querySelector('.code').value;
        loteObject['totalAmount'] = lote.querySelector('.amount').value;
        loteObject['expirationDate'] = lote.querySelector('.expirationDate').value;

        loteList.push(loteObject);
    });

    const strloteList = JSON.stringify(loteList);

    let params = {
		'action': `create${capitalize(listName)}`,
        'token': localStorage.getItem('token'),
        'loteList': strloteList,
	};
    params = {...params, ...additionalParams};

    showSuperPopup('Confirmar la operación', 'Está por generar un registro nuevo. ¿Continuar?', [], 'Continuar', 'Cancelar').then(response => {
        if (response == 1) {
            post(params).then(data => {
                updateProductList();
                if (data.errno == 200) {
                    updateGenericList(listName);

                    clearList(list);
                    addNewLoteCard(listName);

                    showSuperPopup('¡Listo!', data.error, ['success']);
                }
                else {
                    showSuperPopup('¡Error!', data.error, ['error']);
                }
            });
        }
    });
}

function verifyNewLoteList(listName) {
    const verifiedList = document.querySelector(`#new_${listName}_details_lote_list`);
    const list = verifiedList.querySelectorAll('.card:not(:nth-child(1))');

    if (list.length == 0) {
        const msg = 'Ingrese al menos un lote.';
        showSuperPopup('¡Error!', msg, ['error']);
        return false;
    }

    let error = false;
    list.forEach(lote => {
        if (! error) {
            const inpLote = lote.querySelector('.code');
            if (inpLote.value.trim() == '') {
                const msg = `Debe ingresar un código para cada lote.`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }

            const inpProduct = lote.querySelector('.product');
            if (inpProduct.value.trim() == '') {
                const msg = `Debe ingresar un producto para el lote "${inpLote.value}".`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }

            const inpAmount = lote.querySelector('.amount');
            if (inpAmount.value.trim() == '') {
                const msg = `Debe ingresar una cantidad para el lote "${inpLote.value}".`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }
            
            const amountValue = parseInt(inpAmount.value);
            if (amountValue <= 0) {
                const msg = `No se puede ingresar una cantidad inferior a 1: "${inpLote.value}".`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }

            const inpExpirationDate = lote.querySelector('.expirationDate');
            if (inpExpirationDate.value.trim() == '') {
                const msg = `Debe ingresar una fecha de vencimiento para el lote "${inpLote.value}".`;
                showSuperPopup('¡Error!', msg, ['error']);
                error = true;
                return;
            }
        }
    });
    return !error;
}


/* Notifications ================================================================================ */

async function updateNotificationList() {

    let params = {
        action: 'getAboutToExpireProductLotes',
        token: localStorage.getItem('token')
    };
    const almostExpiredData = await get(params);
    params.action = 'getExpiredProductLotes';
    const expiredData = await get(params);


    clearList(notification_list);

    productList.forEach(prod => {
        let prodName = prod.name;
        if (prod.concentration != 'NULL' && prod.concentration != undefined) {
            prodName += ` - ${prod.concentration}`;
        }
        const amount = parseInt(prod.currentAmount);
        if (amount <= 0) {
            addNotificationCard('¡Alerta de stock!', `Ya no quedan unidades de <span>${prodName}</span>.`, 'error', true);
        }
        else {
            const warnAmount = parseInt(prod.criticalStock);
            if (amount <= warnAmount) {
                addNotificationCard('Advertencia de stock', `Quedan solo <span>${amount}</span> unidades de <span>${prodName}</span>.`, 'warning');
            }
        }
    });

    almostExpiredData.forEach(prod => {
        let prodName = prod.name;
        if (prod.concentration != 'NULL' && prod.concentration != undefined) {
            prodName += ` - ${prod.concentration}`;
        }
        const almostExpiredLotesAmount = parseInt(prod.lotesAmount);
        if (almostExpiredLotesAmount > 0) {
            addNotificationCard('Advertencia de vencimiento', `<span>${almostExpiredLotesAmount}</span> lotes de <span>${prodName}</span> vencen en menos de una semana.`, 'warning', false);
        }
    });

    expiredData.forEach(prod => {
        let prodName = prod.name;
        if (prod.concentration != 'NULL' && prod.concentration != undefined) {
            prodName += ` - ${prod.concentration}`;
        }
        const expiredLotesAmount = parseInt(prod.lotesAmount);
        if (expiredLotesAmount > 0) {
            addNotificationCard('¡Alerta de vencimiento!', `Hay <span>${expiredLotesAmount}</span> lotes de <span>${prodName}</span> vencidos. Revise la sección <span class="text-button underline_thin" onclick="selectWorkZoneList(expiredLote_list)">Lotes vencidos</span>.`, 'error', true);
        }
    });

    
    while (userData == null) {}

    let dataIncomplete = false;
    for (const field in userData) {
        if (field != 'photo') {
            if (! userData[field]) {
                dataIncomplete = true;
                break;
            }
        }
    }

    if (dataIncomplete) {
        const title = 'Bienvenido a Eira';
        const content = 'Para finalizar la configuración de su cuenta, complete sus datos <span class="text-button underline_thin" onclick="setElementOn(profile);checkPopups()">aquí</span>.';
        const type = 'success';

        const tpl = tpl__notification.content;
        const clone = tpl.cloneNode(true);
        addNotificationCard(title, content, type, true);
    }

    



}



function addNotificationCard(title, content, type = 'warning', atTop = false) {
    const tpl = tpl__notification.content;
    const clone = tpl.cloneNode(true);

    clone.querySelector('h3').innerHTML = title;
    clone.querySelector('p').innerHTML = content;
    clone.querySelector('.card').classList.add(type);

    if (atTop) {
        notification_list.prepend(clone);
    }
    else {
        notification_list.appendChild(clone);
    }
    
}




// New screen (Transfers & Orders)

async function getList(listName) {
    const actionStr = `get${capitalize(listName)}s`;
    const params = {
        'action': actionStr,
        'token': localStorage.getItem('token')
    }
    return await get(params);
}


async function updateList(listName) {
    const list = document.querySelector(`#${listName}_list`);
    getList(listName).then(data => {
        if (data === undefined) {
            showSuperPopup('¡Error!', 'Ocurrió un error al cargar la lista.', ['error']);
            return;
        }
        clearList(list);

        data.forEach(card => {
            addCard(card, listName);
        });
    });
}


function addCard(cardData, listName) {
    const list = document.querySelector(`#${listName}_list`);
    switch (listName) {
        //case new_transfer_product_list:
        case 'product':
            addProductCard(cardData, list);            
            break;
        case 'transfer':
        case 'order':
            addTransferCard(cardData, list);
            break;
        case 'new_transfer_details_product':
            //addNewListDetailsProductCard(cardData, list.id.split)
            addNewTransferProductCard(cardData, list);
            break;
        case 'unverifiedUser':
            addunverifiedUserCard(cardData);
            break;
    }
}


function cleanNewScreen(screenName) {
    const detailsList = document.querySelector(`#new_${screenName}_details_product_list`);
    const prodList = document.querySelector(`#new_${screenName}_product_list`);
    const search = document.querySelector(`#new_${screenName}_search_products`);

    clearList(detailsList);
    search.value = '';
    selectedProducts = [];
    updateNewListProductList(prodList);
}

/* Side Panel Options =========================================================================== */

function addSidePanelOption(listName, text, icon) {
    const tpl = tpl__side_panel_option.content;
    const clone = tpl.cloneNode(true);

    clone.querySelector('.option').id = `btn_${listName}s`;
    clone.querySelector('.option').title = text;
    clone.querySelector('p').textContent = text;
    clone.querySelector('i').classList.add(`fa-${icon}`);
    clone.querySelector('.option').onclick = () => {
        selectWorkZoneList(document.querySelector(`#${listName}_list`));
    }
    const hr = document.createElement('hr');

    side_panel.appendChild(clone);
    side_panel.appendChild(hr);
}



/* User Verify ================================================================================== */


function addunverifiedUserCard(data) {
    const tpl = tpl__unverifiedUser_card.content;
    const clone = tpl.cloneNode(true);

    const card = clone.querySelector('.card');
    clone.querySelector('.username').textContent = `${data.firstName} ${data.lastName} · ${data.permissionName}`;
    clone.querySelector('.email').textContent = data.email;
    clone.querySelector('.date').textContent = formatDate(data.creationDate);
    clone.querySelector('.btn_yes').onclick = () => {
        showSuperPopup('Aceptar usuario', `¿Está seguro de que quiere aceptar a ${data.firstName} ${data.lastName}?<br>Este usuario podrá ver la información sobre los productos, así como crear registros de ingresos y egresos.`, [], 'Continuar', 'Cancelar').then(response => {
            if (response != 1) {
                return;
            }
            
            const params = {
                'action': 'verifyUser',
                'token': localStorage.getItem('token'),
                'userID': data.userID
            }
            post(params).then(data => {
                if ('errno' in data && data.errno != 200) {
                    return;
                }
                showSuperPopup('Usuario aceptado', 'Este usuario ahora tiene acceso al sistema.', ['success']);
                unverifiedUser_list.removeChild(card);
                onUnverifiedUserListChanged();
            });
        });
    }
    clone.querySelector('.btn_no').onclick = () => {
        showSuperPopup('Rechazar usuario', `¿Está seguro de que quiere rechazar a ${data.firstName} ${data.lastName}?<br>La cuenta de este usuario será borrada de la base de datos de forma permanente.`, ['error'], 'Continuar', 'Cancelar').then(response => {
            if (response != 1) {
                return;
            }
            const params = {
                'action': 'deleteUserByID',
                'token': localStorage.getItem('token'),
                'userID': data.userID
            }
            del(params).then(data => {
                if ('errno' in data && data.errno != 200) {
                    return;
                }
                showSuperPopup('Usuario rechazado', 'La cuenta de este usuario ha sido eliminada del sistema.', ['success']);
                unverifiedUser_list.removeChild(card);
                onUnverifiedUserListChanged();
            });
        });
    }

    unverifiedUser_list.appendChild(clone);
    onUnverifiedUserListChanged();
}


function onUnverifiedUserListChanged() {
    const emptyList = unverifiedUser_list.querySelectorAll('.card.unverifiedUser').length == 0;
    setElementOn(unverifiedUser_list.querySelector('p'), emptyList);
}


function addRequestCard(data, list, first = false) {
    const listName = list.id.split('_')[0];
    const tpl = tpl__request_card.content;
    const clone = tpl.cloneNode(true);
    const card = clone.querySelector('.card');

    clone.querySelector('.sector').textContent = data['sector'];
    const date = formatDate(data['requestDate']);
    clone.querySelector('.date').textContent = date;
    card.setAttribute(`${listName}ID`, data[`${listName}ID`]);

    
    if (! userIsSectorManager()) {
        hideElement(clone.querySelector('.request_state > .set_given'));
    }
    else {
        hideElement(clone.querySelector('.request_state > .create_transfer'));
        hideElement(clone.querySelector('.btn_delete'));
        hideElement(clone.querySelector('h3.sector'));
    }

    if (data['rejected'] == 1) {
        clone.querySelector('.request_state > span > .name').textContent = 'Rechazado';
        clone.querySelector('.request_state > span > i').classList.add('fa-xmark');
        clone.querySelector('.request_state > span > i').classList.add('error');
        hideElement(clone.querySelector('.request_state > .set_given'));
        hideElement(clone.querySelector('.request_state > .create_transfer'));
    }
    else {
        if (data['given'] == 0) {
            clone.querySelector('.request_state > span > .name').textContent = 'Pendiente';
            clone.querySelector('.request_state > span > i').classList.add('fa-spinner');
            clone.querySelector('.request_state > span > i').classList.add('warning');
            if (! userIsSectorManager()) {
                clone.querySelector('.request_state > .create_transfer').onclick = () => {
                    selectWorkZoneList(transfer_list);
                    updateNewListProductList(new_transfer_product_list);
                    delay(200).then(() => {
                        setElementOn(new_transfer_container, true);
                        new_transfer_sector.value = data.sectorID;
                        data['productList'].forEach(product => {
                            const cardData = {
                                productID: product.productID,
                                name: product.name,
                                amount: productList.find(item => item.productID == product.productID).currentAmount
                            };
                            const defaultData = {
                                amount: product.amount
                            };
                            addNewListDetailsProductCard(cardData, 'transfer', defaultData);
                        });
                        checkPopups();
                    });
                }
                clone.querySelector('.btn_delete').onclick = () => {
                    const msg = '¿Está seguro de que quiere rechazar esta petición? No se mostrará más en la lista.';
                    showSuperPopup('Rechazar petición', msg, ['error'], 'Rechazar petición', 'Cancelar').then(response => {
                        if (response == 1) {
                            const params = {
                                action: 'rejectRequest',
                                token: localStorage.getItem('token'),
                                requestID: data.requestID
                            };
            
                            put(params).then(data => {
                                if (data == undefined || !'errno' in data) {
                                    showSuperPopup('Error', CONN_ERROR_MSG, ['error']);
                                    return;
                                }
            
                                if (data.errno != 200) {
                                    showSuperPopup('Error', data.error, ['error']);
                                    return;
                                }
            
                                card.remove();
                            });
                        }
                    });
                }
            }
            else {
                clone.querySelector('.request_state > .set_given').onclick = () => {
                    showSuperPopup('Marcar como completo', 'Hacer esto representa que ya se recibieron los productos solicitados en esta petición.', [], 'Continuar', 'Cancelar').then(response => {
                        if (response == 1) {
                            const params = {
                                action: 'requestGiven',
                                token: localStorage.getItem('token'),
                                requestID: card.getAttribute(`${listName}ID`)
                            }
                            put(params);
                            updateRequestCardData(card, {given: 1, rejected: 0});
                        }
                    });
                }
            }
        }
        else {
            clone.querySelector('.request_state > span > .name').textContent = 'Completo';
            clone.querySelector('.request_state > span > i').classList.add('fa-check');
            clone.querySelector('.request_state > span > i').classList.add('success');
            hideElement(clone.querySelector('.request_state > .set_given'));
            hideElement(clone.querySelector('.request_state > .create_transfer'));
        }
    }


    clone.querySelector('.username').innerHTML = `por <span class="bold">${data['userFirstName']} ${data['userLastName']}</span>`;

    let prodCount = 0;
    data['productList'].forEach( product => {
        addTransferDetail(clone, product, prodCount < 5);
        prodCount++;
    });
    if (prodCount >= 5) {
        const elem = document.createElement('div');
        elem.classList.add('transfer_detail');
        elem.classList.add('text-button');
        elem.textContent = '...';
        elem.onclick = () => {
            elem.parentElement.querySelectorAll('.transfer_detail.hidden').forEach(detail => {
                detail.classList.remove('hidden');
            });
            elem.classList.add('hidden');
        }
        clone.querySelector('.transfer_details').appendChild(elem);
    }

    if (data['requestPerson'] == null) {
        hideElement(clone.querySelector('.patient_data'));
    }
    else {
        clone.querySelector('.patient_data > .name').textContent = data['requestPerson'];
        clone.querySelector('.patient_data > .dni').textContent = data['requestPersonDNI'];
    }

    if (data['details'] == null) {
        hideElement(clone.querySelector('.details'));
    }
    else {
        clone.querySelector('.details').innerHTML = data['details'];
    }

    if (first) {
        list.prepend(clone);
    }
    else {
        list.appendChild(clone);
    }
}


async function updateGenericList(listName) {
    const listType = LIST_TYPES[listName];
    const list = document.querySelector(`#${listName}_list`);

    let checkedCards = Array.from(list.querySelectorAll(`.card.${listType}`)).map(card => {
        return card.getAttribute(`${listName}ID`);
    });

    const data = await getList(listName);
    if (listName == 'request') {
        
    console.log(data);
    }
    if (data === undefined || data.errno >= 400) {
        showSuperPopup('¡Error!', `Ocurrió un error al cargar la lista ${listName}.`, ['error']);
        return;
    }
    data.forEach(cardData => {
        const card = list.querySelector(`.card.${listType}[${listName}ID="${cardData[`${listName}ID`]}"]`);
        if (card == null) {
            window[`add${capitalize(listType)}Card`](cardData, list, true);
            return;
        }
        if (typeof window[`compare${capitalize(listType)}CardData`] === 'function') {
            window[`update${capitalize(listType)}CardData`](card, cardData);
        }
        checkedCards = removeItem(checkedCards, cardData[`${listName}ID`]);
    });
    checkedCards.forEach(cardID => {
        const card = list.querySelector(`.card.${listType}[${listName}ID="${cardID}"]`);
        card.remove();
    });
}

function updateRequestCardData(card, cardData) {
    if (cardData['rejected'] == 1) {
        card.querySelector('.request_state > span > .name').textContent = 'Rechazado';
        card.querySelector('.request_state > span > i').classList.add('fa-xmark');
        card.querySelector('.request_state > span > i').classList.add('error');
        hideElement(card.querySelector('.request_state > .set_given'));
        return;
    }
    if (cardData['given'] == 0) {
        card.querySelector('.request_state > span > .name').textContent = 'Pendiente';
        card.querySelector('.request_state > span > i').classList.add('fa-spinner');
        card.querySelector('.request_state > span > i').classList.add('warning');

        card.querySelector('.request_state > .set_given').onclick = () => {
            showSuperPopup('Marcar como completo', 'Hacer esto representa que ya se recibieron los productos solicitados en esta petición.', [], 'Continuar', 'Cancelar').then(response => {

            });
        }
    }
    else {
        card.querySelector('.request_state > span > .name').textContent = 'Completo';
        card.querySelector('.request_state > span > i').classList.add('fa-check');
        card.querySelector('.request_state > span > i').classList.add('success');
        hideElement(card.querySelector('.request_state > .set_given'));
        hideElement(card.querySelector('.btn_delete'));
    }
}


function updateProductDetailsLoteList() {
    product_details__lotes_container.querySelectorAll('.lote_info').forEach(p => {
        p.remove();
    });
    const loteColumns = product_details__lotes_container.querySelectorAll('.cell:not(.title)');
    loteColumns.forEach(col => {
        hideElement(col);
    });

    const productID = product_details.getAttribute('productID');
    const params = {
        action: 'getNonEmptyLotesByProductID',
        productID: productID,
        token: localStorage.getItem('token')
    }
    
    get(params).then(data => {

        if (data == undefined) {
            return;
        }
        loteColumns.forEach(col => {
            showElement(col);
        });

        data.forEach(lote => {
            const loteCard = {
                loteCode: document.createElement('p'),
                realAmount: document.createElement('p'),
                expirationDate: document.createElement('p')
            }
            for (const key in loteCard) {
                loteCard[key].setAttribute('loteID', lote.loteID);
                loteCard[key].classList.add('lote_info');
                loteCard[key].textContent = lote[key];

                if (lote.expired == 1) {
                    loteCard[key].classList.add('error');
                    loteCard[key].setAttribute('title', 'Lote expirado');
                }

                product_details__lotes_container.querySelector(`.lote_${key}`).appendChild(loteCard[key]);
            }
            loteCard.realAmount.textContent += ` / ${lote.totalAmount}`;
        });

    });
}