if (localStorage.getItem('token') !== null) {
    window.location.replace('/eira/panel');
}

document.addEventListener("DOMContentLoaded", () => {

	function searchSection() {
		let pathname = window.location.pathname;
		let pathArray = pathname.split('/');
		return pathArray[2];
	}

	const section = searchSection();

	switch (section) {
		case 'login':
			hideElement(document.querySelector('#nav-link-login'));
			btn_change_pass.onclick = () => {
				hideElement(login_msg_error);
				hideElement(login_msg_success);

				if (! verifyEmail()) {
					return;
				}
				const params = {
					action: 'sendModificationMail',
					email: input_email.value
				}
				put(params).then(data => {
					if (data == undefined || data == null) {
						showElement(login_msg_error);
						login_msg_error.textContent = CONN_ERROR_MSG;
						return;
					}
					if (data.errno == 200) {
						showElement(login_msg_success);
						login_msg_success.textContent = 'Se le ha enviado un e-mail para reestablecer su contraseña.';
					}
					else {
						showElement(login_msg_error);
						login_msg_error.textContent = data.error;
					}
				});
			}
			break;
		case 'register':
			hideElement(document.querySelector('#nav-link-register'));

			const params = {
				action: 'getEncargadoPermissions'
			};

			get(params).then(data => {
				const inpPermissionSectors = input_permission.querySelector('optgroup[label="Sectores del hospital"]');

				data.forEach(permission => {
					const option = document.createElement('option');
					option.value = permission.permissionID;
					option.textContent = permission.name;

					inpPermissionSectors.appendChild(option);
				});
			});
			break;
	}

	if (login_msg_success.textContent.startsWith('{{')) {
		hideElement(login_msg_success);
	}
	if (login_msg_error.textContent.startsWith('{{')) {
		hideElement(login_msg_error);
	}

	btn_login.addEventListener('click', (event) => {

		event.preventDefault();

		window.scrollTo(0, 0);
		cleanErrors();

		if (! verifyEmptyFields()) {
			return;
		}
		if (! verifyEmail()) {
			return;
		}
		if (! verifyPasswordLength()) {
			return;
		}
		if (! comparePasswords()) {
			return;
		}
		
		switch(section) {
			case 'login':
				login(input_email.value, input_pass.value);
				break;
			case 'register':
				register(input_email.value, input_pass.value, input_repass.value, input_firstName.value, input_lastName.value, input_permission.value);
				break;
		}		
	});
});



function cleanErrors() {

	if (document.getElementById('input_firstName')) {
		setElementError(input_firstName, false);

		updateElementAnimation(input_firstName);

		hideElement(empty_firstName_warning);
	}
	if (document.getElementById('input_lastName')) {
		setElementError(input_lastName, false);

		updateElementAnimation(input_lastName);

		hideElement(empty_lastName_warning);
	}
	
	setElementError(input_email, false);
	setElementError(input_pass, false);
	
	updateElementAnimation(input_email);
	updateElementAnimation(input_pass);

	hideElement(empty_email_warning);
	hideElement(empty_pass_warning);
	hideElement(valid_email_warning);
	hideElement(pass_length_warning);

	if (document.getElementById('input_repass')) {
		setElementError(input_repass, false);

		updateElementAnimation(input_repass);

		hideElement(empty_repass_warning);
		hideElement(compare_pass_warning);
	}
}



function verifyEmptyFields() {
	if (document.getElementById('input_firstName')) {
		if (input_firstName.value.trim() == "") {
			setElementError(input_firstName);
			showElement(empty_firstName_warning);
			input_firstName.focus();
			return false;
		}
	}

	if (document.getElementById('input_lastName')) {
		if (input_lastName.value.trim() == "") {
			setElementError(input_lastName);
			showElement(empty_lastName_warning);
			input_lastName.focus();
			return false;
		}
	}

	if (input_email.value.trim() == "") {
		setElementError(input_email);
		showElement(empty_email_warning);
		input_email.focus();
		return false;
	}

	if (input_pass.value.trim() == "") {
		setElementError(input_pass);
		showElement(empty_pass_warning);
		input_pass.focus();
		return false;
	}

	if (document.getElementById('input_repass')) {
		if (input_repass.value.trim() == "") {
			setElementError(input_repass);
			showElement(empty_repass_warning);
			input_repass.focus();
			return false;
		}
	}
	
	return true;
}



function verifyEmail() {
	if (! input_email.value.includes('@')/*  || ! input_email.value.length > 2 */) {
		setElementError(input_email);
		showElement(valid_email_warning);
		input_email.focus();
		return false;
	}
	return true;
}



function verifyPasswordLength() {
	if (input_pass.value.length < 8) {
		setElementError(input_pass);
		showElement(pass_length_warning);
		input_pass.focus();
		return false;
	}
	return true;
}



function comparePasswords() {
	if (document.getElementById('input_repass') == null) {
		return true;
	}

	if (input_repass.value != input_pass.value) {
		setElementError(input_repass);
		showElement(compare_pass_warning);
		input_repass.focus();
		return false;
	}
	return true;
}



function toggleViewPass(passId) {
	const pass = document.getElementById('input_' + passId);
	const btn = document.getElementById('btn-show-' + passId);
	if (pass.type == 'text') {
		pass.type = 'password';
		btn.className = btn.className.replace("fa-eye-slash", "fa-eye");
	}
	else {
		pass.type = 'text';
		btn.className = btn.className.replace("fa-eye", "fa-eye-slash");
	}
}


function login(email, pass) {
	/* loading_screen.style.opacity = 1
	loading_screen.style.pointerEvents = 'all' */
	
	hideElement(login_msg_error);
	hideElement(login_msg_success);

	sendedData = {
		'action': 'login',
		'email': email,
		'pass': pass
	};

	post(sendedData).then(data => {

		/* loading_screen.style.opacity = null
		loading_screen.style.pointerEvents = null */

		if (data === undefined) {
			showElement(login_msg_error);
			login_msg_error.textContent = CONN_ERROR_MSG;

			return;
		}

		if (data.errno == 200) {
			localStorage.setItem('token', data['token']);
			localStorage.setItem('permission', data['permission']);
			window.location.replace('/eira/panel');
		}
		else {
			showElement(login_msg_error);
			login_msg_error.textContent = data.error;
		}
	});
}



function register(email, pass, repass, firstName, lastName, permissionID) {
	//setElementOn(loading_screen, true);
	/* loading_screen.style.opacity = 1
	loading_screen.style.pointerEvents = 'all' */
	
	hideElement(login_msg_error);
	hideElement(login_msg_success);

	sendedData = {
		'action': 'register',
		'firstName': firstName,
		'lastName': lastName,
		'email': email,
		'pass': pass,
		'repass': repass,
		'permissionID': permissionID
	};

	post(sendedData).then(data => {

		//setElementOn(loading_screen, false);
		/* loading_screen.style.opacity = null
		loading_screen.style.pointerEvents = null */

		if (data === undefined) {
			showElement(login_msg_error);
			login_msg_error.textContent = CONN_ERROR_MSG;

			return;
		}
	
		if (data.errno == 200) {

			showElement(login_msg_success);
			//const msg = "¡Registrado con éxito! Se le ha enviado un email para activar la cuenta.";
			const msg = "¡Registrado con éxito! Ahora espere a que un administrador verifique su cuenta.";
			login_msg_success.textContent = msg;
		}
		else {
			showElement(login_msg_error);
			login_msg_error.textContent = data.error;
		}
	});
}