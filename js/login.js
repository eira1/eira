async function loginVerify(email, pass) {
	loading_screen.style.opacity = 1
	loading_screen.style.pointerEvents = 'all'


	let frm = new FormData()
	frm.append('action', 'login')
	frm.append('email', email)
	frm.append('pass', pass)

	const response = await fetch('https://escuelarobertoarlt.com.ar/eira/api/index.php', {
		method: "POST",
		body: frm
	})
	const data = await response.json()

	return data
}

function login(email, pass) {
	loginVerify(email, pass).then(data => {

		loading_screen.style.opacity = null
		loading_screen.style.pointerEvents = null
	
		if (data.errno == 200) {
			document.querySelector('#login-form').submit()
		}
		else {
			document.querySelector('#login-msg-error').classList.remove('hidden')
			document.querySelector('#login-msg-error').textContent = data.error
		}
	})
}