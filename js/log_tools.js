function toggleViewPass(passId) {
	var pass = document.getElementById('input-' + passId);
	var btn = document.getElementById('btn-show-' + passId);
	if (pass.type == 'text') {
		pass.type = 'password';
		btn.className = btn.className.replace("fa-eye-slash", "fa-eye");
	}
	else {
		pass.type = 'text';
		btn.className = btn.className.replace("fa-eye", "fa-eye-slash");
	}
}