var email;
var code;

document.addEventListener('DOMContentLoaded', () => {
    loadSuperPopup();

    const pathname = window.location.pathname;
    const pathArray = pathname.split('/');
    email = pathArray[3];
    code = pathArray[4];
    /* window.onmousedown = (e) => {
        defaultClickEvent(e);
        checkPopups();
    } */

    btn_change_pass.onclick = (e) => {
        e.preventDefault();
        cleanErrors();

        if (! verifyEmptyFields()) {
			return;
		}
		if (! verifyPasswordLength()) {
			return;
		}
		if (! comparePasswords()) {
			return;
		}

        changePass();
    }
});


function toggleViewPass(passId) {
    const pass = document.getElementById('input_' + passId);
    const btn = document.getElementById('btn-show-' + passId);
    if (pass.type == 'text') {
        pass.type = 'password';
        btn.className = btn.className.replace("fa-eye-slash", "fa-eye");
    }
    else {
        pass.type = 'text';
        btn.className = btn.className.replace("fa-eye", "fa-eye-slash");
    }
}

function cleanErrors() {
    setElementError(input_pass, false);
    setElementError(input_repass, false);
    
    updateElementAnimation(input_pass);
    updateElementAnimation(input_repass);

    hideElement(empty_pass_warning);
    hideElement(empty_repass_warning);
    hideElement(pass_length_warning);
    hideElement(compare_pass_warning);
}

function verifyEmptyFields() {
    if (input_pass.value.trim() == "") {
        setElementError(input_pass);
        showElement(empty_pass_warning);
        input_pass.focus();
        return false;
    }

    if (input_repass.value.trim() == "") {
        setElementError(input_repass);
        showElement(empty_repass_warning);
        input_repass.focus();
        return false;
    }

    return true;
}

function verifyPasswordLength() {
    if (input_pass.value.length < 8) {
        setElementError(input_pass);
        showElement(pass_length_warning);
        input_pass.focus();
        return false;
    }
    return true;
}

function comparePasswords() {
    if (document.getElementById('input_repass') == null) {
        return true;
    }

    if (input_repass.value != input_pass.value) {
        setElementError(input_repass);
        showElement(compare_pass_warning);
        input_repass.focus();
        return false;
    }
    return true;
}


function changePass() {
    const params = {
        action: 'modifyUserPassword',
        email: email,
        modificationCode: code,
        password: input_pass.value,
        repassword: input_repass.value
    }
    put(params).then(data => {
        if (data == undefined || data == null) {
            showSuperPopup('Error', CONN_ERROR_MSG, ['error']);
            return;
        }
        if (data.errno == 200) {
            showSuperPopup('¡Listo!', 'Contraseña modificada con éxito.', ['success']);
        }
        else {
            showSuperPopup('Error', data.error, ['error']);
        }
    });
}