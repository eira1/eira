const CONN_ERROR_MSG = 'Ocurrió un error al conectarse al servidor. Intente nuevamente en unos minutos.';
const USER_PHOTO_PATH = 'img/userphotos/'
const PRODUCT_PHOTO_PATH = 'img/productphotos/'

var superPopupActive = false;
var userData;


updateDarkMode();
document.addEventListener('DOMContentLoaded', () => {

    if (localStorage.getItem('token') != undefined) {

		getUserData().then(data => {
			userData = data;
			nav_username.textContent = `${userData.firstName} ${userData.lastName}`;
			updateUserPhotoDisplay();

			loadProfile().then(() => {
				const inputList = profile.querySelectorAll('.input-info > input');
				inputList.forEach(inp => {
					let inputName = getInputName(inp);
					inp.value = userData[inputName];
				});
				if (input_dni.value == 0) {
					input_dni.value = "";
				}
				btn_update_profile.addEventListener('click', updateUserData);

				btn_dark_mode.checked = localStorage.getItem('darkMode') == 'true';
				btn_dark_mode.onclick = () => {
					localStorage.setItem('darkMode', btn_dark_mode.checked);
					updateDarkMode();
				}
			});
		});

		window.addEventListener('mouseup', (e) => {
			if (defaultClickEvent(e)) {
				document.querySelectorAll('.popup').forEach(popup => {
					if (! popup.contains(e.target)) {
						setElementOn(popup, false);
					}
				});
				checkPopups();
			}
		});

		
        /* window.onmouseup = (e) => {
			defaultClickEvent(e);
		} */
		window.addEventListener('keydown', (e) => {
			defaultKeyEvent(e);
		});

		menu_img.onclick = () => {
			setElementOn(photo_drop_container, true);
		}

		input_photo.onchange = () => {
			hidePhotoDropContainer();
			updateUserPhoto();
		}
		photo_drop.ondragover = (e) => {
			e.preventDefault();
			photo_drop.classList.add('on');
		}
		photo_drop.ondragleave = (e) => {
			e.preventDefault();
			photo_drop.classList.remove('on');
		}
		photo_drop.ondrop = (e) => {
			e.preventDefault();
			input_photo.files = e.dataTransfer.files;
			hidePhotoDropContainer();
			updateUserPhoto();
		}


		btn_show_profile.onclick = () => {
			setElementOn(profile, true);
			btn_profile_menu.checked = false;
			checkPopups();
		}
    }
});


function hideElement(element) {
	element.classList.add('hidden');
}
function showElement(element) {
	element.classList.remove('hidden');
}
function setElementError(element, error = true) {
	if (error) {
		element.classList.add('error');
	}
	else {
		element.classList.remove('error');
	}
}
function setElementOn(element, on = true) {
	if (on) {
		element.classList.add('on');
	}
	else {
		if (element.classList.contains('on')) {
			element.classList.remove('on');
			element.dispatchEvent(new Event('popup_closed'));
		}
	}
}
function disableElement(element, disabled) {
	if (disabled) {
		element.classList.add('disabled');
	}
	else {
		element.classList.remove('disabled');
	}
}

function getElementWindowPosition(element) {
	const topPos = element.getBoundingClientRect().top - window.pageYOffset;
	const leftPos = element.getBoundingClientRect().left - window.pageXOffset;

	return {
		x: leftPos,
		y: topPos
	}
}

/**
 * Muestra el Súper Popup con el título y mensaje proporcionados.
 * Opcionalmente puede recibir el texto de 2 botones para mostrarlos en la parte inferior.
 * @param {String} title Título del popup.
 * @param {String} message Contenido del popup.
 * @param {array} flags Un array con información extra, como agregar la clase error.
 * @param {String} btn1 Texto que irá en el primer botón del popup.
 * @param {String} btn2 Texto que irá en el segundo botón del popup.
 * 
 * @return 1 si pulsó el primer botón, 2 si pulsó el segundo y 0 si cerró el popup.
 */
async function showSuperPopup(title, message, flags = [], btn1 = null, btn2 = null) {
	super_popup.classList.add('on');
	/* console.log(super_popup_dark_background); */
	super_popup_dark_background.classList.add('on');
	super_popup.querySelector('h3').textContent = title;
	super_popup.querySelector('p').innerHTML = message;

	super_popup.querySelector('h3').classList.remove('error');
	super_popup.querySelector('h3').classList.remove('success');
	if (flags.includes('error')) {
		super_popup.querySelector('h3').classList.add('error');
	}
	if (flags.includes('success')) {
		super_popup.querySelector('h3').classList.add('success');
	}

	superPopupActive = true;

	var response = 0;
	if (btn1 != null) {
		super_popup.classList.add('has_options');
		showElement(super_popup.querySelector('.options'));
		super_popup.querySelector('.btn_yes').textContent = btn1;
		super_popup.querySelector('.btn_no').textContent = btn2;
		super_popup.querySelector('.btn_yes').onclick = () => {
			response = 1;
		}
		super_popup.querySelector('.btn_no').onclick = () => {
			response = 2;
		}

	}
	else {
		super_popup.classList.remove('has_options');
		hideElement(super_popup.querySelector('.options'));
		return;
	}

	while (response == 0 && superPopupActive) {
		await new Promise(resolve => setTimeout(resolve, 100));
	}
	hideSuperPopup();
	return response;
}
function hideSuperPopup() {
	super_popup.classList.remove('on');
	super_popup_dark_background.classList.remove('on');
	superPopupActive = false;
}
function updateElementAnimation(element) {
	element.style.animation = 'none';
	element.offsetHeight;
	element.style.animation = null;
}


function updateDarkMode() {
	if(localStorage.getItem('darkMode') == 'true') {
		document.querySelector('html').classList.add('dark');
	}
	else {
		document.querySelector('html').classList.remove('dark');
	}
}
function userIsSectorManager() {
	return parseInt(localStorage.getItem('permission')) > USER_PERMISSION_SECTOR_MANAGER;
}
function updateUserPhoto() {
	let params = {
		'action': 'modifyUserPhoto',
		'token': localStorage.getItem('token'),
		'photo': input_photo.files[0]
	};
	
	post(params).then(data => {
		updateUserPhotoDisplay(true);
	});
}


function updateUserData() {
    const inputList = profile.querySelectorAll('.input-info > input');
    let sendedData = {
        "action": 'modifyUser',
		"token": localStorage.getItem('token')
    };

    inputList.forEach(inp => {
        const inputName = getInputName(inp);
		sendedData[inputName] = inp.value;
    });

	/* console.log(sendedData); */
    put(sendedData).then(data => {

		if (data === undefined) {
			showSuperPopup('Error', CONN_ERROR_MSG, ['error']);
			return;
		}
		getUserData().then(data => {
			userData = data;
			nav_username.textContent = `${userData.firstName} ${userData.lastName}`;
		});
	
		if (data.errno == 200) {
			showSuperPopup('¡Listo!', 'Datos modificados con éxito.', ['success']);
		}
		else {
			showSuperPopup('Error', data.error, ['error']);
		}
	});
}
function getInputName(inp) {
	return inp.id.replace('input_', '');
}


function hidePhotoDropContainer() {
	delay(1).then(() => {
		photo_drop_container.classList.remove('on');
		photo_drop.classList.remove('on');
	});
}

async function loadSuperPopup() {
	await loadHTML('/eira/views/tpl__super_popup.php', 'body');
}
async function loadProfile() {
	await loadHTML('/eira/views/tpl__profile.php', 'body');
}
function enableBtnQuit() {
	document.querySelectorAll('.btn_quit').forEach(btn => {
		if (btn.parentElement.id == 'super_popup') {
			return;
		}
		btn.onclick = () => {
			setElementOn(btn.parentElement, false);
			checkPopups();
		}
	});
}


/**
 * Recarga la foto de perfil en la nav bar.
 * @param {bool} refreshCache Define si debe agregar un número de versión para que la imagen no quede cacheada.
 */
function updateUserPhotoDisplay(refreshCache = false) {
	const profImg = document.querySelector('#label-profile').querySelector('.img');
	const menuImg = document.querySelector('#menu_img').querySelector('img');
	if (refreshCache) {
		localStorage.setItem('userPhotoVersion', Date.now());
	}
	if (userData['photo'] != null) {
		menuImg.src = `${USER_PHOTO_PATH}${userData['photo']}?v${localStorage.getItem('userPhotoVersion')}`;
	}
	else {
		menuImg.src = `${USER_PHOTO_PATH}default.png`;
	}
	profImg.style.backgroundImage = `url(${USER_PHOTO_PATH}${userData['photo']}?v${localStorage.getItem('userPhotoVersion')}), url(${USER_PHOTO_PATH}default.png)`;
	
	/* fileExists(USER_PHOTO_PATH + userData['photo']).then(exists => {
		const profImg = document.querySelector('#label-profile').querySelector('.img');
		const menuImg = document.querySelector('#menu_img').querySelector('img');
		if (! exists) {
			profImg.style.backgroundImage = `url(${USER_PHOTO_PATH}default.png)`;
			menuImg.src = `${USER_PHOTO_PATH}default.png`;
			return;
		}
		if (refreshCache) {
			localStorage.setItem('userPhotoVersion', Date.now());
		}
		profImg.style.backgroundImage = `url(${USER_PHOTO_PATH}${userData['photo']}?v${localStorage.getItem('userPhotoVersion')})`;
		menuImg.src = `${USER_PHOTO_PATH}${userData['photo']}?v${localStorage.getItem('userPhotoVersion')}`;
	}); */
}

async function getUserData() {
	const params = {
		'action': 'getUserModifiableDataByToken',
		'token': localStorage.getItem('token')
	};
	return await get(params);
}

async function fileExists(url) {
	const params = {
		'action': 'fileExists',
		'url': url
	}
	const data = await get(params);
	return data.errno == 200;
}


function defaultClickEvent(e) {
	if (! superPopupActive) {
		if (! menu.contains(e.target)) {
			btn_profile_menu.checked = false;
		}
		if (! photo_drop_container.contains(e.target)) {
			setElementOn(photo_drop_container, false);
		}
		return true;
	}
	if (! super_popup.contains(e.target)) {
		hideSuperPopup();
	}
	return false;
}
function defaultKeyEvent(evt) {
	evt = evt || window.event;
	let isEscape = false;
	if ("key" in evt) {
		isEscape = (evt.key === "Escape" || evt.key === "Esc");
	} else {
		isEscape = (evt.keyCode === 27);
	}
	if (isEscape) {
		if (!superPopupActive) {
			document.querySelectorAll('.popup').forEach(popup => {
				setElementOn(popup, false);
			});
			checkPopups();
			return true;
		}
		hideSuperPopup();
		return false;
	}
}


function checkPopups() {
    let active = false;
    document.querySelectorAll('.popup').forEach(popup => {
        if (popup == photo_drop_container) {
            return;
        }
        if (popup.classList.contains('on')) {
            active = true;
        }
    });
    setElementOn(dark_background, active);
}
/**
 * Convierte la primera letra de cada palabra de un string en mayúscula.
 * @param {string} str El string a modificar
 * @returns {string} El string formateado.
 */
function capitalizeWords(str) {
	const arr = str.split(' ');
	for (let i = 0; i < arr.length; i++) {
		arr[i] = capitalize(arr[i]);
	}
	return arr.join(' ');
}
/**
 * Convierte la primera letra de un string en mayúscula.
 * @param {string} str El string a modificar.
 * @returns {string} El string formateado.
 */
function capitalize(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * Convierte una fecha como viene de la DB a formato "D/M/A H:M hs".
 * @param {String} date La fecha en forma de string.
 * @returns La fecha formateada.
 */
function formatDate(date, includeTime = true) {
	if (includeTime) {
		return new Date(date).toLocaleDateString('es-ar', {year:'numeric', month:'numeric', day:'numeric', hour: '2-digit', minute: '2-digit'}) + ' hs.';
	}
    return new Date(date).toLocaleDateString('es-ar', {year:'numeric', month:'numeric', day:'numeric'});
}

/**
 * Elimina las entradas de un array que coincidan con el valor dado.
 * @param {Array} myArray El array del que se extraen los valores.
 * @param {*} value El valor a buscar para eliminar.
 * @param {boolean} strict Si es true, realiza una comparación estricta (===).
 * @returns El array modificado.
 */
function removeItem(myArray, value, strict = false) {
	if (! strict) {
		return myArray.filter(e => e != value);
	}
	return myArray.filter(e => e !== value);
}



/**
 * Espera determinada cantidad de milisegundos.
 * @param {int} ms Milisegundos
 */
const delay = ms => new Promise(res => setTimeout(res, ms));

/**
 * Carga un archivo y lo agrega al final del elemento que se indique.
 * @param {*} url Url del archivo para cargar.
 * @param {*} containerSelector Selector (como 'body', '#id_del_elemento', etc.) del contenedor en el que se va a insertar el código cargado.
 */
async function loadHTML(url, containerSelector) {
	const response = await fetch(url);
	const html = await response.text();
	document.querySelector(containerSelector).insertAdjacentHTML('beforeend', html);
}
/**
 * Carga un script y lo agrega al final del head.
 * @param {*} url Url del script.
 */
async function loadScript(url) {
	const response = await fetch(url);
	const script = await response.text();
	document.querySelector('head').insertAdjacentHTML('beforeend', script);
}


/**
 * Función genérica para peticiones GET.
 * @param {Array} params Parámetros para la petición.
 */
 async function get(params) {

	let getParams = '?'
	for (const key in params) {
		getParams += `${key}=${params[key]}&`;
	}
	getParams = getParams.slice(0, -1);

	try {
		const response = await fetch(`https://escuelarobertoarlt.com.ar/eira/api/${getParams}`);
		const data = await response.json();

		return data;
	} catch (error) {
		console.error(error);
	}
}
/**
 * Función genérica para peticiones POST.
 * @param {Array} params Parámetros para la petición.
 */
 async function post(params) {
	setElementOn(loading_screen, true);
    let frm = new FormData();

    for (key in params) {
		frm.append(key, params[key]);
	}

    try {
		const response = await fetch(
		`https://escuelarobertoarlt.com.ar/eira/api/`,
            {
                method: 'POST',
                body: frm
            }
        );
		const data = await response.json();
		setElementOn(loading_screen, false);
		return data;
	} catch (error) {
		setElementOn(loading_screen, false);
		console.error(error);
	}
}
/**
 * Función genérica para peticiones DELETE.
 * @param {Array} params Parámetros para la petición.
 */
async function put(params) {
	setElementOn(loading_screen, true);

    try {
		const response = await fetch(
		`https://escuelarobertoarlt.com.ar/eira/api/`,
            {
                method: 'PUT',
                body: JSON.stringify(params)
            }
        );
		const data = await response.json();

		setElementOn(loading_screen, false);
		return data;
	} catch (error) {
		setElementOn(loading_screen, false);
		console.error(error);
	}
}
/**
 * Función genérica para peticiones DELETE.
 * @param {Array} params Parámetros para la petición.
 */
 async function del(params) {
	setElementOn(loading_screen, true);

    try {
		const response = await fetch(
		`https://escuelarobertoarlt.com.ar/eira/api/`,
            {
                method: 'DELETE',
                body: JSON.stringify(params)
            }
        );
		const data = await response.json();

		setElementOn(loading_screen, false);
		return data;
	} catch (error) {
		setElementOn(loading_screen, false);
		console.error(error);
	}
}