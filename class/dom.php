<?php 

	//$html = ob_get_clean(); 


	function createTransferDOMPDF($data_transfer,$data_transferdetails,$data_transferlote,$data_transferperson,$data_transfersector){
		require_once('class/pdf.php'); 

		$pdf = new Pdf();

		$date = explode(" ",$data_transfer["transferDate"])[0];
		//$date = substr($data_transfer["transferDate"],0,10);
		$file_name = 'Egreso-N'.$data_transfersector[0]["transferID"].'-'.$date.'.pdf';

			$output = '
			<table width="100%" border="0" cellpadding="5" cellspacing="5" style="font-family:Arial, san-sarif">';

			//carga cabeceras del hospital

			$output .= '
			<tr>
				<td align="center">
					<b style="font-size:32px">Hospital Gobernador Domingo Mercante</b>
					<br />
					<br />
					<span style="font-size:16px;">4750, Dr. René Favaloro, José C. Paz, Provincia de Buenos Aires</span>
					<br />
					<span style="font-size:16px;">02320 44-0030</span>
					<br /><br />
				</td>
			</tr>
			';

			//carga sector, personal implicado
				$output .= '
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="5" cellspacing="5">
							<tr>
								<td width="20%"><b>Egreso N°: </b>'.$data_transfersector[0]["transferID"].'</td>
								<td width="20%"><b>Sector: </b>'.$data_transfersector[0]["name"].'</td>
								<td width="35%" align="right"><b>Farmacéutico: </b>'.$data_transferperson["firstName"].' '.$data_transferperson["lastName"].'</td>
								<td width="25%"><b>Fecha: </b>'.$date.'</td>
							</tr>
						</table>
					</td>
				</tr>
				';

			//carga de tabla de transferencia

				$output .= '
					<tr>
						<td>
							<table width="100%" border="1" cellpadding="10" cellspacing="0">
								<tr>
									<th width="10%"><center>Código</center></th>
									<th width="50%">Descripción</th>
									<th width="10%"><center>Lote</center></th>
									<th width="10%"><center>Cantidad</center></th>
									<th width="20%"><center>Fecha de caducidad</center></th>
								</tr>';
				foreach($data_transferlote as $key => $value)
				{
					if($data_transferlote[$key]["concentration"] == "NULL"){
						$data_transferlote[$key]["concentration"] = "";
					}
					if($data_transferlote[$key]["amount"]!=0){
						$output .= '
								<tr>
									<td>'.$data_transferlote[$key]["code"].'</td>
									<td>'.$data_transferlote[$key]["name"].'-'.$data_transferlote[$key]["form"].'-'.$data_transferlote[$key]["concentration"].'</td>
									<td>'.$data_transferlote[$key]["loteCode"].'</td>
									<td>'.'<center>'.$data_transferlote[$key]["amount"].'</center>'.'</td>
									<td>'.'<center>'.$data_transferlote[$key]["expirationDate"].'</center>'.'</td>
								</tr>
					';
					}
				} 


				$output .= '
							</table>
						</td>
					</tr>
					';
			



			$output .= '</table>';

			$pdf = new Pdf();

			$pdf->loadHtml($output, 'UTF-8');
			$pdf->render();
			$pdf->stream($file_name, array( 'Attachment'=>0 ));
			exit(0);
		}

		function createOrderDOMPDF($data_order,$data_orderdetails,$data_orderlote,$data_orderperson,$data_ordersupplier){
			require_once('class/pdf.php'); 

			$pdf = new Pdf();

			$date = explode(" ",$data_order["orderDate"])[0];
			$file_name = 'Ingreso-N'.$data_order["orderID"].'-'.$date.'.pdf';

				$output = '
				<table width="100%" border="0" cellpadding="5" cellspacing="5" style="font-family:Arial, san-sarif">';

				//carga cabeceras del hospital

				$output .= '
				<tr>
					<td align="center">
						<b style="font-size:32px">Hospital Gobernador Domingo Mercante</b>
						<br />
						<br />
						<span style="font-size:16px;">4750, Dr. René Favaloro, José C. Paz, Provincia de Buenos Aires</span>
						<br />
						<span style="font-size:16px;">02320 44-0030</span>
						<br /><br />
					</td>
				</tr>
				';

				//carga sector, personal implicado
					$output .= '
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="5" cellspacing="5">
								<tr>
									<td width="17%"><b>Ingreso N°: </b>'.$data_order["orderID"].'</td>
									<td width="25%" align="right"><b>Farmacéutico: </b>'.$data_orderperson["firstName"].' '.$data_orderperson["lastName"].'</td>
									<td width="35%" align="right"><b>Proveedor: </b>'.$data_ordersupplier["supplier"].'</td>
									<td width="23%"><b>Fecha: </b>'.$date.'</td>
								</tr>
							</table>
						</td>
					</tr>
					';

				//carga de tabla de transferencia

					$output .= '
						<tr>
							<td>
								<table width="100%" border="1" cellpadding="10" cellspacing="0">
									<tr>
										<th width="10%"><center>Código</center></th>
										<th width="50%">Descripción</th>
										<th width="10%"><center>Lote</center></th>
										<th width="10%"><center>Cantidad</center></th>
										<th width="20%"><center>Fecha de caducidad</center></th>
									</tr>';
					foreach($data_orderlote as $key => $value)
					{
						if($data_orderlote[$key]["concentration"] == "NULL"){
							$data_orderlote[$key]["concentration"] = "";
						}
						$output .= '
									<tr>
										<td>'.$data_orderlote[$key]["code"].'</td>
										<td>'.$data_orderlote[$key]["name"].'-'.$data_orderlote[$key]["form"].'-'.$data_orderlote[$key]["concentration"].'</td>
										<td>'.$data_orderlote[$key]["loteCode"].'</td>
										<td>'.'<center>'.$data_orderdetails[$key]["amount"].'</center>'.'</td>
										<td>'.'<center>'.$data_orderlote[$key]["expirationDate"].'</center>'.'</td>
									</tr>
						';
					} 


					$output .= '
								</table>
							</td>
						</tr>
						';
				



				$output .= '</table>';

				$pdf = new Pdf();

				$pdf->loadHtml($output, 'UTF-8');
				$pdf->render();
				$pdf->stream($file_name, array( 'Attachment'=>0 ));
				exit(0);
			}

 ?>