Language: Spanish

Eira
Sistema de gestión de stock para el hospital de agudos Eva Perón

¿Quiénes son los desarrolladores del proyecto?
- Carbón, Mauricio Ezequiel
- Choque Jarpa, Gabriel Hernán
- Martínez, Fausto Nicolás
- Rafael, Manuel

¿Qué problema resuelve?
El hospital necesita tener un seguimiento de los productos que usa cada sección

¿Qué beneficios le trae al hospital?
Tener información actualizada en tiempo real de los productos que tienen en stock, de esta manera,
tener un mejor manejo de las compras y ventas de insumos

¿Qué los motivó a hacer el proyecto?
Este proyecto es un trabajo final de la tecnicatura en informática personal y profesional de la
Escuela de Educación Secundaria Técnica Nº3 de Malvinas Argentinas

¿Cuando estará terminado?
El proyecto está actualmente en desarrollo, se estima que para el mes de noviembre esté finalizado

¿De dónde viene el nombre "Eira"?
Eira es la diosa de la sanación y la salud nórdica. 

¿Podrá ser aplicable a otros hospitales/farmacias?
Es una posibilidad que manejamos, en un futuro es una posibilidad, pero por ahora no estamos pensando en eso.


